$(document).ready(function() {		

	//Alerta bloqueante, se usa para que el usuario no pueda hacer nada mientras se carga la interfaz
	//Revisar en librerías internas la librería de gestionModal que usa la librería de sweetalert
	//para todas las alertas, en ella encontraran en detalle cada función, como se usa, parametros de
	//entrada y para que caso se usa
	gestionModal.alertaBloqueante(primario.aplicacion, "Procesando...");

	/*******************************************************************************/
    /****************************** ERROR INTERFAZ *********************************/
    /*******************************************************************************/
	//Función encargada de enviar un correo de error cuando falla la interfaz de usuario

	window.onerror = function(msg, url, lineNo, columnNo, error) {

		primario.qryPrms = {
			'MSG': msg,
			'URL': url,
			'LINE': lineNo,
			'COL': columnNo,
			'ERROR': window.btoa(error.stack)
		};

		html2canvas(document.querySelector("#contenedorPrincipal > div")).then(canvas => {
		   
		   	var pngUrl = canvas.toDataURL();

		   	primario.qryPrms.pngUrl = pngUrl;
			query.callAjax(primario.urlConexion, "errorInterfaz", primario.qryPrms, query.errorEvent);

			primario.qryPrms = null;
		});
	}
	/*******************************************************************************/
	/*******************************************************************************/

	//función para determinar el nombre del navegador y la versión de este
	function getBrowserInfo() {
	    var ua= navigator.userAgent, tem, 
	    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
	    if(/trident/i.test(M[1])){
	        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
	        return 'IE '+(tem[1] || '');
	    }
	    if(M[1]=== 'Chrome'){
	        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
	        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
	    }
	    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
	    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
	    return M.join(' ');
	};

	//Se limpia el contenedor del Login y se inicializa el login
	$("#contenedorPrincipal").empty();
	/* $("#contenedorPrincipal").load("vistas/Registro/registro.html"); */
	$("#contenedorPrincipal").load("vistas/Administrador/Administrador.html");

});