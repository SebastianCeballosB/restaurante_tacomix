var gestionRippler = new Object();

gestionRippler.rippler = function(element, color, speed) {
	$(`${element}`).yarp({
		colors: [`${color}`],
		duration: speed
	});
}