var gestionCA = new Object();

/*Se inicializa la libreria invocando el metodo encargado de traer la informacion de 
la tabla por primera vez*/
gestionCA.initCharge = function(url, nameWeb, idDiv, loginUsuario, idPerfil, callback, parametros){
  //class - barra de progreso
  barClass = "progress-bar bg-info"
  var parameters = new Object();
  parameters.url = url;
  parameters.idDiv = idDiv;
  parameters.barClass = barClass;
  parameters.callback = callback;
  parameters.parametros = parametros;
  parameters.nameWeb = nameWeb;
  parameters.loginUsuario = loginUsuario;
  parameters.firstTime = 1;//Bandera que indica que la pagina se esta cargando por primera vez
  //Se trae la informacion necesaria para dibujar la tabla por primera vez
  query.callAjax(url,
    "draw_table_load_files",
    {nombreElementoWeb:nameWeb,
      id_perfil: idPerfil}, 
    gestionCA.html,parameters);
}

//funcion encargada de generar el html de las barras y aplicar los eventos
gestionCA.html = function(resultado,parameters){
  var tooltip;//Determina si el tpooltip es top o bottom
  //se verifica cuantos elementos con echeckbox existen 0, 1 o 2
  var checkboxCount = 0;
  for(i in resultado){
    if(resultado[i].checkbox){
      checkboxCount++;
    }
    if(checkboxCount == 2){
      break;
    }
  }
  //Se guarda en parameter el div tabla y el div mensajes
  parameters.divTabla = `divTabla-${resultado[0].id}`;
  parameters.divMensajes = `divMensajes-${resultado[0].id}`;
  var txt =`
  <div id="divTabla-${resultado[0].id}" class="divTabla">
  <div>
    <table class="table table-striped table-bordered" id="tabla-${resultado[0].id}">
      <thead>
        <tr>
          <th class="text-center">Entrada</th>
          <th class="text-center mobilHide">Fecha última ejecución</th>
          <th class="text-center mobilHide">Usuario</th>
          <th class="text-center">Estado</th>
          `;
        if(checkboxCount == 2){
          txt = `${txt}
          <th class="text-center" data-toggle="tooltip" title="Sincronizar">
              <input id="switchPrincipal-${resultado[0].id}" type="checkbox" name="set-name" 
              class="switch-input" data-state=1 data-id=0>
              <label for="switchPrincipal-${resultado[0].id}" class="switch-label modalpp switch-label-principal">
                <span class="toggle--on"></span>
                <span class="toggle--off"></span>
              </label>
          </th>
          `;
        }
        else{}
        if(checkboxCount == 2){
          txt = `${txt}
          <th class="widthBarra text-center">
            <button id="btnPrincipal-${resultado[0].id}" type="button" class="btn btn-primary">Sincronizar</button>
          </th>
          `;
        }
        else{
          txt = `${txt}
          <th class="widthBarra"></th>
          `;
        }
        txt = `${txt}
        </tr>
      </thead>
      <tbody>
    `;
    //Se dibuja cada fila
    for(i in resultado){
      if(i == 0){
        tooltip = "tooltipChargeTextBottom";
      }
      else{
        tooltip = "tooltipChargeText";
      }
      //Se dibuja la fila
      txt = `${txt}
        <tr id="fila-${resultado[i].id}" data-tstate=true data-tresult=true 
          data-rstate=true data-rresult=true>
          <td data-toggle="tooltip" data-placement="right" title="${resultado[i].tooltip}"
          class="text-center">${resultado[i].nombreInterfaz}</td>
          <td class="text-center mobilHide" id="date-${resultado[i].id}">${resultado[i].fechaIniTransferencia}</td>
          <td class="text-center mobilHide" id="name-${resultado[i].id}">${resultado[i].nombreCompleto}</td>
          <td class="text-center tooltipCharge">
            <span class="${tooltip}" id="tooltipState-${resultado[i].id}"></span>
            <div id="state-${resultado[i].id}"></div>
          </td>
      `;
      //se verifica si tiene checkbox
      if(checkboxCount == 2){
        if(resultado[i].checkbox){
        txt = `${txt}
          <td>
            <input id="switch-${resultado[i].id}" type="checkbox" name="set-name" class="switch-input
            switchPrincipal-${resultado[0].id}" data-id=${resultado[i].id}>
            <label for="switch-${resultado[i].id}" class="switch-label modalpp">
              <span class="toggle--on"></span>
              <span class="toggle--off"></span>
            </label>
          </td>
        `;
        }
        else{
          txt = `${txt}
          <td></td>
          `;
        }
      }
        txt = `${txt}
          <td class="tooltipCharge">
            <span class="${tooltip}" id="tooltipBar-${resultado[i].id}"></span>
            <button id="btn-${resultado[i].id}" type="button" class="btn btn-primary">${resultado[i].nombreBoton}</button>
            <div id="bar-${resultado[i].id}" class="progress"><div class="${parameters.barClass}"></div></div>
          </th>
        `;
      if(resultado[i].idCategoria == 1){
      txt = `${txt}
        <form  enctype="multipart/form-data" id="archivoSeleccionado-${resultado[i].id}">  
          <input type="file" name ="file" id="subirArchivo-${resultado[i].id}">
        </form>`;
      }
      txt = `${txt}
        </tr>
      `;
    }
    txt = `${txt}
      </tbody>
    </table>
  </div>
  </div>
  <div id="divMensajes-${resultado[0].id}" class="divMensajes">
    <div class="mensajesHeader text-center">
      <h4><b>Titulo</b></h4>
      <button id="mensajesVolver-${resultado[0].id}" type="button" class="btn btn-danger">
        <i class="fas fa-times"></i>
      </button>
    </div>
    <div class="mensajesBody"></div>
    <div class="mensajesFooter"></div>
  </div>
  `;
  //Se dibuja la tabla
  $(`#${parameters.idDiv}`).html(txt);

  //Se activan los tooltips
  $('[data-toggle="tooltip"]').tooltip({animation: false});

  for(i in resultado){
    //Carga archivos
    if(resultado[i].idCategoria == 1){
      //Evento del boton para abrir la ventana de carga de archivos
      $(`#btn-${resultado[i].id}`).off("click").on("click",{id:resultado[i].id},
         gestionCA.seleccionArchivo);
      //se asigna el evento encargado de la carga de archivos
      $(`#subirArchivo-${resultado[i].id}`).off("change").on("change", 
        {parametros:parameters, id:resultado[i].id}, gestionCA.fileUpload);
    }
    //Base de datos privada
    else if(resultado[i].idCategoria == 2){
      //Evento del boton para abrir iniciar el proceso de sincronizacion de archivos
      $(`#btn-${resultado[i].id}`).off("click").on("click",{id:resultado[i].id, parametros:parameters},
         gestionCA.verifyFlagsBDPrivada);
    }
    else{
      //Base_Datos_Publica
      if(resultado[i].idCategoria == 3)
      {
        $(`#btn-${resultado[i].id}`).off("click").on("click",{id:resultado[i].id, parametros:parameters},
         gestionCA.verifyFlagsBDPublica);
      }

    }
  }

  //Boton de sincronizacion global
  if(checkboxCount == 2){
    //Genera el evento click en los botones habilitados que tengan ckeckbox
    $(`#btnPrincipal-${resultado[0].id}`).off("click").on("click", 
      {classCheckbox:`.switchPrincipal-${resultado[0].id}`}, gestionCA.startSyncUpAll);
  }

  //Se activa la funcion del checkbox principal
  if(checkboxCount == 2){
    $(`#switchPrincipal-${resultado[0].id}`).off("change").on("change", 
      {classCheckbox:`.switchPrincipal-${resultado[0].id}`}, gestionCA.changeCheckbox);
    //funcion encargada de ver el estado de los demas checkbox para modificar el principal
    $(`.switchPrincipal-${resultado[0].id}`).off("change").on("change", 
      {checkbox:`switchPrincipal-${resultado[0].id}`},gestionCA.someChecked);
  }
  //Se ocultan las columnas de fecha y usuario si el dispositivo en el que se abre tiene un ancho menor a 700px
  if($(window).width() < 600){
    $(".mobilHide").hide();
  }
  //Se oculta el div de mensajes
  $(`#divMensajes-${resultado[0].id}`).fadeOut(0);
  //Se genera el evento que esconde el div de mensajes y muestra el div de la tabla
  $(`#mensajesVolver-${resultado[0].id}`).off("click").on("click", {parametros:parameters},
   gestionCA.hideMesagge);

  //Se guarda el id de la tabla para parar de refrescar cuando la tabla no exista
  parameters.validate = `#tabla-${resultado[0].id}`;
  /*Se referencia la funcion de refresco a la cual siempre se le pasa parameters 
  (parameter shace las veces de puntero)*/
  parameters.callRefresh = function(){
    gestionCA.refresh(parameters);
  }
  //Se invoca la funcion por primera vez
  gestionCA.refresh(parameters);
}

//funcion encargada de checkear o des checkear todos los checkbox
gestionCA.changeCheckbox = function(event){
  var classCheckbox = event.data.classCheckbox;
  if($(this).prop('checked')){
    $(`${classCheckbox}`).each(gestionCA.checkedAll);
  }
  else{
    $(`${classCheckbox}`).each(gestionCA.uncheckedAll);
  }
}

//checkea todos los checkbox
gestionCA.checkedAll = function(){
  //Si estan habilitados
  if(!$(this).prop("disabled")){
    $(this).prop('checked',true);
  }
}

//des checkea todos los checkbox
gestionCA.uncheckedAll = function(){
  //Si estan habilitados
  if(!$(this).prop("disabled")){
    $(this).prop('checked',false);
  }
}

//funcion encargada de ver el estado de los demas checkbox para modificar el principal
gestionCA.someChecked = function(event){
  var checkbox = event.data.checkbox;
  var state = 1; //bandera del estado del checkbox principal
  //Se recorren todos los checkbox menos el principal
  $(`.${checkbox}`).each(function(){
    //funcion encargada de ver el estado de los demas checkbox habilitados para modificar el principal
    if(!$(this).prop('checked') && !$(this).prop("disabled")){
      state = 0;
    }
  });

  //Se determina en que estado debe estar el checkbox principal
  if(state == 1){
    $(`#${checkbox}`).prop('checked',true);
  }
  else{
    $(`#${checkbox}`).prop('checked',false);
  }
}

/*Funcion encargada de pedir los servicios de refresco de la tabla y detiene el refresco cuando
 la tabla no exista mas.
 Recibe parameters que hace las veces de puntero.*/
gestionCA.refresh = function(parameters){
    //Se invocan los servicios de refreso
    query.callAjax(parameters.url,
      "refresh_table",
      {nombreElementoWeb:parameters.nameWeb}, 
      gestionCA.refreshTable,parameters);
}

//Se verifican los estados y se actualiza la tabla, cada fila se actualiza segun sus estados
gestionCA.refreshTable = function(resultado,parameters){
  //Se recorren todas las vistas y se verifica cuales estados son diferentes
  for(i in resultado){
    if( resultado[i].estadoTransferencia != $(`#fila-${resultado[i].id}`).data("tstate") ||
        resultado[i].resultadoTransferencia != $(`#fila-${resultado[i].id}`).data("tresult") ||
        resultado[i].estadoProcesamiento != $(`#fila-${resultado[i].id}`).data("rstate") ||
        resultado[i].resultadoProcesamiento != $(`#fila-${resultado[i].id}`).data("rresult")
     ){
      //Se actualiza el estado anterior
      $(`#fila-${resultado[i].id}`).data("tstate", resultado[i].estadoTransferencia);
      $(`#fila-${resultado[i].id}`).data("tresult", resultado[i].resultadoTransferencia);
      $(`#fila-${resultado[i].id}`).data("rstate", resultado[i].estadoProcesamiento);
      $(`#fila-${resultado[i].id}`).data("rresult", resultado[i].resultadoProcesamiento);
      //se verifica en que estado se encuentra
      //Primero se verifica el procesamiento
      if( !resultado[i].estadoProcesamiento && !resultado[i].resultadoProcesamiento ){// 0 0
        //se hace el cambio solo si la barra esta en 100%
        if($(`#bar-${resultado[i].id} > div`).html() == "100%" ||
            parameters.firstTime == 1){//condicion para que entre la primera vez
          //Se activa el boton
          $(`#btn-${resultado[i].id}`).prop("disabled",false);
          //Se activa el checkbox, si existe
          if($(`#switch-${resultado[i].id}`).length == 1){
            $(`#switch-${resultado[i].id}`).prop("disabled",false);
          }
          //se crea el boton con icono de errores
          $(`#state-${resultado[i].id}`).html(`<i id="warning-${resultado[i].id}"  
            class="fas fa-exclamation-triangle"></i>`);
          //Se genera el evento de mensajes
          $(`#warning-${resultado[i].id}`).off("click").on("click", {id:resultado[i].id,parametros:parameters},
           gestionCA.bringMesagge);
          //Se actualiza el mensaje del tooltip
          $(`#tooltipState-${resultado[i].id}`).html(`Clic para mostrar mensajes.`);
          //Se esconde la barra de progreso
          $(`#bar-${resultado[i].id}`).css("display","none");
          //Se esconde el tooltip de la barra de progreso
          $(`#tooltipBar-${resultado[i].id}`).css("display","none");
          //Se muestra el boton
          $(`#btn-${resultado[i].id}`).css("display","block");
        }
        else{
          /*Se ponene las banderas de referencia en 1 para obligar a preguntar nuevamente por el estado 
          y se pone la barra en 100%*/
          //Se pone la barra de progreso en el valor recibido para procesamiento
          $(`#bar-${resultado[i].id} > div`).css("width",`100%`);
          $(`#bar-${resultado[i].id} > div`).html(`100%`);
          $(`#fila-${resultado[i].id}`).data("tstate", 1);
          $(`#fila-${resultado[i].id}`).data("tresult", 1);
          $(`#fila-${resultado[i].id}`).data("rstate", 1);
          $(`#fila-${resultado[i].id}`).data("rresult", 1);
        }
      }
      else if( resultado[i].estadoProcesamiento && !resultado[i].resultadoProcesamiento ){ // 1 0
        //Se desactiva el boton
        $(`#btn-${resultado[i].id}`).prop("disabled",true);
        //Se desactiva el checkbox, si existe
        if($(`#switch-${resultado[i].id}`).length == 1){
          $(`#switch-${resultado[i].id}`).prop("disabled",true);
        }
        //se crea el icono de carga
        $(`#state-${resultado[i].id}`).html(`<i class="fas fa-sync-alt"></i>`);
        //Se actualiza el mensaje del tooltip
        $(`#tooltipState-${resultado[i].id}`).html(`Procesando...`);
        //Se muestra la barra de progreso
        $(`#bar-${resultado[i].id}`).css("display","block");
        //Se muestra el tooltip de la barra de progreso
        $(`#tooltipBar-${resultado[i].id}`).css("display","block");
        //Se cambia el mensaje del tooltip de la barra de progreso
        $(`#tooltipBar-${resultado[i].id}`).html(resultado[i].EtiquetaEstado);
        //Se oculta el boton
        $(`#btn-${resultado[i].id}`).css("display","none");
      }
      //Estado de procesamiento 0 1 obliga a ver los estados de transferencia
      else if( !resultado[i].estadoProcesamiento && resultado[i].resultadoProcesamiento ){ // 0 1
        //Estados de transferencia
        if( !resultado[i].estadoTransferencia && !resultado[i].resultadoTransferencia ){ // 0 0
          //se hace el cambio solo si la barra esta en 100%
          if($(`#bar-${resultado[i].id} > div`).html() == "100%" ||
            parameters.firstTime == 1){//condicion para que entre la primera vez
            //Se activa el boton
            $(`#btn-${resultado[i].id}`).prop("disabled",false);
            //Se activa el checkbox, si existe
            if($(`#switch-${resultado[i].id}`).length == 1){
              $(`#switch-${resultado[i].id}`).prop("disabled",false);
            }
            //se crea el boton con icono de errores
            $(`#state-${resultado[i].id}`).html(`<i id="warning-${resultado[i].id}"  
              class="fas fa-exclamation-triangle"></i>`);
            //Se genera el evento de mensajes
            $(`#warning-${resultado[i].id}`).off("click").on("click", {id:resultado[i].id,parametros:parameters},
             gestionCA.bringMesagge);
            //Se actualiza el mensaje del tooltip
            $(`#tooltipState-${resultado[i].id}`).html(`Clic para mostrar mensajes.`);
            //Se esconde la barra de progreso
            $(`#bar-${resultado[i].id}`).css("display","none");
            //Se esconde el tooltip de la barra de progreso
            $(`#tooltipBar-${resultado[i].id}`).css("display","none");
            //Se muestra el boton
            $(`#btn-${resultado[i].id}`).css("display","block");
          }
          else{
            /*Se ponene las banderas de referencia en 1 para obligar a preguntar nuevamente por el estado 
            y se pone la barra en 100%*/
            //Se pone la barra de progreso en el valor recibido para procesamiento
            $(`#bar-${resultado[i].id} > div`).css("width",`100%`);
            $(`#bar-${resultado[i].id} > div`).html(`100%`);
            $(`#fila-${resultado[i].id}`).data("tstate", 1);
            $(`#fila-${resultado[i].id}`).data("tresult", 1);
            $(`#fila-${resultado[i].id}`).data("rstate", 1);
            $(`#fila-${resultado[i].id}`).data("rresult", 1);
          }
        }
        else if( resultado[i].estadoTransferencia && !resultado[i].resultadoTransferencia ){ // 1 0
          //Se desactiva el boton
          $(`#btn-${resultado[i].id}`).prop("disabled",true);
          //Se desactiva el checkbox, si existe
          if($(`#switch-${resultado[i].id}`).length == 1){
            $(`#switch-${resultado[i].id}`).prop("disabled",true);
          }
          //se crea el icono de carga
          $(`#state-${resultado[i].id}`).html(`<i class="fas fa-sync-alt"></i>`);
          //Se actualiza el mensaje del tooltip
          $(`#tooltipState-${resultado[i].id}`).html(`Cargando...`);
          //Se actualiza la fecha y el usuario
          $(`#date-${resultado[i].id}`).html(resultado[i].fechaIniTransferencia);
          $(`#name-${resultado[i].id}`).html(resultado[i].nombreCompleto);
          //Se muestra la barra de progreso
          $(`#bar-${resultado[i].id}`).css("display","block");
          //Se muestra el tooltip de la barra de progreso
          $(`#tooltipBar-${resultado[i].id}`).css("display","block");
          //Se cambia el mensaje del tooltip de la barra de progreso
          $(`#tooltipBar-${resultado[i].id}`).html(resultado[i].EtiquetaEstado);
          //Se oculta el boton
          $(`#btn-${resultado[i].id}`).css("display","none");
        }
        else if( !resultado[i].estadoTransferencia && resultado[i].resultadoTransferencia ){ // 0 1
          //se hace el cambio solo si la barra esta en 100%
          if($(`#bar-${resultado[i].id} > div`).html() == "100%" ||
            parameters.firstTime == 1){//condicion para que entre la primera vez
            //Se activa el boton
            $(`#btn-${resultado[i].id}`).prop("disabled",false);
            //Se activa el checkbox, si existe
            if($(`#switch-${resultado[i].id}`).length == 1){
              $(`#switch-${resultado[i].id}`).prop("disabled",false);
            }
            //se crea el icono de exito
            $(`#state-${resultado[i].id}`).html(`<i class="fas fa-check"></i>`);
            //Se actualiza el mensaje del tooltip
            $(`#tooltipState-${resultado[i].id}`).html(`Operación completada con éxito!`);
            //Se esconde la barra de progreso
            $(`#bar-${resultado[i].id}`).css("display","none");
            //Se esconde el tooltip de la barra de progreso
            $(`#tooltipBar-${resultado[i].id}`).css("display","none");
            //Se muestra el boton
            $(`#btn-${resultado[i].id}`).css("display","block");
          }
          else{
            /*Se ponene las banderas de referencia en 1 para obligar a preguntar nuevamente por el estado 
            y se pone la barra en 100%*/
            //Se pone la barra de progreso en el valor recibido para procesamiento
            $(`#bar-${resultado[i].id} > div`).css("width",`100%`);
            $(`#bar-${resultado[i].id} > div`).html(`100%`);
            $(`#fila-${resultado[i].id}`).data("tstate", 1);
            $(`#fila-${resultado[i].id}`).data("tresult", 1);
            $(`#fila-${resultado[i].id}`).data("rstate", 1);
            $(`#fila-${resultado[i].id}`).data("rresult", 1);
          }
        }
        else{}
      }
      else{}
    }
    //Entra solo si el estado de procesamiento esta en 1
    if(resultado[i].estadoProcesamiento){
      //Se pone la barra de progreso en el valor recibido para procesamiento
      $(`#bar-${resultado[i].id} > div`).css("width",`${resultado[i].avanceProcesamiento}%`);
      $(`#bar-${resultado[i].id} > div`).html(`${resultado[i].avanceProcesamiento}%`);
    }
    //Entra solo si el estado de transferencia esta en 1
    if(resultado[i].estadoTransferencia){
      //Se pone la barra de progreso en el valor recibido para transferencia
      $(`#bar-${resultado[i].id} > div`).css("width",`${resultado[i].avanceTransferencia}%`);
      $(`#bar-${resultado[i].id} > div`).html(`${resultado[i].avanceTransferencia}%`);
    }
    /*Casteo a vacio en caso de null*/
    resultado[i].fechaIniTransferencia = (resultado[i].fechaIniTransferencia == null)?"":resultado[i].fechaIniTransferencia;
    resultado[i].nombreCompleto = (resultado[i].nombreCompleto == null)?"":resultado[i].nombreCompleto;
    resultado[i].EtiquetaEstado = (resultado[i].EtiquetaEstado == null)?"":resultado[i].EtiquetaEstado;
    /*Entra cuandpo ha habido cambios en nombre, etiqueta estado o fecha y la bandera
    de carga por primera vez esta desactivada*/
    if(($(`#date-${resultado[i].id}`).html() != resultado[i].fechaIniTransferencia ||
        $(`#name-${resultado[i].id}`).html() != resultado[i].nombreCompleto ||
        $(`#tooltipBar-${resultado[i].id}`).html() != resultado[i].EtiquetaEstado) &&
        parameters.firstTime == 0){
      //actualiza las filas
      $(`#date-${resultado[i].id}`).html(resultado[i].fechaIniTransferencia);
      $(`#name-${resultado[i].id}`).html(resultado[i].nombreCompleto);
      $(`#tooltipBar-${resultado[i].id}`).html(resultado[i].EtiquetaEstado);
      //Se agrega la clase de transicion
      $(`#date-${resultado[i].id}`).parent().addClass("transicion");
      //Se remueve la clase despues de un tiempo
      setTimeout("gestionCA.removeTransicion()",2000);
    }
    else{
      //actualiza las filas
      $(`#date-${resultado[i].id}`).html(resultado[i].fechaIniTransferencia);
      $(`#name-${resultado[i].id}`).html(resultado[i].nombreCompleto);
      $(`#tooltipBar-${resultado[i].id}`).html(resultado[i].EtiquetaEstado);
    }
  }
  //Se apaga la bandera de carga por primera vez
  parameters.firstTime = 0;
  //Detiene el llamado del refresh si la tabla no existe
  if( $(`${parameters.validate}`).length != 0){
    //Se referencia en parameters el setInterval
    setTimeout(parameters.callRefresh, 1000);
  }
}

//funcion encargada de quitar el efecto de transicion
gestionCA.removeTransicion = function(){
  $(".transicion").removeClass('transicion');
}

/*Funcion encargada de mostrar la barra al usuario cuando presione el boton de envio
 y las banderas lo permitan*/
gestionCA.showBarra = function(id){
    //Se desactiva el boton
    $(`#btn-${id}`).prop("disabled",true);
    //Se desactiva el checkbox, si existe
    if($(`#switch-${id}`).length == 1){
      $(`#switch-${id}`).prop("disabled",true);
    }
    //Se oculta el boton
    $(`#btn-${id}`).css("display","none");
    //se crea el icono de carga
    $(`#state-${id}`).html(`<i class="fas fa-sync-alt"></i>`);
    //Se actualiza el mensaje del tooltip
    $(`#tooltipState-${id}`).html(`Cargando...`);
    //Se muestra la barra de progreso
    $(`#bar-${id}`).css("display","block");
    //Se pone la barra de progreso en el valor recibido para procesamiento
    $(`#bar-${id} > div`).css("width",`0%`);
    $(`#bar-${id} > div`).html(`0%`);
    //Se actualiza el estado anterior
    $(`#fila-${id}`).data("tstate", 1);
    $(`#fila-${id}`).data("tresult", 1);
    $(`#fila-${id}`).data("rstate", 1);
    $(`#fila-${id}`).data("rresult", 1);
}

//AJAX para traer los ultimos mensajes
gestionCA.bringMesagge = function(event){
  var parameters = event.data.parametros;
    query.callAjax(parameters.url,
    "bring_mesagge",
    {id:event.data.id}, 
    gestionCA.showMesagge,parameters);
}

//Se muestran los mensajes
gestionCA.showMesagge = function(resultado,parameters){
  //Se pone el titulo de los mensajes
  $(`#${parameters.divMensajes} > div > h4 > b`).html(`
    Mensajes - ${resultado[0].nombreInterfaz} - ${resultado[0].fechaIniTransferencia}
  `);
  var txt = "";
  //Se ponene los mensajes en el body
  for(var i = 0; i < resultado.length; i++){
    txt = `${txt}
    <div class="mensajesSection">
      <h5><b>Error #${i+1}</b></h5>
      <p>${resultado[i].mensaje}</p>
    </div>`;
  }
  $(`#${parameters.divMensajes} > div.mensajesBody`).html(txt);
  //Desaparesco el div de la tabla y aparesco el div de los mensajes
  $(`#${parameters.divTabla}`).fadeOut(0);
  $(`#${parameters.divMensajes}`).fadeIn(500);
}

//Se esconde el div de mensajes y muestra el div de la tabla
gestionCA.hideMesagge = function(event){
  var parameters = event.data.parametros;
  $(`#${parameters.divMensajes}`).fadeOut(0);
  $(`#${parameters.divTabla}`).fadeIn(500);
}

//funcion encargada de hacer un trigger al form
gestionCA.seleccionArchivo = function(event){
  var id = event.data.id;
  //Se limpia el form para que pueda subir el mismo archivo las veces que quiera
  $(`#subirArchivo-${id}`).val("");
  $(`#subirArchivo-${id}`).trigger("click");
}

//Se carga el archivo a html y se invocan servicio para ver estado de banderas
gestionCA.fileUpload = function(event){debugger;
  var parameters = event.data.parametros;
  var id = event.data.id;
  var formData = new FormData($(`#archivoSeleccionado-${id}`)[0]);
  //Se envia el id listado entrada
  formData.append("idListadoEntrada", id);
  query.callAjax(parameters.url,
      "verify_and_modify_flags",
      {idArchivo:id,
        loginUsuario:parameters.loginUsuario}, 
      gestionCA.verifyFlags, {parameters,id,formData});
}

//Se verifica si se puede comenzar a subir el archivo o se debe mostrar un mensaje de conflicto
gestionCA.verifyFlags = function(resultado, parametros){
  //Se comienza la transferencia
  if(resultado != "FALSE"){
      //Se muestra la barra a nivel de usuario
      gestionCA.showBarra(parametros.id);
      //Se envia el id registro entrada
      parametros.formData.append("idRegistroEntrada", resultado);
      gestionCA.callAjaxFile(parametros.parameters.url, "load_files", parametros.id, parametros.formData, gestionCA.archivoSubido, parametros.parameters);
  }
  else{
    //Se infortma del conflicto
    gestionModal.alertaConfirmacion("P360","Alguien más está tratando de subir este archivo",
      "error", "OK", "#AB0000", function(){});
  }
}

//AJAX para enviar archivos usando forms
gestionCA.callAjaxFile = function(urlConexion,metodo,id,data,successEvent,parametros){
     $.ajax({
        url: urlConexion+"/"+metodo, //variable definida en el index
        type: 'POST',
        dataType: 'json',
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        xhr: function() {
              var xhr = $.ajaxSettings.xhr();
              xhr.upload.onprogress = function(e) {
                  //Se actualiza en base de datos el porcentaje de carga
                  var carga = Math.floor(e.loaded / e.total *100);
                  gestionCA.cargandoArchivo(urlConexion, id, carga); 
          };
          return xhr;
        },
        error:function(objeto1,objeto2,objeto3){
            /*
            console.log(objeto1);
            console.log(objeto2);
            console.log(objeto3);
            */
            gestionCA.errorEvent(data,urlConexion);
        },
        timeout:600000,  // I chose 5 secs for kicks
        success: function (data){
            if(data.ESTADO == "TRUE"){
              if(parametros == undefined){
                successEvent(data.RESULTADO);
              }
              else{
                successEvent(data.RESULTADO,parametros,urlConexion);
              }
            }else{
              gestionCA.errorEvent(data,urlConexion);
            } 
        }
    });
}

//Esta funcion informa del error de la carga de archivos
gestionCA.errorEvent = function(data,urlConexion){
  query.callAjax(urlConexion,
        "fail_load",
        {idListadoEntrada:parseInt(data.get("idListadoEntrada")),
          idRegistroEntrada:parseInt(data.get("idRegistroEntrada"))}, 
        gestionCA.failLoad);
}

//CALLBACK fail_load
gestionCA.failLoad = function(){

}

//funcion que se encarga de actualizar la carga del archivo
gestionCA.cargandoArchivo = function(urlConexion ,ID, CARGA){
  query.callAjax(urlConexion,
        "update_load",
        {id:ID,
          carga:CARGA}, 
        gestionCA.cargandoArchivoCallback);
}

//Recibe la informacion del metodo que hace el update de la carga
gestionCA.cargandoArchivoCallback = function(resultado){

}

//CALLBACK de la funcion que carga el archivo
gestionCA.archivoSubido = function(resultado,parameters,urlConexion){
  query.callAjaxPost(urlConexion,
        "hilo_procesamiento",
        {idListadoEntrada:resultado});
  //Se envia al callback del usuario
  parameters.callback(parameters.parametros);
}

/*Funcion encargada de verrificar y modificar banderas antes de comenzar 
la sincronizacion con base de datos privada*/
gestionCA.verifyFlagsBDPrivada = function(event){
  var parameters = event.data.parametros;
  var ID = event.data.id;
  query.callAjax(parameters.url,
    "start_sync_up_BDPrivada",
    {id:ID}, 
    gestionCA.startSyncUpBDPrivada,{ID,parameters});
}

gestionCA.verifyFlagsBDPublica = function(event){
  var parameters = event.data.parametros;
  var ID = event.data.id;
  query.callAjax(parameters.url,
    "start_sync_up_BDPublica",
    {id:ID}, 
    gestionCA.startSyncUpBDPublica,{ID,parameters});
}

gestionCA.startSyncUpBDPublica = function(resultado,parametros){
  var url_publica = parametros.parameters.url.substring(0,parametros.parameters.url.indexOf('SERVER/')+7) + resultado.claseTransferencia+'.asmx';

  query.callAjax(url_publica,
    "IniciaSincroBDPublica",
    {Id_Lista_Entrada:parametros.ID,
      Id_Usuario:parametros.parameters.loginUsuario}, 
    gestionCA.startSyncUpBDPublicaCallback,parametros.parameters,url_publica);
    //Se muestra la barra a nivel de usuario
    gestionCA.showBarra(parametros.ID);
}

//Funcion encargada de comenzar la sincronizacion a base de datos privada
gestionCA.startSyncUpBDPrivada = function(resultado,parametros){
  //Se verifica que el estado este bien
  if(resultado.state == "TRUE"){
  query.callAjax(resultado.url,
    "IniciaSincroBD",
    {Id_Lista_Entrada:parametros.ID,
      Id_Usuario:parametros.parameters.loginUsuario}, 
    gestionCA.startSyncUpBDPrivadaCallback,parametros.parameters,resultado.url);
    //Se muestra la barra a nivel de usuario
    gestionCA.showBarra(parametros.ID);
  }
  else{
    //Se infortma del conflicto
    gestionModal.alertaConfirmacion("P360","Alguien más está tratando de sincronizar con esta base de datos","error", "OK", "#AB0000", function(){});
  }
}

//CALLBACK gestionCA.startSyncUpBDPrivada
gestionCA.startSyncUpBDPrivadaCallback = function(resultado,parameters){
  //Se envia al callback del usuario
  parameters.callback(parameters.parametros);
}

gestionCA.startSyncUpBDPublicaCallback = function(resultado,parameters){
  //Se envia al callback del usuario
  parameters.callback(parameters.parametros);
}



/*Funcion encargada de llamar a la funcion que recorrer todos los checkbox,
menos el principal*/
gestionCA.startSyncUpAll = function(event){
  var classCheckbox = event.data.classCheckbox;
  $(`${classCheckbox}`).each(gestionCA.SyncUpAll);
}

//Se activa la sincronizacion de los checkbox que no esten deshabilitados*/
gestionCA.SyncUpAll = function(){
  if($(this).prop('checked') && !$(this).prop("disabled")){
    $(`#btn-${$(this).data("id")}`).trigger('click');
  }
}