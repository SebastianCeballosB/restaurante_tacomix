var chart = new Object();

chart.line = function(id, titulo, subtitulo, ejeX, tituloEjeX, tituloEjeY, dataSeries, tituloMenuButton, 
	tooltip, typeGraph, show, advertencia, legend, stack, mark, values, invertir, colors){
	//Configuracion del lenguaje highcharts
	//Si el usuario no define la paleta de colores, se definen por defecto, los colores se 
	//deben enviar en una arreglo ya sea en formato rgb o hexadecimal aceptado por CSS
	if(colors == undefined || colors == null || colors == ""){
		var defaultColors = ["#7cb5ec", "#434348", "#90ed7d", "#f7a35c", "#8085e9",
						"#f15c80", "#e4d354", "#2b908f", "#f45b5b", "#91e8e1"];
	}
	else{
		var defaultColors = colors;
	}

	Highcharts.setOptions({
		colors: defaultColors,
	    lang: {
	        months: [
	            'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre',
	            'Octubre','Noviembre','Diciembre'
	        ],
	        weekdays: [
	           'Lunes','Martes','Miércoles','Jueves','Viernes','Sábado','Domingo'
	        ],
	        contextButtonTitle: undefined,//Con undefined no pone tooltip en elcontext buttom
	        viewFullscreen: 'Pantalla completa',
	        printChart: 'Imprimir gráfica',
	        downloadPNG: 'Descargar imagen PNG',
	       	downloadJPEG: 'Descargar imagen JPEG',
	       	downloadPDF: 'Descargar documento PDF',
	       	downloadSVG: 'Descargar imagen vectorial SVG',
	        downloadCSV: 'Descargar CSV',
	        downloadXLS: 'Descargar XLS',
	        viewData: 'Ver tabla de datos',
	        openInCloud: 'Abrir en Highcharts Cloud',
	        loading: 'Cargando...',
	        noData: 'No hay información para mostrar'
	    }
	});


	//Solo si no es type pie
	if(typeGraph != "pie"){
		if(invertir == false){
			var offset = 0;
			if(legend){
				offset = 5;
			}
			var divisor = 1;
			if( (typeGraph == "column" && stack != "normal") ||
				(typeGraph == "bar" && stack != "normal") ){

				divisor = Math.ceil(dataSeries.length/20);
			}

			var ancho;
			var scroll = true;
			if($(`#${id}`).width() >= 3000){
				ancho = Math.floor((125-offset)/divisor);
			}
			else if( $(`#${id}`).width() < 3000 && $(`#${id}`).width() >= 2750 ){
				ancho = Math.floor((115-offset)/divisor);
			}
			else if( $(`#${id}`).width() < 2750 && $(`#${id}`).width() >= 2500){
				ancho = Math.floor((105-offset)/divisor);
			}
			else if ( $(`#${id}`).width() < 2500 && $(`#${id}`).width() >= 2250 ){
				ancho = Math.floor((95-offset)/divisor);
			}
			else if( $(`#${id}`).width() < 2250 && $(`#${id}`).width() >= 2000 ){
				ancho = Math.floor((85-offset)/divisor);
			}
			else if( $(`#${id}`).width() < 2000 && $(`#${id}`).width() >= 1750){
				ancho = Math.floor((75-offset)/divisor);
			}
			else if ( $(`#${id}`).width() < 1750 && $(`#${id}`).width() >= 1500 ){
				ancho = Math.floor((65-offset)/divisor);
			}
			else if( $(`#${id}`).width() < 1500 && $(`#${id}`).width() >= 1250 ){
				ancho = Math.floor((55-offset)/divisor);
			}
			else if( $(`#${id}`).width() < 1250 && $(`#${id}`).width() >= 1000){
				ancho = Math.floor((45-offset)/divisor);
			}
			else if ( $(`#${id}`).width() < 1000 && $(`#${id}`).width() >= 750 ){
				ancho = Math.floor((35-offset)/divisor);
			}
			else if( $(`#${id}`).width() < 750 && $(`#${id}`).width() >= 500 ){
				ancho = Math.floor(25/divisor);
			}
			else if( $(`#${id}`).width() < 500 && $(`#${id}`).width() >= 250){
				ancho = Math.floor(15/divisor);
			}
			else if ( $(`#${id}`).width() < 250 && $(`#${id}`).width() >= 0 ){
				ancho = Math.floor(5/divisor);
			}
			else{
				ancho = 1;
			}

			if(ancho + 1 >= ejeX.length){
				scroll = false;
			}

			if(ancho >= ejeX.length){
				ancho = ejeX.length-1;
			}

			//correccion para no mostrar un espacio vacio
			ancho = ancho == 0 && scroll ? ancho + 1 : ancho;

			//mueve el boton del menu segun el ancho de la grafica 
			var vert = 10;
			var hori = -20;
			var posi = 'top';
			if($(`#${id}`).width() < 750 && scroll){
				tituloMenuButton = "";
				vert = -40;
				hori = 0;
				posi = 'bottom';
			}
			else if( $(`#${id}`).width() < 750 &&  !scroll){
				tituloMenuButton = "";
				vert = 0;
				hori = 0;
				posi = 'bottom';
			}
		}
		else
		{
			var offset = 0;

			var divisor = 1;
			if( (typeGraph == "column" && stack != "normal") ||
				(typeGraph == "bar" && stack != "normal") ){

				divisor = Math.ceil(dataSeries.length/20);
			}

			var ancho;
			var scroll = true;
			if($(`#${id}`).height() >= 3000){
				ancho = Math.floor((125-offset)/divisor);
			}
			else if( $(`#${id}`).height() < 3000 && $(`#${id}`).height() >= 2750 ){
				ancho = Math.floor((115-offset)/divisor);
			}
			else if( $(`#${id}`).height() < 2750 && $(`#${id}`).height() >= 2500){
				ancho = Math.floor((105-offset)/divisor);
			}
			else if ( $(`#${id}`).height() < 2500 && $(`#${id}`).height() >= 2250 ){
				ancho = Math.floor((95-offset)/divisor);
			}
			else if( $(`#${id}`).height() < 2250 && $(`#${id}`).height() >= 2000 ){
				ancho = Math.floor((85-offset)/divisor);
			}
			else if( $(`#${id}`).height() < 2000 && $(`#${id}`).height() >= 1750){
				ancho = Math.floor((75-offset)/divisor);
			}
			else if ( $(`#${id}`).height() < 1750 && $(`#${id}`).height() >= 1500 ){
				ancho = Math.floor((65-offset)/divisor);
			}
			else if( $(`#${id}`).height() < 1500 && $(`#${id}`).height() >= 1250 ){
				ancho = Math.floor((55-offset)/divisor);
			}
			else if( $(`#${id}`).height() < 1250 && $(`#${id}`).height() >= 1000){
				ancho = Math.floor((45-offset)/divisor);
			}
			else if ( $(`#${id}`).height() < 1000 && $(`#${id}`).height() >= 750 ){
				ancho = Math.floor((35-offset)/divisor);
			}
			else if( $(`#${id}`).height() < 750 && $(`#${id}`).height() >= 500 ){
				ancho = Math.floor(25/divisor);
			}
			else if( $(`#${id}`).height() < 500 && $(`#${id}`).height() >= 250){
				ancho = Math.floor(15/divisor);
			}
			else if ( $(`#${id}`).height() < 250 && $(`#${id}`).height() >= 0 ){
				ancho = Math.floor(5/divisor);
			}
			else{
				ancho = 1;
			}

			if(ancho + 1 >= ejeX.length){
				scroll = false;
			}

			if(ancho >= ejeX.length){
				ancho = ejeX.length-1;
			}

			//correccion para no mostrar un espacio vacio
			ancho = ancho == 0 && scroll ? ancho + 1 : ancho;

			//mueve el boton del menu segun el ancho de la grafica 
			var vert = 10;
			var hori = -20;
			var posi = 'top';
			if($(`#${id}`).height() < 750 && scroll){
				tituloMenuButton = "";
				vert = -40;
				hori = 0;
				posi = 'bottom';
			}
			else if( $(`#${id}`).height() < 750 &&  !scroll){
				tituloMenuButton = "";
				vert = 0;
				hori = 0;
				posi = 'bottom';
			}
		}
	}

	var heightScroll = 8;
	 if(( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))){
       // se quita la clase para habilitar los eventos en el sidebar nuevamente
       heightScroll = 30;
   }

	/*En esta funcion es donde se le dice a highcharts que plotear y como plotearlo, 
	todo lo que este dentro de un div grafica se configura aqui*/
	Highcharts.chart(id, {//parametro de entrada
	    chart: {//parametro de entrada
	    	renderTo: id,
	    	plotBackgroundColor: null,
        	plotBorderWidth: null,
        	plotShadow: false,
	        type: typeGraph,//column//spline//line- sin tooltip y siempre muestra datos
	        zoomType: 'xy',
	        inverted: invertir
	    },
	    title: {
	        text: titulo,//parametro de entrada
	        y: 22,
	        style: {
				width: '100%',
				textOverflow: 'ellipsis'
	        }
	    },
	    credits: {
	    	text: `<b>NO SE MOSTRARON TODAS LAS SERIES!</b>`,
        	href: '#',
   	        position: {
   	        	verticalAlign: 'bottom',//TOP
	            align: 'left',
	            x: 10,//10
	            y: -5//14
        	},
        	style: {
        		"cursor": "auto", 
        		"color": "#dc3545", 
        		"fontSize": "11px"
        	},
        	enabled: advertencia
    	},
	    subtitle: {
	        text: subtitulo,//parametro de entrada
	        style: {
				width: '100%',
				textOverflow: 'ellipsis'
	        }
	    },
	    legend: {
	    	enabled: legend,
	    	margin: 0,
        	layout: 'vertical',
        	align: 'right',//Cambia el legend de posision
        	verticalAlign: 'middle',
        	y: 10,
        	maxHeight: 300,
        	navigation: {
	        	activeColor: '#3E576F',
	        	animation: true,
	        	arrowSize: 12,
	        	inactiveColor: '#CCC',
	        	style: {
	            	fontWeight: 'bold',
	            	color: '#333',
	            	fontSize: '12px'
	        	}
	    	},
    		itemStyle: {
        		fontSize: "10px",
        		fontWeight: "none"
        	}
    	},
	    tooltip: {
	    	enabled: tooltip,//parametro de entrada
	    	crosshairs: true, //Se comparte un div que contiene los puntos señalados el cual es sombreado
        	shared: true, //Se comparten los puntos en el tooltip
            delayForDisplay: 200,
            hideDelay: 200,
	        backgroundColor: {
	            linearGradient: [0, 0, 0, 60],
	            stops: [
	                [0, '#FFFFFF'],
	                [1, '#E0E0E0']
	            ]
	        },
	        borderWidth: 1,
	        borderColor: '#AAA',
	        //pointFormat: undefined//'{series.name}: <b>{point.percentage:.1f}%</b>'
	    },
	    xAxis: {
	    	tickPositions: show,//parametro de entrada
	    	tickWidth: 2,
	        categories: ejeX,//parametro de entrada
	        title:{
	        	text: tituloEjeX//parametro de entrada
	        },
	        max: ancho
	    },
		scrollbar: {
			enabled: scroll,
            barBackgroundColor: '#999',
            barBorderRadius: 4,
            barBorderWidth: 0,
            button: 'none',
            buttonArrowColor: '#fff',
            buttonBackgroundColor: '#fff',
            buttonBorderWidth: 0,
            buttonBorderRadius: 4,
            trackBackgroundColor: 'none',
            trackBorderWidth: 1,
            trackBorderRadius: 4,
            trackBorderColor: '#fff',
            rifleColor: '#999',
            size: heightScroll,
            liveRedraw: true,
            minWidth: 40
		},
	    yAxis: {
	        title: {
	            text: tituloEjeY//parametro de entrada
	        }
	    },
	    exporting:{
	    	buttons:{
	    		contextButton:{
	    			menuItems: ["viewFullscreen", "printChart", "separator", "downloadPNG", "downloadJPEG", 
	    				"downloadPDF", "downloadSVG", "separator", "downloadCSV", "downloadXLS"],
	    			text: tituloMenuButton//parametro de entrada
	    		}
	    	},
			sourceWidth: $(`#${id}`).width(),
			sourceHeight: $(`#${id}`).height()
	    },
	    //Mueve el boton del menu a la izquierda o derecha
	    navigation: {
	        buttonOptions: {
	        	align: 'right',
	            verticalAlign: posi,
	            y: vert,
	            x: hori,
	            scale: 1
	        }
	    },
	    plotOptions: {
	    	spline: {
	            marker: {
                	enabled: mark//salen o no puntos
            	},
               	dataLabels: {
                	enabled: values//salen o no puntos
                }
            },
            areaspline: {
	            marker: {
                	enabled: mark//salen o no puntos
            	},
            	dataLabels: {
                	enabled: values//salen o no puntos
                }
            },
	        line: {
				marker: {
                	enabled: mark//salen o no puntos
            	},
	            dataLabels: {
	                enabled: values//salen o no puntos
	            }
	        },
	        area: {
				marker: {
                	enabled: mark//salen o no puntos
            	},
	            dataLabels: {
	                enabled: values//salen o no puntos
	            }
	        },
	        column: {
            	stacking: stack,
            	dataLabels: {
                	enabled: values,//salen o no puntos
                	color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'black'
            	}
			},
			pie: {
            allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: values,
	                //Formato de puntos en pie chart, salga el porcentaje
	                format: '<b>{point.name}</b>: {point.percentage:.1f} %',
	                style: {
	                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
	                },
	                connectorColor: 'silver'
	            }
	        }
	    },
	    //Series a plotear
	    series: dataSeries//parametro de entrada
	});
}

/**********************************************************************************/
/*Este arreglo es una variable global que se compara con parametros de consulta para mermitir la entrada 
a los callbacks solo de la peticion akjax mas reciente*/
chart.state = new Array();
//Guarda el estado actual del modal al abrirlo
chart.$initModalBody = ""; 
//bandera de control para ajuste de legend
countGraphInPanel = 0;

/* método initTable */
/* Hace el llamado para obtener las columnas a consultar y plotear											*/
/* @var parameters, prepara los parametros necesarios para realizar las configuraciones después de llamar 	*/
/* al webservice																			*/
//Esta funcion es la que debe llamar el usuario de la libreria.
chart.initGraph = function (urlConexion, nombre, containerGraph, tituloMenuButton, indice, 
	replace, timeOut, callback, JChartPanel) {
	var metodo = "data_graph";
	var col_name = metodo+'_column_names';

	callback = callback == undefined ? function(){} : callback;

	//Control de prioridad de consultas-inicializacion 0
	chart.state[indice] = 0;

	var parameters = {
		metodo,
		cols: "",
		urlConexion,
		nombre,
		containerGraph,
		tituloMenuButton,
		indice,
		JChartPanel,
		n_graph : 0,
		legend : null,
		//En esta variable se arma la estructura JSON que se maneja con las funciones de los filtros C#
		consulta : null,
		//En esta variable se guarda el Objeto que se maneja con las funciones de los filtros C#
		objConsulta : null,
		timeOut,
		windowResize:false,
		firstTime: 1,
		callback
	};

	if(replace == undefined || replace == null){
		parameters.replace = " ";
	}
	else{
		parameters.replace = JSON.stringify(replace);
	}

	query.callAjax( urlConexion, col_name, {nombre: nombre}, chart.receiveInfo, parameters );
}

/* método receiveInfo */
/* Recibe la información de las columnas y se la envia a otras funciones para 
dibujar la grafica y obtener los filtros */
/* @param columns, respuesta del AJAX 									   */
/* @param parameters, var pasada a través de la función que invocó el AJAX */
chart.receiveInfo = function (resultado, parameters) {
	//Columns es el nombre de las columnas de la vista y el tipo de dato que manejan
	var columns = resultado.COLUMNS;

	/*Se arma el JSON que contiene la informacion y se pasa a objeto guardandose en parameters para que 
	sea unica en cada llamado*/
	chart.getColsFilter(columns,parameters);

	//Variables array que extraen la informacion de configuracion de highcharts y la guardan en parameters
	parameters.titulo = new Array(resultado.HIGHCHARTS.length);
	parameters.subtitulo = new Array(resultado.HIGHCHARTS.length);
	parameters.tituloEjeY = new Array(resultado.HIGHCHARTS.length);
	parameters.tituloEjeX = new Array(resultado.HIGHCHARTS.length);
	parameters.typeGraph = new Array(resultado.HIGHCHARTS.length);
	parameters.mark = new Array(resultado.HIGHCHARTS.length);
	parameters.values = new Array(resultado.HIGHCHARTS.length);
	parameters.legendUser = new Array(resultado.HIGHCHARTS.length);
	parameters.ejeX = new Array(resultado.HIGHCHARTS.length);
	parameters.type = new Array(resultado.HIGHCHARTS.length);
	parameters.detalle = new Array(resultado.HIGHCHARTS.length);

	for(var i = 0; i < resultado.HIGHCHARTS.length; i++){
		parameters.titulo[i] = resultado.HIGHCHARTS[i].titulo;
		parameters.subtitulo[i] = resultado.HIGHCHARTS[i].subtitulo;
		parameters.tituloEjeY[i] = resultado.HIGHCHARTS[i].tituloEjeY;
		parameters.tituloEjeX[i] = resultado.HIGHCHARTS[i].tituloEjeX;
		parameters.typeGraph[i] = resultado.HIGHCHARTS[i].type;
		parameters.mark[i] = resultado.HIGHCHARTS[i].mark;
		parameters.values[i] = resultado.HIGHCHARTS[i].values;
		parameters.legendUser[i] = resultado.HIGHCHARTS[i].legendUser;
		parameters.ejeX[i] = resultado.HIGHCHARTS[i].EJEX;
		parameters.detalle[i] = resultado.HIGHCHARTS[i].detalle;
		//Se averigua de que tipo de dato es el ejex
		for(var j = 0; j < columns.length; j++){
			if(columns[j].data.slice(0, 10) == parameters.ejeX[i])
			{
				parameters.type[i] = columns[j].type;
			}
		}
	}

	//Pone los eventos de los selectores en 1
	parameters.Fchange = new Array(columns.length).fill(1);

	//Se mira cual es el modo de uso deseado por el usuario a traves de la variable parameters.JChartPanel
	if(parameters.JChartPanel == undefined){/*Una grafica por link*/
		//Genera el html que contendra las graficas y los selectores
		chart.html(resultado.HIGHCHARTS,parameters);

		//Se dibuja la grafica por primera vez
		chart.graph(parameters);
		$(window).on("orientationchange", {parametros:parameters}, chart.orientationchange);

		//Se define la funcion de refresco cuando exista un timeOut
		if(parameters.timeOut != undefined) {
			parameters.callRefresh = function(){
				chart.graph(parameters, parameters.timeOut);
			}
		}

		//Dibuja los selectores y botones
		chart.drawFilters(columns, resultado.HIGHCHARTS, parameters);
	}
	else{/*M links * N graficas*/
		//legend panels (segun tamaño de grafica inicializado en lo mismo que puso el usuario en la vista)
		parameters.legend = parameters.legendUser.slice();

		//Genera el html que contendra las graficas y los selectores
		chart.panel(resultado.HIGHCHARTS,parameters);
		$(window).on("orientationchange", {parametros:parameters}, chart.orientationchangePanel);

		//Se dibuja la grafica por primera vez
		chart.graphPanel(parameters);

		//Se define la funcion de refresco cuando exista un timeOut
		if(parameters.timeOut != undefined) {
			parameters.callRefresh = function(){
				chart.graphPanel(parameters, parameters.timeOut);
			}
		}

		//Dibuja los selectores y botones
		chart.drawFilters(columns, resultado.HIGHCHARTS, parameters);
	}
}

chart.orientationchange = function(event){
	parameters = event.data.parametros;
	parameters.windowResize = true;
	var parametros = new Object();
	parametros.parameters = parameters;
	parametros.state = chart.state[parameters.indice];
	chart.callBackGraph(parameters.resultado, parametros);
}

chart.orientationchangePanel = function(event){
	parameters = event.data.parametros;
	chart.graphPanel(parameters.resultado, parameters);
}

//Se arma la estructura del JSON consulta
chart.getColsFilter = function(columns,parameters){
	/*Se busca en todos los selectores pertenecientes a la grafica y
	se arma un JSON que contenga los parametros a filtrar*/
	parameters.consulta = `[`;
	//Se busca que columnas hay que consultar y cuantos parametros hay
	for(var i = 0; i < columns.length; i++){
		parameters.consulta = `${parameters.consulta}{"data":"${columns[i].data}","name":"","searchable":false,"orderable":false,"search":{"value":"","regex":false}},`;
	}
	parameters.consulta = `${parameters.consulta}]`;
	//Elimina la coma al final del Json
	var coma = parameters.consulta.length-parameters.consulta.lastIndexOf(",");
	if(coma == 2){
		coma = parameters.consulta.lastIndexOf(",");
		parameters.consulta = parameters.consulta.substring(0, coma);
		parameters.consulta = `${parameters.consulta}]`;
	}
	//Se pasa a objeto
	parameters.objConsulta = JSON.parse(parameters.consulta);
}

/*Se genera el esqueleto html con los filtros en un dropdown y una sola caja por grafica la cual 
se invoca a traves de un navbar*/
//highcharts es la tabla de configuracion de la vista
chart.html = function(highcharts, parameters){
	var dimGraph = highcharts.length; //Numero total de graficas
	var txt = "";
	var idGraph = new Array();
	var titulo = new Array();

	//Se guardan los titulos de los ejesY
	for(var i = 0; i < highcharts.length; i++)
	{
		//Se guarda el nombre de los ejes y
		titulo[i] = highcharts[i].nombre;
	}

	//Generacion del html con id unicos y algunas clases unicas por llamado a la funcion chart.initGraph
	/*hasta el salto de linea se ha generado el dropdown de los selectores, despues se genera el navbar 
	con botones para mover a derecha e izquierda*/
	txt = `
		<div id="accordionGraph${parameters.indice}" class="accordionGraph">
			<div class="card">
			    <div class="card-header">
			    	<button class="card-link ${parameters.indice}iconDownUp widthLinks btn" 
			    		data-toggle="collapse" href="#graphCollapseOne${parameters.indice}" disabled>
			        	<i class="fas fa-angle-double-down"></i> <p class="inlineGraph">Filtros deshabilitados hasta cargar la gráfica</p>
			    	</button>
					<button id='${parameters.indice}clearBtnFilter' href="#" class='text-center widthLinks btn' disabled>
						<i class="fas fa-times text-danger"></i>
						<i class="fas fa-filter"></i>
						Limpiar Filtros
					</button>
			    </div>
			    <div id="graphCollapseOne${parameters.indice}" class="collapse" data-parent="#accordionGraph${parameters.indice}">
			       	<div id="${parameters.indice}selectoresId" class="card-body slider"></div><!--Div selectores-->
			       	<div id="accordionGraph${parameters.indice}">
						<div class="card cardIn">
						    <div class="card-header">
						    	<a class="card-link ${parameters.indice}iconDownUp" data-toggle="collapse" href="#graphCollapseOne${parameters.indice}">
						        	<div class='text-center'><i class="fas fa-angle-double-down"></i></div>
						    	</a>
						    </div>
				    	</div>
					</div>
			   	</div>
	    	</div>
		</div>

		<div id="${parameters.indice}scroller-left" class="scroller scroller-left"><i class="fas fa-angle-left"></i></div>
		<div id="${parameters.indice}scroller-right" class="scroller scroller-right"><i class="fas fa-angle-right"></i></div>
		<div class="wrapper ${parameters.indice}wrapper">
			<nav class="nav nav-tabs list">
				<a data-ngraph=0 class="nav-item nav-link active ${parameters.indice}callGraph" href="#" data-toggle="tab">
					${titulo[0]}
				</a>
				<i id="detalleGP${parameters.indice}-0" class="fas fa-exclamation-circle btnDetalleGP"></i>`;
		for(var i = 1; i < dimGraph; i++){
			txt = `${txt}
				<a data-ngraph=${i} class="nav-item nav-link ${parameters.indice}callGraph" href="#" data-toggle="tab">
					${titulo[i]}
				</a>
				<i id="detalleGP${parameters.indice}-${i}" class="fas fa-exclamation-circle btnDetalleGP"></i>`;
		}
		//i puede verse como la posicion del eje y en la vista, solo se cuentan los ejes y
	txt = `${txt}
			</nav>
		</div>
		<div id="${parameters.indice}graficaId" class="grafica"></div>`;//Caja de las graficas
	$(`#${parameters.containerGraph}`).append(txt);

	//Mousewheels, funcion que permite moverse con la rueda del mouse en el div de los selectores.
	//permite desplazarse saltando de filtro en filtro y de manera horizontal
	$(`#${parameters.indice}selectoresId`).off('mousewheel').on('mousewheel',{parameters},chart.mousewheelsSelect);

	//Se guarda en parametros los contenedores de las graficas y el contenedor de los selectores
	parameters.idGraph = `${parameters.indice}graficaId`;
	parameters.idDivSelectores = `${parameters.indice}selectoresId`;
	//Fincion que se encarga de alternar los iconos del drop down selectores
	$(`.${parameters.indice}iconDownUp`).off("click").on("click",{parameters}, chart.toggleIcon);
	//Se llama cada on click navbar, cambia de grafica segun el link
	$(`.${parameters.indice}callGraph`).off('click').on('click',{parameters},chart.callGraph);

	//Eventos para el boton de detalles
	for(var i = 0; i < dimGraph; i++){
		if(parameters.detalle[i] === null){//Si no hay detalle se elimina el btn
			$(`#detalleGP${parameters.indice}-${i}`).css("display", "none");
			$(`#detalleGP${parameters.indice}-${i}`).prev().css("padding-right", "15px");
		}else{
			$(`#detalleGP${parameters.indice}-${i}`).off("click").on("click", {parameters:parameters, i:i}, 
				chart.modalDetalle);
		}
	}
}

//Muestra el detalle de las graficas
chart.modalDetalle = function(event){
	gestionModal.alertaConfirmacion(primario.aplicacion, event.data.parameters.detalle[event.data.i],
		"warning","OK", '#3085d6',function(){});
}

//Esta funcion genera el html necesario para paneles
chart.panel = function(highcharts, parameters){
	var dimGraph = parameters.JChartPanel.length;//numero total de ejesY y graficas
	var txt = "";
	var idGraph = new Array();
	var titulo = new Array();

	//Se guardan los titulos de los panels
	for(var i = 0; i < dimGraph; i++)
	{
		//Se guarda el nombre de los panels
		titulo[i] = parameters.JChartPanel[i].titulo;
	}

	//Generacion del html con id unicos y algunas clases unicas por llamado a la funcion chart.callGraphPanel
	/*hasta el salto de linea se ha generado el dropdown de los selectores, despues se genera el navbar 
	con botones para mover a derecha e izquierda*/
	txt = `
		<div id="accordionGraph${parameters.indice}" class="accordionGraph">
			<div class="card">
			    <div class="card-header">
			    	<a class="card-link ${parameters.indice}iconDownUp" data-toggle="collapse" href="#graphCollapseOne${parameters.indice}">
			        	<div class='text-center widthLinks'><i class="fas fa-angle-double-down"></i> Filtros</div>
			    	</a>
					<button id='${parameters.indice}clearBtnFilter' href="#" class='text-center widthLinks btn' disabled>
						<i class="fas fa-times text-danger"></i>
						<i class="fas fa-filter"></i>
						Limpiar Filtros
					</button>
			    </div>
			    <div id="graphCollapseOne${parameters.indice}" class="collapse" data-parent="#accordionGraph${parameters.indice}">
			       	<div id="${parameters.indice}selectoresId" class="card-body slider"></div><!--Div selectores-->
			       	<div id="accordionGraph${parameters.indice}">
						<div class="card cardIn">
						    <div class="card-header">
						    	<a class="card-link ${parameters.indice}iconDownUp" data-toggle="collapse" href="#graphCollapseOne${parameters.indice}">
						        	<div class='text-center'><i class="fas fa-angle-double-down"></i></div>
						    	</a>
						    </div>
				    	</div>
					</div>
			   	</div>
	    	</div>
		</div>

		<div id="${parameters.indice}scroller-left" class="scroller scroller-left"><i class="fas fa-angle-left"></i></div>
		<div id="${parameters.indice}scroller-right" class="scroller scroller-right"><i class="fas fa-angle-right"></i></div>
		<div class="wrapper ${parameters.indice}wrapper">
			<nav class="nav nav-tabs list">
				<a data-number=0 class="nav-item nav-link active ${parameters.indice}callGraph" href="#" data-toggle="tab">${titulo[0]}</a>`;
		for(var i = 1; i < dimGraph; i++){
			txt = `${txt}
				<a data-number=${i} class="nav-item nav-link ${parameters.indice}callGraph" href="#" data-toggle="tab">${titulo[i]}</a>`;
		}
		//i la i puede verse como el numero del panel a mostrar
	txt = `${txt}
			</nav>
		</div>
		<div id="${parameters.indice}graficaIdPanel" class='chartPanelMain'></div>`;//Caja de las graficas
	$(`#${parameters.containerGraph}`).append(txt);

	//Mousewheels, funcion que permite moverse con la rueda del mouse en el div de los selectores.
	//permite desplazarse saltando de filtro en filtro y de manera horizontal
	$(`#${parameters.indice}selectoresId`).off('mousewheel').on('mousewheel',{parameters},chart.mousewheelsSelect);

	//Se guarda en parametros el contenedor de los selectores
	parameters.idDivSelectores = `${parameters.indice}selectoresId`;
	//Fincion que se encarga de alternar los iconos del drop down selectores
	$(`.${parameters.indice}iconDownUp`).off("click").on("click",{parameters}, chart.toggleIcon);
	//Se llama cada on click navbar, cambia de grafica segun el link
	$(`.${parameters.indice}callGraph`).off('click').on('click',{parameters},chart.callGraphPanel);
	//Se actualiza el numero de panel a mostrar a cero
	parameters.nPanel = 0;
}

//Manejo de ajuste de la pantallas (navbar, legend highcharts y .section)
chart.adjust = function(parameters){
	/*Scroll horizontal*/
	//Ancho de la lista navbar
	chart.widthOfList(parameters);

	//Posicion del navbar a la izquierda
	chart.getLeftPosi(parameters);

	//Ajusta pantallas dependiendo del modo de uso (panel M* N o 1 grafica por link)
	if(parameters.JChartPanel == undefined){
		//variables que indican un cambio en la ventana
		parameters.flagAdjustAnt = 0;
		parameters.flagAdjustPost = 0;
		chart.reAdjust(parameters);
	}
	else{
		//variables que indican un cambio en la ventana
		parameters.flagAdjustAnt = new Array();
		parameters.flagAdjustPost = new Array();
		chart.reAdjustPanel(parameters);
	}

	//Este evento debe apagarse antes de llamar chart.initGraph();
	$(window).on('resize',{parameters},chart.windowResize);
	//desplaza el navbar a la derecha
	$(`#${parameters.indice}scroller-right`).off('click').on('click',{parameters},chart.scrollerRight);
	//desplaza el navbar a la izquierda
	$(`#${parameters.indice}scroller-left`).off('click').on('click',{parameters},chart.scrollerLeft);
}

//Funcion que retorna el ancho de la lista navbar
chart.widthOfList = function(parameters){
	var itemsWidth = 0;
	$(`.${parameters.indice}wrapper .list a`).each(function(){
		var itemWidth = $(this).outerWidth();
		itemsWidth+=itemWidth;
	});
	return itemsWidth;
}

//funcion que retorna la posicion a la izquierda del navbar (inicialmente es cero por que no se ha movido a la derecha)
chart.getLeftPosi = function(parameters){
	return $(`.${parameters.indice}wrapper .list`).position().left;
}

/*Esta funcion ajusta el navbar despues de moverse como tabn ajusta el legend y el navbar si se mueve 
el ancho de la pantalla. tambien se encarga del ancho de cada culumna de filtros o .section
Solo esta enfocada al modo de uso de una grafica por link*/
chart.reAdjust = function(parameters){//$(`#${parameters.indice}graficaId`).width() ancho de la ventana a trabajar
	//navbar
	if ( !(chart.widthOfList(parameters)-$(`#${parameters.indice}graficaId`).width()-(-1*chart.getLeftPosi(parameters)) < 1) ) {
		$(`#${parameters.indice}scroller-right`).fadeIn(0);
	}
	else {
		$(`#${parameters.indice}scroller-right`).fadeOut(0);
	}

	if (chart.getLeftPosi(parameters) < 0) {
		$(`#${parameters.indice}scroller-left`).fadeIn(0);
	}
	else {
	$(`.${parameters.indice}wrapper nav .item`).animate({left:"-="+chart.getLeftPosi(parameters)+"px"},0);
		$(`#${parameters.indice}scroller-left`).fadeOut(0);
	}

	//legend
	if($(`#${parameters.indice}graficaId`).width() < 750){
		parameters.legend = false;
		parameters.flagAdjustPost = 0;
	}
	else{
		parameters.legend = true;
		parameters.flagAdjustPost = 1;
	}

	var parametros = new Object();
	parameters.windowResize = true;
	parametros.parameters = parameters;
	parametros.state = chart.state[parameters.indice];
	if(parameters.typeGraph[0] == "pie" && parameters.resultado != undefined && parameters.resultado.DATA != undefined){
		parameters.resultado.DATA = parameters.resultado.DATA[0].data
		chart.callBackGraph(parameters.resultado, parametros);
	}
	else{
		chart.callBackGraph(parameters.resultado, parametros);
	}

	parameters.flagAdjustAnt = parameters.flagAdjustPost;

	//Ancho del div que contiene los selectores
	var widthSelect;

	//.section, numero de columnas de filtros visibles segun el ancho del contenedor
	widthSelect = $(`#accordionGraph${parameters.indice}`).width();
	if(widthSelect >= 1050){
		$(`#${parameters.indice}selectoresId .section`).css("width", "25%");
	}
	else if( widthSelect < 1050 && widthSelect >= 750 ){
		$(`#${parameters.indice}selectoresId .section`).css("width", "33.3333%");
	}
	else if( widthSelect < 750 && widthSelect >= 450){
		$(`#${parameters.indice}selectoresId .section`).css("width", "50%");
	}
	else if ( widthSelect < 450 && widthSelect >= 0 ){
		$(`#${parameters.indice}selectoresId .section`).css("width", "100%");
	}
	else{

	}
}

/*Esta funcion ajusta el navbar despues de moverse como tabn ajusta el legend y el navbar si se mueve el 
ancho de la pantalla. tambien se encarga del ancho de cada culumna de filtros o .section.
Esta es solo enfocada a los paneles de graficas*/
chart.reAdjustPanel = function(parameters){//$(`#${parameters.indice}graficaId`).width() ancho de la ventana a trabajar
	//navbar
	if ( !(chart.widthOfList(parameters)-$(`#accordionGraph${parameters.indice}`).width()-(-1*chart.getLeftPosi(parameters)) < 1) ) {
		$(`#${parameters.indice}scroller-right`).fadeIn(0);
	}
	else {
		$(`#${parameters.indice}scroller-right`).fadeOut(0);
	}

	if (chart.getLeftPosi(parameters) < 0) {
		$(`#${parameters.indice}scroller-left`).fadeIn(0);
	}
	else {
	$(`.${parameters.indice}wrapper nav .item`).animate({left:"-="+chart.getLeftPosi(parameters)+"px"},0);
		$(`#${parameters.indice}scroller-left`).fadeOut(0);
	}

	//legend, pone o quita el legend a cada grafica segun el ancho del contenedor
	var n = parameters.nPanel;
	var parametros = new Object();
	var n_graph;
	var j = 0;

	for(var i = 0; i < parameters.JChartPanel[n].dim; i++){
		n_graph = $(`#chartPanel${parameters.indice}-${n}-${i}`).data('position');
		if($(`#chartPanel${parameters.indice}-${parameters.nPanel}-${i}`).width() < 750){
			parameters.legend[n_graph] = false;
			parameters.flagAdjustPost[n_graph] = 0;
		}
		else{
			parameters.legend[n_graph] = true;
			parameters.flagAdjustPost[n_graph] = 1;
		}

		if(parameters.flagAdjustAnt[n_graph] != parameters.flagAdjustPost[n_graph]){
			//Entra aqui solo si hubo un cambio que debe afectar al legend highcharts

			/*En este caso por ser un panel de graficas solo debe actualizarse el estado de consulta 
			al principio y solo una vez para que permita la entrada a todos los callbacks del panel*/
			if(j == 0){
				/*Se guarda parameter en parametros y se le agrega state a parametros que una variable 
				que se encarga de manejar el estado de consultas ,se hace de esta manera para no perder 
				la referenciacion*/
				
				parametros.parameters = parameters;
				//Se resetea el estado a 50
				if(chart.state[parameters.indice] == 50)
				{
					chart.state[parameters.indice] = 0;
				}
				//Se incrementa el estado de consulta
				chart.state[parameters.indice]++;
				parametros.state = chart.state[parameters.indice];
			}

			//Spinner
			$(`#chartPanel${parameters.indice}-${n}-${i}`).empty();
			$(`#chartPanel${parameters.indice}-${n}-${i}`).append(
				'<div class="graphCenter"><div class="spinner-border text-primary"></div></div>');

			parametros.parameters.n_graph = $(`#chartPanel${parameters.indice}-${n}-${i}`).data('position');
			parametros.parameters.idGraph = `chartPanel${parameters.indice}-${n}-${i}`;
			//Se hace una copia del objeto
			query.callAjax(parameters.urlConexion,parameters.metodo,
			{n_graph: parametros.parameters.n_graph,
				n_col:parameters.objConsulta.length,
				nombre:parameters.nombre,
				columns:parameters.objConsulta, 
				order:[{column: 0, dir: 'asc'}],
				search: {value: "", regex: ""},
				replace: parameters.replace},
				chart.callBackGraph,JSON.parse(JSON.stringify(parametros)));
			j++;
		}
		parameters.flagAdjustAnt[n_graph] = parameters.flagAdjustPost[n_graph];
	}

	//Ancho del div que contiene los selectores
	var widthSelect;

	//.section, numero de columnas de filtros visibles segun el ancho del contenedor
	widthSelect = $(`#accordionGraph${parameters.indice}`).width();
	if(widthSelect >= 1050){
		$(`#${parameters.indice}selectoresId .section`).css("width", "25%");
	}
	else if( widthSelect < 1050 && widthSelect >= 750 ){
		$(`#${parameters.indice}selectoresId .section`).css("width", "33.3333%");
	}
	else if( widthSelect < 750 && widthSelect >= 450){
		$(`#${parameters.indice}selectoresId .section`).css("width", "50%");
	}
	else if ( widthSelect < 450 && widthSelect >= 0 ){
		$(`#${parameters.indice}selectoresId .section`).css("width", "100%");
	}
	else{

	}
}

//Esta funcion es llamada en un cambio de la pantalla, la cual llama readjust segun corresponda
chart.windowResize = function(e){
	parameters = e.data.parameters;
	if(parameters.JChartPanel == undefined){
		chart.reAdjust(parameters);
	}
	else{
		chart.reAdjustPanel(parameters);
	}
}

//Evento de desplazamiento del navbar a la izquierda
chart.scrollerRight = function(e) {
	parameters = e.data.parameters;
	//variable de cantidad a desplazar en el scroll horizontal
	var cant;
	cant = chart.widthOfList(parameters)-$(`#accordionGraph${parameters.indice}`).width()-(-1*chart.getLeftPosi(parameters));
	if( cant >= 300){
		$(`#${parameters.indice}scroller-right`).off('click');
		$(`.${parameters.indice}wrapper .list`).animate({left:'+=-300px'},300);
		window.setTimeout("chart.onScrollerRight(parameters)", 350);
	}
	else{
		$(`#${parameters.indice}scroller-right`).off('click');
		$(`.${parameters.indice}wrapper .list`).animate({left:`+=-${cant+20}px`},300);
		window.setTimeout("chart.onScrollerRight(parameters)", 350);
	}
}

//Ajusta la pantalla segun corresponda despues del evento de mover navbar a la derecha
chart.onScrollerRight = function(parameters){
	if(parameters.JChartPanel == undefined){
		chart.reAdjust(parameters);
	}
	else{
		chart.reAdjustPanel(parameters);
	}
	$(`#${parameters.indice}scroller-right`).off('click').on('click',{parameters},chart.scrollerRight);
}

//Evento de desplazamiento del navbar a la derecha
chart.scrollerLeft = function(e) {
	parameters = e.data.parameters;
	//variable de cantidad a desplazar en el scroll horizontal
	var cant;
	if (chart.getLeftPosi(parameters) < 0) {
		cant = -1*chart.getLeftPosi(parameters);
		if( cant > 300 ){
			$(`#${parameters.indice}scroller-left`).off('click');
			$(`.${parameters.indice}wrapper .list`).animate({left:'+=300px'},300);
			window.setTimeout("chart.onScrollerLeft(parameters)", 350);
		}
		else{
			$(`#${parameters.indice}scroller-left`).off('click');
			$(`.${parameters.indice}wrapper .list`).animate({left:`+=${cant}px`},300);
			window.setTimeout("chart.onScrollerLeft(parameters)", 350);
		}
	}
}

//Ajusta la pantalla segun corresponda despues del evento de mover navbar a la izquierda
chart.onScrollerLeft = function(parameters){
	if(parameters.JChartPanel == undefined){
		chart.reAdjust(parameters);
	}
	else{
		chart.reAdjustPanel(parameters);
	}
	$(`#${parameters.indice}scroller-left`).off('click').on('click',{parameters},chart.scrollerLeft);
}
//Scroll horizontal fin

/*Mousewheels selectores, configura el div de los selectores para desplazarse horizontalemente con un delta
 del tamaño de `#${parameters.indice}selectoresId .section`*/
chart.mousewheelsSelect = function(event, delta){
	parameters = event.data.parameters;
	this.scrollLeft -= (delta * $(`#${parameters.indice}selectoresId .section`).width());
	event.preventDefault();
}

/*esta funcion alterna el icono del collapse selectores con flechas que suben o bajan dependiendo de si se
  estan o no mostrando los selectores*/
chart.toggleIcon = function(e){
	var parameters = e.data.parameters;
	if($(`#graphCollapseOne${parameters.indice}`).hasClass('show') && $(`#graphCollapseOne${parameters.indice}`).hasClass('collapse')){
		$(`.${parameters.indice}iconDownUp i`).addClass('fa-angle-double-down');
		$(`.${parameters.indice}iconDownUp i`).removeClass('fa-angle-double-up');
	}
	else if( $(`#graphCollapseOne${parameters.indice}`).hasClass('collapse') && !( $(`#graphCollapseOne${parameters.indice}`).hasClass('show') ) ){
		$(`.${parameters.indice}iconDownUp i`).removeClass('fa-angle-double-down');
		$(`.${parameters.indice}iconDownUp i`).addClass('fa-angle-double-up');
	}
}

/*Esta funcion cambia el parametro encargado del ejey que se mostrara y llama la funcion que plotea
la grafica(1 grafica por link)*/
chart.callGraph = function(e){
	var parameters = e.data.parameters;
	parameters.n_graph = $(this).data('ngraph');
	//Se pone como activo el link al cual se le dio click y se desactivan los demas
	$("a.active").removeClass('active');
	$(this).addClass("active");
	chart.graph(parameters);
}

/*Esta funcion cambia el parametro encargado del ejey que se mostrara y llama la funcion que plotea
la grafica(panels) es activada desde el navbar*/
chart.callGraphPanel = function(e){
	var parameters = e.data.parameters;
	//Indica que debe actualizar los legend
	parameters.reAdjust = 1;
	//bandera de control para ajuste de legend
	countGraphInPanel = 0;
	var n = $(this).data('number');
	parameters.nPanel = n;
	//Spinner
	$(`#${parameters.indice}graficaIdPanel`).empty();
	$(`#${parameters.indice}graficaIdPanel`).append(parameters.JChartPanel[n].html);
	for(var i = 0; i < parameters.JChartPanel[n].dim; i++){
		$(`#chartPanel${parameters.indice}-${n}-${i}`).append(
			'<div class="graphCenter"><div class="spinner-border text-primary"></div></div>');
	}
	/*Se guarda parameter en parametros y se le agrega state a parametros que una variable 
	que se encarga de manejar el estado de consultas ,se hace de esta manera para no perder 
	la referenciacion*/
	var parametros = new Object();
	parametros.parameters = parameters;
	//Se resetea el estado a 50
	if(chart.state[parameters.indice] == 50)
	{
		chart.state[parameters.indice] = 0;
	}
	//Se incrementa el estado de consulta
	chart.state[parameters.indice]++;
	parametros.state = chart.state[parameters.indice];

	for(var i = 0; i < parameters.JChartPanel[n].dim; i++){
		parametros.parameters.n_graph = $(`#chartPanel${parameters.indice}-${n}-${i}`).data('position');
		parametros.parameters.idGraph = `chartPanel${parameters.indice}-${n}-${i}`;
		//Verificar legend
		parametros.parameters.flagAdjustAnt[parametros.parameters.n_graph] = 2;
		//consulta data
		query.callAjax(parameters.urlConexion,parameters.metodo,
		{n_graph: parametros.parameters.n_graph,
			n_col:parameters.objConsulta.length,
			nombre:parameters.nombre,
			columns:parameters.objConsulta, 
			order:[{column: 0, dir: 'asc'}],
			search: {value: "", regex: ""},
			replace: parameters.replace},
			chart.callBackGraph,JSON.parse(JSON.stringify(parametros)));
	}
}

//Esta funcion se invoca cada que se aplica un filtro para panels
chart.graphPanel = function(parameters, timeOut){
	var n_graph;
	var n = parameters.nPanel;
	if(timeOut == undefined){
		//Spinner
		$(`#${parameters.indice}graficaIdPanel`).empty();
		$(`#${parameters.indice}graficaIdPanel`).append(parameters.JChartPanel[n].html);
		for(var i = 0; i < parameters.JChartPanel[n].dim; i++){
			$(`#chartPanel${parameters.indice}-${n}-${i}`).append(
				'<div class="graphCenter"><div class="spinner-border text-primary"></div></div>');
		}
	}
	/*Se guarda parameter en parametros y se le agrega state a parametros que una variable 
	que se encarga de manejar el estado de consultas ,se hace de esta manera para no perder 
	la referenciacion*/
	var parametros = new Object();
	parametros.parameters = parameters;
	//Se resetea el estado a 50
	if(chart.state[parameters.indice] == 50)
	{
		chart.state[parameters.indice] = 0;
	}
	//Se incrementa el estado de consulta
	chart.state[parameters.indice]++;
	parametros.state = chart.state[parameters.indice];

	for(var i = 0; i < parameters.JChartPanel[n].dim; i++){
		//consulta data
		parametros.parameters.n_graph = $(`#chartPanel${parameters.indice}-${n}-${i}`).data('position');
		parametros.parameters.idGraph = `chartPanel${parameters.indice}-${n}-${i}`;
		//Se hace una copia del objeto
		query.callAjax(parameters.urlConexion,parameters.metodo,
		{n_graph: parametros.parameters.n_graph,
			n_col:parameters.objConsulta.length,
			nombre:parameters.nombre,
			columns:parameters.objConsulta, 
			order:[{column: 0, dir: 'asc'}],
			search: {value: "", regex: ""},
			replace: parameters.replace},
			chart.callBackGraph,JSON.parse(JSON.stringify(parametros)));
	}
}

//Esta funcion se invoca cada que se aplica un filtro para 1 grafica por link
chart.graph = function(parameters, timeOut){
	if($(`#${parameters.containerGraph}`).length == 1){
		if(timeOut == undefined){
			//Se limpian el div de la grafica y añade la clase del Spinner
			$(`#${parameters.idGraph}`).empty();
			$(`#${parameters.idGraph}`).append(
				'<div class="graphCenter"><div class="spinner-border text-primary"></div></div>');
		}

		/*Se guarda parameter en parametros y se le agrega state a parametros que una variable 
		que se encarga de manejar el estado de consultas ,se hace de esta manera para no perder 
		la referenciacion*/
		var parametros = new Object();
		parametros.parameters = parameters;
		//Se resetea el estado a 50
		if(chart.state[parameters.indice] == 50)
		{
			chart.state[parameters.indice] = 0;
		}
		//Se incrementa el estado
		chart.state[parameters.indice]++;
		parametros.state = chart.state[parameters.indice];

		parameters.key = Math.random();
		parameters.numerCalls = 0;
		parameters.timeInitPeriod = new Date(moment());

		//Se bloquea el navbar hasta que cargue la grafica
		for(var i = 0; i < $(`.${parameters.indice}callGraph`).length; i ++){
			$(`.${parameters.indice}callGraph`).eq(i).addClass("disabled");
		}

		//consulta data
		query.callAjax(parameters.urlConexion,parameters.metodo,
		{n_graph: parameters.n_graph,
			n_col:parameters.objConsulta.length,
			nombre:parameters.nombre,
			columns:parameters.objConsulta, 
			order:[{column: 0, dir: 'asc'}],
			search: {value: "", regex: ""},
			replace: parameters.replace,
			key: parameters.key
		},
			chart.periodGraph,parametros);
	}
}

//Funcion encargada de preguntar si la respuesta esta lista
chart.periodGraph = function(resultado,parametros){
	var parameters = parametros.parameters;
	if( chart.state[parameters.indice] == parametros.state && 
		$(`#${parameters.containerGraph}`).length != 0){ //Verifica que exista el contenedor de la grafica
		if(parameters.numerCalls < 360){ //Se verifica que el numero de llamados no supere los 20 minutos
			//Se verifica si hubo error
			if(resultado[0].error){
				//Se limpia el spinner y se agrega mensaje de error
				var txt = `
					<div class="errorGraph">
					<i class="fas fa-exclamation-triangle"></i>
						<p>Algo ha fallado con esta función y el equipo de soporte ya ha sido notificado 
						para solucionarlo lo más pronto posible. Disculpe las molestias ocasionadas.</p>
					</div>`
				$(`#${parametros.parameters.idGraph}`).empty();
				$(`#${parametros.parameters.idGraph}`).append(txt);
			}else{
				//Revisa si ya cargo debe llamar al servicio de nuevo
				if(!resultado[0].activo){
					var result = JSON.parse(resultado[0].json);
					parameters.numerCalls = 0;
					chart.callBackGraph(result, parametros);
				}else{
					var periodGraph = function(){
						query.callAjax(parameters.urlConexion, "period_graph",
						{id_historic: resultado[0].id},
							chart.periodGraph,parametros);
					}
					parameters.numerCalls++;
					setTimeout(periodGraph, 1000);
				}
			}
		}else{
			//Se nciende la bandera abortar
			query.callAjax(parameters.urlConexion, "abort_graph",
			{id_historic: resultado[0].id},
				function(){});
			//Se limpia el spinner y se agrega mensaje de error
			var txt = `
				<div class="errorGraph">
				<i class="fas fa-exclamation-triangle"></i>
					<p>Su consulta ha excedido el tiempo máximo permitido de procesamiento.</p>
				</div>`
			$(`#${parametros.parameters.idGraph}`).empty();
			$(`#${parametros.parameters.idGraph}`).append(txt);			
		}
	}
	else{
		//Se nciende la bandera abortar
		query.callAjax(parameters.urlConexion, "abort_graph",
		{id_historic: resultado[0].id},
			function(){});
	}
}

//Funcion que indica cuantos hilos estan activos
chart.numberThreads = function(url, callback){
	query.callAjax(
		url, 
		"number_threads",
		0,
		callback);
}

//Recibe los datos y plotea con highcharts (1 link por grafica)
chart.callBackGraph = function(resultado,parametros){
	var htmlSpinner = '<div class="graphCenter"><div class="spinner-border text-primary"></div></div>';
	/*Se restablece parameter a la normalidad y se mira el estado de la consulta 
	(manejo de prioridades solo entra al call back de la ultima peticion echa)*/
	var parameters = parametros.parameters;
	if( chart.state[parameters.indice] == parametros.state && resultado != undefined ){
		//Condicion creada para que no entre si es por set time out y los datos no han cambiado
		if( ( $(`#${parameters.idGraph}`).html() == htmlSpinner || parameters.windowResize  ||
			JSON.stringify(resultado) != JSON.stringify(parameters.resultado) ) )
		{
			var timeEnd = new Date(moment());
			//Habilita filtros o deshabilita permanentemenete
			if( ((timeEnd-parameters.timeInitPeriod)/60000) < 1.5 ){
				$(`.${parameters.indice}iconDownUp`).prop("disabled", false);
				$(`.${parameters.indice}iconDownUp p`).html("Filtros");
			}
			else{
				$(`.${parameters.indice}iconDownUp p`).html("Filtros no disponibles");
			}

			//Se invoca el callback del usuario la primera vez
			if(parameters.firstTime == 1){
				parameters.firstTime = 0;
				parameters.callback(0);
			}

			//Se desbloquea el navbar
			$(".disabled").removeClass("disabled");

			//Se pone en false el resize
			parameters.windowResize = false;
			//Se guardan los datos a plotear
			parameters.resultado = resultado;
			if(resultado.DIM > 0){
				var type = parameters.type[parameters.n_graph];
				var tooltip;
				var advertencia;
				var stack;
				var auxType;
				var invertir;
				var tamaSeries = 100;

				//tooltip (activo por debajo de 20 series)
				if(resultado.DATA.length > tamaSeries){
					tooltip = false;
				}
				else{
					tooltip = true;
				}

				/*Se verifica si se mostraron todas las series, en caso de que falten se muestra la advertencia en 
				highcharts*/
				if(resultado.DIM > tamaSeries){
					advertencia = true;
				}
				else{
					advertencia = false;
				}

				//Se guarda el tipo de grafica original
				auxType = parameters.typeGraph[parameters.n_graph];

				//Si es tipo bar cambiar a column con staking none, tambien se verifica si es inverted o no
				if(parameters.typeGraph[parameters.n_graph] == "bar"){
					parameters.typeGraph[parameters.n_graph] = "column";
					stack = undefined;
					invertir = false;
				}
				else if(parameters.typeGraph[parameters.n_graph] == "barI"){
					parameters.typeGraph[parameters.n_graph] = "column";
					stack = undefined;
					invertir = true;
				}
				else if(parameters.typeGraph[parameters.n_graph] == "columnI"){
					parameters.typeGraph[parameters.n_graph] = "column";
					stack = "normal";
					invertir = true;
				}
				else{
					stack = "normal";
					invertir = false;
				}

				//Formatea el ejeX para las graficas que no sean tipo pie
				if( !(parameters.typeGraph[parameters.n_graph] == "pie") ){
					//Format ejeX
					for(var i = 0; i < resultado.EJEX.length; i++){
						if(type == "date"){
							resultado.EJEX[i] = resultado.EJEX[i].slice(0, 10);
						}
						else if((type == "int") || (type == "float")){
							resultado.EJEX[i] = Intl.NumberFormat("de-DE").format(resultado.EJEX[i]);
						}
					}
				}
				else{//Tipo pie
					//Se termina de organizar el formato que debe tener series: cuando se plotea pie.
					var arrayData = new Array();
					var Jdata = {
						name : parameters.tituloEjeY[parameters.n_graph], 
						data : resultado.DATA
					};
					arrayData[0] = Jdata;
					resultado.DATA = arrayData;
				}

				// Se verifica si el contenedor de las graficas existe para enviar el servicio,
				// si no exite no se graficara nada, se hace porque la consulta toma mucho tiempo
				// y el usuario puede cambiar de vista destruyendo el contenedor
				if($(`#${parameters.containerGraph}`).length == 1){
					if(parameters.JChartPanel == undefined){
						//Esta funcion plotea la grafica y le pone los nombres de titulo, ejes, etc
						chart.line(parameters.idGraph, parameters.titulo[parameters.n_graph], 
							parameters.subtitulo[parameters.n_graph], resultado.EJEX, parameters.tituloEjeX[parameters.n_graph], 
							parameters.tituloEjeY[parameters.n_graph], resultado.DATA, parameters.tituloMenuButton, 
							tooltip, parameters.typeGraph[parameters.n_graph], resultado.SHOW, advertencia, 
							parameters.legend&parameters.legendUser[parameters.n_graph], stack, 
							parameters.mark[parameters.n_graph], parameters.values[parameters.n_graph], invertir, resultado.COLORS);
					}
					else{
						//Esta funcion plotea la grafica y le pone los nombres de titulo, ejes, etc
						chart.line(parameters.idGraph, parameters.titulo[parameters.n_graph], 
							parameters.subtitulo[parameters.n_graph], resultado.EJEX, parameters.tituloEjeX[parameters.n_graph], 
							parameters.tituloEjeY[parameters.n_graph], resultado.DATA, parameters.tituloMenuButton, 
							tooltip, parameters.typeGraph[parameters.n_graph], resultado.SHOW, advertencia, 
							parameters.legend[parameters.n_graph]&parameters.legendUser[parameters.n_graph], stack, 
							parameters.mark[parameters.n_graph], parameters.values[parameters.n_graph], invertir, resultado.COLORS);
					}
				}
				else{
					$(window).off("orientationchange");
				}
				//Se devuelve el tipo de grafica a su estado original
				parameters.typeGraph[parameters.n_graph] = auxType;
				// Se verifica si el contenedor de las graficas existe para enviar el servicio,
				// si no exite no se graficara nada, se hace porque la consulta toma mucho tiempo
				// y el usuario puede cambiar de vista destruyendo el contenedor
				if($(`#${parameters.idGraph}`).length == 1){
					/*verificacion de legends por tamaño de pantalla solo cuando es llamado a traves de un link en el navbar,
					se han cargado las graficas por primera vez y entra solo una vez cuando se han llamado todos los callbacks*/
					countGraphInPanel++;
					if(parameters.reAdjust == 1 && (parameters.JChartPanel[parameters.nPanel].dim == countGraphInPanel) ){
						parameters.reAdjust = 0;
						chart.reAdjustPanel(parameters);
					}
				}
			}
			else{
				//Se limpian el div de la gráfica y añade el mensaje de ausencia de datos
				$(`#${parameters.idGraph}`).empty();
				$(`#${parameters.idGraph}`).append(
					'<div class="graphCenter">Ningún dato disponible en esta grafica</div>');
			}
			//Se invoca la funcion de refresco si existe el contenedor de la grafica
			if( $(`#${parameters.containerGraph}`).length != 0 && parameters.timeOut != undefined){				// Deteniendo el setTimeout
				clearTimeout(parameters.setTimeout);
				//Se invoca nuevamente la funcion
				parameters.setTimeout = setTimeout(parameters.callRefresh, parameters.timeOut);
			}
		}
		else{
			//Se invoca la funcion de refresco si existe el contenedor de la grafica
			if( $(`#${parameters.containerGraph}`).length != 0 && parameters.timeOut != undefined){	

				//Se desbloquea el navbar
				$(".disabled").removeClass("disabled");
				// Deteniendo el setTimeout
				clearTimeout(parameters.setTimeout);
				//Se invoca nuevamente la funcion
				parameters.setTimeout = setTimeout(parameters.callRefresh, parameters.timeOut);
			}
		}
	}
}

/* método drawFilters */
/* Dibuja los selectores y botones por primera vez, luego carga de forma dínamica las 
opciones de los selectores */
/* @param respuesta, recibe los datos que devuelve el AJAX que invoca getFilters 					   */
/* @param parameters, recibe los parametros necesarios para realizar las diferentes acciones		   */
/* -------------- Dibujar y cargar selectores en thead>tr.filter ------------------ */
chart.drawFilters = function(columns, highcharts, parameters) {
	//Para verificar si los selectores existen o no
	var $arrTh = $(`#${parameters.idDivSelectores} div>div.opciones`); 
	var cut;
	var j = 0;
	var dimColumn = 1;
	var divCol = 12;
	var append = new Array(columns.length).fill(1);

	//Se busca el bool en highcharts que indica la aparicion o no en filtros y se guarda en append
	for(var i = 0; i < columns.length; i++)
	{
		for(var j = 0; j < highcharts.length; j++){
			if(columns[i].data == highcharts[j].EJEY){
				if(highcharts[j].append == 0){
					append[i] = 0;
				}
			}
		}
	}

	/* Dibujar y cargar selectores, verifica que el Nº de columnas obtenidas son iguales a las columnas
	 que se pidieron */
	/*	Dibuje los selectores una vez, no se redibujan	*/
	j = 0;
	var txt = "";
	if($arrTh.length == 0)
	{
		parameters.columns = columns;
		for(var i = 0; i < columns.length; i++)
		{
			if( append[i] == 1 && columns[i].data.substring(0, 10) != "%#%SHOW%#%"){
				//Se plotean los selectores vacios para cada columna
				if( (j > 0) && (j%dimColumn == 0) ){
					txt = `${txt}
					</div>`;
				}
				/*section, bloques*/
				if(j%(dimColumn*2) == 0 && j > 0){
					txt = `${txt}</section>`;
				}
				if(j%(dimColumn*2) == 0){
					txt = `${txt}<section class="section">`;
				}
				/**/
				if(j%dimColumn == 0){
					txt = `${txt}
					<div class='row'>`;
				}
				//Selectores
				txt = `${txt}
						<div class='col-${divCol} graficaSelectorPadding'>
							<div class='${parameters.indice}opciones'  data-col='${i}' data-toggle="tooltip" 
								title="${columns[i].data}">
									<button class="btn btn-light btn-sm" data-for="${i}">
										<small>
											<i class="fas fa-times text-danger"></i>
											<i class="fas fa-filter"></i>
										</small>
									</button>
									<button class="btn btn-light btn-sm ${parameters.id_graph}" 
										data-toggle="modal" data-class='${parameters.id_graph}'
										data-target="#customFilter${parameters.indice}-${i}" data-col='${i}' 
										data-backdrop="static" data-keyboard="false">
											<i class="fas fa-ellipsis-v"></i>
									</button>
									<select multiple id='${parameters.indice}select_${i}' 
										class='selectpicker ${parameters.id_graph}' 
										data-col='${i}' data-class='${parameters.id_graph}'>
											<option value="chart.initGraph"></option>
									</select>
							</div>
						</div>
				`;

				if((j == columns.length-1) && !( (j > 0) )){
								txt = `${txt}
					</div>
				</section>`;
				}
				j++;
			}
		}
		$(`#${parameters.idDivSelectores}`).append(txt);

		//Se llama la funcion una vez existan los elementos .section
		chart.adjust(parameters);

		for(var i = 0; i < columns.length; i++)
		{
			if( append[i] == 1 && columns[i].data.substring(0, 10) != "%#%SHOW%#%"){
				//Tooltip
				$('[data-toggle="tooltip"]').tooltip({title: `${columns[i].data}`, delay: {show: 500, hide: 100}});

				/* Estilo para selectores */
				gestionBSelect.generic2(`#${parameters.indice}select_${i}`, columns[i].data);
				$(`#${parameters.idDivSelectores} div.${parameters.indice}opciones [data-for]`).hide();
				$(`#${parameters.indice}select_${i}`).on("show.bs.select", {parameters: parameters}, chart.getFilters);
				$(`button[data-target="#customFilter${parameters.indice}-${i}"]`).on("click", {parameters: parameters}, chart.getFilters);

				//Se remueve el tooltip viejo del selector
				$(`[data-id="${parameters.indice}select_${i}"]`).removeAttr("title");
			}
		}
		//Se pone el evento del boton que limpia todos los filtros
		$(`#${parameters.indice}clearBtnFilter`).on("click", {parameters: parameters}, chart.cleanFilters);

		//Pone los eventos a los selectores y botones la primera vez que se dibujan
		chart.filtersReady(parameters);
		chart.drawModal(parameters);
	}
}

/* método filtersReady */
/* Después de dibujados los elementos de los filtros, les asigna los eventos */
chart.filtersReady = function (parameters) {
	//Evento para cuando se selecciona una opción
	$(`#${parameters.idDivSelectores} div>div.${parameters.indice}opciones`).on('changed.bs.select', {parameters:parameters}, chart.searchSel);

	//Evento limpiar filtro por columna
	$(`#${parameters.idDivSelectores} div>div.${parameters.indice}opciones>button[data-for]`).on("click", {parameters: parameters}, chart.cleanFilters);
}

/* método searchSel */
/* Dispara el evento de acuerdo a la opción seleccionada, usando value 			 */
/* @param e, objeto del evento, para extraer la opción seleccionada y la columna */
/* Logica de filtrado */
/* Filtro Columna */
chart.searchSel = function(e) {
	var idCol = parseInt(e.target.getAttribute("data-col"));
	var searchValue = $(e.target).val();
	var searchs;
	var parameters = e.data.parameters;
	
	//Cambia clase de acuerdo a si hay uno una opción seleccionada
	if(searchValue != ""){
		e.target.nextElementSibling.classList.add("btn-primary");
		$(e.currentTarget.firstElementChild).show();
		$(`#${parameters.indice}clearBtnFilter`).prop("disabled", false);
	} 
	else {
		e.target.nextElementSibling.classList.remove("btn-primary");
	}
	var lastSearch = parameters.objConsulta[idCol].search.value;/*SEARCH();*/

	if( lastSearch != "" )
	{
		lastSearch = lastSearch.split("#,");
		lastSearch = lastSearch.filter( s => s.includes("**/"));
		if(!(lastSearch.length == 0))
		{
			searchValue = searchValue.concat(String(lastSearch));
		}
		//Estilo botones
		if(searchValue == "")
		{
			$(e.currentTarget.firstElementChild).hide();
		}
	}

	//Se concatena cada valor con caracteres de separacion
	for(var i = 0; i < searchValue.length-1; i++){
		searchValue[i] = `${searchValue[i]}#`;
	}

	//Se refresca la variable de consulta
	parameters.objConsulta[idCol].search.value = String(searchValue);

	/* Se redibuja la grafica */
	if(parameters.JChartPanel == undefined){
		chart.graph(parameters);
	}
	else{
		chart.graphPanel(parameters);
	}

	//Se remueve el tooltip viejo del selector
	$(`[data-id="${parameters.indice}select_${idCol}"]`).removeAttr("title");

	//Estilo boton limpiar todo
	searchs = 0;
	for(var i = 0; i < parameters.objConsulta.length; i++){
		if(parameters.objConsulta[i].search.value != ""){
			searchs++;
		}
	}

	if(searchs == 0)
	{
		$(`#${parameters.indice}clearBtnFilter`).prop("disabled", true);
	}

	//Se cambia el estado de actualizacion de todos los selectores menos el que esta en uso
	for (var i=0; i<parameters.objConsulta.length; i++)
	{
		if (i == idCol)
		{
			parameters.Fchange[i] = 0;
		} else {
			parameters.Fchange[i] = 1;
		}
	}
	//Se pone el icono o el 1
	if(idCol == undefined){
		var cantidad = 0;
	}
	else{
		var cantidad = $(`#${parameters.indice}select_${idCol}`).val().length;
	}

	if(cantidad > 0){
		$(`button[data-id='${parameters.indice}select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).html(`${cantidad}`);
	}
}

/* método cleanFilters */
/* Limpia los filtros, todas las opciones seleccionadas en los filtros-selectores */
/* y filtros avanzados o el mismo comportamiento por columna */
chart.cleanFilters = function (e) {
	var searchs;
	var $rows;
	var idCol;
	var parameters = e.data.parameters;

	if( e.currentTarget.id == `${parameters.indice}clearBtnFilter` ){
		/*Mejorar, que no haga una consulta por cada columna que tiene filtro, hacer una consulta 
		general que vacie todo*/

		$(`#${parameters.idDivSelectores} .selectpicker`).selectpicker('deselectAll');
		e.currentTarget.disabled = true;
		$(`#${parameters.idDivSelectores} div.${parameters.indice}opciones [data-for]`).hide();
		$(`#${parameters.idDivSelectores} div.${parameters.indice}opciones .btn-primary[data-target]`).removeClass('btn-primary').addClass('btn-light');
		$(`#${parameters.idDivSelectores} div.${parameters.indice}opciones .bootstrap-select .btn-primary`).removeClass('btn-primary');

		searchs = 0;
		for(var i = 0; i < parameters.objConsulta.length; i++){//busquedas de la tabla recorrer
			if(parameters.objConsulta[i].search.value != ""){
				chart.cleanModal(i,e.data.parameters);
				//Se refresca la variable de consulta
				parameters.objConsulta[i].search.value = "";
			}
			parameters.Fchange[i] = 1;
		}
		/* Se redibuja la grafica */
		if(parameters.JChartPanel == undefined){
			chart.graph(parameters);
		}
		else{
			chart.graphPanel(parameters);
	}
	}
	else if( $(e.currentTarget).data("for") != undefined ) 
	{
		var $rows;
		idCol = $(e.currentTarget).data("for");

		// Deseleccionar lo que haya en el selector de filtro
		$(`#${parameters.idDivSelectores} #${parameters.indice}select_${idCol}`).selectpicker('deselectAll');
		//El estilo se quita en el método searchSel
		
		/* Disparar evento de busqueda en DataTable para limpiar modal */
		if(parameters.objConsulta[idCol].search.value != "")
		{
			chart.cleanModal(idCol,e.data.parameters);
		} 
		
		// Logica estilo de los botones
		$(e.currentTarget).hide();
		e.currentTarget.nextElementSibling.classList.remove("btn-primary");
		e.currentTarget.nextElementSibling.classList.add("btn-light");
		$(e.currentTarget).next().next().children("button").removeClass("btn-primary");

		//Se refresca la variable de consulta
		parameters.objConsulta[idCol].search.value = "";

		//busquedas de la tabla recorrer
		searchs = 0;
		for(var i = 0; i < parameters.objConsulta.length; i++){
			if(parameters.objConsulta[i].search.value != ""){
				searchs++;
			}
		}

		if(searchs == 0){
			$(`#${parameters.indice}clearBtnFilter`).prop('disabled', true);
		}

		/* Se redibuja la grafica */
		if(parameters.JChartPanel == undefined){
			chart.graph(parameters);
		}
		else{
			chart.graphPanel(parameters);
		}

		parameters.Fchange[idCol] = 1;
	}
	else 
	{
		//limpiar el modal 
		idCol = e.currentTarget.dataset.col;
		chart.cleanModal(idCol,e.data.parameters);
		$(`#${parameters.idDivSelectores} button[data-target="#customFilter${parameters.indice}-${idCol}"]`).removeClass("btn-primary").addClass("btn-light");
	}
	//Se pone el icono o el 1
	if(idCol == undefined){
		var cantidad = 0;
	}
	else{
		var cantidad = $(`#${parameters.indice}select_${idCol}`).val().length;
	}

	if(cantidad > 0){
		$(`button[data-id='${parameters.indice}select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).html(`${cantidad}`);
	}
}

/* método cleanModal */
/* Limpia un modal, volviendo a su estado inicial y la busqueda de la columna, con cada llamado */
/* @param $table, objeto DataTable con referencia a la tabla */
/* @param idCol, número de columna a borrar */
chart.cleanModal = function(idCol, parameters) {
	var $rows = $(`[data-id="modal-body${parameters.indice}-${idCol}"] .row`);
	var lastSearch;
	//Borrar una sola fila
	if( $rows.length == 1 )
	{
		$rows.find(`select#opt_${idCol}`).prop("selectedIndex", 0);
		$(`#${parameters.idDivSelectores} [data-id='modals${parameters.indice}'] [data-id='selectM_${parameters.indice}-${idCol}']`).selectpicker('val', "")
		$rows.find("input").val("");
	}
	else //Borrar N filas
	{
		
		var btnAdd = $rows.find(`#add_${parameters.indice}-${idCol}`).detach();
		$rows.find(`select#opt_${parameters.indice}-${idCol}`).first().prop("selectedIndex", 0);
		$(`#${parameters.idDivSelectores} [data-id='modals${parameters.indice}'] [data-id='selectM_${parameters.indice}-${idCol}']`).first().selectpicker('val', "");
		$rows.find("input").first().val("");
		$rows.find(`.col-2.text-center`).first().empty().append(btnAdd);

		$rows.slice(1).remove();
	}
	var lastSearch = parameters.objConsulta[idCol].search.value;

	if(lastSearch != "")
	{
		lastSearch = lastSearch.split("#,");
		lastSearch = lastSearch.filter( s => !s.includes("**/"));
		//Se concatena cada valor con caracteres de separacion
		for(var i = 0; i < lastSearch.length-1; i++){
			lastSearch[i] = `${lastSearch[i]}#`;
		}
		//Se refresca la variable consulta
		parameters.objConsulta[idCol].search.value = String(lastSearch);

		/* Se redibuja la grafica */
		if(parameters.JChartPanel == undefined){
			chart.graph(parameters);
		}
		else{
			chart.graphPanel(parameters);
		}

		//Estilos botones
		if(lastSearch.length == 0)
		{
			$(`div.${parameters.indice}opciones button[data-for="${idCol}"]`).click();
		}
	} 
	else 
	{
		//Se refresca la variable consulta
		parameters.objConsulta[idCol].search.value = String(lastSearch);

		/* Se redibuja la grafica */
		if(parameters.JChartPanel == undefined){
			chart.graph(parameters);
		}
		else{
			chart.graphPanel(parameters);
		}
	}
}

/* método drawModal */
/* La primera ejecución recibe los datos de las columnas, nombre y tipo, 
dibujando N modales para cada filtro avanzado, */
/* habilitando los diferentes eventos para cada modal */
chart.drawModal = function (parameters) {
	var titulo;
	var opt = {
		number: [ { label: "Es igual a", value: "=" },
				  { label: "No es igual a", value: "!=" },
				  { label: "Mayor que", value: ">" },
				  { label: "Mayor o igual que", value: ">=" },
				  { label: "Menor que", value: "<"},
				  { label: "Menor o igual que", value: "<=" } ],

		text: [ { label: "Es igual a", value: "=" },
				{ label: "No es igual a", value: "!=" },
				{ label: "Comienza por", value: "LIKE**/'_%'"},
				{ label: "Termina con", value: "LIKE**/'%_'"},
				{ label: "Contiene", value: "LIKE**/'%_%'"},
				{ label: "No contiene", value: "NOT**/LIKE**/'%_%'"} ]
	}

	$(`#graphCollapseOne${parameters.indice}`).remove(`[data-id='modals${parameters.indice}']`).append(`<div data-id='modals${parameters.indice}'></div>`);
	$(`#graphCollapseOne${parameters.indice} [data-id='modals${parameters.indice}']`).empty();

	for (var i = 0; i < parameters.columns.length; i++)
	{
		if( !(parameters.columns[i].data.indexOf('%#%FALSE%#%') != -1) )
		{
			//Se organiza el nombre de los selectores
			titulo = parameters.columns[i].data;
			cut = titulo.lastIndexOf("%#%");
			if (!(cut == -1)) {
				titulo = titulo.slice(cut+3);
			}
			var hModal = `
			  <div class="modal" id="customFilter${parameters.indice}-${i}">
			    <div class="modal-dialog">
			      <div class="modal-content">
			      
			        
			        <div class="modal-header">
			          <h5 class="modal-title" data-id="modal-title">Mostrar las filas en las cuales: <span class="form-check">${titulo}</span></h5>
			          <button type="button" class="close" data-dismiss="modal" data-id="close_${parameters.indice}-${i}">&times;</button>
			        </div>
			        
			        <div class="modal-body" data-id="modal-body${parameters.indice}-${i}">
			    		<div class="row">
			    			<div class="col-4 px-2">
				        		<select class="form-control" id="opt_${parameters.indice}-${i}"></select>
				        	</div>
				        	<div class="col-6 input-group">
				        			<input class="form-control" id="data_${parameters.indice}-${i}" />
			        		</div>
							<div class="col-2 text-center">
								<a class="cursor_sel" id="add_${parameters.indice}-${i}"><i class="fas fa-plus-circle text-success"></i></a>
							</div>
			        	</div>
			        </div>
			        
			        <div class="modal-footer" data-id="modal-footer">
			          <div class="col-6">
			          	<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" data-col=${i} id="cleanF_${parameters.indice}-${i}">Limpiar Filtro</button>
			          </div>
			          <div class="col-6 text-right">
			          	<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id="accept_${parameters.indice}-${i}">Aplicar</button>
			          </div>
			        </div>
			        
			      </div>
			    </div>
			  </div>`;
		}
		$(`#graphCollapseOne${parameters.indice} [data-id='modals${parameters.indice}']`).append(hModal);

		//Instrucciones dependiendo el tipo de dato
		if(parameters.columns[i].type == "int" || parameters.columns[i].type == "float")
		{
			for( var j in opt.number )
			{
				$(`#customFilter${parameters.indice}-${i} #opt_${parameters.indice}-${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
			}
		}
		else if(parameters.columns[i].type == "varchar" || parameters.columns[i].type == "nvarchar")
		{
			for( var j in opt.text )
			{
				$(`#customFilter${parameters.indice}-${i} #opt_${parameters.indice}-${i}`).append(`<option value="${opt.text[j].value}">${opt.text[j].label}</option>`);
			}
		}
		else if(parameters.columns[i].type == "date")
		{
			//DateRangePicker en el input
			$(".graficaStyleCss .daterangepicker").remove();
			gestionDRP.simpleDRP3($(`[data-id="modals${parameters.indice}"] #data_${parameters.indice}-${i}`), "YYYY-MM-DD");
			$(`[data-id="modals${parameters.indice}"] #data_${parameters.indice}-${i}`).val("");
			for( var j in opt.number )
			{
				$(`#customFilter${parameters.indice}-${i} #opt_${parameters.indice}-${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
			}
		}
		else if(parameters.columns[i].type.includes("datetime"))
		{
			//DateRangePicker en el input
			$(".graficaStyleCss .daterangepicker").remove();
			gestionDRP.simpleDRP3($(`[data-id="modals${parameters.indice}"] #data_${parameters.indice}-${i}`), "YYYY-MM-DD HH:mm:ss");
			$(`[data-id="modals${parameters.indice}"] #data_${parameters.indice}-${i}`).val("");
			for( var j in opt.number )
			{
				$(`#customFilter${parameters.indice}-${i} #opt_${parameters.indice}-${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
			}
		}
		else if(parameters.columns[i].type.includes("time"))
		{
			//DateRangePicker en el input
			$(`.graficaStyleCss .daterangepicker`).remove();
			gestionDRP.simpleDRP3($(`[data-id="modals"] #data_${i}`), "HH:mm:ss");
			$(`[data-id="modals${parameters.indice}"] #data_${parameters.indice}-${i}`).val("");
			for( var j in opt.number )
			{
				$(`#customFilter${parameters.indice}-${i} #opt_${parameters.indice}-${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
			}
		}

		//Eventos para botones y teclas
		$(`#customFilter${parameters.indice}-${i} #add_${parameters.indice}-${i}`).on('click', {columns: parameters.columns}, chart.addCustomFilter);

		$(`#customFilter${parameters.indice}-${i} #accept_${parameters.indice}-${i}`).on('click', {parameters: parameters}, chart.searchCustom);

		$(`#customFilter${parameters.indice}-${i} #cleanF_${parameters.indice}-${i}`).on('click', {parameters: parameters}, chart.cleanFilters);

		$(`#customFilter${parameters.indice}-${i}`).on('keyup', chart.keyupEv);

		$(`#customFilter${parameters.indice}-${i} [data-id="close_${parameters.indice}-${i}"]`).on('click', chart.closeModal);
	}
}

/* método openModal */
/* Callback cuando se presiona el botón que muestra el modal */
/* redibuja selectores y guarda la vista inicial */
chart.openModal = function(parameters) {
	//Siguientes llamados después de inicialización
	//var idCol = parseInt(e.currentTarget.getAttribute("data-col"));

	var idCol = parameters.idCol;
	var $modalBody = $(`body [data-id="modal-body${parameters.indice}-${idCol}"]`);
	var $select = $(`#${parameters.idDivSelectores} #${parameters.indice}select_${idCol}`);
	var $selectClone = $select.clone();
	var $selectM = `<select data-id="selectM_${parameters.indice}-${idCol}" class="selectpicker"></select>`;
	var columns = parameters.columns;
	var $selects;

	/*Borra el BS Selector y pone un selector normal, además de borra las opcs seleccionadas 
	al momento de clonar*/
	$selectClone.children().prop("selected",false);
	$modalBody.children().children(".col-6").find(".dropdown.bootstrap-select").remove();
	$modalBody.children().children(".col-6").remove('select').append($selectM);
	$modalBody.children().children(".col-6").find('select').append( $selectClone.children() );

	$selectM = $modalBody.children().children(".col-6").find("select");

	//Aplica esto con BS Select y evento
	gestionBSelect.wrapSelectDTM2($selectM,`modals${parameters.indice}`);
	$($selectM).off('changed.bs.select').on('changed.bs.select', chart.changeSelM);
	$(`#customFilter${parameters.indice}-${idCol} #add_${parameters.indice}-${idCol}`).off('click').on('click', 
		{parameters: parameters}, chart.addCustomFilter);
	$(`#customFilter${parameters.indice}-${idCol} .fas.fa-minus-circle.text-danger`).parent().off("click").on("click", chart.deleteCustomFilter);

	if(parameters.op == "DENIED"){
		$($selectM).next().prop("disabled", true);
		parameters.Fchange[idCol] = 1;
	}
	//Para fechas
	if(columns[idCol].type == "date")
	{
		$(".graficaStyleCss .daterangepicker").remove();
		gestionDRP.simpleDRP3($modalBody.find(`input#data_${parameters.indice}-${idCol}`), "YYYY-MM-DD");
	} 
	else if(columns[idCol].type.includes("datetime")) 
	{
		$(".graficaStyleCss .daterangepicker").remove();
		gestionDRP.simpleDRP3($modalBody.find(`input#data_${parameters.indice}-${idCol}`), "YYYY-MM-DD HH:mm:ss");
	}
	else if(parameters.columns[i].type.includes("time"))
	{
		//DateRangePicker en el input
		$(`.graficaStyleCss .daterangepicker`).remove();
		gestionDRP.simpleDRP3($modalBody.find(`input#data_${parameters.indice}-${idCol}`), "HH:mm:ss");
	}

	//Estado inicial
	chart.$initModalBody = $modalBody.clone();

	//Poner los selectores de la copia en las mismas opciones
	$selects = chart.$initModalBody.find(`select#opt_${parameters.indice}-${idCol}`);
	
	for (var i=0; i<$selects.length; i++)
	{
		$selects[i].value = $modalBody.find(`select#opt_${parameters.indice}-${idCol}`)[i].value;
	}
	gestionModal.cerrar();
}

/* método closeModal */
/* Callback cuando se cierra el modal con tecla esc o con click en X, para regresarlo a su estado anterior */
chart.closeModal = function(e) {

	var $closeModalBody;
	if( e.type == 'click')
	{
		$closeModalBody = $(e.currentTarget).parent().next();
		$closeModalBody.click();
	}
	else if( e.type = 'keyup')
	{
		$closeModalBody = $(e.currentTarget).find(".modal-body");
	}

	//Devolver al estado inicial, revisar eventos de los botones agregar y eliminar, y revisar selector
	$closeModalBody.empty().append(chart.$initModalBody.children());
}

/* método keyupEv */
/* Callback evento keyup en el modal, key: "esc"(27) cierra el modal, key: "enter"(13) aplica el filtro */
chart.keyupEv = function(e) {
	var keyP = e.which;
	switch(keyP) {
		case 13:
			$(e.currentTarget).find("button.btn-primary").last().click();
		break;

		case 27:
			$(e.currentTarget).modal("hide");
			gestionDT.closeModal(e);
		break;

		default:
		//Nothing to do
	}
}

/* método changeSelM */
/* Evento que dispara el cambio de un selector en el modal, el cual escribe dentro del input de esa fila */
chart.changeSelM = function(e) {
	var $select = $(e.target);
	var selectValue = $select.val();
	//esto cambia con imask
	$select.parent().prev().val(selectValue); 
}

/* método addCustomFilter */
/* Crea una fila en el modal del filtro avanzado para agregar otra opción de filtrado */
chart.addCustomFilter = function(e) {
	var parameters = e.data.parameters;
	var columns = parameters.columns;
	var idCol = parseInt(e.currentTarget.id.split("-")[1]);
	var $modalBody = $(e.currentTarget).parent().parent().parent();
	var $row = $(e.currentTarget).parent().parent();
	var $btnAdd = $(e.currentTarget).detach();
	var $rowClone = $row.clone();
	var $select = $rowClone.find('select.selectpicker').detach();

	$modalBody.append(`<div class="row">
								<div class="col-12 form-inline py-2">
						        	<div class="form-check px-1">
									  <label class="form-check-label">
									    <input type="radio" class="form-check-input" name="optfilter${parseInt(e.timeStamp)}" value="AND" checked>Y
									  </label>
									</div>
									<div class="form-check px-1">
									  <label class="form-check-label">
									    <input type="radio" class="form-check-input" name="optfilter${parseInt(e.timeStamp)}" value="OR">O
									  </label>
									</div>
								</div>
						</div>`);

	$rowClone.children(".col-2").empty()
								.append(`<a class="cursor_sel" id="deleteBtnModal"><i class="fas fa-minus-circle text-danger"></i></a>`)
								.append($btnAdd)
								.children("#deleteBtnModal").on("click", chart.deleteCustomFilter);

	$rowClone.find("div.dropdown").remove();
	$select[0][0].remove();
	$rowClone.children(".col-6").append($select);
	$rowClone.find('input').val("");

	if(columns[idCol].type == "date")
	{
		gestionDRP.simpleDRP3($rowClone.find('input'), "YYYY-MM-DD");
		$rowClone.find('input').val("");
	} 
	else if(columns[idCol].type.includes("datetime")) 
	{
		gestionDRP.simpleDRP3($rowClone.find('input'), "YYYY-MM-DD HH:mm:ss");
		$rowClone.find('input').val("");
	}

	$modalBody.append($rowClone);

	gestionBSelect.wrapSelectDTM2($rowClone.find('select.selectpicker'),`modals${parameters.indice}`);
	$rowClone.find('select.selectpicker').on('changed.bs.select', chart.changeSelM);

	if( $row.prev().length == 0 && $row.children(".col-2").children().length == 0 )
	{
		$row.children(".col-2").append(`<a class="cursor_sel" id="deleteBtnModal"><i class="fas fa-minus-circle text-danger"></i></a>`)
							   .children("#deleteBtnModal").on("click", chart.deleteCustomFilter);
	}
}

/* método deleteCustomFilter */
/* Elimina una fila en el modal del filtro avanzado */
chart.deleteCustomFilter = function(e) {
	
	var $row = $(e.currentTarget).parent().parent();
	var $btnAdd = $(e.currentTarget).next().detach();

	if( $row.prev().length == 0 )
	{
		$row.next().remove();
		if( $row.next().next().length == 0 && $row.prev().length == 0 ){
			$btnAdd = $row.next().children(".col-2").children().last().detach()
			$row.next().children(".col-2").empty();
			$row.next().children(".col-2").append($btnAdd);
		}
		$row.remove();
	} else {
		$row.prev().remove();
		if( $row.prev().prev().length == 0 && $row.next().length == 0 ){
			$row.prev().children(".col-2").empty();
		}
		$row.prev().children(".col-2").append($btnAdd);
		$row.remove();
	}
}

/* método searchCustom */
/* Esta función es el llamado de un evento click sobre el boton aceptar del modal de filtro avanzado, 		*/
/* recoge los datos del formulario y luego llama al método de buscar de datatable para que haga la consulta */
chart.searchCustom = function(e) {
	var parameters = e.data.parameters;
	var idCol = parseInt(e.currentTarget.id.split("-")[1]);
	var $rows = $(`[data-id="modals${parameters.indice}"] [data-id="modal-body${parameters.indice}-${idCol}"] .row`);
	var searchVals = new Array();
	chart.$initModalBody = "";

	if( $rows.length == 1)
	{
		//Una sola fila
		var op;
		var val;
		op = $($rows[0].children[0]).find("select").val();
		val = $($rows[0].children[1]).find("input").val();
		if( op.includes("LIKE") )
		{
			op = op.replace("_", val);
			searchVals.push(`**/data**/${op}**/`);
		} else {
			searchVals.push(`**/data**/${op}**/'${val}'**/`);
		}

	} 
	else {
		var op;
		var val;
		var opl;
		var searchVal;
		//Varias filas
		//Primer elemento
		op = $($rows[0].children[0]).find("select").val();
		val = $($rows[0].children[1]).find("input").val();
		if( op.includes("LIKE") )
		{
			op = op.replace("_", val);
			searchVal = `**/data**/${op}**/`;
		} else {
			searchVal = `**/data**/${op}**/'${val}'**/`;
		}
		searchVals.push(searchVal);

		//Resto de elementos
		for(var i=2; i<$rows.length; i+=2)
		{
			op = $($rows[i].children[0]).find("select").val();
			val = $($rows[i].children[1]).find("input").val();
			opl = $($rows[i-1].children).find("input:checked").val()
			if( op.includes("LIKE") )
			{
				op = op.replace("_", val);
				searchVal = `**/${opl}**/data**/${op}**/`;
			} else {
				searchVal = `**/${opl}**/data**/${op}**/'${val}'**/`;
			}
			searchVals.push(searchVal);
		}
	}
	var lastSearch = parameters.objConsulta[idCol].search.value;

	//Concatenar busqueda
	if(lastSearch != "")
	{
		lastSearch = lastSearch.split("#,");
		lastSearch = lastSearch.filter( s => !s.includes("**/"));
		searchVals = searchVals.concat(lastSearch);
		//Se concatena cada valor con caracteres de separacion
		for(var i = 0; i < searchVals.length-1; i++){
			searchVals[i] = `${searchVals[i]}#`;
		}
		//Se refresca la variable consulta
		parameters.objConsulta[idCol].search.value = String(searchVals);
	}
	else{
		//Se refresca la variable consulta
		parameters.objConsulta[idCol].search.value = String(searchVals);
	}

	/* Se redibuja la grafica */
	if(parameters.JChartPanel == undefined){
		chart.graph(parameters);
	}
	else{
		chart.graphPanel(parameters);
	}

	$(`#${parameters.indice}clearBtnFilter`).prop("disabled", false);
	$(`.${parameters.indice}opciones [data-for='${idCol}']`).show().next().removeClass("btn-light").addClass("btn-primary");

	//Se cambia el estado de actualizacion de todos los selectores menos el que esta en uso
	for (var i=0; i<parameters.objConsulta.length; i++)
	{
		if (i == idCol)
		{
			parameters.Fchange[i] = 0;
		} else {
			parameters.Fchange[i] = 1;
		}
	}
}

/* método getFilters */
/* Ejecuta la consulta para traer los campos de los selectores de filtros 		 */
/* @param parameters, recibe los parametros necesarios para ejecutar la consulta */
/*  				  en la inicialización desde receiveInfo y cuando se realiza */
/*					  una busqueda desde el método filterSel 					 */
chart.getFilters = function(e) {
	/* Ejecuta la consulta para cargar los selectores, de acuerdo a los nombre recibidos */
	var idCol;
	if(e.type=="click")
	{
		idCol = $(e.currentTarget).data("col");
	}
	else
	{
		idCol = $(this)[0].id.split("_").slice(1);
	}

	var parameters = e.data.parameters;
	parameters.idCol = idCol;
	parameters.eType = e.type;

	if( parameters.Fchange[idCol] == 1)
	{
		var col_filter = parameters.metodo+'_column_filters';
		var data = { col_name: JSON.stringify(parameters.columns.map( c => c.data )),
					 cols: JSON.stringify(parameters.objConsulta),
					 col: parameters.columns[idCol].data,
					 nombre: parameters.nombre,
					 replace: parameters.replace
			};
		gestionModal.alertaBloqueante("Procesando");
		query.callAjax( parameters.urlConexion, col_filter, data, chart.loadFilters, parameters );
		parameters.Fchange[idCol] = 0;
	} 
	else {
		if(e.type == "click")
		{
			chart.openModal(parameters);
		}
	}
}

/* método loadFilters */
/* carga los datos de los filtros que se obtienen de la consulta */
chart.loadFilters = function(opts, parameters){	
	/* Cargue los selectores con los datos, la carga se hace en cada busqueda */
	var values = new Array();//Extrae las opciones seleccionadas para redibujarlas de este modo
	var idCol = parameters.idCol;
	var col_name = parameters.columns[idCol].data;
	var type = parameters.columns[idCol].type;

	// Primera carga
	if(parameters.cols == "" && opts.length != 0 && opts[0][col_name] != "DENIED")
	{
		
		/*Carga cuando se empieza a buscar, se verifica si esta o no seleccionada la opción para 
		mostrarla asi de nuevo*/
		values[idCol] = parameters.objConsulta[idCol].search.value.split("#,");
		$(`#${parameters.indice}select_${idCol}`).empty();
		
		if( opts.length > 0)
		{
			for(var j = 0; j < opts.length; j++)
			{
				var opt;
				
				//Renderizado de date y datetime
				if(type == "date"){
					opts[j][col_name] = chart.renderDate(opts[j][col_name]);
				} else if (type.includes("datetime")){
					opts[j][col_name] = chart.renderDateTime(opts[j][col_name]);
				}

				//Para volver a poner como seleccionado
				if( $.inArray(`${opts[j][col_name]}`, values[idCol]) > -1)
				{
					opt = `<option value='${opts[j][col_name]}' title=' ' selected>${opts[j][col_name]}</option>`;
				} else {
					opt = `<option value='${opts[j][col_name]}' title=' '>${opts[j][col_name]}</option>`;
				}

				$(`#${parameters.indice}select_${idCol}`).append(opt);
			}
		} else {
			
			var opt = "<option value='0' disabled></option>"
			$(`#${parameters.indice}select_${idCol}`).append(opt);
		}
		//Refresca el selector por medio de la API de selectpicker
		$(`#${parameters.indice}select_${idCol}`).selectpicker('refresh');

		if(parameters.eType == "click")
		{
			parameters.op = "";
			chart.openModal(parameters);
		}
		else
		{
			//Cierra modal
			gestionModal.cerrar();
		}
	} 
	else
	{
		//Cierra modal
		gestionModal.cerrar(); 
		$(`#${parameters.indice}select_${idCol}`).empty().append("<option></option>");
		$(`#${parameters.indice}select_${idCol}`).selectpicker('refresh');
		if(parameters.eType == "click")
		{
			parameters.op = "DENIED";
			chart.openModal(parameters);
		}
		else
		{
			var label;
			if(opts.length == 0)
			{
				label = "Este filtro no tiene opciones disponibles";
			} else {
				label = "Este filtro no se encuentra disponible por el número de opciones, sólo se puede usar el filtro avanzado"
			}
			setTimeout(chart.delayConfirm, 2, label);
			$(`#${parameters.indice}select_${idCol}`).selectpicker('close');
			parameters.Fchange[idCol] = 1;
		}
	}
	//Se pone el icono o el 1
	if(idCol == undefined){
		var cantidad = 0;
	}
	else{
		var cantidad = $(`#${parameters.indice}select_${idCol}`).val().length;
	}

	if(cantidad > 0){
		$(`button[data-id='${parameters.indice}select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).append(`${cantidad}`);
	}
}

/* método delayConfirm */
/* delay para la alerta de confirmación, por algunos problemas en los tiempos con sweetalert se usa un delay */
chart.delayConfirm = function(label) {
	gestionModal.alertaConfirmacion("",label,"info","OK","#007bff",function(Continuar){});
}

/* método renderDate */
/* Renderiza el tipo de dato date, para que solo se muestre la fecha */
chart.renderDate = function(data, t, r) {
	data = moment(data).format("YYYY-MM-DD");
	return data;
}

/* método renderDateTime */
/* Renderiza el tipo de dato datetime*, para se muestre la fecha y hora */
chart.renderDateTime = function(data, t, r) {
	data = moment(data).format("YYYY-MM-DD HH:mm:ss");
	return data;
}

/* método renderNumber */
/* Renderiza el tipo de dato int o float, con separadores de miles */
chart.renderNumber = function(data, t, r) {
	data = Intl.NumberFormat("de-DE").format(data);
	return data;
}

/* método deleteGraph */
/* limpia el div perteneciente a la grafica */
chart.deleteGraph = function(id_graph) {
	//Se vacia el div de la grafica para redibujarse
	$(`#${id_graph}`).empty();
}