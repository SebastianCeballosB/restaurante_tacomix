
var gestionModal = new Object();

gestionModal.alertaBloqueante = function(titulo,contenido){
	swal({
	  title: titulo,
	  allowOutsideClick: false,
	  text: contenido,
	  onBeforeOpen: () => {
	    swal.showLoading();
	  }
	});
}

gestionModal.cerrar = function(){
	swal.close();
}

gestionModal.alertaOpciones = function(titulo,contenido,tipo,callbackContinuar,callbackCancelarAccion){
	swal({
	  title: titulo,
	  allowOutsideClick: false,
	  text: contenido,
	  type: tipo,
	  showCancelButton: true,
	  allowOutsideClick: false,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Si',
	  cancelButtonText:'No'
	}).then((result) => {
	  if (result.value) {
	  	callbackContinuar();
	  }else{
	  	callbackCancelarAccion();
	  }
	});
}

gestionModal.alertaConfirmacion = function(titulo,contenido,tipo,textoBoton,colorBoton,callbackContinuar){
	/*tipo : warning, error, success, info, question*/	
	swal({
	  title: titulo,
	  text: contenido,
	  type: tipo,
	  allowOutsideClick: false,
	  showCancelButton: false,
	  confirmButtonColor: colorBoton,	  
	  confirmButtonText: textoBoton,	  
	}).then((result) => {
	  if (result.value) {
	  	callbackContinuar();
	  }
	});
}

gestionModal.confirmacionSinBoton = function(tipo,titulo){
	swal({
		position: 'center',
		type: tipo,
		title: titulo,
		showConfirmButton: false,
		timer: 1000
	});
}

gestionModal.alertaParaUsuario = function(titulo,texto,tipo,textoBotonNo,textoBotonSi,callbackContinuar){
swal({
 title: titulo,
 text: texto,
 type: tipo,
 showCancelButton: true,
 cancelButtonText: textoBotonNo,
 confirmButtonColor: '#3085d6',
 cancelButtonColor: '#d33',
 confirmButtonText: textoBotonSi
}).then((result) => {
 if (result.value) {
 	callbackContinuar();
 }
})
}


gestionModal.alertaDeleteTagResumen = function(titulo,texto,tipo,textoBotonNo,textoBotonSi,data,callbackContinuar){
	swal({
		title: titulo,
		text: texto,
		type: tipo,
		showCancelButton: true,
		cancelButtonText: textoBotonNo,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: textoBotonSi
	}).then((result) => {
 		if (result.value) {
 			callbackContinuar(data);
 		}
	})
}

gestionModal.alertaConfirmacionHTML = function(titulo,contenido,tipo,textoBoton,colorBoton,callbackContinuar){
	/*tipo : warning, error, success, info, question*/	
	swal({
	  title: titulo,
	  html: contenido,
	  type: tipo,
	  allowOutsideClick: false,
	  showCancelButton: false,
	  confirmButtonColor: colorBoton,	  
	  confirmButtonText: textoBoton,	  
	}).then((result) => {
	  if (result.value) {
	  	callbackContinuar();
	  }
	});
}
