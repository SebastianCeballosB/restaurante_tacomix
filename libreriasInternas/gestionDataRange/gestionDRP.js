var gestionDRP = new Object();

gestionDRP.simpleDRP = function ($input, format) {
	var timePicker = format.includes("HH:mm:ss") ? true : false;

	var config = {
	    singleDatePicker: true,
	    showDropdowns: true,
	    minYear: 1901,
	    showWeekNumbers: true,
		showISOWeekNumbers: true,
		timePicker: timePicker,
		timePicker24Hour: true,
		timePickerSeconds: true,
		parentEl: "#divtabla",
      	locale: {
      		format: format,
      		applyLabel: "Aplicar",
	        cancelLabel: "Cancelar",
        	weekLabel: "S",
	        daysOfWeek: [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        monthNames: [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ]
      	}
  	}
	$input.daterangepicker(config);

	if(format == "HH:mm:ss")
	{
		$(`#divtabla .daterangepicker .calendar-table`).css("display","none");
	}

}

gestionDRP.simpleDRP2 = function ($input, format) {
	var timePicker = format.includes("HH:mm:ss") ? true : false;

	var config = {
	    singleDatePicker: true,
	    showDropdowns: true,
	    minYear: 1901,
	    showWeekNumbers: true,
		showISOWeekNumbers: true,
		timePicker: timePicker,
		timePicker24Hour: true,
		timePickerSeconds: true,
		parentEl: "#contGanttH",
      	locale: {
      		format: format,
      		applyLabel: "Aplicar",
	        cancelLabel: "Cancelar",
        	weekLabel: "S",
	        daysOfWeek: [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        monthNames: [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ]
      	}
  	}
	$input.daterangepicker(config);

	if(format == "HH:mm:ss")
	{
		$(`#contGanttH .daterangepicker .calendar-table`).css("display","none");
	}
}

gestionDRP.simpleDRP3 = function ($input, format) {
	var timePicker = format.includes("HH:mm:ss") ? true : false;

	var config = {
	    singleDatePicker: true,
	    showDropdowns: true,
	    minYear: 1901,
	    showWeekNumbers: true,
		showISOWeekNumbers: true,
		timePicker: timePicker,
		timePicker24Hour: true,
		timePickerSeconds: true,
		parentEl: "#contGanttH",
      	locale: {
      		format: format,
      		applyLabel: "Aplicar",
	        cancelLabel: "Cancelar",
        	weekLabel: "S",
	        daysOfWeek: [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        monthNames: [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ]
      	}
  	}
	$input.daterangepicker(config);

	if(format == "HH:mm:ss")
	{
		$(`.graficaStyleCss .daterangepicker .calendar-table`).css("display","none");
	}

}

gestionDRP.FechasSimple = function(tagDiv){

	var start = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
    	showDropdowns: true,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
    	alwaysShowCalendars: true,
    }

    function cb(start) {
        $(`#${tagDiv} span`).html(start.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start);
}

gestionDRP.FechaHoraSimple = function(tagDiv){

	var start = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
        timePicker24Hour: true,    	
    	timePicker: true,
    	timePickerSeconds: true,
    	showDropdowns: true,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
    	alwaysShowCalendars: true,
    }

    function cb(start) {
        $(`#${tagDiv} span`).html(start.format('YYYY-MM-DD H:mm:ss'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start);
}

gestionDRP.HoraSimple = function(tagDiv){

	var start = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerIncrement: 1,
        timePickerSeconds: true,
    	showDropdowns: true,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
    	alwaysShowCalendars: true,
    }
	
	$(`#${tagDiv}`).on('show.daterangepicker', function (ev, picker) {
            picker.container.find(".calendar-table").hide();
            picker.container.find(".drp-selected").hide();
    });

    function cb(start) {
        $(`#${tagDiv} span`).html(start.format('H:mm:ss'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start);
}

gestionDRP.HoraSinSegundos = function(tagDiv){

	var start = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerIncrement: 1,
        timePickerSeconds: false,
    	showDropdowns: true,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
    	alwaysShowCalendars: true,
    }
	
	$(`#${tagDiv}`).on('show.daterangepicker', function (ev, picker) {
            picker.container.find(".calendar-table").hide();
            picker.container.find(".drp-selected").hide();
    });

    function cb(start) {
        $(`#${tagDiv} span`).html(start.format('H:mm'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start);
}

/******************KODA y PERU*************************************/ 

var gestionDateRangePicker = new Object();

gestionDateRangePicker.Fechas = function(tagDiv){

	var start = moment().subtract(29, 'days');
 	var end = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	showDropdowns: true,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
        ranges: {
	        'Hoy'				: [moment(), moment()],
	        'Ayer'				: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Últimos 7 Días'	: [moment().subtract(6, 'days'), moment()],
	        'Últimos 30 Días'	: [moment().subtract(29, 'days'), moment()],
	        'Este Mes'			: [moment().startOf('month'), moment().endOf('month')],
	        'Último Mes'		: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    	},
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
        $(`#${tagDiv} span`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDateRangePicker.FechasPeruOP = function(tagDiv,nombreWeb,ini,fin){
	var start = moment(new Date(ini));
	var end = moment(new Date(fin));

    // Se configura las opciones para configurar el idioma 
    var option = {
    	showDropdowns: true,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
        ranges: {
	        'Hoy'				: [moment(), moment()],
	        'Ayer'				: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Últimos 7 Días'	: [moment().subtract(6, 'days'), moment()],
	        'Últimos 30 Días'	: [moment().subtract(29, 'days'), moment()],
	        'Este Mes'			: [moment().startOf('month'), moment().endOf('month')],
	        'Último Mes'		: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    	},
    	alwaysShowCalendars: true,
    }

    function cb(start, end, firstTime) {
        $(`#${tagDiv} span`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        //Registra el cambio en base de datos cuando hay un cambio
	    var data = {
	    	nombreWeb: nombreWeb,
			fechaIni: start.format('YYYY-MM-DD'),
			fechaFin: end.format('YYYY-MM-DD')
	    };
	    query.callAjax(
	        Ordenes_Previsionales.urlConexionVista,
	        "change_date",
	        data,
	        function(){});
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end, "first_time");
}

gestionDateRangePicker.FechasLimited = function(tagDiv, maxDay){

	var start = moment().subtract(0, 'days');
 	var end = moment().subtract(-maxDay+1, 'days');

    // Se configura las opciones para configurar el idioma 
    var option = {
    	minDate: moment().subtract(0, 'days'),
    	maxDate: moment().subtract(-maxDay+1, 'days'),
    	showDropdowns: true,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
    	balance.fechaIni = start.format('YYYY-MM-DD');
		balance.fechaFin = end.format('YYYY-MM-DD');
		balance.funciones.changeDate();
        $(`#${tagDiv} span`).html("Rango de fechas  &nbsp; &nbsp; &nbsp; &nbsp;");
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDateRangePicker.FechasLimitedShowDate = function(tagDiv, maxDay){

	var start = moment().subtract(0, 'days');
 	var end = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	minDate: moment().subtract(0, 'days'),
    	maxDate: moment().subtract(-maxDay+1, 'days'),
    	showDropdowns: true,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
       $(`#${tagDiv} span`).html(start.format('YYYY-MM-DD') + ' / ' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDateRangePicker.FechasGraficas = function(tagDiv, parentEl){

	var start = moment().subtract(0, 'days');
 	var end = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	showDropdowns: true,
    	parentEl: parentEl,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
       $(`#${tagDiv} span`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDateRangePicker.FechasGraficas2 = function(tagDiv, parentEl, minDate, maxDate){

	var start = moment();
 	var end = start.clone();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	showDropdowns: true,
		minDate: minDate,
    	maxDate: maxDate,
    	parentEl: parentEl,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
       $(`#${tagDiv} span`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

/***********************************************************************************************/ 
/****************************** Fecha - Linea de envase ****************************************/
/***********************************************************************************************/

// variable donde guardara la fecha que este en la vista de linea de envase
gestionDRP.fecha_linea_envase = '';
gestionDRP.fecha_min = '';
gestionDRP.fecha_max = '';

gestionDRP.fecha_ini = '';
gestionDRP.fecha_fin = '';

// función para configurar el calendario 
gestionDRP.Fecha_envase = function(tagDiv,fecha_min,fecha_max){

 	var start = moment();
 	gestionDRP.fecha_min = fecha_min;
 	gestionDRP.fecha_max = fecha_max;



    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
    	parentEl: "div#contenedorM_registro_linea_de_envase",
    	minDate: fecha_min,
    	maxDate: fecha_max,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start
    }

    $(`#${tagDiv}`).daterangepicker(option,gestionDRP.fecha_linea);
    
    gestionDRP.fecha_linea(start);
}

gestionDRP.fecha_linea = function(start) {

	// Se toma la fecha que se seleccionada en el formato de moment para usarla,
	// cuando se este usando los botones de atras y siguiente y se actualice en el calendario
	gestionDRP.fecha_linea_envase = start;

	var dia = start.format('dddd').substring(0, 3).toUpperCase();
	var fecha = start.format('D');
	var mes = new Array(3);
	mes[0] = start.format('MMMM').substring(0, 1).toUpperCase();
	mes[1] = start.format('MMMM').substring(1, 2).toUpperCase();
	mes[2] = start.format('MMMM').substring(2, 3).toUpperCase();

	var txt=`	<div style="display: flex;">
					<div style="width: 80%;">
						<div style="font-size: 10px;font-weight: bold;">
							${dia}
						</div>
						<div style="font-weight: bold; font-size: 14px;">
							${fecha}
						</div>
					</div>
					<div style="width: 20%; font-size: 9px; font-weight: bold;">
						<div style="line-height: initial;">
							${mes[0]}
						</div>
						<div style="line-height: initial;">
							${mes[1]}
						</div>
						<div style="line-height: initial;">
							${mes[2]}
						</div>
					</div>
				</div>`;

    $(`#fecha-escritorio`).html(txt);
    $(`#fecha-movil`).html(start.format('LL'));


    M_registro_linea_de_envase.fechaAnterior = start.clone().subtract(1, 'days').format("YYYY-MM-DD");
    M_registro_linea_de_envase.fecha = start.format("YYYY-MM-DD");

    var id_recurso = $("#linea").val();
    if(id_recurso != "")
    {
		M_registro_linea_de_envase.funciones.selectTurno();
    }

    M_registro_linea_de_envase.funciones.selectoresdisabled();
}


gestionDRP.ReporteBD = function(tagDiv,fecha_min,fecha_max){

	var start = moment().subtract(29, 'days');
 	var end = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	showDropdowns: true,
    	parentEl: "div#contenedorD_Reporte_BD",
    	minDate: fecha_min,
    	maxDate: fecha_max,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
        ranges: {
	        'Hoy'				: [moment(), moment()],
	        'Ayer'				: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Últimos 7 Días'	: [moment().subtract(6, 'days'), moment()],
	        'Últimos 30 Días'	: [moment().subtract(29, 'days'), moment()],
	        'Este Mes'			: [moment().startOf('month'), moment().endOf('month')],
	        'Último Mes'		: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    	},
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
        $(`#${tagDiv} span#date-escritorio`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        $(`#${tagDiv} span#date-movil`).html(start.format('YYYY-MM-DD') + '<br>' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDRP.ReporteGantt = function(tagDiv,fecha_min,fecha_max){

	var start = moment().subtract(29, 'days');
 	var end = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	showDropdowns: true,
    	parentEl: "div#contenedorHistorico_Gantt",
    	minDate: fecha_min,
    	maxDate: fecha_max,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
        ranges: {
	        'Hoy'				: [moment(), moment()],
	        'Ayer'				: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Últimos 7 Días'	: [moment().subtract(6, 'days'), moment()],
	        'Últimos 30 Días'	: [moment().subtract(29, 'days'), moment()],
	        'Este Mes'			: [moment().startOf('month'), moment().endOf('month')],
	        'Último Mes'		: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    	},
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
        $(`#${tagDiv} span#date-escritorio`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        $(`#${tagDiv} span#date-movil`).html(start.format('YYYY-MM-DD') + '<br>' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDRP.ReporteGLY = function(tagDiv,year,month){

	Mes =  new Date(year + "/" + month + "/1");
	var start = moment(Mes).startOf('month');
 	var end = moment(Mes).endOf('month');
 	var hoy = moment();
 	var maxDate;

 	if(hoy > end)
 	{
 		maxDate = end.clone();
 	}
 	else
 	{
 		maxDate = hoy.clone();
 		end = hoy.clone();
 	}

    // Se configura las opciones para configurar el idioma 
    var option = {
    	parentEl: "div#contenedorD_Reporte_GLY",
    	minDate: start,
    	maxDate: maxDate,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
        $(`#${tagDiv} span#date-escritorio`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDRP.ReporteKPI = function(tagDiv,year,month){

	Mes =  new Date(year + "/" + month + "/1");
	var start = moment(Mes).startOf('month');
 	var end = moment(Mes).endOf('month');
 	var hoy = moment();
 	var maxDate;

 	if(hoy > end)
 	{
 		maxDate = end.clone();
 	}
 	else
 	{
 		maxDate = hoy.clone();
 		end = hoy.clone();
 	}

    // Se configura las opciones para configurar el idioma 
    var option = {
    	parentEl: "div#contenedorD_Reporte_KPI",
    	minDate: start,
    	maxDate: maxDate,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
        $(`#${tagDiv} span#date-escritorio`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDRP.Reporte_uso = function(tagDiv,fecha_min,fecha_max,container){

	var start = moment();
 	var end = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	showDropdowns: true,
    	parentEl: `div#${container}`,
    	minDate: fecha_min,
    	maxDate: fecha_max,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
        ranges: {
	        'Hoy'				: [moment(), moment()],
	        'Ayer'				: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Últimos 7 Días'	: [moment().subtract(6, 'days'), moment()],
	        'Últimos 30 Días'	: [moment().subtract(29, 'days'), moment()],
	        'Este Mes'			: [moment().startOf('month'), moment().endOf('month')],
	        'Último Mes'		: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    	},
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
        $(`#${tagDiv} > span.fecha_visual`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDRP.RegMasivoIni = function(fecha_min,fecha_max){

	gestionDRP.fecha_ini = fecha_min;
	gestionDRP.fecha_fin = fecha_max;
	var start = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
    	showDropdowns: true,
    	parentEl: "div#contenedorM_registro_linea_de_envase",
    	minDate: fecha_min,
    	maxDate: fecha_max,
    	opens: "left",
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: start,
    	alwaysShowCalendars: true,
    }

    function cb(start) {

    	M_registro_linea_de_envase.fecha_start = start.clone().format('YYYY-MM-DD');
        $(`#cont_Fecha_ini span#Fecha_ini`).html(start.format('dddd') + ' - ' + start.format("LL"));
        gestionDRP.RegMasivoFin(start.clone());
    }

    $(`#cont_Fecha_ini`).daterangepicker(option, cb);

    cb(start);
}


gestionDRP.RegMasivoFin = function(start){

	gestionDRP.fecha_ini = start.clone().format('DD-MM-YYYY');

    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
    	showDropdowns: true,
    	parentEl: "div#contenedorM_registro_linea_de_envase",
    	minDate: gestionDRP.fecha_ini,
    	maxDate: gestionDRP.fecha_fin,
    	opens: "left",
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: start,
    	alwaysShowCalendars: true,
    }

    function cb(start) {

		M_registro_linea_de_envase.fecha_end = start.clone().format('YYYY-MM-DD');
        $(`#cont_Fecha_fin span#Fecha_fin`).html(start.format('dddd') + ' - ' + start.format("LL"));
    }

    $(`#cont_Fecha_fin`).daterangepicker(option, cb);

    cb(start);
}

gestionDRP.historico = function(tagDiv,fecha_min,fecha_max){

	var start = moment().subtract(29, 'days');
 	var end = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	showDropdowns: true,
    	parentEl: "div#contenedorD_turno_linea_ejecucion",
    	minDate: fecha_min,
    	maxDate: fecha_max,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
        ranges: {
	        'Hoy'				: [moment(), moment()],
	        'Ayer'				: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Últimos 7 Días'	: [moment().subtract(6, 'days'), moment()],
	        'Últimos 30 Días'	: [moment().subtract(29, 'days'), moment()],
	        'Este Mes'			: [moment().startOf('month'), moment().endOf('month')],
	        'Último Mes'		: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    	},
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
        $(`#${tagDiv} span#date-escritorio`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        $(`#${tagDiv} span#date-movil`).html(start.format('YYYY-MM-DD') + '<br>' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDRP.historicoMovil = function(tagDiv,fecha_min,fecha_max){

	var start = moment().subtract(29, 'days');
 	var end = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	showDropdowns: true,
    	autoApply: true,
    	parentEl: "div#contenedorD_turno_linea_ejecucion",
    	minDate: fecha_min,
    	maxDate: fecha_max,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end,
    	alwaysShowCalendars: true,
    }

    function cb(start, end) {
        $(`#${tagDiv} span#date-escritorio`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        $(`#${tagDiv} span#date-movil`).html(start.format('YYYY-MM-DD') + '<br>' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

/************************************************************************************************************************/ 
// Dashboard 70 - 90 Comparación

// función para configurar el calendario 
// Día
gestionDRP.SelectSimple = function(tagDiv,fecha_min,fecha_max){

 	var start = moment();
 	var end = moment();

 	var date_min = moment(fecha_min, "YYYY-MM-DD");
 	var date_max = moment(fecha_max, "YYYY-MM-DD");

    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
    	parentEl: "div#DB_70_90_Comp",
    	minDate: date_min,
    	maxDate: date_max,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start
    }

    $(`#${tagDiv}`).daterangepicker(option,gestionDRP.fecha_linea);
    
    function cb(start, end) {
        $(`#${tagDiv} span`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

// Semana - Mes y Año
gestionDRP.SelectRango = function(tagDiv,fecha_min,fecha_max){

 	var start 	= moment(fecha_min, "YYYY-MM-DD");
 	var end;

 	var date_min = moment(fecha_min, "YYYY-MM-DD");
 	var date_max;

 	var date_actual = moment().format("YYYY-MM-DD");

 	if(date_actual < fecha_max){
 		date_max = moment();
 		end 	 = moment();
 	}
 	else
 	{
 		date_max = moment(fecha_max, "YYYY-MM-DD");
 		end  	 = moment(fecha_max, "YYYY-MM-DD");
 	}

    // Se configura las opciones para configurar el idioma 
    var option = {
    	showISOWeekNumbers: true,
    	parentEl: "div#DB_70_90_Comp",
    	minDate: date_min,
    	maxDate: date_max,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: end
    }

    $(`#${tagDiv}`).daterangepicker(option,gestionDRP.fecha_linea);
    
    function cb(start, end) {
        $(`#${tagDiv} span`).html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start, end);
}

gestionDRP.fecha_Reporte_GLY_LEF = function(tagDiv,fecha_min,fecha_max){

 	var start = moment();
    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
    	opens: "center",
    	parentEl: "div#contenedorReporte_LEF_GLY",
    	minDate: fecha_min,
    	maxDate: fecha_max,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start
    }

    $(`#${tagDiv}`).daterangepicker(option,gestionDRP.fecha_Reporte_GLY_LEF);

    function cb(start) {

    	Reporte_LEF_GLY.fecha = start.format("YYYY-MM-DD");

	    $(`#date-escritorio`).html(start.format('LL'));
	    $(`#date-movil`).html(start.format('ll'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start);
}

gestionDRP.DB_52_12_4 = function(tagDiv,fecha_min,fecha_max,container){

 	var start = moment();
    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
    	opens: "center",
    	parentEl: `div${container}`,
    	minDate: fecha_min,
    	maxDate: fecha_max,
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start
    }

    $(`#${tagDiv}`).daterangepicker(option,gestionDRP.fecha_Reporte_GLY_LEF);

    function cb(start) {
		
		$(`#${tagDiv} p`).html(start.format('YYYY-MM-DD'));
    	//DB_ParadaST_52_12_4.fecha = start.format("YYYY-MM-DD");

	    $(`#${tagDiv} span`).html(start.format('LL'));
    }

    $(`#${tagDiv}`).daterangepicker(option, cb);

    cb(start);
}


gestionDRP.rangeDRP = function ($elem, cont, minD, maxD) {

    var option = {
    	singleDatePicker: true,
	    showWeekNumbers: true,
		showISOWeekNumbers: true,
		timePicker: true,
		timePicker24Hour: true,
    	parentEl: cont,
    	opens: "center",
    	minDate: minD,
    	maxDate: maxD,
    	locale: {
    		format: "YYYY-MM-DD HH:mm:ss",
	        applyLabel: "Aplicar",
	        cancelLabel: "Cancelar",
	        fromLabel: "Desde",
	        toLabel: "Hasta",
	        customRangeLabel: "Rango",
	        weekLabel: "S",
	        daysOfWeek: [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        monthNames: [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        firstDay: 1
    	}
    }
	$elem.daterangepicker(option);
}

/****************************************************************************************/ 

gestionDRP.fecha_ini_gantt = '';
gestionDRP.fecha_fin_gantt = '';
gestionDRP.fecha_fin_aux_gantt = '';

gestionDRP.GanttH_Ini = function(fecha_min,fecha_max){

	gestionDRP.fecha_ini_gantt = fecha_min;
	gestionDRP.fecha_fin_gantt = fecha_max;
	var start = moment();

    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
    	showDropdowns: true,
    	parentEl: "div#contenedorHistorico_Gantt",
    	minDate: fecha_min,
    	maxDate: fecha_max,
    	opens: "center",
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: start,
    	alwaysShowCalendars: true,
    }

    function cb(start) {

    	var maxDate = start.clone().add(9, 'days');

    	if(maxDate > moment(gestionDRP.fecha_fin_gantt,"DD/MM/YYYY"))
		{
	 		gestionDRP.fecha_fin_aux_gantt = gestionDRP.fecha_fin_gantt;
	 	}
	 	else
	 	{
	 		gestionDRP.fecha_fin_aux_gantt = maxDate.format("DD/MM/YYYY");
	 	}

        $(`#cont_Fecha_ini span#Fecha_ini`).html(start.format('dddd') + ' - ' + start.format("LL"));
        $(`#cont_Fecha_ini span#F_ini`).html(start.format('YYYY-MM-DD'));

        gestionDRP.GanttH_Fin(start.clone());
    }

    $(`#cont_Fecha_ini`).daterangepicker(option, cb);

    cb(start);
}


gestionDRP.GanttH_Fin = function(start){

	var f_ini = start.format("DD/MM/YYYY");

    // Se configura las opciones para configurar el idioma 
    var option = {
    	singleDatePicker: true,
    	parentEl: "div#contenedorHistorico_Gantt",
    	minDate: f_ini,
    	maxDate: gestionDRP.fecha_fin_aux_gantt,
    	opens: "center",
    	locale: {
	        "applyLabel": "Aplicar",
	        "cancelLabel": "Cancelar",
	        "fromLabel": "Desde",
	        "toLabel": "Hasta",
	        "customRangeLabel": "Rango",
	        "weekLabel": "S",
	        "daysOfWeek": [
	            "Do",
	            "Lu",
	            "Ma",
	            "Mi",
	            "Ju",
	            "Vi",
	            "Sa"
	        ],
	        "monthNames": [
	            "Enero",
	            "Febrero",
	            "Marzo",
	            "Abril",
	            "Mayo",
	            "Junio",
	            "Julio",
	            "Agosto",
	            "Septiembre",
	            "Octubre",
	            "Noviembre",
	            "Diciembre"
	        ],
	        "firstDay": 1
    	},
        startDate: start,
        endDate: start,
    	alwaysShowCalendars: true,
    }

    function cb(start) {
        $(`#cont_Fecha_fin span#Fecha_fin`).html(start.format('dddd') + ' - ' + start.format("LL"));
        $(`#cont_Fecha_fin span#F_fin`).html(start.format('YYYY-MM-DD'));
    }

    $(`#cont_Fecha_fin`).daterangepicker(option, cb);

    cb(start);
}