var gestionImask = new Object();

gestionImask.cualquierCaracter = function(id,index){
	if(index == undefined){
		index = 0;
	}
	var element = $(id)[index];
	var maskOptions = {
  	mask: '****************************************'
	};
	var mask = new IMask(element, maskOptions);
}

gestionImask.cualquierLetra = function(id,index){
	if(index == undefined){
		index = 0;
	}
	var element = $(id)[index];
	var maskOptions = {
  	mask: 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa'
	};
	var mask = new IMask(element, maskOptions);
}	

gestionImask.cualquierNumeroDecimal = function(id,index,minlim,maxlim){
	if(minlim == undefined){
		minlim = -1000000000;
	}
	if(maxlim == undefined){
		maxlim = 1000000000;
	}
	if(index == undefined){
		index = 0;
	}
	var element = $(id)[index];	
	new IMask(element, {
	  mask: Number,  // enable number mask
	  // other options are optional with defaults below
	  scale: 8,  // digits after point, 0 for integers
	  signed: false,  // disallow negative
	  thousandsSeparator: '.',  // any single char
	  padFractionalZeros: false,  // if true, then pads zeros at end to the length of scale
	  normalizeZeros: true,  // appends or removes zeros at ends
	  radix: ',',  // fractional delimiter
	  mapToRadix: ['.'],  // symbols to process as radix
	  // additional number interval options (e.g.)
	  min: minlim,
	  max: maxlim
	});
}

gestionImask.codigoNumerico  = function(id,index){
	minlim = 0;
	maxlim = 100000000000;

	if(index == undefined){
		index = 0;
	}
	var element = $(id)[index];	
	new IMask(element, {
	  mask: Number,  // enable number mask
	  // other options are optional with defaults below
	  scale: 0,  // digits after point, 0 for integers
	  signed: false,  // disallow negative
	  thousandsSeparator: '',  // any single char
	  padFractionalZeros: false,  // if true, then pads zeros at end to the length of scale
	  normalizeZeros: true,  // appends or removes zeros at ends
	  radix: ',',  // fractional delimiter
	  mapToRadix: ['.'],  // symbols to process as radix
	  // additional number interval options (e.g.)
	  min: minlim,
	  max: maxlim
	});
}

gestionImask.cualquierNumeroEntero  = function(id,index,minlim,maxlim){
	if(minlim == undefined){
		minlim = -1000000000;
	}
	if(maxlim == undefined){
		maxlim = 1000000000;
	}
	if(index == undefined){
		index = 0;
	}
	var element = $(id)[index];	
	new IMask(element, {
	  mask: Number,  // enable number mask
	  // other options are optional with defaults below
	  scale: 0,  // digits after point, 0 for integers
	  signed: false,  // disallow negative
	  thousandsSeparator: '.',  // any single char
	  padFractionalZeros: false,  // if true, then pads zeros at end to the length of scale
	  normalizeZeros: true,  // appends or removes zeros at ends
	  radix: ',',  // fractional delimiter
	  mapToRadix: ['.'],  // symbols to process as radix
	  // additional number interval options (e.g.)
	  min: minlim,
	  max: maxlim
	});
}

gestionImask.soloNumerosNoMil  = function(id,index){
	var maxlim = 1000000000;
	if(index == undefined){
		index = 0;
	}
	var element = document.getElementById(id);	
	new IMask(element, {
	  mask: Number,  // enable number mask
	  // other options are optional with defaults below
	  scale: 0,  // digits after point, 0 for integers
	  signed: false,  // disallow negative
	  thousandsSeparator: '.',  // any single char
	  padFractionalZeros: false,  // if true, then pads zeros at end to the length of scale
	  normalizeZeros: true,  // appends or removes zeros at ends
	  radix: ',',  // fractional delimiter
	  mapToRadix: ['.'],  // symbols to process as radix
	  // additional number interval options (e.g.)
	  min: -maxlim,
	  max: maxlim
	});
}


gestionImask.horaCambioDia  = function(id){
	var momentFormat = 'HH:mm:ss';
	var element = document.getElementById(id);
	var momentMask = IMask(element, {
  		mask: Date,
  		pattern: momentFormat,
  		lazy: false,

  		format: function (date) {
    		return moment(date).format(momentFormat);
  		},
  		parse: function (str) {
    		return moment(str, momentFormat);
  		},

		blocks: {
		    	HH: {
		      		mask: IMask.MaskedRange,
					from: 0,
		      		to: 23
		    	},
		    	mm: {
		      		mask: IMask.MaskedRange,
		      		from: 0,
		     		to: 59
		    	},
				ss: {
		      		mask: IMask.MaskedRange,
		      		from: 0,
		      		to: 59
		    	}
		 	}
		});
}

gestionImask.hora  = function(id,min,max){
	//Si el numero es de un digito se antepone el 0 sino se castea a string
	if(min >=0 && min <= 9){
		min = `0${min}`;
	}
	else{
		min = `${min}`;
	}
	//Si el numero es de un digito se antepone el 0 sino se castea a string
	if(max >=0 && max <= 9){
		max = `0${max}`;
	}
	else{
		max = `${max}`;
	}
	
	var momentFormat = 'HH:mm:ss';
	var element = document.getElementById(id);
	var momentMask = IMask(element, {
  		mask: Date,
  		pattern: momentFormat,
  		lazy: false,


  		format: function (date) {
    		return moment(date).format(momentFormat);
  		},
  		parse: function (str) {
    		return moment(str, momentFormat);
  		},

		blocks: {
		    	HH: {
		      		mask: IMask.MaskedRange,
					from: min,
		      		to: max
		    	},
		    	mm: {
		      		mask: IMask.MaskedRange,
		      		from: 0,
		     		to: 59
		    	},
				ss: {
		      		mask: IMask.MaskedRange,
		      		from: 0,
		      		to: 59
		    	}
		 	}
		});
}

gestionImask.horaMinutos  = function(id){
	var momentFormat = 'HH:mm';
	var element = document.getElementById(id);
	var momentMask = IMask(element, {
  		mask: Date,
  		pattern: momentFormat,
  		lazy: false,

  		format: function (date) {
    		return moment(date).format(momentFormat);
  		},
  		parse: function (str) {
    		return moment(str, momentFormat);
  		},

		blocks: {
		    	HH: {
		      		mask: IMask.MaskedRange,
					from: 0,
		      		to: 23
		    	},
		    	mm: {
		      		mask: IMask.MaskedRange,
		      		from: 0,
		     		to: 59
		    	}
		 	}
		});
}

gestionImask.Numerico = function(id,index){
	if(index == undefined){
		index = 0;
	}
	var element = $(id)[index];
	var maskOptions = {
	mask: '########################################',
	definitions: {
	   	'#': /[0-9]/
	 	}
	};
	var mask = new IMask(element, maskOptions);
}

gestionImask.alfaNumerico = function(id,index){
	if(index == undefined){
		index = 0;
	}
	var element = $(id)[index];
	var maskOptions = {
	mask: '########################################',
	definitions: {
	   	'#': /^[\sa-zA-Z0-9-]$/
	 	}
	};
	var mask = new IMask(element, maskOptions);
}