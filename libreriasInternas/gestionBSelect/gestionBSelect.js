var gestionBSelect = new Object();

/* Estilo y funciones para selectores, se aplica al elemento final, es decir, cuando ya  */
/* tiene cargadas todas las opciones, si se desea agregar otra opción ver metodos 		 */
/* en la libreria externa bootstrap select 												 */
/* @param text_count se modifica para configurar la visualización de las opciones seleccionas,	 */
/* Por defecto muestra números para las opciones seleccionadas									 */

gestionBSelect.wrapSelectDT = function(id_select, header=false) {
	//Config the object to pass a selectpicker method
	var config = {
		actionsBox: true,
		container: "#divtabla",
		header: header,
		liveSearchNormalize: true,
		hideDisabled: true,
		liveSearch: true,
		selectedTextFormat: "count>0",
		title: "",
		virtualScroll: false
	};

	$(id_select).selectpicker(config);
	//Se pone el icono
	id_select = id_select.substring(1);
	$(`button[data-id='${id_select}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).append("<i class='fas fa-filter'></i>");
}

gestionBSelect.wrapSelectDTM = function($select) {
	//Config the object to pass a selectpicker method
	var config = {
		actionsBox: true,
		container: "div[data-id='modals']",
		liveSearchNormalize: true,
		hideDisabled: true,
		liveSearch: false,
		selectedTextFormat: "count",
		title: " ",
		width: "fit",
		virtualScroll: false
	};

	$select.selectpicker(config);

	//$select.selectpicker('refresh');
}

gestionBSelect.wrapSelectDTM2 = function($select,contenedor) {
	//Config the object to pass a selectpicker method
	var config = {
		actionsBox: true,
		container: `div[data-id='${contenedor}']`,
		liveSearchNormalize: true,
		hideDisabled: true,
		liveSearch: false,
		selectedTextFormat: "count",
		title: " ",
		width: "fit",
		virtualScroll: false
	};

	$select.selectpicker(config);
}


/* Utilizar Bootstrap select de manera generica*/
gestionBSelect.generic = function(id_select, title, header){
	//Config the object to pass a selectpicker method
	var config = {
		actionsBox: true,
		container: "body",
		header: header,
		liveSearchNormalize: true,
		hideDisabled: true,
		liveSearch: true,
		selectedTextFormat: "count>2",
		title: title,
		virtualScroll: false
	};

	$(id_select).selectpicker(config);
}

/* Utilizar Bootstrap select de manera generica*/
gestionBSelect.generic2 = function(id_select, title, header){
	//Config the object to pass a selectpicker method
	var config = {
		actionsBox: true,
		container: "body",
		header: header,
		liveSearchNormalize: true,
		hideDisabled: true,
		liveSearch: true,
		selectedTextFormat: "count>0",
		title: title,
		virtualScroll: false
	};
	$(id_select).selectpicker(config);
}

/* Utilizar Bootstrap select de manera generica indicando el contenedor padre*/
gestionBSelect.generic3 = function(id_select, id_content, title, header){
	//Config the object to pass a selectpicker method
	var config = {
		actionsBox: true,
		container: id_content,
		header: header,
		hideDisabled: true,
		liveSearch: true,
		selectedTextFormat: "count>2",
		title: title,
		virtualScroll: false
	};
	$(id_select).selectpicker(config);
}

gestionBSelect.ganttFilter = function(id_select, header=false) {
	//Config the object to pass a selectpicker method
	var config = {
		actionsBox: true,
		container: "div#contGanttH",
		header: header,
		hideDisabled: true,
		liveSearch: true,
		selectedTextFormat: "static",
		title: header,
		virtualScroll: false,
		style: "btn-sm"
	};
	$(id_select).selectpicker(config);
}