//Añade funcionalidades de convertir em a pixeles o pixeles a em
$.fn.toEm = function(settings){
    settings = jQuery.extend({
        scope: 'body'
    }, settings);
    var that = parseInt(this[0],10),
        scopeTest = jQuery('<div style="display: none; font-size: 1em; margin: 0; padding:0; height: auto; line-height: 1; border:0;">&nbsp;</div>').appendTo(settings.scope),
        scopeVal = scopeTest.height();
    scopeTest.remove();
    return (that / scopeVal).toFixed(8) + 'em';
};

$.fn.toPx = function(settings){
    settings = jQuery.extend({
        scope: 'body'
    }, settings);
    var that = parseFloat(this[0]),
        scopeTest = jQuery('<div style="display: none; font-size: 1em; margin: 0; padding:0; height: auto; line-height: 1; border:0;">&nbsp;</div>').appendTo(settings.scope),
        scopeVal = scopeTest.height();
    scopeTest.remove();
    return Math.round(that * scopeVal) + 'px';
};

//tipo de dato columna similar a datatable para crear el arreglo de objetos para los filtros
function column(data, type) {
  this.data = data;
  this.name = "";
  this.searchable = true;
  this.orderable = true;
  this.search = { 
    value: "",
    regex: false
  };
  this.type = type;
}

var GanttH = new Object();

moment.locale("es"); //Traducción de momentJS

GanttH.callback = function(){console.log("Listo")};
GanttH.resultado = "";//variable para guardar la consulta y las opciones
GanttH.lastScrollLeft = 0;//variable para realizar el desplazamiento del texto con el scroll
GanttH.lastScrollTop = 0;
GanttH.nombre = ""; //var para guardar el método del servicio
GanttH.urlconexion = "";  //var para la url del servicio
GanttH.id_cont = "";  //id del contenedor del gantt
GanttH.config = ""; //objeto css de configuración
GanttH.show = 0;  //elementos a mostrar (luego se convierte en un arreglo)
GanttH.hide = 0;  //elementos a esconder (luego se convierte en un arreglo)
GanttH.details = "";//guarda los objetos que hay en detalles antes de redibujar todo
GanttH.rulesId = "";//guarda el id del evento que tiene la regla
GanttH.modalHeight = 0;//guarda la altura inicial del modal(en movil)
GanttH.timeToShw = 500;//tiempo para mostrar modal
GanttH.loadModal;//objeto de setTimeout para mostrar el modal o cancelar
GanttH.loadPopOver;//objeto de setTimeout para mostrar el popover o cancelar
GanttH.datesRules = new Array(); //arreglo que guarda las fechas de las reglas dibujadas
GanttH.columns = new Array(); //arreglo de columnas para realizar los filtros
GanttH.Fchange = new Array(); //bandera de cambio en los filtros
GanttH.eventsFiltered = ""; //eventos filtrados
GanttH.resources = ""; //Guarda los recursos a mostrar
GanttH.hideR = "";
GanttH.showR = "";

GanttH.brecha = 0;

//parametros necesarios para utilizar convertT2Px, en la opcion de ir a una fecha y hora
GanttH.escala = 0; //guarda la escala actual
GanttH.width = 0; //guarda la altura actual
GanttH.mStart = 0; //guarda la fecha de inicio actual (global)
GanttH.opt = 0; //guarda la opcion actual

//Controles que se renderizan en la parte superior
GanttH.controls = [
  {
    "scale": "Year",
    "label": "Año",
    "zoom": [
      // {
      //   "val": "year",
      //   "label": "Anual",
      //   "length": [1,2]
      // },
      {
        "val": "month",
        "label": "Mensual",
        "length": [1,2,3,6,12]
      }
    ],
    "rules": [
      {
        "val": "row-date",
        "label": "Año-Mes"
      },
      {
        "val": "row-time",
        "label": "Mes"
      }
    ]
  },
  {
    "scale": "Month",
    "label": "Mes",
    "zoom": [
      // {
      //   "val": "month",
      //   "label": "Mensual",
      //   "length": [1,2,3,6,12]
      // },
      {
        "val": "week",
        "label": "Semanal",
        "length": [1,2,4,8]
      }
    ],
    "rules": [
      {
        "val": "row-date",
        "label": "Año-Mes"
      },
      {
        "val": "row-week",
        "label": "Semana"
      },
      {
        "val": "row-time",
        "label": "Día"
      }
    ]
  },
  {
    "scale": "Week",
    "label": "Semana",
    "zoom": [
      {
        "val": "day",
        "label": "Dias",
        "length": [1,2,4,5,7]
      }
    ],
    "rules": [
      {
        "val": "row-date",
        "label": "Año-Mes"
      },
      {
        "val": "row-week",
        "label": "Semana"
      },
      {
        "val": "row-time",
        "label": "Día"
      }
    ]
  },
  {
    "scale": "Day",
    "label": "Día",
    "zoom": [
      {
        "val": "hour",
        "label": "1 hora",
        "length": [1,2,4,8,16,24]
      },
      {
        "val": "hHour",
        "label": "30 min",
        "length": [1,2,4,8,16,32]
      },
      {
        "val": "qHour",
        "label": "15 min",
        "length": [1,2,4,8,16,32]
      },
      {
        "val": "5m",
        "label": "5 min",
        "length": [1,3,6,12,24]
      },
      {
        "val": "1m",
        "label": "1 min",
        "length": [1,2,5,15,30]
      }
    ],
    "rules": [
      {
        "val": "row-date",
        "label": "Año-Mes"
      },
      {
        "val": "row-week",
        "label": "Semana"
      },
      {
        "val": "row-day",
        "label": "Día"
      },
      {
        "val": "row-time",
        "label": "Hora"
      }
    ]
  }
];

/* initGantt */
/* Función para iniciar el ganttvertical, recibe el id del contenedor, url del servicio y nombre del webmethod
   además objeto @config con estructura CSS para aplicarle al contenedor del gantt $(".c1").css(config)*/
GanttH.initGantt = function(id_cont, urlconexion, nombre, replace, config, callback) {
  //Alerta bloqueante para el dibujo del gantt
  // gestionModal.alertaBloqueante("Procesando");


    // <div id="spinnerG" class="spinner-border text-muted" style="top: ${parseInt(config.height)/2}px">
    // </div>
    // <div id="backd" style="width: ${$(id_cont).width()+18}px; height: calc(${config.height} + 20px)">
    // </div>

  $(id_cont).html(`
    <div class="spinner-position-gantt">
      <div class="spinner-border text-muted"></div>
    </div>
    `
  );

  //Cada vez que se dibuja en gantt se limpia la variable que almacena el filtrado y las columnas
  GanttH.eventsFiltered = "ALL";
  GanttH.columns = [];
  GanttH.filter_active = false;

  /*La variable init se usa para identificar si es la petición inicial o es una petición realizada por la interacción del usuario
    con los controles que se han renderizado, init=true es la petición inicial, init=false interacción del usuario*/
  var parameters = {
    id_cont: id_cont,
    init: true,
    config: config,
    replace: JSON.stringify(replace)
  };
  //Variables globales para guardar datos necesarios en otras funciones
  GanttH.callback = callback;
  GanttH.urlconexion = urlconexion;
  GanttH.nombre = nombre;
  GanttH.id_cont = id_cont;
  GanttH.config = config;
  GanttH.replace = JSON.stringify(replace);

  //AJAX para obtener los datos para dibujar el gantt
  GanttH.getData(parameters);
}

/* getData */
/* Hace llamado del AJAX para los datos */
GanttH.getData = function(parameters) {
  //Ajax de los datos
  var data = {
    nombre:   GanttH.nombre,
    replace:  parameters.replace
  };

  query.callAjax( GanttH.urlconexion, "ganttH_data", data, GanttH.receiveInfo, parameters );
}

/* receiveInfo */
/* Callback del ajax, recibe el objeto que contiene los eventos
   a dibujar y también recibe las opciones, si no están definidas
   se definen en esta función, además de sobreescribirlas si el 
   llamado es por interacción del cliente */
GanttH.receiveInfo = function(resultado, parameters) {

  //Se elimina la etiqueta modal en el html cuando se general el gantt o cuando se actualiza
  $(".tooltip").tooltip("dispose");

  // Cada vez que se dibuja el gantt se pone esta variable en cero la cual es el offset de las reglas del gantt
  GanttH.brecha = 0;

  /*Si por algun motivo el resultado que recibe no tiene el objeto de opciones
  aqui se configura con valores por defecto*/
  if(resultado.data != ""){
    if(resultado.options === undefined)
    {
      resultado.options = {
        "start": resultado.data[0].start,
        // "start": "2018-12-23 22:00:00",
        // "start": "2019-01-22 22:00:00",
        "end": resultado.data[resultado.data.length-1].end,
        "scale": "Day", //Year, Month, Week, Day
        "zoom": "hour",//hour //hHour(30mm) //qHour(15mm) // 5m(5mm) //1m(1mm)
        "v_length": 2,
        "h_length": 10,
        "showDate": 1
      };
      //se define el objeto showdate para las reglas de las fechas
      GanttH.show = resultado.options.showDate;
      GanttH.hide = resultado.options.showDate;
    }
    else if(!parameters.init)
    {
      /*Si init es false se sobreescribe el objeto opciones que proviene de la consulta*/
      resultado.options = GanttH.resultado.options;
    }
    else
    {
      //en la petición inicial, define el objeto showdate que muestra u oculta las reglas
      GanttH.show = resultado.options.showDate;
      GanttH.hide = resultado.options.showDate;
    }
    /*se almacena el resultado de la petición en una variable global*/
    GanttH.resultado = resultado;
    //metodo para dibujar los contenedores del gantt
    GanttH.drawAll(resultado, parameters);
  }
  else
  {
    $(parameters.id_cont).html(`
      <div class="spinner-position-gantt">
        <span>Ningún dato disponible en este gantt</div>
      </div>
      `
    );
    GanttH.callback();
  }
}

/* drawAll */
/* dibuja los contenedores principales de los eventos y las reglas,
   dependiendo de la escala invoca a una función para configurar ciertos parametros */
GanttH.drawAll = function(resultado, parameters) {
  var cont = $(parameters.id_cont); //contenedor del gantt
  var height;//altura contenedor
  var width; //ancho del contenedor
  var size = new Object();

  //altura de las lineas horizontales
  var box_v;
  //ancho lineas horizontales
  var box_h;

  $("#eventsModal").remove();
  $(".pop-over").remove();
  cont.empty();//vaciar contenedor

  //dibuja los controles para cambiar las escalas del gantt, los botones para actualizar y ver detalles
  cont.append(`<div class="topnav">
  <a href="javascript:void(0);" class="icon btn btn-outline-secondary btn-sm btn-block" id="iconNav">
    <i class="fa fa-bars"></i>
  </a>
  <div id="cont-controls">
    <div class="controls form-inline" id="controles">
      <div class="p-1" role="shwResource" data-toggle="tooltip" title="Mostrar Recursos"><select class="selectpicker" multiple
        id="shwResources"
        data-live-search="true" 
        data-actions-box="true" 
        data-selected-text-format="static"
        data-title="Ver Recursos"
        data-width="110px"
        data-style="btn btn-outline-primary btn-sm"
        data-header="Ver Recursos"></select></div>
      <div class="p-1" role="nRecursos" data-toggle="tooltip" title="Nº de filas en pantalla (Líneas)"><select class="custom-select-sm form-control form-control-sm"></select></div>
      <div class="p-1" role="escala" data-toggle="tooltip" title="Escala Principal"><select class="custom-select-sm form-control form-control-sm"></select></div>
      <div class="p-1" role="zoom" data-toggle="tooltip" title="División de la escala principal"><select class="custom-select-sm form-control form-control-sm"></select></div>
      <div class="p-1"><label>x</label></div>
      <div class="p-1" role="length" data-toggle="tooltip" title="Nº de divisiones en pantalla"><select class="custom-select-sm form-control form-control-sm"></select></div>
      <div class="p-1" role="shwDt" data-toggle="tooltip" title="Mostrar Escalas de Tiempo"><select class="selectpicker" multiple data-live-search="false" 
        data-actions-box="false" 
        data-selected-text-format="static"
        data-title="Ver escalas"
        data-width="100px"
        data-style="btn btn-outline-primary btn-sm"
        data-header="Ver escalas"></select></div>
      <div class="p-1">
        <button class="btn btn-outline-primary btn-sm" id="searchDate"><i class="far fa-calendar-alt"></i></button>
      </div>
      <div class="p-1" role="delRules">
        <button class="btn btn-danger btn-sm" id="hideRulesBtn" data-toggle="tooltip" title="Limpiar Reglas"><i class="fas fa-ruler"></i></button>
      </div>
      <div class="p-1" role="shwFilters">
        <a href="javascript:void(0);" class="btn btn-outline-secondary btn-sm btn-block" id="iconFilters" data-toggle="tooltip" title="Mostrar Filtros">
          <i class="fas fa-angle-double-down"></i>
          <i class="fas fa-filter"></i>
        </a>
      </div>
      <div class="p-1" role="delFilters">
        <button id="clearBtnFilter" type="button" class="btn btn-primary btn-sm btn-clear-filter" data-toggle="tooltip" title="Limpiar Filtros">
          <small>
            <i class="fas fa-times text-danger"></i>
            <i class="fas fa-filter"></i>
          </small>
        </button>
      </div>
      <div class="p-1" role="updateDraw" data-toggle="tooltip" title="Actualizar...">
        <button class="btn btn-primary btn-sm" id="updtBtn"><i class="fas fa-sync"></i></button>
      </div>
      <div class="p-1" role="shwDetails">
        <button class="btn btn-secondary btn-sm" id="shwDetBtn" data-target="#details" data-toggle="tooltip" title="Mostrar detalles de eventos">
          <i class="fas fa-info-circle"></i>
        </button>
      </div>
    </div>
    <div id="cont-filtros">
      <div class="card-body slider"></div>
    </div>
  </div>
  </div>`);

  //Eventos iniciados en los controles
  //Evento cambio de algun selector para cambiar las opciones de los otros que dependen de este
  $("#controles").find("select").on("change", GanttH.appendOptions);
  //Evento boton de actulizacion dependiendo de el cambio de los controles (escala)
  $("#updtBtn").on("click.GanttH", GanttH.refreshGantt);
  //Evento boton mostrar el contenedor de los detalles
  $("#shwDetBtn").on("click.GanttH", GanttH.shwDet);
  //Evento boton limpiar reglas
  $("#hideRulesBtn").on("click.GanttH", GanttH.hideRules);
  //Evento mostrar filtros
  $("#iconFilters").on("click.GanttH", GanttH.showFilters);

  /*-------------Movil----------------*/
  //Evento boton para mostrar controles
  $("#iconNav").on("click.GanttH", GanttH.showControls);
  /*----------------------------------*/

  //Añade las opciones a los selectores que existen en los controles
  GanttH.appendOptions(0, resultado.options.scale, resultado.options.zoom, resultado.options.h_length, resultado.options.showDate);

  //dibuja los elementos contenedores de los eventos, las etiquetas, las reglas y los detalles
  cont.append(`
  <div class="c2" style="height: calc(${parameters.config.height} - 50px)">
    <div class="cont contA" id="cont-main">
      <div aria-hidden="true" tabindex="-1" class="row-time r-line" id="row-date">
        <div class="date"></div>
      </div>
      <div aria-hidden="true" tabindex="-1" class="row-time r-line" id="row-week">
        <div class="week"></div>
      </div>
      <div aria-hidden="true" tabindex="-1" class="row-time r-line" id="row-day">
        <div class="day"></div>
      </div>
      <div aria-hidden="true" tabindex="-1" class="row-time r-line" id="row-time">
        <div class="time"></div>
      </div>
        <div class="inner-cont">
          <div aria-hidden="true" tabindex="-1" class="col-recursos r-line" id="col-resource">
            <div class="recursos"></div>
          </div>
          <div role="presentation" class="pres" id="scroll" tabindex="-1">
            <div class="gantt" role="row" id="col-ev">
              <div aria-hidden="true" class="rules"></div>
              <div class="vertical-line"></div>
              <div role="gridcell" tabindex="-1" class="cont-events cont-evs">
                <div class="no-v"></div>
                <div role="presentation" class="presentation"></div>
              </div>
            </div>
          </div>
        </div>
    </div>
    <div role="details" class="details" id="details" tabindex="-1">
      <div class="opt-cont">
        <div class="options-D" id="close-D" data-target="#details"><i class="fas fa-times"></i></div>
        <button class="btn btn-outline-secondary btn-sm btn-block" id="clearDetBtn">Limpiar</button>
      </div>
      <div class="cont-det" id="cont-dets">
      </div>
    </div>

      <!-- Modal para el movil -->
      <div class="modalC" id="evsModalMobile">
        <div class="modalC-dialog">
          <div class="modalC-content">

            <div class="modalC-header">
              <h5 class="modalC-title"></h5>
              <div class="options" id="M-append-details" data-target="#details #cont-dets"><i class="fas fa-thumbtack"></i></div>
              <div class="options" id="M-edit-M"><i class="fas fa-pen"></i></div>
              <div class="options" id="M-delete-M"><i class="far fa-trash-alt"></i></div>
              <div class="options" id="M-close-M" data-dismiss="modal"><i class="fas fa-times"></i></div>
            </div>

            <div class="modalC-body">
            </div>

          </div>
        </div>
      </div>
    </div>
    </div>`);

  //cierra contenedor de detalles
  $("#close-D").on("click.GanttH", GanttH.shwDet);
  //Pone el evento que se dispara cuando todo esta listo, (funcion drawArrows)
  $("#cont-main").on("ready.GanttH", GanttH.readyDraw);
  //envuelve en c2 y c1 (<c1><c2></></>) el contenido dibujado
  // cont.children().wrapAll("<div class='c2' />");
  cont.children().wrapAll(`
    <div id='contGanttH' class='c1' />
  `);

  //si el dibujado viene de la interacción del usuario se han guardado los eventos que hay en detalles y ahora se vuelven a poner
  cont.find("#details #cont-dets").append(GanttH.details);

  //aplica estilos del desarrollador que esta usando la libreria
  cont.find(".c1").css(parameters.config);

  //Si showDate es 1 muestra todas las reglas, si es 0 esconde todas las reglas
  if(resultado.options.showDate == 1)
  {
    $("#col-resource").show();
  }
  else
  {
    $("#col-resource").hide();
  }

  //dibuja el modal para observar los datos de los eventos con click
  cont.append(`
    <div class="modalC" id="eventsModal">
      <div class="modalC-dialog">
        <div class="modalC-content">

          <div class="modalC-header">
            <h5 class="modalC-title"></h5>
            <div class="options" id="append-details" data-target="#details #cont-dets" data-toggle="tooltip" title="Añadir a detalles"><i class="fas fa-thumbtack"></i></div>
            <div class="options" id="edit-M" data-toggle="tooltip" title="Editar"><i class="fas fa-pen"></i></div>
            <div class="options" id="delete-M" data-toggle="tooltip" title="Eliminar"><i class="far fa-trash-alt"></i></div>
          </div>

          <div class="modalC-body">
          </div>

        </div>
      </div>
    </div>`);

  if(resultado.options.isEdit === 0)
  {
    $("#edit-M").hide();
    $("#delete-M").hide();
    $("#M-edit-M").hide();
    $("#M-delete-M").hide();
  }
  else
  {
    $("#edit-M").show();
    $("#delete-M").show();
    $("#M-edit-M").show();
    $("#M-delete-M").show();
  }
  //evento de scroll en el contenedor de los eventos para mover las reglas
  cont.find("#scroll").on("scroll", GanttH.moveTags);
  //Dibuja una sombra en el contenedor de los controles basandose en la posición del scroll
  $("#contGanttH #scroll").on("scroll", GanttH.drawVHLines);
  //Events scroll (para mover las etiquetas con el scroll) and click in divs.event (para ver los datos en el modal)
  $("#contGanttH #scroll").on("scroll", GanttH.moveElem);
  //Scroll para mostrar las flechas de ir al final o al principio
  $("#contGanttH #scroll").on("scroll", GanttH.showScrollButtons);
  //evento para mostrar modal en las cajas de evento normal
  // $(".presentation").on("click.GanttH", ".event:not(.group-events)", GanttH.showModal);
  //Click para los eventos en el popover
  // $(".inner-cont").on("click.GanttH", ".event:not(.group-events)", GanttH.showModal);

  //mostrar el popover de los grupos de eventos
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
    // Check if pointer events are supported.
    if (window.PointerEvent) {
      // Add Pointer Event Listener
      $("#contGanttH .presentation").on("pointerdown", ".group-events", GanttH.showPopOver);
      // swipeFrontElement.addEventListener('pointerdown', this.handleGestureStart, true);
      // swipeFrontElement.addEventListener('pointermove', this.handleGestureMove, true);
      // swipeFrontElement.addEventListener('pointerup', this.handleGestureEnd, true);
      // swipeFrontElement.addEventListener('pointercancel', this.handleGestureEnd, true);
    } else {
      //para mostar el pop-over como un backdrop en movil
      $("#contGanttH .presentation").on("touchstart", ".group-events", GanttH.showPopOver);
    }
    //abrir o cerrar detalles con desplazamientos horizontales sobre el contenedor .c1 (swipe rigth and left)
    // $(".c1").on('touchstart', GanttH.gesture4Details);
    //Click para los eventos en el popover
    $("#contGanttH .inner-cont").on("click.GanttH", ".event:not(.group-events)", GanttH.showModal);
    $("#contGanttH #controles [role]:not([role='shwFilters'])").on("click.GanttH", GanttH.hideFilters);
    $("#contGanttH .c2").on("touchstart.GanttH", GanttH.hideFilters);
  }
  else
  {
    $("#contGanttH #cont-main").on("mouseenter.GanttH", ".group-events", GanttH.showPopOver);
    //esconder popover cuando se posiciona otro evento
    $("#contGanttH #cont-main").on("mouseenter.GanttH", ".event:not(.group-events)", GanttH.hidePopOver);
    //esconder popover si se posiciona en el div de presentacion
    $("#contGanttH #cont-main").on("mouseenter.GanttH", GanttH.hidePopOver);
    //esconder popover si se deja el div de este
    $("body").off("mouseleave.GanttH", ".pop-over").on("mouseleave.GanttH", ".pop-over", GanttH.hidePopOver);
    //Click para los eventos en el popover
    // $(".inner-cont").on("mouseenter.GanttH", ".event:not(.group-events)", GanttH.triggerShowModal);
    $("body").off("mouseenter.GanttH", ".event:not(.group-events)").on("mouseenter.GanttH", ".event:not(.group-events)", GanttH.triggerShowModal);
    $("body").off("click.GanttH", ".event:not(.group-events)").on("click.GanttH", ".event:not(.group-events)", GanttH.showRulesEv);
    $("#contGanttH .inner-cont").on("mouseleave.GanttH", ".event:not(.group-events)", GanttH.cancelShowModal);
    $("body").off("mouseleave.GanttH", ".event:not(.group-events)").on("mouseleave.GanttH", ".pop-over .event:not(.group-events)", GanttH.cancelShowModal);
    $("body").on("mouseleave.GanttH", "#eventsModal", GanttH.hideModal);

    $("#contGanttH #controles [role]:not([role='shwFilters'])").on("click.GanttH", GanttH.hideFilters);
    $("#contGanttH #cont-filtros").on("mouseenter.GanttH", GanttH.cancelHideFilters);
    $("#contGanttH #cont-filtros").on("mouseleave.GanttH", GanttH.triggerHideFilters);

    $("#contGanttH #details").on("mouseenter.GanttH", GanttH.cancelHideDet);
    //Evento que cierra los detalles cuando el mouse sale del contenedor de derails
    // $("#details").on("mouseleave.GanttH", GanttH.triggerHideDet);
  }

  //boton añadir a lista de detalles, toma la información del modal y la copia en el contenedor de detalles
  $("#append-details, #M-append-details").on("click.GanttH", GanttH.appendDetail);
  $("body").off("dblclick.GanttH").on("dblclick.GanttH", ".event:not(.group-events)", GanttH.appendDetailEv);
  //borrar evento de lista de detalles
  $(".details").on("click.GanttH", ".detail .delD", GanttH.deleteDet);
  //ir a un evento desde la lista de detalles
  $(".details").on("click.GanttH", ".detail .gotoEvD", GanttH.goToEvent);
  //borrar todos los eventos de la lista de detalles
  $("#clearDetBtn").on("click.GanttH", GanttH.deleteDetAll);
  //tooltip para el boton
  $("#searchDate").tooltip({title: "Ir a..."});
  //calendario para posicionarse en el gantt

  gestionDRP.rangeDRP($("#searchDate"), "#contenedorHistorico_Gantt", resultado.options.start, resultado.options.end);
  
  //evento cuando aplica la opcion del calendario
  $("#searchDate").on("apply.daterangepicker", GanttH.searchDate);

  //altura contenedor
  height = cont.find("#cont-main").height();
  //altura cajas de cuadricula
  box_v = height/resultado.options.v_length;

  //concuerda con el css .time-item para que la altura minima sea 1em
  if( box_v < parseInt($(1).toPx()) )
  {
    box_v = parseInt($(1).toPx());
  }

  //ancho contenedor
  width = cont.find("#cont-main #scroll").width();
  //altura cajas de cuadricula
  //Se resta 18 px para evitar el scroll horizontal cuando los datos que lleguen
  //se ajusten a la pantalla
  box_h = (width - 18) / resultado.options.h_length;

  //concuerda con el css .time-item para que la altura minima sea 1em
  if( box_h < parseInt($(2.2).toPx()) )
  {
    box_h = parseInt($(2.2).toPx());
  }

  size.height = box_v;
  size.width = box_h;
  // Decisión para la primer escala en los controles, de acuerdo a esto llama a la función correspondiente
  switch(resultado.options.scale)
  {
    case "Year":
      GanttH.yearScale(resultado, size, cont, parameters.init);
      break;

    case "Month":
      GanttH.monthScale(resultado, size, cont, parameters.init);
      break;

    case "Week":
      GanttH.weekScale(resultado, size, cont, parameters.init);
      break;

    case "Day":
      GanttH.dayScale(resultado, size, cont, parameters.init);
      break;

    default:
      throw new Error('Unknown Scale Option');
  }

  GanttH.drawScrollBtns(cont, parameters.config);
  GanttH.getColsFilters(parameters);
}

/* Las funciones de cada escala toman como referencia las fechas de inicio y fin globales para crear el intervalo
  de dibujo de los eventos (int)*/

/* yearScale */
/* Escala de años con diferentes opciones de zoom, es decir se puede observar
  en años y meses, el intervalo se toma en Meses */
GanttH.yearScale = function(resultado, size, cont, init) {
  //start-end
  var mStart = moment(resultado.options.start);
  var mEnd = moment(resultado.options.end);
  var int = moment.duration(mEnd.diff(mStart));//cantidad de cajas a dibujar
  
  switch(resultado.options.zoom)
  {
    case "month":
      int = int.asMonths();
      GanttH.drawBoxes(mStart, size, int, cont, 1, resultado.data, resultado.options.scale, resultado.options.zoom, init);
      break;

    default:
      throw new Error('Unknown Zoom Option');
  }
}

/* monthScale */
/* Escala de meses con las opciones de mostrar por semana, el intervalo se toma en semanas */
GanttH.monthScale = function(resultado, size, cont, init) {
  //start-end
  var mStart = moment(resultado.options.start);
  var mEnd = moment(resultado.options.end);
  var int = moment.duration(mEnd.diff(mStart));//cantidad de cajas a dibujar

  switch(resultado.options.zoom)
  {
    case "week":
      int = int.asWeeks();
      GanttH.drawBoxes(mStart, size, int, cont, 1, resultado.data, resultado.options.scale, resultado.options.zoom, init);
      break;

    default:
      throw new Error('Unknown Zoom Option');
  }
}

/* weekScale */
/* Escala de semana con las opciones de mostrar por día, el intervalo se toma en dias */
GanttH.weekScale = function(resultado, size, cont, init) {
  //start-end
  var mStart = moment(resultado.options.start);
  var mEnd = moment(resultado.options.end);
  var int = moment.duration(mEnd.diff(mStart));//cantidad de cajas a dibujar

  switch(resultado.options.zoom)
  {
    case "day":
      int = int.asDays();
      GanttH.drawBoxes(mStart, size, int, cont, 1, resultado.data, resultado.options.scale, resultado.options.zoom, init);
      break;

    default:
      throw new Error('Unknown Zoom Option');
  }
}

/* dayScale */
/* Escala de días con las opciones de horas, 30min, 15min, 5min, 1min, el intervalo se toma en horas y minutos */
GanttH.dayScale = function(resultado, size, cont, init) {
  //start-end
  var mStart = moment(resultado.options.start);
  var mEnd = moment(resultado.options.end);
  var int = moment.duration(mEnd.diff(mStart));//cantidad de cajas a dibujar

  switch(resultado.options.zoom)
  {
    case "hour":
      int = int.asHours();
      GanttH.drawBoxes(mStart, size, int, cont, 1, resultado.data, resultado.options.scale, resultado.options.zoom, init);
      break;

    case "hHour":
      int = int.asHours()*2;
      GanttH.drawBoxes(mStart, size, int, cont, 2, resultado.data, resultado.options.scale, resultado.options.zoom, init);
      break;

    case "qHour":
      int = int.asHours()*4;
      GanttH.drawBoxes(mStart, size, int, cont, 4, resultado.data, resultado.options.scale, resultado.options.zoom, init);
      break;

    case "5m":
      int = int.asMinutes()/5;
      GanttH.drawBoxes(mStart, size, int, cont, 12, resultado.data, resultado.options.scale, resultado.options.zoom, init);
      break;

    case "1m":
      int = int.asMinutes();
      GanttH.drawBoxes(mStart, size, int, cont, 60, resultado.data, resultado.options.scale, resultado.options.zoom, init);
      break;

    default:
      throw new Error('Unknown Zoom Option');
  }
}

/* appendOptions */
/* crea las opciones a partir del objeto GanttH.controls que las contiene y las renderiza en selectores 
   también las renderiza cuando hay algún cambio en dichos selectores */
GanttH.appendOptions = function(e, escala, zoom, length, shw) {
  var $controls = $("#controles");
  var $select;
  var opt;
  var aZoom;
  var val;
  var obj;
  var rules;
  //el control de el numero de recursos se le ponen las opciones en la funcion drawBoxes

  /* decision para hacer el dibujo inicial, o realizar un renderizado a petición del usuario por medio del evento de
  cambio de algun selector */
  if(e===0) //dibujo inicial
  {
    //encuentra el selector de la escala (Año, Mes, Semana, Dia)
    $select = $controls.find("div[role='escala'] select").empty();
    //dibuja las opciones
    for(var i=0; i<GanttH.controls.length; i++)
    {
      $select = $controls.find("div[role='escala'] select");
      opt=`<option value="${GanttH.controls[i].scale}">${GanttH.controls[i].label}</option>`;
      $select.append(opt);
      //selecciona la opcion del primer selector y busca los valores del segundo selector (zoom) y dibuja las opciones
      if(escala===GanttH.controls[i].scale)
      {
        $select.val(escala);
        aZoom = GanttH.controls[i].zoom;
        $select = $controls.find("div[role='zoom'] select");
        for(var j=0; j<aZoom.length; j++)
        {
          opt=`<option value="${aZoom[j].val}">${aZoom[j].label}</option>`;
          $select.append(opt);
          //selecciona la opcion del segundo selector y dibuja las opciones del tercer selector (length)
          if(zoom===aZoom[j].val)
          {
            $select.val(zoom);
            $select = $controls.find("div[role='length'] select");
            for(var k=0; k<aZoom[j].length.length; k++)
            {
              opt=`<option value="${aZoom[j].length[k]}">${aZoom[j].length[k]}</option>`;
              $select.append(opt);
              //selecciona la opcion del tercer selector
              if(length === aZoom[j].length[k])
              {
                $select.val(length);
              }
            }
            $select = $controls.find("div[role='zoom'] select");
          }
        }

        //dibujo opciones reglas
        rules = GanttH.controls[i].rules;
        $select = $controls.find("div[role='shwDt'] select");
        $select.empty();
        for(var j=0; j<rules.length; j++)
        {
          if(shw === 1)
          {
            $select.append(`<option title=' ' value='#${rules[j].val}' selected>${rules[j].label}</option>`);
          }
          else
          {
            $select.append(`<option title=' ' value='#${rules[j].val}'>${rules[j].label}</option>`);
          }
        }
        //todo esta configurado desde los 'data-*' presentes en el elemento
        $select.selectpicker();
        $select.next().removeAttr("title");
      }
    }
  }
  else if($(e.currentTarget).parent().attr('role') === "escala") //cambio selector de escala
  {
    $select = $controls.find("div[role='zoom'] select");
    $select.empty();
    val = e.currentTarget.value
    obj = GanttH.controls.find( x => x.scale === val );
    aZoom = obj.zoom;
    for(var j=0; j<aZoom.length; j++)
    {
      opt=`<option value="${aZoom[j].val}">${aZoom[j].label}</option>`;
      $select.append(opt);
      if(j==0)
      {
        $select = $controls.find("div[role='length'] select");
        $select.empty();
        for(var k=0; k<aZoom[j].length.length; k++)
        {
          opt=`<option value="${aZoom[j].length[k]}">${aZoom[j].length[k]}</option>`;
          $select.append(opt);
        }
        $select = $controls.find("div[role='zoom'] select");
      }
    }
    //dibujo opciones reglas
    rules = obj.rules;
    $select = $controls.find("div[role='shwDt'] select");
    $select.empty();
    for(var j=0; j<rules.length; j++)
    {
      $select.append(`<option title=' ' value='#${rules[j].val}' selected>${rules[j].label}</option>`);
    }
    $select.selectpicker('refresh');
  }
  else if($(e.currentTarget).parent().attr('role') === "zoom") //cambio selector zoom
  {
    $select = $controls.find("div[role='escala'] select");
    val = $select.val();
    obj = GanttH.controls.find( x => x.scale === val );
    val = e.currentTarget.value
    aZoom = obj.zoom.find( x => x.val === val );
    $select = $controls.find("div[role='length'] select");
    $select.empty();
    for(var k=0; k<aZoom.length.length; k++)
    {
      opt=`<option value="${aZoom.length[k]}">${aZoom.length[k]}</option>`;
      $select.append(opt);
    }
  }

  //$("#cont-main").trigger("ready.GanttH");
}

/* drawBoxes */
/* dibuja la cuadricula y las etiquetas de cada caja, además de dibujar las reglas */
GanttH.drawBoxes = function(mStart, size, int, cont, escala, data, opt, zoom, init) {

  var height;
  var width = size.width;

  var lastDate = mStart.clone();
  var lastWeek = mStart.clone();
  var lastDay = mStart.clone();
  var prevWidth = new Array();//elemento para almacenar la altura del objeto HTML anterior
  var prevLeft = new Array();//elemento para almacenar la posicion del objeto HTML anterior
  var conf = new Object();//objeto de configuración para visualización de fechas con la libreria momentJS
  //regla recursos
  var $ruleResource = $("#col-resource");
  //nombres de los recursos
  var lRecursos;
  //eventos de cada recurso
  var events;

  //opciones recursos
  var opts = "";

  GanttH.mStart = mStart;
  GanttH.escala = escala;
  GanttH.opt = opt;
  GanttH.width = size.width;
  //Decisión para identificar la opción de la escala y configurar los parametos de la libreria momentJS
  switch(opt)
  {
    case "Year":
      conf.type = "months";
      conf.format = "MMM";
      conf.label = "YYYY";
      conf.comp = "year()";
      conf.zoom = zoom;
      conf.wLabel = "WW";
      conf.yTooltip = "YYYY";
      conf.tooltip = "MMMM";
      if(int < 12)
      {
        int = 12;
      }
      break;

    case "Month":
      conf.type = "weeks";
      conf.format = "ddd DD";
      conf.label = "YYYY-MMM";
      conf.comp = "format('YYYY-MM')";
      conf.wComp = "format('WW')";
      conf.zoom = zoom;
      conf.wLabel = "WW";
      conf.yTooltip = "MMMM YYYY";
      conf.tooltip = "dddd DD";
      if(int < 8)
      {
        int = 8;
      }
      break;

    case "Week":
      conf.type = "days";
      conf.format = "ddd DD";
      conf.label = "YYYY-MMM";
      conf.comp = "format('YYYY-MM')";
      conf.wComp = "format('WW')";
      conf.zoom = zoom;
      conf.wLabel = "WW";
      conf.yTooltip = "MMMM YYYY";
      conf.tooltip = "dddd DD";
      if(int < 7)
      {
        int = 7;
      }
      break;

    case "Day":
      conf.type = "hours";
      conf.format = "HH:mm";
      conf.label = "YYYY-MMM";
      conf.comp = "format('YYYY-MM')";
      conf.wComp = "format('WW')";
      conf.dComp = "format('DD')";
      conf.zoom = zoom;
      conf.wLabel = "WW";
      conf.dLabel = "ddd DD";
      conf.yTooltip = "MMMM YYYY";
      conf.dTooltip = "dddd DD";
      if(int < 16)
      {
        int = 16;
      }
      break;

    default:
      throw new Error('Unknown Option Boxes');
  }

  $(".gantt").css({width: `${(int*width)}px`}); //int * box_h


  if($("#scroll")[0].offsetWidth < $("#scroll")[0].scrollWidth)
  {

    $("#row-date .date").css({width: `${((int+1)*width)}px`}); //int * box_h
    $("#row-week .week").css({width: `${((int+1)*width)}px`}); //int * box_h
    $("#row-day .day").css({width: `${((int+1)*width)}px`}); //int * box_h
    $("#row-time .time").css({width: `${((int+1)*width)}px`}); //int * box_h

  }

  $("#row-date").append(`<div class="header"></div>`);
  $("#row-week").append(`<div class="header"></div>`);
  $("#row-day").append(`<div class="header"></div>`);
  $("#row-time").append(`<div class="header"></div>`);

  var $ruleDate = $("#row-date");
  var $ruleWeek = $("#row-week");
  var $ruleDay = $("#row-day");

  var days = ["L","M","X","J","V","S","D"];
  
  if(data.length != 0){

    // lRecursos = data.map( x => rtn={recurso: x.recurso, shortL: x.shortL} );
    lRecursos = data.map( x => x.recurso);
    lRecursos = lRecursos.filter( GanttH.onlyUnique );
    //Se agrega esta función que organiza los recursos por orden alfabetico
    lRecursos = lRecursos.sort();

    //Se calcula la altura del contenedor donde se pintara el gantt quitando la altura de las reglas
    //y la altura del scroll

    //other rules and hide or show rules
    if(opt == "Year")
    {
      //para la escala de año, deja visibles solo las reglas necesarias
      if(conf.zoom == "year" || conf.zoom == "month")
      {
        $ruleWeek.hide();
        $ruleDay.hide();
      }
    }
    else if(opt == "Month" || opt == "Week")
    {
      $ruleWeek.show();
      $ruleDay.hide();
    }
    else if(opt == "Day")
    {
      $ruleWeek.show();
      $ruleDay.show();
    }
    if(init || isNaN(GanttH.resultado.options.v_length)){
      size.height = 46;
    }
    else
    {
      size.height = ($(".inner-cont").height() - 8) / GanttH.resultado.options.v_length;
      size.height = size.height < 46 ? 46 : size.height;
    }
    
    height = size.height;
    
    //dibuja las lineas horizontales de la cuadricula dependiendo de la cantidad enviada en v_length y pone los recursos
    for(var i = 0; i<lRecursos.length; i++)
    {
      //line-h of the grid
      cont.find(".rules").append("<div class='rule'></div>");
      //resource-rule
      cont.find(".recursos").append(`
        <div class="recursos-item" style="height: ${height}px" data-toggle="tool_recurso" title="${lRecursos[i]}"
        data-resource="${lRecursos[i]}">
        </div>
      `);
      cont.find(".recursos").children().last().append(`
        <span class="item r-item">
          ${data.find(x=> x.recurso === lRecursos[i]).shortL}
        </span>
      `);

      events = data.filter( x => x.recurso === lRecursos[i] );
      
      // GanttH.drawEvents = function(mStart, size, int, $recurso, escala, data, opt){
      GanttH.drawEvents(mStart, size, int, cont.find(".recursos .recursos-item").last(), escala, events, opt, lRecursos[i]);

      if(GanttH.resources === "")
      {
        opts = `${opts}
          <option title=" " value="${lRecursos[i]}" selected>${lRecursos[i]}</option>`;
      }
      else
      {
        //Para volver a poner como seleccionado
        if( $.inArray(lRecursos[i], GanttH.showR) > -1 || GanttH.showR === "")
        {
          opts = `${opts}
          <option title=" " value="${lRecursos[i]}" selected>${lRecursos[i]}</option>`;
        }
        else
        {
          opts = `${opts}
          <option title=" " value="${lRecursos[i]}">${lRecursos[i]}</option>`;
        }
      }
    }
  }

  cont.find("#shwResources").append(opts);

  GanttH.resources = lRecursos;

  //configura altura de las reglas horizontales y verticales
  $(".rule").css({height: `${height}px`});
  $(".rule").last().css({height: `${height-1}px`});

  if(opt === "Month" && mStart.day() !== 0)
  {
    GanttH.brecha = (mStart.day()-1)*width/7;
  }

  GanttH.drawVRules(width,int);

  $(`#shwResources`).selectpicker();

  /***************-----------------¿se puede poner en una funcion? para optimizar----------------------------------******/
  for(var i = 0; i<int; i++)
  {
    // //line-h of the grid
    // cont.find(".rules").append("<div class='rule'></div>");
    //time-rule
    //La suma funciona de acuerdo a @i divido entre @escala y el tipo de intervalo de tiempo @conf.type (año, mes, semana, hora)
    if(opt != "Day" && opt != "Month")
    {
      cont.find(".time").append(`<div class="time-item" style="width: ${width}px">
        <div data-toggle="tooltip" class="item" title="${mStart.add(i/escala, conf.type).format(conf.tooltip).toUpperCase()}"> 
          <span class="t-item">${mStart.format(conf.format).toUpperCase()}</span>
        </div>
      </div>`);
    }
    else if (opt == "Month")
    {
      mStart.add(i/escala, conf.type);
      //dibujar L-M-X-J-V-S-D
      cont.find(".time").append(`<div class="time-item" style="width: ${width}px">
        </div>`);

      for(var day of days)
      {
        cont.find(".time .time-item").last().append(`<span class="m-item" style="display: inline-block; width: ${100/7}%">${day}</span>`);
      }
    }
    else
    {
      cont.find(".time").append(`<div class="time-item" style="width: ${width}px">
          <span class="item t-item">${mStart.add(i/escala, conf.type).format(conf.format).toUpperCase()}</span>
        </div>`);
    }

    //dibuja las etiquetas de la fecha en la parte izquierda
    if( i == 0 )
    {
      
      //(1) year-month rule
      prevWidth[0] = 0;
      prevLeft[0] = 0;
      if(opt == "Year")
      {
        cont.find("#row-date .date").append(`
          <div class="vertical-date" style="top: 0px; width: ${width}px">
            <div data-toggle="tooltip" class="date-text" title="${mStart.format(conf.yTooltip).toUpperCase()}">
              <span>
                ${mStart.format(conf.label)}
              </span>
            </div>
          </div>`
        );
      }
      else
      {
        cont.find("#row-date .date").append(`
          <div class="vertical-date" style="top: 0px; width: ${width}px">
            <div data-toggle="tooltip" class="date-text" title="${mStart.format(conf.yTooltip).toUpperCase()}">
              <span>
                ${mStart.format(conf.label).split("-")[1].toUpperCase()}
                ${mStart.format(conf.label).split("-")[0]}
              </span>
            </div>
          </div>`
        );
      }

      //other rules and hide or show rules
      if(opt == "Year")
      {
        //para la escala de año, deja visibles solo las reglas necesarias
        if(conf.zoom == "year" || conf.zoom == "month")
        {
          $ruleWeek.find(".week").empty();
          $ruleDay.find(".day").empty();
        }
      }
      else if(opt == "Month" || opt == "Week")
      {
        //para la escala de semana, deja visibles solo las reglas necesarias
        $ruleWeek.find(".week").empty();
        $ruleDay.find(".day").empty();
        prevWidth[1] = 0;
        prevLeft[1] = 0;
        //Revisar el dia en que empieza el Gantt
        if(opt === "Month" && mStart.day() !== 0)
        {
          cont.find("#row-week .week").append(`<div class="vertical-week" style="width: ${(8-mStart.day())*width/7}px">
            <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
              <span>SEM. ${mStart.format(conf.wLabel)}</span>
            </div>
          </div>`);
          cont.find(".time .time-item").first().css({width: `${(8-mStart.day())*width/7}px`});
          cont.find(".time .time-item .m-item").slice(0,mStart.day()-1).remove();
          cont.find(".time .time-item .m-item").css({width: `${100/(8-mStart.day())}%`});
        }
        else
        {
          cont.find("#row-week .week").append(`<div class="vertical-week" style="width: ${width}px">
            <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
              <span>SEM. ${mStart.format(conf.wLabel)}</span>
            </div>
          </div>`);
        }
      }
      else if(opt == "Day")
      {
        //para la escala de dias, mostrar las reglas necesarias
        $ruleWeek.find(".week").empty();
        $ruleDay.find(".day").empty();
        //week-rule
        prevWidth[1] = 0;
        prevLeft[1] = 0;
        cont.find("#row-week .week").append(`<div class="vertical-week">
          <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
            <span>SEM. ${mStart.format(conf.wLabel)}</span>
          </div>
        </div>`);
        cont.find("#row-week .week .vertical-week").last().css({width: `${width}px`});
        //day-rule
        prevWidth[2] = 0;
        prevLeft[2] = 0;
        cont.find("#row-day .day").append(`<div class="vertical-day">
          <div data-toggle="tooltip" class="day-text" title="${mStart.format(conf.dTooltip).toUpperCase()}">
            <span>${mStart.format(conf.dLabel.split(" ")[0]).toUpperCase()}
            ${mStart.format(conf.dLabel.split(" ")[1]).toUpperCase()}</span>
          </div>
        </div>`);
        cont.find("#row-day .day .vertical-day").last().css({width: `${width}px`});
      }
    }
    else
    {
      //realiza la comparación dinámica de acuerdo al objeto de configuracion definido anteriormente en el switch
      if(eval(`lastDate.${conf.comp} == mStart.${conf.comp}`))
      {
        cont.find(".date .vertical-date").last().css({width: `${width*(i+1)-(prevWidth[0]+prevLeft[0])}px`});
      }
      else
      {
        prevWidth[0] = cont.find(".date .vertical-date").last().outerWidth();
        prevLeft[0] = cont.find(".date .vertical-date").last().position().left;
        lastDate = mStart.clone();
        cont.find(".date").append(`<div class="vertical-date" style="width: ${width}px">
          <div data-toggle="tooltip" class="date-text" title="${mStart.format(conf.yTooltip).toUpperCase()}">
            <span>${mStart.format(conf.label).split("-")[1].toUpperCase()}
            ${mStart.format(conf.label).split("-")[0]}</span>
          </div>
        </div>`);
      }
      
      //filtra de acuerdo a la opcion elegida en la escala
      if(opt != "Year")
      {
        if(opt == "Month" || opt == "Week")
        {
          if(eval(`lastWeek.${conf.wComp} == mStart.${conf.wComp}`))
          {
            cont.find(".week .vertical-week").last().css({width: `${width*(i+1)-(prevWidth[1]+prevLeft[1])}px`});
          }
          else
          {
            prevWidth[1] = cont.find(".week .vertical-week").last().outerWidth();
            // prevLeft[1] = cont.find(".week .vertical-week").last().position().left;
            lastWeek = mStart.clone();
            cont.find(".week").append(`<div class="vertical-week" style="width: ${width}px">
              <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
                <span>SEM. ${mStart.format(conf.wLabel)}</span>
              </div>
            </div>`);
          }
        }
        else if(opt == "Day")
        {
          if(eval(`lastWeek.${conf.wComp} == mStart.${conf.wComp}`))
          {
            cont.find(".week .vertical-week").last().css({width: `${width*(i+1)-(prevWidth[1]+prevLeft[1])}px`});
          }
          else
          {
            prevWidth[1] = cont.find(".week .vertical-week").last().outerWidth();
            prevLeft[1] = cont.find(".week .vertical-week").last().position().left;
            lastWeek = mStart.clone();
            cont.find(".week").append(`<div class="vertical-week" style="width: ${width}px">
              <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
                <span>SEM. ${mStart.format(conf.wLabel)}</span>
              </div>
            </div>`);
          }

          if(eval(`lastDay.${conf.dComp} == mStart.${conf.dComp}`))
          {
            cont.find(".day .vertical-day").last().css({width: `${width*(i+1)-(prevWidth[2]+prevLeft[2])}px`});
          }
          else
          {
            //day-rule
            prevWidth[2] = cont.find(".day .vertical-day").last().outerWidth();
            prevLeft[2] = cont.find(".day .vertical-day").last().position().left;
            lastDay = mStart.clone();
            cont.find(".day").append(`<div class="vertical-day" style="width: ${width}px">
              <div data-toggle="tooltip" class="day-text" title="${mStart.format(conf.dTooltip).toUpperCase()}">
                <span>${mStart.format(conf.dLabel.split(" ")[0]).toUpperCase()}
                ${mStart.format(conf.dLabel.split(" ")[1]).toUpperCase()}</span>
              </div>
            </div>`);
          }
        }
      }
    }
    //Se le resta a la fecha inicial lo que se le habia sumado antes
    mStart.add(-i/escala, conf.type);
  }

  //verifica opciones de mostrar u ocultar reglas
  if(typeof(GanttH.show) !== "number" && typeof(GanttH.hide) !== "number")
  {
    for(var i=0; i<GanttH.hide.length; i++)
    {
      $(GanttH.hide[i]).hide();
    }
    for(var i=0; i<GanttH.show.length; i++)
    {
      $(GanttH.show[i]).show();
    }
    var $select = $("div[role='shwDt'] select");
    $select.selectpicker('val',GanttH.show);
  }
  else if(GanttH.show === 0 && GanttH.hide === 0)
  {
    $ruleDate.hide();
    $ruleWeek.hide();
    $ruleDay.hide();
    $ruleTime.show();
    var $select = $("div[role='shwDt'] select");
    $select.selectpicker('val',"#row-time");
  }

  //se le agregan 8px que corresponden al scroll horizontal en el contenedor de los eventos
  if(GanttH.isOverflow($(`#scroll`), 1))
  {
    $("#col-resource .recursos").css({"margin-bottom": "8px"});
  }

  $select = $("#controles").find("div[role='nRecursos'] select").empty();

  //dibuja opciones del control de numero de recursos en pantalla
  var max = lRecursos.length >= 10 ? 10:lRecursos.length;
  //numero de recursos disponibles, maximo 20
  opt=`<option value="NaN">Alto Mínimo</option>`;
  $select.append(opt);
  for(var i=1; i<= max; i++)
  {
    opt=`<option value="${i}">${i}</option>`;
    $select.append(opt);
  }

  if(init){
    //selecciona la opción del número de recursos
    $select.val("NaN");
  }
  else
  {
    //selecciona la opción del número de recursos
    $select.val(GanttH.resultado.options.v_length);
  }
}

/* drawVRules */
/* dibuja las reglas verticales de la cuadricula */
GanttH.drawVRules = function(width, int) {
  var $cont = $(".cont-events .presentation");
  for(var i=0; i<int; i++)
  {
    $cont.append(`<div class="v-rule" style="left: ${width*(i+1)-GanttH.brecha}px"></div>`);
  }

  $cont.append(`<div class="v-rule ev-rule"></div>
    <div class="v-rule ev-rule"></div>`);
}

/* onlyUnique */
/* filtra un arreglo con valores repetidos */
GanttH.onlyUnique = function(value, index, self) {
  return self.indexOf(value) === index;
}

/* drawEvents */
/* dibuja los eventos de cada recurso en una linea horizontal */
GanttH.drawEvents = function(mStart, size, int, $recurso, escala, data, opt, lRecurso){

  // Se quita los espacion del lrecurso para que abra el evento del modal, ya que
  // los ids no permiten espacios
  /*****************************************/ 
  var estado = true;

  while(estado){
    lRecurso = lRecurso.replace(" ","");
    if(lRecurso.indexOf(" ") == -1){
      estado = false;
    }
  }
  
  /*****************************************/ 
  var ancho;
  var pos_agr;

  var $contG;
  var $contG_ev;

  var $lastEl, $currEl;

  /*****************************************/ 

  var cont = $(GanttH.id_cont);
  $(".cont-events .presentation").append(`
    <div class="events-resource" data-resource="${lRecurso}" style="height: ${size.height}px">
    </div>`
  );
  var pos = new Object();
  var num;

  pos.top = $recurso.position().top;
  //dibuja los eventos manteniendo una posicion fija en el top y variando la posición left
  var i=0;
  for(var event of data)
  {
    //Extrae su intervalo de tiempo
    mdStart = moment(event.start);
    mdEnd = moment(event.end);
    //Calcula la diferencia de estos intervalos con la fecha inicial global (options.start)
    pInit = moment.duration(mdStart.diff(mStart));
    pEnd = moment.duration(mdEnd.diff(mStart));

    //Obtiene posición y altura a partir del tiempo y la opción de visualización
    pos.left = GanttH.convertT2Px(escala, size.width, pInit, opt);
    eWidth = GanttH.convertT2Px(escala, size.width, pEnd, opt) - pos.left;

    //Crea el div que contiene cada evento, colocando algunas opciones en atributos data,
    // data-field se usa para reescribir la información cuando los eventos se agrupan, solo toma los dos primeros elementos
    // data-pop es la información que se muestra al hacer click en el evento
    // data-id identificación unica del evento
    $(`.cont-events .presentation .events-resource[data-resource="${lRecurso}"]`).append(`
      <div class="event" data-id="${event.id}"
      data-id_evt="${event.id_evt}"
      data-start="${event.start}"
      data-end="${event.end}"
      data-field='${JSON.stringify(event.data_field)}'
      data-pop='${JSON.stringify(event.data_pop)}'>
      </div>`
    );

    $(`.cont-events .presentation .events-resource[data-resource="${lRecurso}"] .event`).last().css({
      left: `${pos.left + 1}px`,
      height: `${size.height - 10}px`,
      width: `${eWidth - 2}px`,
      "background": event.color
    });

    if(event.color_letra != undefined){
      $(`.cont-events .presentation .events-resource[data-resource="${lRecurso}"] .event`).last().css({
        color: event.color_letra
      });
    }

    num = (size.height-10) <= 16 ? 1:Math.floor( (size.height-10) / 16 );
    if( num > 0){
      GanttH.appendData($(`.cont-events .presentation .event`).last(), event.data_field, num);
    }

    if(i == 0)
    {
      if(eWidth < 9)
      {
        $currEl = $(`.presentation .events-resource[data-resource="${lRecurso}"] .event`).last();

        $contG = $(`.cont-events .presentation  .events-resource[data-resource="${lRecurso}"]`);
        $contG.append(`
          <div class="event group-events" data-target="#group-events-${lRecurso}-${i}" data-group="true">
          </div>
        `);

        $contG_ev = $(`.cont-events .presentation  .events-resource[data-resource="${lRecurso}"] .event`);
        $contG_ev.last().css({
          left: `${pos.left + 1}px`,
          height: `${size.height - 10}px`,
          width: `7px`
        });

        cont.append(`
          <div class="pop-over" id="group-events-${lRecurso}-${i}">
            <div></div>
            <div class="close-po">
              <i class="fas fa-times"></i>
            </div>
          </div>`);
        $currEl = $currEl.detach();
        $currEl.children().slice(1).remove();
        $currEl.addClass("pop-event").css({top: "", height: "", width: "", left: ""});

        idContPop = `group-events-${lRecurso}-${i}`;
        $(`#group-events-${lRecurso}-${i}`).children().first().append($currEl);
      }
    }
    else
    {
      $currEl = $(`.presentation .events-resource[data-resource="${lRecurso}"] .event`).last();
      $lastEl = $(`.presentation .events-resource[data-resource="${lRecurso}"] .event`).last().prev();

      if(eWidth < 9)
      {
        if($lastEl[0].hasAttribute('data-group')){

          if($lastEl.css("border-radius").length == 3)
          {
            eWidth = pos.left - $lastEl.position().left + eWidth;
            pos.left = $lastEl.position().left;
            //configura el evento para ponerlo en el pop over
            $currEl = $currEl.detach();
            $currEl.addClass("pop-event").css({top: "", height: "", width: "", left: ""});
            $currEl.children().slice(1).remove()

            $(`#${idContPop}`).children().first().append($currEl);

            $lastEl.css({
              left: `${pos.left}px`,
              width: `${eWidth}px`
            });
          }
          else
          {
            ancho =  $lastEl.prev().width() + eWidth + 5;
            pos_agr = parseFloat($lastEl.prev().css("left").replace("px","")) + ancho;

            $currEl = $currEl.detach();
            $currEl.addClass("pop-event").css({top: "", height: "", width: "", left: ""});
            $currEl.children().slice(1).remove()

            $(`#${idContPop}`).children().first().append($currEl);

            $lastEl.prev().css({
              width: `${ancho}px`
            });

            $lastEl.last().css({
              left: `${pos_agr}px`
            });
          }
        }
        else if($lastEl[0].offsetWidth <= 10){
          eWidth = pos.left - $lastEl.position().left + eWidth;
          pos.left = $lastEl.position().left;

          $contG = $(`.cont-events .presentation  .events-resource[data-resource="${lRecurso}"]`);
          $contG.append(`
            <div class="event group-events" data-target="#group-events-${lRecurso}-${i}" data-group="true">
            </div>`
          );

          $contG_ev = $(`.cont-events .presentation  .events-resource[data-resource="${lRecurso}"] .event`);
          $contG_ev.last().css({
            left: `${pos.left + 1}px`,
            height: `${size.height - 10}px`,
            width: `${eWidth - 2}px`
          });

          cont.append(`
            <div class="pop-over" id="group-events-${lRecurso}-${i}">
            <div></div>
            <div class="close-po">
              <i class="fas fa-times"></i>
            </div>`
          );
          $currEl = $currEl.detach();
          $lastEl = $lastEl.detach();
          $currEl.children().slice(1).remove()
          $lastEl.children().slice(1).remove()
          $currEl.addClass("pop-event").css({top: "", height: "", width: "", left: ""});
          $lastEl.addClass("pop-event").css({top: "", height: "", width: "", left: ""});

          idContPop = `group-events-${lRecurso}-${i}`;
          $(`#group-events-${lRecurso}-${i}`).children().first().append($lastEl);
          $(`#group-events-${lRecurso}-${i}`).children().first().append($currEl);
        }
        else
        {
          ancho =  $lastEl.width() + eWidth - 1;
          pos_agr = Math.floor(parseFloat($lastEl.css("left").replace("px","")) + ancho);

          $contG = $(`.cont-events .presentation  .events-resource[data-resource="${lRecurso}"]`);
          $contG.append(`
            <div class="event group-events" data-target="#group-events-${lRecurso}-${i}" data-group="true">
            </div>`
          );

          $contG_ev = $(`.cont-events .presentation  .events-resource[data-resource="${lRecurso}"] .event`);

          $lastEl.css({
            width: `${ancho}px`,
            "border-radius": "3px 0 0 3px" 
          });

          $contG_ev.last().css({
            left: `${pos_agr}px`,
            height: `${size.height - 10}px`,
            width: `7px`,
            "border-radius": "0 3px 3px 0"

          });

          cont.append(`
            <div class="pop-over" id="group-events-${lRecurso}-${i}">
              <div></div>
              <div class="close-po">
                <i class="fas fa-times"></i>
              </div>
            </div>`);
          $currEl = $currEl.detach();
          $currEl.children().slice(1).remove();
          $currEl.addClass("pop-event").css({top: "", height: "", width: "", left: ""});

          idContPop = `group-events-${lRecurso}-${i}`;
          $(`#group-events-${lRecurso}-${i}`).children().first().append($currEl);
        }
      }
    }

    i++;
  }

  if($(".pop-over").length > 0)
  {
    $(".close-po").on("touchstart", GanttH.hidePopOver);
  }
  //dibujar flechas para los eventos agrupados
  // GanttH.drawArrows();
}

/* drawArrows */
/* después de dibujados los eventos verifica si hay evento agrupados y les pone una flecha en el lado izquierdo */
GanttH.drawArrows = function(){
  var $groupEv = $(".cont-events .presentation .group-events");
  var pos;
  var height;

  //de acuerdo a la cantidad de eventos tipo grupo se obtiene un posicion y altura para posicionar correctamente las flechas
  for(var i=0; i<$groupEv.length; i++)
  {
    pos = $($groupEv[i]).position().top;
    height = $($groupEv[i]).height();
    $(".cont-events .presentation").prepend(`<div class="arrow-group" style="top: ${pos+(height/2)-7}px"><i class="fas fa-chevron-right"></i></div>`);
  }
  // gestionModal.cerrar();
}

/* drawScrollBtns */
/* dibuja los botones para ir al inicio o al final del scroll, crea los eventos para desplar el scroll */
GanttH.drawScrollBtns = function($cont, config){
  var $target = $("#scroll");
  //draw buttons to scroll start and end
  $target.append(`<div class="button button-up" id="btnUpScroll">
    <i class="fas fa-chevron-up"></i>
  </div>
  <div class="button button-down" id="btnDownScroll">
    <i class="fas fa-chevron-down"></i>
  </div>
  <div class="button button-right" id="btnRightScroll">
    <i class="fas fa-chevron-right"></i>
  </div>
  <div class="button button-left" id="btnLeftScroll">
    <i class="fas fa-chevron-left"></i>
  </div>`);

  //Eventos para el scroll
  $("#btnUpScroll").on("click.GanttH", {scroll: "up"}, GanttH.scroll);
  $("#btnDownScroll").on("click.GanttH", {scroll: "down"}, GanttH.scroll);
  $("#btnRightScroll").on("click.GanttH", {scroll: "right"}, GanttH.scroll);
  $("#btnLeftScroll").on("click.GanttH", {scroll: "left"}, GanttH.scroll);

  if($target[0].offsetHeight == $target[0].scrollHeight)
  {
    $("#btnUpScroll").hide();
    $("#btnDownScroll").hide();
  }

  if($target[0].offsetWidth >= $target[0].scrollWidth)
  {
    $("#btnRightScroll").hide();
    $("#btnLeftScroll").hide();
  }
}

/* isOverflow */
/* verifica si hay overflow o no (scroll), opt para verificar si en X o en Y,
   @opt 0 para Y y 1 para X */
GanttH.isOverflow = function($obj, opt) {
  var scroll;
  var offset;

  switch(opt)
  {
    case 0:
      scroll = $obj[0].scrollHeight;
      offset = $obj[0].offsetHeight;
      break;

    case 1:
      scroll = $obj[0].scrollWidth;
      offset = $obj[0].offsetWidth;
      break;

    default:
      throw new Error("Unknown Option");
  }
  
  if(scroll > offset)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/* appendData */
/* agrega los datos al objeto enviado, se usa para poner datos en los modales y en los eventos agrupados.
   de acuerdo a la estructura Campo: Valor */
GanttH.appendData = function($El, data, length, init=0) {
  $El.empty();
  if(data.length < length)
  {
    length = data.length;
  }
  for(var i=init; i<length; i++)
  {
    line = "<div>";
    if(data[i].Mostrar_Campo === 1)
    {
      line = `${line}<span>${data[i].Campo}: </span>`;
    }
    line = `${line}${data[i].Valor}</div>`;
    $El.append(line);
  }
}

/* drawTagsGroup */
/* dibuja las etiquetas verticales del grupo de eventos, este llamado se ejecuta en un ciclo for */
/*GanttH.drawTagsGroup = function(i, cont, pos, eHeight, data){
  var lHeight;
  //Verificando el campo label de cada objeto se dibuja su etiqueta de grupo
  if(i==0)
  {
    cont.find(".gantt").append(`<div class="vertical-text">
       <span class="label-text" for="${data[i].label.replace(" ","_")}">${data[i].label}</span>
       <!--<span class="label-line"></span>-->
    </div>`);
    cont.find(".vertical-text").last().css({top: `${pos}px`, height: `${eHeight}px`});
  }
  else
  {
    if(data[i].label == data[i-1].label)
    {
       lHeight = pos+eHeight-cont.find(".vertical-text").last().position().top;
       cont.find(".vertical-text").last().css({height: `${lHeight}px`});
    }
    else
    {
       cont.find(".gantt").append(`<div class="vertical-text">
          <span class="label-text" for="${data[i].label.replace(" ","_")}">${data[i].label}</span>
          <!--<span class="label-line"></span>-->
       </div>`);
       cont.find(".vertical-text").last().css({top: `${pos}px`, height: `${eHeight}px`});
    }
  }
}*/

/* convertT2Px */
/* Convierte tiempo a pixeles, dependiendo de la opción seleccionada para la visualización */
GanttH.convertT2Px = function(escala, height, diff, opt) {
  //deacuerdo a la opcion hace la conversion en diferentes escalas de tiempo ( mes, semana, dia, hora )
  switch(opt)
  {
    case "Year":
      return (diff.asMonths()*escala)*height;
      break;

    case "Month":
      return (diff.asWeeks()*escala)*height;
      break;

    case "Week":
      return (diff.asDays()*escala)*height;
      break;

    case "Day":
      return (diff.asHours()*escala)*height;
      break;

    default:
      throw new Error('Unknown Option Boxes');
  }
}

/* drawVHLines */
/* Dibuja una sombras en la parte superior del div que contiene los eventos o en  la parte izquierda
   de acuerdo a la posicion del scroll */
GanttH.drawVHLines = function(e) {
  if($(this).scrollTop() == 0 )
  {
    $(".inner-cont").removeClass("h-line");
  }
  else
  {
    $(".inner-cont").addClass("h-line");
  }

  if($(this).scrollLeft() == 0 )
  {
    $("#col-resource").removeClass("v-line");
    $(".header").removeClass("v-line");
  }
  else
  {
    $("#col-resource").addClass("v-line");
    $(".header").addClass("v-line");
  }
}

/* moveTags */
/* scroll de los contenedores de las reglas */
GanttH.moveTags = function(e) {
  var scroll = $(`#${this.id}`).scrollTop();
  var scrollX = $(`#${this.id}`).scrollLeft();
  $("#col-resource").scrollTop(scroll);
  $("#row-date").scrollLeft(scrollX);
  $("#row-week").scrollLeft(scrollX);
  $("#row-day").scrollLeft(scrollX);
  $("#row-time").scrollLeft(scrollX);
}

/* moveElem */
/* callback de evento scroll, mueve las etiquetas de las reglas dentro de su contenedor */
GanttH.moveElem = function(e) {
  var x = $(`#${this.id}`).scrollLeft();
  var y = $(`#${this.id}`).scrollTop();
  var width = $(`#${this.id}`).width();//-$(".controls").height();
  var sWidth = x+width;//24-> margin top del div
  var $divsDate = $(".vertical-date");
  var $divsWeek = $(".vertical-week");
  var $divsDay = $(".vertical-day");
  var $divsTime = $(".time-item");
  var $divsResource = $(".recursos-item");
  // var $divsLabel = $(".vertical-text");
  var $spanDate;
  var $spanWeek;
  var $spanDay;
  // var $spanLabel;

  var $div;

  if(x !== GanttH.lastScrollLeft)
  {
    //año-mes
    for(var i=0; i<$divsDate.length; i++)
    {
      $div = $($divsDate[i]);
      if($divsDate[i].offsetLeft < x)
      {
        $spanDate = $div.find(".date-text");
        if( x < GanttH.lastScrollLeft && x < $div.width()-$spanDate.width()+$div.position().left)
        {
          //izquierda
          $spanDate.css({left: (x-$divsDate[i].offsetLeft)});
        }
        else if( x > GanttH.lastScrollLeft && ($spanDate.position().left-$div.position().left) < ($div.width()-$spanDate.width()))
        {
          //derecha
          $spanDate.css({left: (x-$divsDate[i].offsetLeft)});
        }
      }
      else
      {
        $spanDate = $div.find(".date-text");
        if( x < GanttH.lastScrollLeft )
        {
          $spanDate.css({left: 0});
        }
      }
    }

    //semana
    for(var i=0; i<$divsWeek.length; i++)
    {
      $div = $($divsWeek[i]);
      if($divsWeek[i].offsetLeft < x)
      {
        $spanWeek = $div.find(".week-text");
        if( x < GanttH.lastScrollLeft && x < $div.width()-$spanWeek.width()+$div.position().left)
        {
            //subir
          $spanWeek.css({left: (x-$divsWeek[i].offsetLeft)});
        }
        else if( x > GanttH.lastScrollLeft && ($spanWeek.position().left-$div.position().left) < $div.width()-$spanWeek.width())
        {
            //bajar
          $spanWeek.css({left: (x-$divsWeek[i].offsetLeft)});
        }
      }
      else
      {
        $spanWeek = $div.find(".week-text");
        if( x < GanttH.lastScrollLeft )
        {
          $spanWeek.css({left: 0});
        }
      }
    }

    //día
    for(var i=0; i<$divsDay.length; i++)
    {
      $div = $($divsDay[i]);
      if($divsDay[i].offsetLeft < x)
      {
         $spanDay = $div.find(".day-text");
         if( x < GanttH.lastScrollLeft && x < $div.width()-$spanDay.width()+$div.position().left)
         {
            //subir
            $spanDay.css({left: (x-$divsDay[i].offsetLeft)});
         }
         else if( x > GanttH.lastScrollLeft && ($spanDay.position().left-$div.position().left) < $div.width()-$spanDay.width())
         {
            //bajar
            $spanDay.css({left: (x-$divsDay[i].offsetLeft)});
         }
      }
      else
      {
        $spanDay = $div.find(".day-text");
        if( x < GanttH.lastScrollLeft )
        {
          $spanDay.css({left: 0});
        }
      }
    }

    //time no funciona en escala mes semanal
    for(var i=0; i<$divsTime.length; i++)
    {
      $div = $($divsTime[i]);
      if($divsTime[i].offsetLeft < x && ($divsTime[i].offsetLeft+$divsTime[i].offsetWidth-16) > x && $divsTime[i].offsetWidth > 80)
      {
         $spanTime = $div.find(".item");
         if( x < GanttH.lastScrollLeft && x < $div.width()-$spanTime.width()+$div.position().left && $spanTime.length != 0)
         {
            //subir
            $spanTime.css({left: (x-$divsTime[i].offsetLeft)});
         }
         else if( x > GanttH.lastScrollLeft && $spanTime.length != 0 && ($spanTime.position().left-$div.position().left) < $div.width()-$spanTime.width())
         {
            //bajar
            $spanTime.css({left: (x-$divsTime[i].offsetLeft)});
         }
      } 
      else
      {
        $spanTime = $div.find(".item");
        if( x < GanttH.lastScrollLeft  && $spanTime.length != 0)
        {
          $spanTime.css({left: 0});
        }
      }
    }
  }

  if(y !== GanttH.lastScrollTop)
  {
    for(var i=0; i<$divsResource.length; i++)
    {
      $div = $($divsResource[i]);
      if($divsResource[i].offsetTop < y && ($divsResource[i].offsetTop+$divsResource[i].offsetHeight-16) > y && $divsResource[i].offsetHeight > 35)
      {
         $spanTime = $div.find(".item");
         if( y < GanttH.lastScrollTop && y < $div.height()-$spanTime.height()+$div.position().top)
         {
            //subir
            $spanTime.css({top: (y-$divsResource[i].offsetTop)});
         }
         else if( y > GanttH.lastScrollTop && ($spanTime.position().top-$div.position().top) < $div.height()-$spanTime.height())
         {
            //bajar
            $spanTime.css({top: (y-$divsResource[i].offsetTop)});
         }
      } 
      else
      {
        $spanTime = $div.find(".item");
        if( y < GanttH.lastScrollTop )
        {
          $spanTime.css({top: 0});
        }
      }
    }
  }

  GanttH.lastScrollLeft = x;
  GanttH.lastScrollTop = y;
}

/* isCollide */
/* función que verifica si los elementos estan sobre puestos o traslapados entre si 
   devuelve falso o verdadero */
GanttH.isCollide = function(el1, el2) {
  if(el1 !== undefined && el2 !== undefined)
  {
    el1.offsetBottom = el1.offsetTop + el1.offsetHeight;
    el1.offsetRight = el1.offsetLeft + el1.offsetWidth;
    el2.offsetBottom = el2.offsetTop + el2.offsetHeight;
    el2.offsetRight = el2.offsetLeft + el2.offsetWidth;

    return !((el1.offsetBottom < el2.offsetTop) ||
             (el1.offsetTop > el2.offsetBottom) ||
             (el1.offsetRight < el2.offsetLeft) ||
             (el1.offsetLeft > el2.offsetRight))
  }
  else
  {
    return "No Data";
  }
}

/* makeId */
/* crea un id aleatorio de tamaño @length */
GanttH.makeId = function(length) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < length; i++)
  {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

/* isEllipsis */
/* calcula overflow en ancho, se usa para los titulos de los modales */
GanttH.isEllipsis = function(elem) {
  return (elem.offsetWidth < elem.scrollWidth);
}

/* readyDraw */
/* callback evento personalizado, despues de cargados los dibujos, 
   verifica el ancho de la pantalla y quita el modal de carga,
   además activa los tooltips */
GanttH.readyDraw = function(e) {
  //verificacion para movil (ancho <= 768px)
  var mediaQuery = window.matchMedia("(max-width: 768px)");
  var opts = ["#row-date", "#row-week", "#row-day", "#row-time"];
  mediaQuery.addListener(GanttH.validateWindow);
  GanttH.validateWindow(mediaQuery);

  $("#row-date .date").css({left: `${Math.round($("#col-resource").width()+9)}px`});
  $("#row-week .week").css({left: `${Math.round($("#col-resource").width()+9)}px`});
  $("#row-day .day").css({left: `${Math.round($("#col-resource").width()+9)}px`});
  $("#row-time .time").css({left: `${Math.round($("#col-resource").width()+9)}px`});

  $("#row-date .header").css({top: `${$("#row-date .date").position().top}px`});
  $("#row-week .header").css({top: `${$("#row-week .week").position().top}px`});
  $("#row-day .header").css({top: `${$("#row-day .day").position().top}px`});
  $("#row-time .header").css({top: `${$("#row-time .time").position().top}px`});

  $(".header").css({width: `${Math.round($("#col-resource").width())}px`});

  $("#btnLeftScroll").css({left: `${Math.round($("#col-resource").width()+12)}px`});

  if($(".header:visible").length > 0)
  {
    switch($(".header:visible").length)
    {
      case 0:
        $("#btnUpScroll").css({top: "0.1rem"});
      break;

      case 1:
        $("#btnUpScroll").css({top: "1rem"});
      break;

      case 2:
        $("#btnUpScroll").css({top: "1.9rem"});
      break;

      case 3:
        $("#btnUpScroll").css({top: "2.8rem"});
      break;

      case 4:
        $("#btnUpScroll").css({top: "3.7rem"});
      break;
    }
  }

  $(".event.group-events").empty();
  if($(`[data-id="${GanttH.rulesId}"].event`).length !== 0)
  {
    GanttH.showRulesEv({currentTarget: $(`[data-id="${GanttH.rulesId}"].event`)[0]});
  }

  if(GanttH.datesRules.length !== 0)
  {
    for(var i=0; i<GanttH.datesRules.length; i++)
    {
      GanttH.searchDate(0, {startDate: GanttH.datesRules[i]}, false);
    }
  }

  //activa los tooltip
  $('[data-toggle="tooltip"]').tooltip("dispose");
  $('[data-toggle="tooltip"]').tooltip({
    trigger: "hover",
    container: "#contGanttH",
    placement: "top"
  });

  $('[data-toggle="tool_recurso"]').tooltip("dispose");
  $('[data-toggle="tool_recurso"]').tooltip({
    trigger: "hover",
    offset: '-200,0',
    placement: "top"
  });

  $(window).off('resize.GanttH').on('resize.GanttH', GanttH.adjustColsFilters);

  //Mousewheels, funcion que permite moverse con la rueda del mouse en el div de los selectores.
  //permite desplazarse saltando de filtro en filtro y de manera horizontal
  $(`#cont-filtros>div`).off('mousewheel.GanttH').on('mousewheel.GanttH', GanttH.mousewheelsSelect);

  GanttH.callback();
}

/*Mousewheels selectores, configura el div de los selectores para desplazarse horizontalemente con un delta
 del tamaño de `#${parameters.indice}selectoresId .section`*/
GanttH.mousewheelsSelect = function(event, delta){
  this.scrollLeft -= (delta * $(`#cont-filtros>div .section`).width());
  event.preventDefault();
}

/*----------------------------------------------------------------------------------------*/
/*----------------------------User Interaction--------------------------------------------*/
/*----------------------------------------------------------------------------------------*/

/* showFilters */
/* evento para mostrar el contenedor de los filtros */
GanttH.showFilters = function(e) {
  $filters = $("#cont-filtros");
  $filters.stop().slideToggle(300);
  GanttH.adjustColsFilters();
}

/* showRulesEv */
/* muestras reglas verticales punteadas al hacer clic sobre un evento */
GanttH.showRulesEv = function(e) {
  var $this = $(e.currentTarget);
  var $ruleL = $(".ev-rule").first();
  var $ruleR = $(".ev-rule").last();
  var $el;

  $(".has-rule").removeClass("has-rule");
  $(".has-ev-rule").removeClass("has-ev-rule");
  $this.addClass("has-rule");
  $(".ev-rule").show();

  if($this.parents(".pop-over").length > 0)
  {
    $el = $(`[data-target = "#${$this.parents(".pop-over")[0].id}"]`);
    $el.addClass("has-ev-rule");
    $ruleL.css({left: `${$el.position().left-3}px`});
    $ruleR.css({left: `${$el.position().left+$el.outerWidth()}px`});
  } 
  else
  {
    $ruleL.css({left: `${$this.position().left-3}px`});
    $ruleR.css({left: `${$this.position().left+$this.outerWidth()}px`});
  }
}

/* hideRules */
/* esconde y elimina las reglas de evento y de busqueda de tiempo */
GanttH.hideRules = function(e) {
  $(".has-rule").removeClass("has-rule");
  $(".has-ev-rule").removeClass("has-ev-rule");
  GanttH.rulesId = undefined;
  $(".ev-rule").hide();
  $(".d-rule").remove();
  GanttH.datesRules = new Array();
}

/* triggerShowModal */
/* Inicia settimeout para mostrar el modal */
GanttH.triggerShowModal = function(e) {
  var $this = $(this);
  if ($("body").hasClass("modal-open") && $("#eventsModal:visible").length === 1)
  {
    GanttH.hideModal();
  }
  GanttH.loadModal = setTimeout(GanttH.showModal, GanttH.timeToShw, e, $this);
}

/* cancelShowModal */
/* Cancela settimeout de mostrar el modal */
GanttH.cancelShowModal = function(e) {
  clearTimeout(GanttH.loadModal);
}

/* showModal */
/* callback evento click en los diferentes eventos dibujados. Dibuja el modal con los datos del objeto data_pop,
   verifica además si es movil (width <= 768px) o desktop (width > 768px), si es movil invoca la funcion
   showModalMobile */
GanttH.showModal = function(e, $this) {
  if($(".pop-over:visible"))
  {
    $("body").off("mouseleave.GanttH", ".pop-over");
  }
  //verificacion para movil (ancho <= 768px)
  var mediaQuery = window.matchMedia("(max-width: 768px)");

  //si no es menor a 768px actua de manera normal en desktop
  if(!GanttH.validateWindow(mediaQuery)){
    //identifica el modal
    var $modal = $("#eventsModal");
    
    //coloca atributo data-id para realizar cualquier accion en el modal (editar, eliminar)
    $modal.attr("data-id", $this.attr("data-id"));
    $modal.attr("data-id_evt", $this.attr("data-id_evt"));
    $(".modalC-open").removeClass("modalC-open");
    $(e.currentTarget).addClass("modalC-open");
    var modalHeight;
    var jPop = JSON.parse(e.currentTarget.dataset.pop);
    var $mBody = $modal.find(".modalC-body");
    var line;
    var parent = $this.parents(".pop-over");
    //calcula posiciones del modal
    var top_position = e.pageY;
    var left = e.pageX;
    var scroll = e.currentTarget.offsetTop;
    var scrollX = e.currentTarget.offsetLeft;
    var contWidth = $this.parents(".inner-cont").width();
    var contHeight = $this.parents(".inner-cont").height();

    //Dibuja el titulo del modal
    $mBody.prev(".modalC-header").find("h5").empty().append(jPop[0].Valor);

    //dibuja el cuerpo del modal
    $mBody.empty();
    //drawData in the modal body
    GanttH.appendData($mBody, jPop, jPop.length, 1);

    //si el llamado es de eventos dentro del pop-over (eventos agrupados), calcula la posicion de acuerdo al evento tipo grupo
    if(parent.length > 0)
    {
      // $(".modalC-open").removeClass("modalC-open");
      // top = $(`[data-target='#${parent.attr("id")}']`).offset().top;
      // left = $(`[data-target='#${parent.attr("id")}']`).offset().left;
      scroll = $(`[data-target='#${parent.attr("id")}']`)[0].offsetTop;
      scrollX = $(`[data-target='#${parent.attr("id")}']`)[0].offsetLeft;
      // $(`[data-target='#${parent.attr("id")}']`).addClass("modalC-open");
    }

    //muestra el modal
    // $modal.modal({backdrop: false, show: true});
    $modal.modal({backdrop: false});

    mHeight = $modal.find(".modalC-dialog").height();
    mWidth = $modal.find(".modalC-dialog").width();

    //calcula donde posicionar el pop-over de acuerdo a la posicion del evento mouseenter
    if((left+mWidth) >= window.innerWidth)
    {
      //put left
      left -= mWidth;
      left += 40;
    }
    if((top_position+mHeight) >= window.innerHeight)
    {
      //put up
      top_position -= mHeight;
      top_position += 40;
    }

    //posicion el modal
    $modal.css({top : `${top_position-20}px`, left: `${left-20}px`}); //10px del ancho del scroll

    //si hay overflow en el titulo agrega un tooltip si no lo quita
    if( GanttH.isEllipsis($mBody.prev(".modalC-header").find("h5")[0]) )
    {
      $mBody.prev(".modalC-header").find("h5").tooltip("dispose");
      $mBody.prev(".modalC-header").find("h5").tooltip({"title": jPop[0].Valor, trigger: "hover"});
    }
    else
    {
      $mBody.prev(".modalC-header").find("h5").tooltip("dispose");
      $mBody.prev(".modalC-header").find("h5").removeAttr("title data-toggle");
    }
  }
  else
  {
    /*Llama a la funcion de mostrar modal en movil*/
    GanttH.showModalMobile(e);
  }
}

/* hideModal */
/* cancela el timeout de mostrar modal y también esconde el modal */
GanttH.hideModal = function(e) {
  clearTimeout(GanttH.loadModal);
  //identifica el modal
  var $modal = $("#eventsModal");
  $modal.modal("hide");
  $("body").off("mouseleave.GanttH", ".pop-over").on("mouseleave.GanttH", ".pop-over", GanttH.hidePopOver);
}

/* showPopOver */
/* callback de evento mouseenter, muestra div con los eventos agrupados */
GanttH.showPopOver = function(e) {

  var $target = $($(e.currentTarget).data("target"));

  if(e.type === "touchstart" || e.type === "pointerdown")
  {
    e.preventDefault();
  }
  else
  {
    //calcula las posicion del evento y calcula las posicion que tendra inicialmente el pop-over
    var offset = $(this).parents(".inner-cont").offset();
    var contWidth = $(this).parents(".inner-cont").width();
    var contHeight = $(this).parents(".inner-cont").height();
    var popWidth = $target.width();
    var popHeight = $target.height();
    var relativeX = (e.clientX - offset.left)-10;
    var relativeY = (e.clientY - offset.top)+160;
    // var relativeX = e.clientX-100;
    // var relativeY = e.clientY-80;

    //calcula donde posicionar el pop-over de acuerdo a la posicion del evento mouseenter
    if((relativeX+popWidth) >= window.innerWidth)
    {
      //put left
      relativeX -= popWidth;
    }
    if((relativeY+popHeight) >= window.innerHeight)
    {
      //put up
      relativeY -= popHeight;
    }

    //posiciona el pop-voer
    $target.css({top: `${relativeY}px`, left: `${relativeX}px`});
  }
  //esconde los demas pop-over
  $(".pop-over").hide();
  //muestra el pop-over actual
  clearTimeout(GanttH.loadPopOver);
  GanttH.loadPopOver = setTimeout(function(){$target.show();}, GanttH.timeToShw)
}

/* hidePopOver */
/* callback de evento mouseleave, esconde div con los eventos agrupados */
GanttH.hidePopOver = function(e) {
  clearTimeout(GanttH.loadPopOver);
  $(".pop-over").hide();
}

/* refreshGantt */
/* callback de la interacción del usuario con el boton de refrescar,
   recoge los datos de los controles y realiza de nuevo la consulta,
   utilizando como función de callback a GanttH.receiveInfo.
   En el caso de mostrar fecha solo muestra o esconde el div que
   contiene las etiquetas */
GanttH.refreshGantt = function(e) {

  //extrae las opciones de los controles
  var vLength = parseInt($(".controls div[role='nRecursos'] select").val()); 
  var escala = $(".controls div[role='escala'] select").val();
  var zoom = $(".controls div[role='zoom'] select").val();
  var hLength = parseInt($(".controls div[role='length'] select").val());

  var opts = ["#row-date", "#row-week", "#row-day", "#row-time"];
  var $shwDt = $("div[role='shwDt'] select");

  var options = GanttH.resultado.options;
  
  var values;
  var hide;
  var show;
  var top_position;

  
  values = $shwDt.selectpicker('val');
  //extrae opciones del selector de reglas
  hide = opts.filter( x => !values.includes(x));
  show = opts.filter( x => values.includes(x));


  // var $shwR = $("#shwResources");
  // var valR = $shwR.selectpicker('val');
  // GanttH.toggleResource(valR);
  
  // esconde las opciones que se filtran en hide
  if(hide.length > 0)
  {
    for(var i=0; i<hide.length; i++)
    {
      $(hide[i]).hide();
    }
  }

  //muestra las opciones que estan en show
  if(show.length > 0)
  {
    for(var i=0; i<show.length; i++)
    {
      top_position = $(show[i]).show().position().top;
      $(`${show[i]} .header`).css({top: `${top_position}px`});
    }

    switch($(".header:visible").length)
    {
      case 0:
        $("#btnUpScroll").css({top: "0.1rem"});
      break;

      case 1:
        $("#btnUpScroll").css({top: "1rem"});
      break;

      case 2:
        $("#btnUpScroll").css({top: "1.9rem"});
      break;

      case 3:
        $("#btnUpScroll").css({top: "2.8rem"});
      break;

      case 4:
        $("#btnUpScroll").css({top: "3.7rem"});
      break;
    }
  }
  else
  {
    $("#btnUpScroll").css({top: "0.1rem"});
  }

  // guarda en las variables goblales el valor de estos arreglos
  GanttH.hide = hide;
  GanttH.show = show;
  
  //Si las opciones han cambiado dibuje de nuevo todo, y actualice los datos de options
  if(vLength !== options.v_length || escala !== options.scale || zoom !== options.zoom || hLength !== options.h_length)
  {

    GanttH.details = $("#details #cont-dets .detail").detach();
    GanttH.$filtros = $("#cont-filtros").detach();
    GanttH.resultado.options.scale = escala;
    GanttH.resultado.options.zoom = zoom;
    GanttH.resultado.options.h_length = hLength;
    GanttH.resultado.options.v_length = vLength;
    GanttH.resultado.options.showDate = 1;

    var parameters = {
      id_cont:  GanttH.id_cont,
      init:     false,
      config:   GanttH.config,
      replace:  GanttH.replace
    };
    // GanttH.rulesId = $(".has-rule").data("id");

    $(GanttH.id_cont).html(`
      <div class="spinner-position-gantt">
        <div class="spinner-border text-muted"></div>
      </div>
      `
    );

    setTimeout(function(){GanttH.receiveInfo(GanttH.resultado, parameters);}, 500);
  }
}

/* toggleResource */
/* esconde o muestra los diferentes recursos que contiene el gantt */
GanttH.toggleResource = function(valR) {
  if(valR.length != GanttH.resources.length || $(`[data-resource]:hidden`).length != 0)
  {
    GanttH.Fchange = new Array(GanttH.columns.length).fill(1);
    GanttH.hideR = GanttH.resources.filter( x=> !valR.includes(x) );
    for(var val of GanttH.hideR)
    {
      $(`[data-resource='${val}']:visible`).hide();
    }

    GanttH.showR = GanttH.resources.filter( x=> valR.includes(x) );
    for(var val of GanttH.showR)
    {
      $(`[data-resource='${val}']:hidden`).show();
    }

    if($(".rule:visible").length != GanttH.showR.length)
    {
      $(".rule:hidden").slice(0,GanttH.showR.length).show();
    }

    if($(".rule:hidden").length != GanttH.hideR.length)
    {
      $(".rule:visible").slice(0,GanttH.hideR.length).hide();
    }
  }
}

GanttH.showDets = function($el) {
  $("#details").show(300, function() {$("#cont-dets").animate({scrollTop: $el.offsetTop-70}, 500);});
  $("#cont-dets").css({height: `calc(100% - ${$("#cont-dets").prev().outerHeight()}px)`});
}

/* shwDet */
/* muestra el contenedor con los modales que se han añadido
   y mueve los botones del scroll */
GanttH.shwDet = function(e) {
  //si no es menor a 768px actua de manera normal en desktop
  // var id = $(this).data("target");

  $("#details").toggle(300);
  $("#cont-dets").css({height: `calc(100% - ${$("#cont-dets").prev().outerHeight()}px)`});
}

/* appendDetail */
/* añade la información del modal a el contenedor de detalles */
GanttH.appendDetail = function(e) {
  var id = $("#eventsModal").attr("data-id_evt");
  var scroll = 0;
  //verifica si existe ya el evento en detalles
  if($(`[data-id_evt="${id}"].detail`).length === 0)
  {
    //clona los datos del modal
    var $items = $(this).parent().next().clone();
    var title = $(this).prevAll(".modalC-title").text()
    var $target = $($(this).data("target"));
    var dataId = $(this).parents("#eventsModal").attr("data-id");
    var Id_evt = $(this).parents("#eventsModal").attr("data-id_evt");

    if(dataId === undefined)
    {
      dataId = $(this).parents("#evsModalMobile").attr("data-id");
      Id_evt = $(this).parents("#evsModalMobile").attr("data-id_evt");
    }
    //crea un id aleatorio
    id = GanttH.makeId(5);

    //verifica que no exista otro id igual
    while($(`#${id}`).length != 0)
    {
      id = GanttH.makeId(5);
    }

    $(".modalC-open").attr("data-id-detail", `#${id}`);
    //añade los datos del modal en un elemento del contenedor de detalles
    $target.append(`<div class="detail" id="${id}" data-id="${dataId}" data-id_evt="${Id_evt}">
        <div role="delete-d" class="text-right px-1" data-target="#${id}">
          <span class="gotoEvD"><i class="fas fa-calendar-day"></i></span>
          <span class="editEvD"><i class="fas fa-pen"></i></span>
          <span class="delEvD"><i class="far fa-trash-alt"></i></span>
          <span class="delD"><i class="fas fa-times-circle"></i></span>
          <div class="titleD">${title}</div>
        </div>
      </div>`);
    $(".gotoEvD").tooltip("dispose").tooltip({title: "Ir a evento", trigger: "hover"});
    $(".editEvD").tooltip("dispose").tooltip({title: "Editar evento", trigger: "hover"});
    $(".delEvD").tooltip("dispose").tooltip({title: "Eliminar evento", trigger: "hover"});
    $(".delD").tooltip("dispose").tooltip({title: "Quitar de detalles", trigger: "hover"});
    $target.find(`#${id}`).append($items);
    $(`#${id}`).addClass("selectEv");
    GanttH.showDets($(`#${id}`)[0]);
    // scroll = $(`#${id}`)[0].offsetTop;
  }
  else
  {
    //pone efecto de blink en detalles y desplaza el scroll
    $(`[data-id="${id}"].detail`).addClass("selectEv");
    GanttH.showDets($(`[data-id="${id}"].detail`)[0]);
    // scroll = $(`[data-id="${id}"].detail`)[0].offsetTop;
  }
  setTimeout(GanttH.deselectEv, 2000);

  //abre contenedor de detalles

  if(GanttH.resultado.options.isEdit === 0)
  {
    $(".editEvD").hide();
    $(".delEvD").hide();
  }
  else
  {
    $(".editEvD").show();
    $(".delEvD").show();
  }
}

/* appendDetail */
/* añade la información del modal a el contenedor de detalles */
GanttH.appendDetailEv = function(e) {
  var $this = $(this);
  var id = $this.attr("data-id");
  var scroll = 0;
  
  //verifica si existe ya el evento en detalles
  if($(`[data-id="${id}"].detail`).length === 0)
  {
    var jPop = JSON.parse(e.currentTarget.dataset.pop);
    
    var title = jPop[0].Valor;
    var $target = $("#details #cont-dets");
    var dataId = id;

    //crea un id aleatorio
    id = GanttH.makeId(5);

    //verifica que no exista otro id igual
    while($(`#${id}`).length != 0)
    {
      id = GanttH.makeId(5);
    }

    $(".modalC-open").attr("data-id-detail", `#${id}`);
    //añade los datos del modal en un elemento del contenedor de detalles
    $target.append(`<div class="detail" id="${id}" data-id="${dataId}">
        <div role="delete-d" class="text-right px-1" data-target="#${id}">
          <span class="gotoEvD"><i class="fas fa-calendar-day"></i></span>
          <span class="editEvD"><i class="fas fa-pen"></i></span>
          <span class="delEvD"><i class="far fa-trash-alt"></i></span>
          <span class="delD"><i class="fas fa-times-circle"></i></span>
          <div class="titleD">${title}</div>
        </div>
        <div class="modalC-body"></div>
      </div>`);
    $(".gotoEvD").tooltip("dispose").tooltip({title: "Ir a evento", trigger: "hover"});
    $(".editEvD").tooltip("dispose").tooltip({title: "Editar evento", trigger: "hover"});
    $(".delEvD").tooltip("dispose").tooltip({title: "Eliminar evento", trigger: "hover"});
    $(".delD").tooltip("dispose").tooltip({title: "Quitar de detalles", trigger: "hover"});

    //drawData in the modal body
    GanttH.appendData($(`#${id} .modalC-body`), jPop, jPop.length, 1);

    // $target.find(`#${id}`).append($items);
    $(`#${id}`).addClass("selectEv");
    GanttH.showDets($(`#${id}`)[0]);
    // scroll = $(`#${id}`)[0].offsetTop;
  }
  else
  {
    //pone efecto de blink en detalles y desplaza el scroll
    $(`[data-id="${id}"].detail`).addClass("selectEv");
    GanttH.showDets($(`[data-id="${id}"].detail`)[0]);
    // scroll = $(`[data-id="${id}"].detail`)[0].offsetTop;
  }
  setTimeout(GanttH.deselectEv, 2000);

  //abre contenedor de detalles

  if(GanttH.resultado.options.isEdit === 0)
  {
    $(".editEvD").hide();
    $(".delEvD").hide();
  }
  else
  {
    $(".editEvD").show();
    $(".delEvD").show();
  }
}

/* deleteDet */
/* elimina un objeto de la lista de detalles y el atributo de su evento correspondiente */
GanttH.deleteDet = function(e) {
  var id = $(this).parent().data("target");
  $(`[data-id-detail="${id}"]`).removeAttr("data-id-detail");
  $(`${id} .delD`).tooltip("dispose");
  $(id).remove();
}

/* deleteDetAll */
/* elimina todos los objetos de la lista de detalles y todos los
   atributos en sus correspondientes eventos */
GanttH.deleteDetAll = function(e) {
  $("[data-id-detail]").removeAttr("data-id-detail")
  $(".detail").remove();
}

/* searchDate */
/* desplaza el scroll hasta una fecha y hora indicada desde el calendario del daterangepicker */
GanttH.searchDate = function(e, picker, isScroll=true) {
  var $cont = $(".cont-events .presentation");
  //extrae fecha seleccionada
  var date = picker.startDate;
  //diferencia de fechas
  var pInit = moment.duration(date.diff(GanttH.mStart));
  //conversion de tiempo a pixeles
  var scroll = GanttH.convertT2Px(GanttH.escala, GanttH.width, pInit, GanttH.opt);

  $cont.append(`<div class="v-rule d-rule" style="left: ${scroll}px" title="${date.format("YYYY-MM-DD HH:mm")}"></div>`);
  $(".d-rule").tooltip("dispose");
  $(".d-rule").tooltip({trigger: "hover", placement: "right"});
  // $(".d-rule").off('show.bs.tooltip').on('shown.bs.tooltip', GanttH.posTooltip);
  $(".d-rule").show();

  if(isScroll === true)
  {
    GanttH.datesRules.push(date);
    //scroll a esos pixeles
    $("#scroll").animate({scrollLeft: scroll-($("#scroll").outerWidth()/2)}, 500);
  }
}

/* goToEvent */
/* desplaza el scroll hasta un evento especifico, y le agrega efecto de seleccion */
GanttH.goToEvent = function(e) {
  var mediaQuery = window.matchMedia("(max-width: 768px)");
  if(mediaQuery.matches)
  {
    $("#details").toggle(300);
    $(".gotoEvD").tooltip("hide");
  }
  var $ruleL = $(".ev-rule").first();
  var $ruleR = $(".ev-rule").last();
  //ir a evento
  var id = $(this).parents(".detail").data("id_evt");
  var $ev = $(`.event[data-id_evt="${id}"]`);
  var scroll = $ev.position().top;
  var scrollX = $ev.position().left;

  $(".has-rule").removeClass("has-rule");
  $(".has-ev-rule").removeClass("has-ev-rule");
  $ev.addClass("has-rule");
  $(".ev-rule").show();

  var pos;
  if($ev.parents(".pop-over").length != 0)
  {
    $el = $(`[data-target="#${$ev.parents(".pop-over").attr("id")}"]`);
    $el.addClass("has-ev-rule");
    $ruleL.css({left: `${$el.position().left-3}px`});
    $ruleR.css({left: `${$el.position().left+$el.outerWidth()}px`});
    scroll = $(`[data-target="#${$ev.parents(".pop-over").attr("id")}"]`).addClass("selectEv").position().top;
    scrollX = $(`[data-target="#${$ev.parents(".pop-over").attr("id")}"]`).addClass("selectEv").position().left;
    $("#scroll").animate({scrollTop: scroll-100, scrollLeft: scrollX-100}, 300);

    pos = $(`[data-target="#${$ev.parents(".pop-over").attr("id")}"]`).offset();
    pos.top = pos.top; //- $(".inner-cont").offset().top;
    pos.left = pos.left; //- $(".inner-cont").offset().left;

    if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
      $ev.parents(".pop-over").css({left: `0px`});
    }
    $ev.parents(".pop-over").css({left: `${pos.left}px`, top: `${pos.top}px`});
    $ev.parents(".pop-over").show();

    scroll = $ev[0].offsetTop;
    $ev.parent().animate({scrollTop: scroll-10}, 200);
  }
  else
  {
    $("#scroll").animate({scrollTop: scroll-100, scrollLeft: scrollX-100}, 500);
    $ruleL.css({left: `${$ev.position().left-3}px`});
    $ruleR.css({left: `${$ev.position().left+$ev.outerWidth()}px`});
  }
  $ev.addClass("selectEv");
  setTimeout(GanttH.deselectEv, 3000);
}

/* deselectEv */
/* quita la clase que tiene la animación de seleccionar */
GanttH.deselectEv = function() {
  $(".selectEv").removeClass("selectEv");
  $(".pop-over").hide();
}

/* scrollButtons */
/* dibuja los botones y les pone los eventos para ir al principio o al final */
GanttH.showScrollButtons = function(e) {
  var $el = $(this);
  if( $el.scrollTop() == 0 )
  {
    $("#btnUpScroll").hide();
  }
  else 
  {
    $("#btnUpScroll").show();
  }

  if( ($el.scrollTop() + $el.height()) >= $el[0].scrollHeight )
  {
    $("#btnDownScroll").hide();
  }
  else
  {
    $("#btnDownScroll").show();
  }

  if( $el.scrollLeft() == 0 )
  {
    $("#btnLeftScroll").hide();
  }
  else
  {
    $("#btnLeftScroll").show();
  }

  if( ($el.scrollLeft() + $el.width()) >= $el[0].scrollWidth )
  {
    $("#btnRightScroll").hide();
  }
  else
  {
    $("#btnRightScroll").show();
  }
}

/* scroll */
/* scroll hacia arriba, abajo, izquierda, derecha */
GanttH.scroll = function(e) {
  var opt = e.data.scroll;
  switch(opt)
  {
    case "up":
      $("#scroll").animate({scrollTop: 0}, 500);
      break;

    case "down":
      $("#scroll").animate({scrollTop: $("#col-ev").height()}, 500);
      break;

    case "right":
      $("#scroll").animate({scrollLeft: $("#col-ev").width()}, 500);
      break;

    case "left":
      $("#scroll").animate({scrollLeft: 0}, 500);
      break;

    default:
      throw new Error('Unknown Scale Option');
  }
}

/*----------------------------------------------------------------------------------*/
/*----------------------------------------Movil-------------------------------------*/
/*----------------------------------------------------------------------------------*/

/* validateWindow */
/* verifica el ancho de la pantalla, si es menor a 768px que es el llamado
  entonces esconde las reglas secundarias */
GanttH.validateWindow = function(e) {
  var opts = ["#row-date", "#row-week", "#row-day", "#row-time"];
  var values = new Array();

  if (e.matches) {
    /* The viewport width is currently <= 768px */
    values[0] = "#row-time";
    //extrae opciones del selector de reglas
    hide = opts.filter( x => !values.includes(x));

    //esconde las opciones que se filtran en hide
    if(hide.length > 0)
    {
      for(var i=0; i<hide.length; i++)
      {
        $(hide[i]).hide();
      }
    }
    //esconde el contenedor de detalles
    $("#details:visible").hide();
    $("#cont-controls:visible").hide();

    return true;
  }
  else
  {
    /* The viewport width is currently > 768px */
    $("#cont-controls:hidden").show();
    return false;
  }
}

/* showControls */
/* evento disparado por el boton del icono bars para mostrar los controles */
GanttH.showControls = function(e) {
  var $mBody = $("#evsModalMobile:visible").find(".modalC-body");
  var $nav = $("#cont-controls");

  /* cambiar tamaño de modal si esta abierto y los controles se esconden */
  if($("#cont-controls:visible").length === 1)
  {
    $mBody.css({height: GanttH.modalHeight});
  }
  $nav.slideToggle(300, GanttH.controlsToggle);
}

/* controlsToggle */
/* callback del metodo slideToggle de los controles, cambia el tamaño del modal si esta abierto */
GanttH.controlsToggle = function(e) {
  var $nav = $(this);
  var $mBody = $("#evsModalMobile:visible").find(".modalC-body");
  //revisar condicion para saber si esta abierto o cerrado
  if($("#cont-controls:visible").length === 1)
  {
    $mBody.css({height: `${$mBody.height()-$nav.height()}px`});
  }
  // else
  // {
  //   $mBody.css({height: GanttH.modalHeight});
  // }
}

/* showModalMobile */
/* muestra el modal como un backdrop que cubre todo el contenedor principal del gantt */
GanttH.showModalMobile = function(e) {
  $("#cont-controls:visible").slideToggle(300);
  //identifica el modal
  var $modal = $("#evsModalMobile");

  //coloca atributo data-id para realizar cualquier accion en el modal (editar, eliminar)
  $modal.attr("data-id", $(e.currentTarget).attr("data-id"));
  $(".modalC-open").removeClass("modalC-open");
  $(e.currentTarget).addClass("modalC-open");
  var modalHeight;
  var jPop = JSON.parse(e.currentTarget.dataset.pop);
  var $mBody = $modal.find(".modalC-body");
  var line;

  //Dibuja el titulo del modal
  $mBody.prev(".modalC-header").find("h5").empty().append(jPop[0].Valor);

  //dibuja el cuerpo del modal
  $mBody.empty();
  //drawData in the modal body
  GanttH.appendData($mBody, jPop, jPop.length, 1);

  //si hay overflow en el titulo agrega un tooltip si no lo quita
  if( GanttH.isEllipsis($mBody.prev(".modalC-header").find("h5")[0]) )
  {
    $mBody.prev(".modalC-header").find("h5").tooltip("dispose");
    $mBody.prev(".modalC-header").find("h5").tooltip({"title": jPop[0].Valor});
  }
  else
  {
    $mBody.prev(".modalC-header").find("h5").tooltip("dispose");
    $mBody.prev(".modalC-header").find("h5").removeAttr("title data-toggle");
  }

  $modal.modal({backdrop: false, show: true});
  GanttH.modalHeight = `calc(${$("#cont-main").height()-$mBody.prev().height()}px - 0.5rem)`;
  $mBody.css({height: GanttH.modalHeight});
}

/* gesture4Details */
/* Captura el evento de touchstart y touchmove sobre el contenedor de .c1, el desplazamiento 
   que se captura es horizontal, para abrir o cerrar el contenedor de los detalles */
GanttH.gesture4Details = function(e){

  var swipe = e.originalEvent.touches,
  //captura la posicion inicial del movimiento
  start = swipe[0].pageX;

  //captura el evento de touchmove
  $(`${GanttH.id_cont} .c1`).on('touchmove', function(e) {
    var contact = e.originalEvent.touches,
    //posicion final del movimiento
    end = contact[0].pageX,
    //calcula la distancia desplazada
    distance = end-start;

    //abre
    if (distance < -100 && $("#details:visible").length === 0){
      $("#details").show(300);
    }
    else if (distance > 100 && $("#details:visible").length === 1){
      //cierra
      $("#details").hide(300);
    }
  }).one('touchend', function() {
    //pone 1 vez el evento touchend y quita los eventos de touchmove y touchend
    $(this).off('touchmove touchend');
  });
}


/*----------------------------------------------------------------------------------*/
/*------------------------Filtros---------------------------------------------------*/
/*----------------------------------------------------------------------------------*/

/* getColsFilters */
/* hace el llamado al ajax que responde con los datos que se van a filtar, (nombre columna, tipo de dato)*/
GanttH.getColsFilters = function(parameters) {
  //Ajax de los datos
  query.callAjax( GanttH.urlconexion, "ganttH_columns_filters", {nombre: GanttH.nombre}, GanttH.receiveFilters, parameters );
}

/* receiveFilters */
/* callback del ajax que obtiene los nombres de los filtros */
GanttH.receiveFilters = function(resultado, parameters) {
  //verificar los resultados si son diferentes volver a escribir el arreglo, sino dejarlo igual
  var lastSearchs = new Array();
  if(GanttH.columns.length != 0)
  {
    lastSearchs = GanttH.columns.map( x => x.search.value );
  }
  GanttH.columns = new Array();
  for(var field of resultado)
  {
    GanttH.columns.push(new column(field.data, field.type));
  }

  if(lastSearchs.length != 0)
  {
    for(var i=0; i<lastSearchs.length; i++)
    {
      GanttH.columns[i].search.value = lastSearchs[i];
    }
  }
  GanttH.Fchange = new Array(resultado.length).fill(1);
  //dibujar filtros
  GanttH.drawFilters(GanttH.columns, parameters);
}

/* método getFilters */
/* Ejecuta la consulta para traer los campos de los selectores de filtros      */
/* @param parameters, recibe los parametros necesarios para ejecutar la consulta */
/*            en la inicialización desde receiveInfo y cuando se realiza */
/*            una busqueda desde el método filterSel           */
GanttH.getFilters = function(e) {
  /* Ejecuta la consulta para cargar los selectores, de acuerdo a los nombre recibidos */
  var idCol;
  if(e.type=="click")
  {
    idCol = $(e.currentTarget).data("col");
  }
  else if(e.type == "manual")
  {
    idCol = e.idCol;
  }
  else
  {
    idCol = $(this)[0].id.split("_").slice(1);
  }

  var parameters = e.data.parameters;
  parameters.idCol = idCol;
  parameters.eType = e.type;

  if( GanttH.Fchange[idCol] == 1)
  {
    var recursos = $("#shwResources").selectpicker("val");
    var col_filter = "ganttH_filters";

    var data = { 
      col_name:   JSON.stringify(parameters.columns.map( c => c.data )),
      cols:       JSON.stringify(GanttH.columns),
      col:        parameters.columns[idCol].data,
      nombre:     GanttH.nombre,
      replace:    parameters.replace
    };

    gestionModal.alertaBloqueante("Procesando");
    query.callAjax( GanttH.urlconexion, col_filter, data, GanttH.loadFilters, parameters );
    GanttH.Fchange[idCol] = 0;
  } 
  else {
    if(e.type == "click")
    {
      GanttH.openModal(parameters);
    }
  }
}

/* método drawFilters */
/* Dibuja los selectores y botones por primera vez, luego carga de forma dínamica las 
opciones de los selectores */
/* @param respuesta, recibe los datos que devuelve el AJAX que invoca getFilters             */
/* @param parameters, recibe los parametros necesarios para realizar las diferentes acciones       */
/* -------------- Dibujar y cargar selectores en thead>tr.filter ------------------ */
GanttH.drawFilters = function(columns, params) {
  //Para verificar si los selectores existen o no
  var $filters = $(`#controles #cont-filtros div.opciones`); 
  var cut;
  var j = 0;
  var dimColumn = 1;
  var divCol = 12;
  var append = new Array(columns.length).fill(1);
  var txt = "";

  /* Dibujar y cargar selectores, verifica que el Nº de columnas obtenidas son iguales a las columnas
   que se pidieron */
  /*  Dibuje los selectores una vez, no se redibujan  */
  // j = 0;
  params.columns = columns;
  if($filters.length == 0 && params.init === true)
  {
    for(var i = 0; i < columns.length; i++)
    {
      // if( append[i] == 1 && columns[i].data != "%#%SHOW%#%"){
        //Se plotean los selectores vacios para cada columna
        if( (j > 0) && (j%dimColumn == 0) ){
          txt = `${txt}
          </div>`;
        }
        /*section, bloques*/
        if(j%(dimColumn*2) == 0 && j > 0){
          txt = `${txt}</section>`;
        }
        if(j%(dimColumn*2) == 0){
          txt = `${txt}<section class="section">`;
        }
        /**/
        if(j%dimColumn == 0){
          txt = `${txt}
          <div class='row'>`;
        }
        //Selectores
        txt = `${txt}
            <div class='col-${divCol} filter-item px-0'>
              <div class='opciones'  data-col='${i}'>
                  <button class="btn btn-light btn-sm" data-for="${i}">
                    <small>
                      <i class="fas fa-times text-danger"></i>
                      <i class="fas fa-filter"></i>
                    </small>
                  </button>
                  <button class="btn btn-light btn-sm" 
                    data-toggle="modal"
                    data-target="#customFilter${i}" data-col='${i}' 
                    data-backdrop="static" data-keyboard="false">
                      <i class="fas fa-ellipsis-v"></i>
                  </button>
                  <select multiple id='select_${i}' 
                    class='selectpicker' 
                    data-col='${i}'>
                      <option value="ganttH.init"></option>
                  </select>
              </div>
            </div>
        `;

        if((j == columns.length-1) && !( (j > 0) )){
                txt = `${txt}
          </div>
        </section>`;
        }
        j++;
      // }
    }
    $(`#cont-controls #cont-filtros>.slider`).append(txt);

    GanttH.adjustColsFilters();

    for(var i = 0; i < columns.length; i++)
    {
        /* Estilo para selectores */
        gestionBSelect.ganttFilter(`#select_${i}`, columns[i].data);
        $(`#controles div.opciones [data-for]`).hide();
        $(`#select_${i}`).on("show.bs.select", {parameters: params}, GanttH.getFilters);
        $(`#select_${i}`).on("show.bs.select", GanttH.quitListenerFilters);
        $(`#select_${i}`).on("hide.bs.select", GanttH.pushListenerFilters);
        $(`button[data-target="#customFilter${i}"]`).on("click.GanttH", {parameters: params}, GanttH.getFilters);

        //Se remueve el tooltip viejo del selector
        $(`[data-id="select_${i}"]`).removeAttr("title");
      // }
    }

    //Pone los eventos a los selectores y botones la primera vez que se dibujan
    GanttH.filtersReady(params);
    GanttH.drawModal(params);
    // setTimeout(GanttH.verifyLastFilter,100,params);
    setTimeout(function(){$("#cont-main").trigger("ready.GanttH")}, 20);
  }
  else
  {
    $("#cont-filtros").remove()
    $("#cont-controls").append(GanttH.$filtros);

    params.columns = GanttH.columns;
    
    GanttH.receiveDataFiltered(GanttH.eventsFiltered, params);

    setTimeout(function(){$("#cont-main").trigger("ready.GanttH")}, 20);

    GanttH.Fchange = new Array(GanttH.columns.length).fill(1);
  }

  //Se pone el evento del boton que limpia todos los filtros
  $(`#clearBtnFilter`).on("click.GanttH", {parameters: params}, GanttH.cleanFilters);
}

/* método drawModal */
/* La primera ejecución recibe los datos de las columnas, nombre y tipo, dibujando N modales para cada filtro avanzado, */
/* habilitando los diferentes eventos para cada modal */
GanttH.drawModal = function (parameters) {
  
  var opt = {
    number: [ { label: "Es igual a", value: "=" },
          { label: "No es igual a", value: "!=" },
          { label: "Mayor que", value: ">" },
          { label: "Mayor o igual que", value: ">=" },
          { label: "Menor que", value: "<"},
          { label: "Menor o igual que", value: "<=" } ],

    text: [ { label: "Es igual a", value: "=" },
        { label: "No es igual a", value: "!=" },
        { label: "Comienza por", value: "LIKE**/'_%'"},
        { label: "Termina con", value: "LIKE**/'%_'"},
        { label: "Contiene", value: "LIKE**/'%_%'"},
        { label: "No contiene", value: "NOT**/LIKE**/'%_%'"} ]
  }
  
  $("body").find("[data-id='modals']").remove();
  $("body").append("<div data-id='modals'></div>");
  $("body [data-id='modals']").empty();

  for (var i = 0; i < parameters.columns.length; i++)
  {
    var hModal = `
      <div class="modal" id="customFilter${i}">
        <div class="modal-dialog">
          <div class="modal-content">
          
            
            <div class="modal-header">
              <h5 class="modal-title" data-id="modal-title">Mostrar las filas en las cuales: <span class="form-check">${parameters.columns[i].data}</span></h5>
              <button type="button" class="close" data-dismiss="modal" data-id="close_${i}">&times;</button>
            </div>
            
            <div class="modal-body" data-id="modal-body${i}">
            <div class="row">
              <div class="col-4 px-2">
                  <select class="form-control" id="opt_${i}"></select>
                </div>
                <div class="col-6 input-group">
                    <input class="form-control" id="data_${i}" />
                </div>
            <div class="col-2 text-center">
              <a class="cursor_sel" id="add_${i}"><i class="fas fa-plus-circle text-success"></i></a>
            </div>
              </div>
            </div>
            
            <div class="modal-footer" data-id="modal-footer">
              <div class="col-6">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" id="cleanF_${i}">Limpiar Filtro</button>
              </div>
              <div class="col-6 text-right">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id="accept_${i}">Aplicar</button>
              </div>
            </div>
            
          </div>
        </div>
      </div>`;
    
    $("body [data-id='modals']").append(hModal);
    
    //Instrucciones dependiendo el tipo de dato
    if(parameters.columns[i].type == "int" || parameters.columns[i].type == "float")
    {
      for( var j in opt.number )
      {
        $(`#customFilter${i} #opt_${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
      }
    }
    else if(parameters.columns[i].type == "varchar" || parameters.columns[i].type == "nvarchar")
    {
      for( var j in opt.text )
      {
        $(`#customFilter${i} #opt_${i}`).append(`<option value="${opt.text[j].value}">${opt.text[j].label}</option>`);
      }
    }
    else if(parameters.columns[i].type == "date")
    {
      //DateRangePicker en el input
      $(`#contGanttH .daterangepicker`).remove();
      gestionDRP.simpleDRP2($(`[data-id="modals"] #data_${i}`), "YYYY-MM-DD");
      $(`[data-id="modals"] #data_${i}`).val("");
      for( var j in opt.number )
      {
        $(`#customFilter${i} #opt_${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
      }
    }
    else if(parameters.columns[i].type.includes("datetime"))
    {
      //DateRangePicker en el input
      $(`#contGanttH .daterangepicker`).remove();
      gestionDRP.simpleDRP2($(`[data-id="modals"] #data_${i}`), "YYYY-MM-DD HH:mm:ss");
      $(`[data-id="modals"] #data_${i}`).val("");
      for( var j in opt.number )
      {
        $(`#customFilter${i} #opt_${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
      }
    }
    else if(parameters.columns[i].type.includes("time"))
    {
      //DateRangePicker en el input
      $(`#contGanttH .daterangepicker`).remove();
      gestionDRP.simpleDRP2($(`[data-id="modals"] #data_${i}`), "HH:mm:ss");
      $(`[data-id="modals"] #data_${i}`).val("");
      for( var j in opt.number )
      {
        $(`#customFilter${i} #opt_${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
      }
    }

    //Eventos para botones y teclas
    $(`#customFilter${i} #add_${i}`).on('click', {columns: parameters.columns}, GanttH.addCustomFilter);

    $(`#customFilter${i} #accept_${i}`).on('click', {parameters: parameters}, GanttH.searchCustom);

    $(`#customFilter${i} #cleanF_${i}`).on('click', {parameters: parameters}, GanttH.cleanFilters);

    $(`#customFilter${i}`).on('keyup', GanttH.keyupEv);

    $(`#customFilter${i} [data-id="close_${i}"]`).on('click', GanttH.closeModal);
  }
}

/* método openModal */
/* Callback cuando se presiona el botón que muestra el modal */
/* redibuja selectores y guarda la vista inicial */
GanttH.openModal = function(parameters) {
  //Siguientes llamados después de inicialización
  //var idCol = parseInt(e.currentTarget.getAttribute("data-col"));
  GanttH.quitListenerFilters();
  var idCol = parameters.idCol;
  var $modalBody = $(`body [data-id="modal-body${idCol}"]`);
  var $select = $(`.c1 #select_${idCol}`);
  var $selectClone = $select.clone();
  var $selectM = `<select data-id="selectM_${idCol}" class="selectpicker"></select>`;
  //var columns = e.data.parameters.columns;
  var columns = parameters.columns;
  var $selects;

  //Borra el BS Selector y pone un selector normal, además de borra las opcs seleccionadas al momento de clonar
  $selectClone.children().prop("selected",false);
  $modalBody.children().children(".col-6").find(".dropdown.bootstrap-select").remove();
  $modalBody.children().children(".col-6").remove('select').append($selectM);
  $modalBody.children().children(".col-6").find('select').append( $selectClone.children() );

  $selectM = $modalBody.children().children(".col-6").find("select");

  //Aplica estio con BS Select y evento
  gestionBSelect.wrapSelectDTM($selectM);
  $($selectM).off('changed.bs.select').on('changed.bs.select', GanttH.changeSelM);
  $(`#customFilter${idCol} #add_${idCol}`).off('click').on('click', {columns: parameters.columns}, GanttH.addCustomFilter);
  $(`#customFilter${idCol} .fas.fa-minus-circle.text-danger`).parent().off("click.GanttH").on("click.GanttH", GanttH.deleteCustomFilter);

  if(parameters.op == "DENIED"){
    $($selectM).next().prop("disabled", true);
    GanttH.Fchange[idCol] = 1;
  }
  //Para fechas
  if(columns[idCol].type == "date")
  {
    $("#contGanttH .daterangepicker").remove();
    gestionDRP.simpleDRP2($modalBody.find(`input#data_${idCol}`), "YYYY-MM-DD");
  } 
  else if(columns[idCol].type == ("datetime")) 
  {
    $("#contGanttH .daterangepicker").remove();
    gestionDRP.simpleDRP2($modalBody.find(`input#data_${idCol}`), "YYYY-MM-DD HH:mm:ss");
  }
  else if(columns[idCol].type == "time")
  {
    $(`#contGanttH .daterangepicker`).remove();
    gestionDRP.simpleDRP2($modalBody.find(`input#data_${idCol}`), "HH:mm:ss");
  }

  //Estado inicial
  GanttH.$initModalBody = $modalBody.clone();

  //Poner los selectores de la copia en las mismas opciones
  $selects = GanttH.$initModalBody.find(`select#opt_${idCol}`);
  
  for (var i=0; i<$selects.length; i++)
  {
    $selects[i].value = $modalBody.find(`select#opt_${idCol}`)[i].value;
  }

  gestionModal.cerrar();
}

/* método closeModal */
/* Callback cuando se cierra el modal con tecla esc o con click en X, para regresarlo a su estado anterior */
GanttH.closeModal = function(e) {

  var $closeModalBody;
  if( e.type == 'click')
  {
    $closeModalBody = $(e.currentTarget).parent().next();
    $closeModalBody.click();
  }
  else if( e.type = 'keyup')
  {
    $closeModalBody = $(e.currentTarget).find(".modal-body");
  }

  GanttH.pushListenerFilters();
  //Devolver al estado inicial, revisar eventos de los botones agregar y eliminar, y revisar selector
  $closeModalBody.empty().append(GanttH.$initModalBody.children());
}

/* método keyupEv */
/* Callback evento keyup en el modal, key: "esc"(27) cierra el modal, key: "enter"(13) aplica el filtro */
GanttH.keyupEv = function(e) {
  var keyP = e.which;
  switch(keyP) {
    case 13:
      $(e.currentTarget).find("button.btn-primary").last().click();
    break;

    case 27:
      $(e.currentTarget).modal("hide");
      GanttH.closeModal(e);
    break;

    default:
    //Nothing to do
  }
}

/* método changeSelM */
/* Evento que dispara el cambio de un selector en el modal, el cual escribe dentro del input de esa fila */
GanttH.changeSelM = function(e) {
  var $select = $(e.target);
  var selectValue = $select.val();
  $select.parent().prev().val(selectValue); //esto cambia con imask
}

/* método addCustomFilter */
/* Crea una fila en el modal del filtro avanzado para agregar otra opción de filtrado */
GanttH.addCustomFilter = function(e) {
  
  var columns = e.data.columns;
  var idCol = parseInt(e.currentTarget.id.split("_")[1]);
  var $modalBody = $(e.currentTarget).parent().parent().parent();
  var $row = $(e.currentTarget).parent().parent();
  var $btnAdd = $(e.currentTarget).detach();
  var $rowClone = $rowClone = $row.clone();
  var $select = $rowClone.find('select.selectpicker').detach();

  $modalBody.append(`<div class="row">
                <div class="col-12 form-inline py-2">
                      <div class="form-check px-1">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="optfilter${parseInt(e.timeStamp)}" value="AND" checked>Y
                    </label>
                  </div>
                  <div class="form-check px-1">
                    <label class="form-check-label">
                      <input type="radio" class="form-check-input" name="optfilter${parseInt(e.timeStamp)}" value="OR">O
                    </label>
                  </div>
                </div>
            </div>`);

  $rowClone.children(".col-2").empty()
                .append(`<a class="cursor_sel" id="deleteBtnModal"><i class="fas fa-minus-circle text-danger"></i></a>`)
                .append($btnAdd)
                .children("#deleteBtnModal").on("click.GanttH", GanttH.deleteCustomFilter);

  $rowClone.find("div.dropdown").remove();
  $select[0][0].remove();
  $rowClone.children(".col-6").append($select);
  $rowClone.find('input').val("");

  if(columns[idCol].type == "date")
  {
    gestionDRP.simpleDRP2($rowClone.find('input'), "YYYY-MM-DD");
    $rowClone.find('input').val("");
  } 
  else if(columns[idCol].type == "datetime") 
  {
    gestionDRP.simpleDRP2($rowClone.find('input'), "YYYY-MM-DD HH:mm:ss");
    $rowClone.find('input').val("");
  }
  else if(columns[idCol].type == "time") 
  {
    gestionDRP.simpleDRP2($rowClone.find('input'), "HH:mm:ss");
    $rowClone.find('input').val("");
  }

  $modalBody.append($rowClone);

  gestionBSelect.wrapSelectDTM($rowClone.find('select.selectpicker'));
  $rowClone.find('select.selectpicker').on('changed.bs.select', GanttH.changeSelM);

  if( $row.prev().length == 0 && $row.children(".col-2").children().length == 0 )
  {
    $row.children(".col-2").append(`<a class="cursor_sel" id="deleteBtnModal"><i class="fas fa-minus-circle text-danger"></i></a>`)
                 .children("#deleteBtnModal").on("click.GanttH", GanttH.deleteCustomFilter);
  }
}

/* método deleteCustomFilter */
/* Elimina una fila en el modal del filtro avanzado */
GanttH.deleteCustomFilter = function(e) {
  
  var $row = $(e.currentTarget).parent().parent();
  var $btnAdd = $(e.currentTarget).next().detach();

  if( $row.prev().length == 0 )
  {
    $row.next().remove();
    if( $row.next().next().length == 0 && $row.prev().length == 0 ){
      $btnAdd = $row.next().children(".col-2").children().last().detach()
      $row.next().children(".col-2").empty();
      $row.next().children(".col-2").append($btnAdd);
    }
    $row.remove();
  } else {
    $row.prev().remove();
    if( $row.prev().prev().length == 0 && $row.next().length == 0 ){
      $row.prev().children(".col-2").empty();
    }
    $row.prev().children(".col-2").append($btnAdd);
    $row.remove();
  }
}

/* método loadFilters */
/* carga los datos de los filtros que se obtienen de la consulta */
GanttH.loadFilters = function(opts, parametrs){ 
  /* Cargue los selectores con los datos, la carga se hace en cada busqueda */
  var values = new Array();//Extrae las opciones seleccionadas para redibujarlas de este modo
  var idCol = parametrs.idCol;
  var col_name = parametrs.columns[idCol].data;
  var type = parametrs.columns[idCol].type;

  // Primera carga
  if(opts.length != 0 && opts[0][col_name] != "DENIED")
  {
    
    /*Carga cuando se empieza a buscar, se verifica si esta o no seleccionada la opción para 
    mostrarla asi de nuevo*/
    values[idCol] = parametrs.columns[idCol].search.value.split(",");
    $(`#select_${idCol}`).empty();
    
    if( opts.length > 0)
    {
      for(var j = 0; j < opts.length; j++)
      {
        var opt;
        
        //Renderizado de date y datetime
        if(type == "date"){
          opts[j][col_name] = GanttH.renderDate(opts[j][col_name]);
        } else if (type == "datetime"){
          opts[j][col_name] = GanttH.renderDateTime(opts[j][col_name]);
        }

        if(opts[j][col_name] != "")
        {
          //Para volver a poner como seleccionado
          if( $.inArray(`${opts[j][col_name]}`, values[idCol]) > -1)
          {
            opt = `<option value='${opts[j][col_name]}' title=' ' selected>${opts[j][col_name]}</option>`;
          } else {
            opt = `<option value='${opts[j][col_name]}' title=' '>${opts[j][col_name]}</option>`;
          }
          $(`#select_${idCol}`).append(opt);
        }
      }
    } else {
      var opt = "<option value='0' disabled></option>"
      $(`#select_${idCol}`).append(opt);
    }
    //Refresca el selector por medio de la API de selectpicker
    $(`#select_${idCol}`).selectpicker('refresh');

    if(parametrs.eType == "click")
    {
      parametrs.op = "";
      GanttH.openModal(parametrs);
    }
    else
    {
      //Cierra modal
      gestionModal.cerrar();
    }
  } 
  else
  {
    //Cierra modal
    gestionModal.cerrar(); 
    $(`#select_${idCol}`).empty().append("<option></option>");
    $(`#select_${idCol}`).selectpicker('refresh');
    if(parametrs.eType == "click")
    {
      parametrs.op = "DENIED";
      GanttH.openModal(parametrs);
    }
    else
    {
      var label;
      if(opts.length == 0)
      {
        label = "Este filtro no tiene opciones disponibles";
      } else {
        label = "Este filtro no se encuentra disponible por el número de opciones, sólo se puede usar el filtro avanzado"
      }
      setTimeout(GanttH.delayConfirm, 2, label);
      $(`#select_${idCol}`).selectpicker('close');
      GanttH.Fchange[idCol] = 1;
    }
  }
  // setTimeout(GanttH.autoSelectFilters, 500, GanttH.index);
  // GanttH.autoSelectFilters(idCol);
}

/* método filtersReady */
/* Después de dibujados los elementos de los filtros, les asigna los eventos */
GanttH.filtersReady = function (parameters) {
  //Evento para cuando se selecciona una opción
  $("#cont-filtros div.opciones").on('changed.bs.select', {parameters: parameters}, GanttH.searchSel);

  //Evento para la carga del modal
  //$("tr.filter th>div.opciones>button[data-target]").on('click', {parameters: parameters}, GanttH.openModal);

  //Evento limpiar filtro por columna
  $("#cont-filtros div.opciones>button[data-for]").on("click.GanttH", {parameters: parameters}, GanttH.cleanFilters);
}

/* getDataFiltered */
/* funcion que realiza el ajax de los datos que se van a filtrar, envia el arreglo de columnas al webservice
   para que este lo verifique y responda con los ids de los eventos que coinciden */
GanttH.getDataFiltered = function(parameters) {

  var data = {
    n_col:    parameters.columns.length, 
    nombre:   GanttH.nombre,
    replace:  parameters.replace,
    columns:  parameters.columns
  }

  query.callAjax( GanttH.urlconexion, "ganttH_data_filtered", data, GanttH.receiveDataFiltered, parameters );
}

/* receiveDataFiltered */
/* funcion que recibe los ids filtrados, de acuerdo a esto les cambia el estilo a los que coincidan para resaltarlos */
GanttH.receiveDataFiltered = function(resultado, parameters) {
  GanttH.eventsFiltered = resultado;
  if(resultado.length === 0 || resultado[0].id === "ALL" || resultado === "ALL")
  {
    //borrar backdrop si existe si no dejar asi
    $("#col-ev .backdrop").remove();
    $(".event").css({"z-index": 10}).removeClass("filtered");
    setTimeout(function() {$("#cont-main").trigger("ready.GanttH")}, 20);
    $("#clearBtnFilter").hide();
  }
  else
  {
    $("#clearBtnFilter").show();
    GanttH.filterEvents(resultado, parameters);
  }
}

/* filterEvents */
/* cambia el estilo de los eventos que coinciden con los datos filtrados */
GanttH.filterEvents = function(resultado, parameters) {
  var $elP;
  $(".event.filtered").css({"z-index": 10}).removeClass("filtered");

  for(var event of resultado)
  {
    $(`[data-id_evt="${event.id}"].event`).css({"z-index": 21}).addClass("filtered");
    $elP = $(`[data-id_evt="${event.id}"].event`).parents(".pop-over");
    if($elP.length != 0)
    {
      $(`[data-target="#${$elP[0].id}"].event`).css({"z-index": 21}).addClass("filtered");
    }
  }

  if($("#col-ev .backdrop").length === 0)
  {
    $("#col-ev").append(`<div class="backdrop" style="width: ${$("#col-ev").width()}px; height: ${$("#col-ev").height()}px;"></div>`);
  }
  setTimeout(function() {$("#cont-main").trigger("ready.GanttH")}, 20);
}

/* método cleanFilters */
/* Limpia los filtros, todas las opciones seleccionadas en los filtros-selectores */
/* y filtros avanzados o el mismo comportamiento por columna */
GanttH.cleanFilters = function (e) {

  GanttH.filter_active = true;
  var searchs;
  var $rows;
  var idCol;
  var parameters = e.data.parameters;
  parameters.columns = GanttH.columns;

  if( e.currentTarget.id == `clearBtnFilter` ){
    /*Mejorar, que no haga una consulta por cada columna que tiene filtro, hacer una consulta 
    general que vacie todo*/

    $(`#cont-filtros .selectpicker`).selectpicker('deselectAll');
    $(`#cont-filtros .selectpicker`).selectpicker("refresh");
    $(e.currentTarget).hide();
    $(`#cont-filtros div.opciones [data-for]`).hide();
    $(`#cont-filtros div.opciones .btn-primary[data-target]`).removeClass('btn-primary').addClass('btn-light');
    $(`#cont-filtros div.opciones .bootstrap-select .btn-primary`).removeClass('btn-primary');

    searchs = 0;
    for(var i = 0; i < parameters.columns.length; i++)
    {//recorre busquedas de la tabla
      if(parameters.columns[i].search.value != "")
      {
        GanttH.cleanModal(i, parameters, e.currentTarget.id);
        //Se refresca la variable de consulta
        parameters.columns[i].search.value = "";
        GanttH.columns[i].search.value = "";
      }
      GanttH.Fchange[i] = 1;
    }

    GanttH.eventsFiltered = "ALL";    
    GanttH.receiveDataFiltered(GanttH.eventsFiltered, parameters);

    GanttH.filter_active = false;
  }
  else if( $(e.currentTarget).data("for") != undefined ) 
  {
    var $rows;
    idCol = $(e.currentTarget).data("for");

    // Deseleccionar lo que haya en el selector de filtro
    $(`#cont-filtros #select_${idCol}`).selectpicker('deselectAll');
    //El estilo se quita en el método searchSel
    
    /* Disparar evento de busqueda en DataTable para limpiar modal */
    if(parameters.columns[idCol].search.value != "")
    {
      GanttH.cleanModal(idCol, parameters);
    } 
    
    // Logica estilo de los botones
    $(e.currentTarget).hide();
    e.currentTarget.nextElementSibling.classList.remove("btn-primary");
    e.currentTarget.nextElementSibling.classList.add("btn-light");
    $(e.currentTarget).next().next().children("button").removeClass("btn-primary");

    //Se refresca la variable de consulta
    parameters.columns[idCol].search.value = "";
    GanttH.columns[idCol].search.value = "";

    //busquedas de la tabla recorrer
    searchs = 0;
    for(var i = 0; i < parameters.columns.length; i++){
      if(parameters.columns[i].search.value != ""){
        searchs++;
      }
    }

    if(searchs == 0){
      $(`#clearBtnFilter`).hide();
      $("#clearBtnFilter").tooltip('hide');
    }

    // GanttH.getDataFiltered(parameters);
    GanttH.Fchange = new Array(GanttH.Fchange.length).fill(1);

    GanttH.filter_active = false;
  }
  else 
  {
    //limpiar el modal 
    idCol = e.currentTarget.id.split("_")[1];
    GanttH.cleanModal(idCol, parameters);
    $(`button[data-target="#customFilter${idCol}"]`).removeClass("btn-primary").addClass("btn-light");

    GanttH.filter_active = false;
  }
}

/* método cleanModal */
/* Limpia un modal, volviendo a su estado inicial y la busqueda de la columna, con cada llamado */
/* @param $table, objeto DataTable con referencia a la tabla */
/* @param idCol, número de columna a borrar */
GanttH.cleanModal = function(idCol, parameters, clearBtn) {
  var $rows = $(`[data-id="modal-body${idCol}"] .row`);
  var lastSearch;
  //Borrar una sola fila
  if( $rows.length == 1 )
  {
    $rows.find(`select#opt_${idCol}`).prop("selectedIndex", 0);
    $(`[data-id='modals'] [data-id='selectM_-${idCol}']`).selectpicker('val', "")
    $rows.find("input").val("");
  }
  else //Borrar N filas
  {
    var btnAdd = $rows.find(`#add_${idCol}`).detach();
    $rows.find(`select#opt_${idCol}`).first().prop("selectedIndex", 0);
    $(`[data-id='modals'] [data-id='selectM_${idCol}']`).first().selectpicker('val', "");
    $rows.find("input").first().val("");
    $rows.find(`.col-2.text-center`).first().empty().append(btnAdd);

    $rows.slice(1).remove();
  }

  // lastSearch = parameters.columns[idCol].search.value;

  // if(lastSearch != "")
  // {
  //   lastSearch = lastSearch.split(",");
  //   lastSearch = lastSearch.filter( s => !s.includes("**/"));
  //   //Se refresca la variable consulta
  //   parameters.columns[idCol].search.value = String(lastSearch);

  //   GanttH.getDataFiltered(parameters);

  //   //Estilos botones
  //   if(lastSearch.length == 0)
  //   {
  //     $(`div.opciones button[data-for="${idCol}"]`).click();
  //   }
  // } 
  // else 
  // {
  //Se refresca la variable consulta
  parameters.columns[idCol].search.value = "";

  /* Se redibuja la grafica */
  if(clearBtn == undefined){
    GanttH.getDataFiltered(parameters);
  }
  // }
}

/* método searchSel */
/* Dispara el evento de acuerdo a la opción seleccionada, usando value       */
/* @param e, objeto del evento, para extraer la opción seleccionada y la columna */
/* Logica de filtrado */
/* Filtro Columna */
GanttH.searchSel = function(e) {
  //Debido a que esta función se activa con un cambio en los selectores, cuando se hace el clean
  //general de los selectores o por cada uno se activa debido a que se quitan las selecciones
  //que tenga el selector, por lo cual con esta condicción se asegura que solo se ejecute la
  //función cuando se haga un cambio desde el selector y no por una función externa
  if(GanttH.filter_active == false){

    var idCol = parseInt(e.target.getAttribute("data-col"));
    var searchValue = $(e.target).val();
    var searchs;
    var parameters = e.data.parameters;
    parameters.columns = GanttH.columns;
    
    //Cambia clase de acuerdo a si hay uno una opción seleccionada
    if(searchValue != ""){
      e.target.nextElementSibling.classList.add("btn-primary");
      $(e.currentTarget.firstElementChild).show();
      $(`#clearBtnFilter`).show();
    } 
    else {
      e.target.nextElementSibling.classList.remove("btn-primary");
    }
    var lastSearch = GanttH.columns[idCol].search.value;/*SEARCH();*/

    if( lastSearch != "" )
    {
      lastSearch = lastSearch.split(",");
      lastSearch = lastSearch.filter( s => s.includes("**/"));
      if(!(lastSearch.length == 0))
      {
        searchValue = searchValue.concat(String(lastSearch));
      }
      //Estilo botones
      if(searchValue == "")
      {
        $(e.currentTarget.firstElementChild).hide();
      }
    }

    //Se refresca la variable de consulta
    parameters.columns[idCol].search.value = String(searchValue);
    GanttH.columns[idCol].search.value = String(searchValue);

    //Realizar la consulta de los id de datos
    GanttH.getDataFiltered(parameters);

    // Se redibuja la grafica 
    // if(parameters.JGanttHPanel == undefined){
    //   GanttH.graph(parameters);
    // }
    // else{
    //   GanttH.graphPanel(parameters);
    // }

    //Se remueve el tooltip viejo del selector
    $(`[data-id="select_${idCol}"]`).removeAttr("title");

    //Estilo boton limpiar todo
    searchs = 0;
    for(var i = 0; i < parameters.columns.length; i++){
      if(parameters.columns[i].search.value != ""){
        searchs++;
      }
    }

    if(searchs == 0)
    {
      $(`#clearBtnFilter`).hide();
      $("#clearBtnFilter").tooltip('hide');
    }

    //Se cambia el estado de actualizacion de todos los selectores menos el que esta en uso
    for (var i=0; i<parameters.columns.length; i++)
    {
      if (i == idCol)
      {
        GanttH.Fchange[i] = 0;
      } else {
        GanttH.Fchange[i] = 1;
      }
    }
  }
}

/* método searchCustom */
/* Esta función es el llamado de un evento click sobre el boton aceptar del modal de filtro avanzado,     */
/* recoge los datos del formulario y luego llama al método de buscar de datatable para que haga la consulta */
GanttH.searchCustom = function(e) {
  var parameters = e.data.parameters;
  parameters.columns = GanttH.columns;
  var idCol = parseInt(e.currentTarget.id.split("_")[1]);
  var $rows = $(`[data-id="modals"] [data-id="modal-body${idCol}"] .row`);
  var searchVals = new Array();
  GanttH.$initModalBody = "";

  if( $rows.length == 1)
  {
    //Una sola fila
    var op;
    var val;
    op = $($rows[0].children[0]).find("select").val();
    val = $($rows[0].children[1]).find("input").val();
    if( op.includes("LIKE") )
    {
      op = op.replace("_", val);
      searchVals.push(`**/data**/${op}**/`);
    } else {
      searchVals.push(`**/data**/${op}**/'${val}'**/`);
    }

  } 
  else {
    var op;
    var val;
    var opl;
    var searchVal;
    //Varias filas
    //Primer elemento
    op = $($rows[0].children[0]).find("select").val();
    val = $($rows[0].children[1]).find("input").val();
    if( op.includes("LIKE") )
    {
      op = op.replace("_", val);
      searchVal = `**/data**/${op}**/`;
    } else {
      searchVal = `**/data**/${op}**/'${val}'**/`;
    }
    searchVals.push(searchVal);

    //Resto de elementos
    for(var i=2; i<$rows.length; i+=2)
    {
      op = $($rows[i].children[0]).find("select").val();
      val = $($rows[i].children[1]).find("input").val();
      opl = $($rows[i-1].children).find("input:checked").val()
      if( op.includes("LIKE") )
      {
        op = op.replace("_", val);
        searchVal = `**/${opl}**/data**/${op}**/`;
      } else {
        searchVal = `**/${opl}**/data**/${op}**/'${val}'**/`;
      }
      searchVals.push(searchVal);
    }
  }

  var lastSearch = GanttH.columns[idCol].search.value;

  //Concatenar busqueda
  if(lastSearch != "")
  {
    lastSearch = lastSearch.split(",");
    lastSearch = lastSearch.filter( s => !s.includes("**/"));
    searchVals = searchVals.concat(lastSearch);
    //Se refresca la variable consulta
    parameters.columns[idCol].search.value = String(searchVals);
  }
  else{
    //Se refresca la variable consulta
    parameters.columns[idCol].search.value = String(searchVals);
  }

  GanttH.columns = parameters.columns;
  GanttH.getDataFiltered(parameters);

  $(`#clearBtnFilter`).show();
  $(`.opciones [data-for='${idCol}']`).show().next().removeClass("btn-light").addClass("btn-primary");

  //Se cambia el estado de actualizacion de todos los selectores menos el que esta en uso
  for (var i=0; i<parameters.columns.length; i++)
  {
    if (i == idCol)
    {
      GanttH.Fchange[i] = 0;
    } else {
      GanttH.Fchange[i] = 1;
    }
  }
}

/* método delayConfirm */
/* delay para la alerta de confirmación, por algunos problemas en los tiempos con sweetalert se usa un delay */
GanttH.delayConfirm = function(label) {
  gestionModal.alertaConfirmacion("",label,"info","OK","#007bff",function(Continuar){});
}

/* adjustColsFilters */
/* ajusta las columnas de los filtros dependiendo del ancho del contenedor hermano */
GanttH.adjustColsFilters = function() {
  //Ancho del div hermano de los selectores
  var widthSelect;

  //.section, numero de columnas de filtros visibles segun el ancho del contenedor
  widthSelect = $(`#controles`).width();
  
  if(widthSelect >= 1050){
    $(`#cont-filtros>div .section`).css("width", "25%");
  }
  else if( widthSelect < 1050 && widthSelect >= 800 ){
    $(`#cont-filtros>div .section`).css("width", "33.3333%");
  }
  else if( widthSelect < 800 && widthSelect >= 550){
    $(`#cont-filtros>div .section`).css("width", "50%");
  }
  else if ( widthSelect < 550 && widthSelect >= 0 ){
    $(`#cont-filtros>div .section`).css("width", "100%");
  }
  else{

  }
}

/* hideFilters */
/* esconde contenedor de filtros */
GanttH.hideFilters = function() {
  $("#cont-filtros:visible").slideUp(300);
}

/* triggerShowModal */
/* Inicia settimeout para mostrar el modal */
GanttH.triggerHideFilters = function(e) {
  var $this = $(this);
  // if ($("body").hasClass("modal-open") && $("#eventsModal:visible").length === 1)
  // {
  //   GanttH.hideModal();
  // }
  GanttH.hideF = setTimeout(GanttH.hideFilters, 700);
}

/* cancelShowModal */
/* Cancela settimeout de mostrar el modal */
GanttH.cancelHideFilters = function(e) {
  clearTimeout(GanttH.hideF);
}

/* triggerShowModal */
/* Inicia settimeout para mostrar el modal */
GanttH.triggerHideDet = function(e) {
  GanttH.hideDet = setTimeout(function() {$("#details").hide(300)}, 700);
}

/* cancelShowModal */
/* Cancela settimeout de mostrar el modal */
GanttH.cancelHideDet = function(e) {
  clearTimeout(GanttH.hideDet);
}

/* quitListenerFilters */
/* quita los eventos de esconder contenedor de filtros mientas se abre el BTSelect */
GanttH.quitListenerFilters = function(e) {
  // $("#cont-filtros").on("mouseenter.GanttH", GanttH.cancelHideFilters);
  $("#cont-filtros").off("mouseleave.GanttH", GanttH.triggerHideFilters);
}

/* pushListenerFilters */
/* pone los eventos cuando se cierra el bootstrap select */
GanttH.pushListenerFilters = function(e) {
  // $("#cont-filtros").on("mouseenter.GanttH", GanttH.cancelHideFilters);
  $("#cont-filtros").on("mouseleave.GanttH", GanttH.triggerHideFilters);
}
/*-------------------------------RENDERIZADO-----------------------------------*/

/* método renderDate */
/* Renderiza el tipo de dato date, para que solo se muestre la fecha */
GanttH.renderDate = function(data, t, r) {
  data = moment(data).format("YYYY-MM-DD");
  return data;
}

/* método renderDateTime */
/* Renderiza el tipo de dato datetime*, para se muestre la fecha y hora */
GanttH.renderDateTime = function(data, t, r) {
  data = moment(data).format("YYYY-MM-DD HH:mm:ss");
  return data;
}

/* método renderNumber */
/* Renderiza el tipo de dato int o float, con separadores de miles */
GanttH.renderNumber = function(data, t, r) {
  data = Intl.NumberFormat("es-CO").format(data);
  return data;
}