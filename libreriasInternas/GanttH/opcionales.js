/*******/
/* drawEvents */
/* dibuja los eventos, calculando su tamaño en pixeles y su posicion, por medio de la función convertT2Px */
// GanttH.drawEvents = function(mStart, height, int, cont, escala, data, opt) {
//   GanttH.mStart = mStart;
//   GanttH.height = height;
//   GanttH.escala = escala;
//   GanttH.opt = opt;

//   var mdStart;
//   var pInit;
//   var pEnd;
//   var eHeight;
//   var pos;
//   var field;
//   var lastEH;
//   var isCollide;
//   var $lastEl;
//   var $currEl;
//   var idContPop;

//   //Recorre el arreglo de los datos para identificar su intervalo de tiempo y convertirlo en pixeles de tamaño y posición
//   // for(var i=0; i<data.length; i++)
//   // {
//   //   //Extrae su intervalo de tiempo
//   //   mdStart = moment(data[i].start);
//   //   mdEnd = moment(data[i].end);
//   //   //Calcula la diferencia de estos intervalos con la fecha inicial global (options.start)
//   //   pInit = moment.duration(mdStart.diff(mStart));
//   //   pEnd = moment.duration(mdEnd.diff(mStart));

//   //   //Crea el div que contiene cada evento, colocando algunas opciones en atributos data,
//   //   // data-field se usa para reescribir la información cuando los eventos se agrupan, solo toma los dos primeros elementos
//   //   // data-pop es la información que se muestra al hacer click en el evento
//   //   // data-id identificación unica del evento
//   //   $(".cont-events .presentation").append(`<div class="event" data-id="${data[i].id}" data-start="${data[i].start}" data-end="${data[i].end}" data-field='${JSON.stringify(data[i].data_field)}' data-pop='${JSON.stringify(data[i].data_pop)}'></div>`);
//   //   //Obtiene posición y altura a partir del tiempo y la opción de visualización
//   //   pos = GanttH.convertT2Px(escala, height, pInit, opt);
//   //   eHeight = GanttH.convertT2Px(escala, height, pEnd, opt)-pos;
//   //   //Configura posición y altura, los eventos tienen una regla de min-heigth: 3px;
//   //   if(lastEH === undefined)
//   //   {
//   //     eHeight -= 2.5;
//   //   }
//   //   else if(eHeight >= 6 && lastEH >= 6)
//   //   {
//   //     eHeight -= 5;
//   //     pos += 2.5;
//   //     if(eHeight <= 3)
//   //     {
//   //       //put a POINT or ARROW
//   //     }
//   //   }
//   //   else if(eHeight >= 6 && lastEH <= 6)
//   //   {
//   //     eHeight -= 5;
//   //     pos += 2.5;
//   //     if(eHeight <= 3)
//   //     {
//   //       //put a POINT or ARROW
//   //     }
//   //   }
//   //   else if(eHeight <= 6 && lastEH >= 6)
//   //   {
//   //     //pos += 2.5;
//   //     if(eHeight <= 3)
//   //     {
//   //       //put a POINT or ARROW
//   //     }
//   //   }
    
//   //   $(`.cont-events .presentation .event`).last().css({top: `${pos}px`, height: `${eHeight}px`, "background-color": data[i].color});
//   //   lastEH = eHeight;//altura el elemento anterior
//   //   /*dibuja los campos dentro de la caja del evento (data_field), verificando si el 
//   //   nombre del campo será visible o solo el valor de este.*/
//   //   field = data[i].data_field;
//   //   //Función para añadir datos a un elemento de la forma Campo: Valor
//   //   GanttH.appendData($(`.cont-events .presentation .event`).last(), field, field.length);
//   //   //Verifica si la caja del evento presenta sobreflujo (overflow) y elimina los campos hasta que no exista sobreflujo (fn Recursiva)
//   //   GanttH.isOverflow($(`.presentation>.event`).last());

//   //   //elemento anterior y elemento actual
//   //   $lastEl = $(`.presentation>.event`).last().prev();
//   //   $currEl = $(`.presentation>.event`).last();

//   //   //verificar si estan sobre puestos
//   //   isCollide = GanttH.isCollide($currEl[0], $lastEl[0]);
//   //   //si estan sobrepuestos agrupelos, de acuerdo a si se sobreponen dos eventos normales, o un evento tipo grupo con otro evento normal
//   //   if(isCollide === true)
//   //   {
//   //     eHeight = pos - $lastEl.position().top + eHeight;
//   //     pos = $lastEl.position().top;
//   //     $(".cont-events .presentation").append(`<div class="event group-events" data-target="#group-events-${i}" data-group="true"></div>`);
//   //     $(`.cont-events .presentation .event`).last().css({top: `${pos}px`, height: `${eHeight}px`, "background-color": "black"});

//   //     //si se sobrepone con un grupo de eventos
//   //     if($lastEl[0].hasAttribute('data-group'))
//   //     {
//   //       //quita el dibujo del evento tipo grupo anterior y agrega el ultimo evento dibujado al pop over
//   //       $lastEl.remove();
//   //       //configura el evento para ponerlo en el pop oveer
//   //       $currEl = $currEl.detach();
//   //       $currEl.addClass("pop-event").css({top: "", height: ""});
//   //       field = $currEl.data("field");
//   //       GanttH.appendData($currEl, field, 2);

//   //       $(`#${idContPop}`).children().first().append($currEl);
//   //       $(".event.group-events").last().attr("data-target", `#${idContPop}`);
//   //     }
//   //     else
//   //     {
//   //       //si son dos eventos normales, crear el pop-over y luego los cambia a este contendor
//   //       $(".inner-cont").append(`<div class="pop-over" id="group-events-${i}"><div></div><div class="close-po"><i class="fas fa-times"></i></div>`);
//   //       $currEl = $currEl.detach();
//   //       $lastEl = $lastEl.detach();
//   //       $currEl.addClass("pop-event").css({top: "", height: ""});
//   //       $lastEl.addClass("pop-event").css({top: "", height: ""});

//   //       field = $currEl.data("field");
//   //       GanttH.appendData($currEl, field, 2);

//   //       field = $lastEl.data("field");
//   //       GanttH.appendData($lastEl, field, 2);

//   //       idContPop = `group-events-${i}`;
//   //       $(`#group-events-${i}`).children().first().append($lastEl);
//   //       $(`#group-events-${i}`).children().first().append($currEl);
//   //     }
//   //   }

//   //   //Dibujar las etiquetas verticales de cada grupo de eventos
//   //   // GanttH.drawTagsGroup(i, cont, pos, eHeight, data);

//   // }

//   if($(".pop-over").length > 0)
//   {
//     $(".close-po").on("touchstart", GanttH.hidePopOver);
//   }
//   //dibujar flechas para los eventos agrupados
//   GanttH.drawArrows();
// }



/*for drawboxes*/

/*for(var i = 0; i<int; i++)
  {
    //line-h of the grid
    cont.find(".rules").append("<div class='rule'></div>");
    //time-rule
    cont.find(".time").append(`<div class="time-item" style="height: ${height}px"></div>`);
    //La suma funciona de acuerdo a @i divido entre @escala y el tipo de intervalo de tiempo @conf.type (año, mes, semana, hora)
    cont.find(".time").children().last().append(`
      <span class="item t-item">${mStart.add(i/escala, conf.type).format(conf.format.split(" ")[0]).toUpperCase()}</span>
      `);
    //decision para darle el formato a las reglas cuando tienen el nombre del dia y el numero o con la semana, (cuestion de visualizacion)
    if(conf.format.split(" ").length > 1)
    {
      cont.find(".time").children().last().append(`
      <span class="item t-item">${mStart.format(conf.format.split(" ")[1]).toUpperCase()}</span>
      `);
    }

    if(opt != "Day")
    {
      cont.find(".time").children().last().children().wrapAll(`<div data-toggle="tooltip" class="item" 
        title="${mStart.format(conf.tooltip).toUpperCase()}" />`);
      cont.find(`.time [data-toggle="tooltip"] .item`).removeClass("item");
    }
    //dibuja las etiquetas de la fecha en la parte izquierda
    if( i == 0 )
    {
      //(1) year-month rule
      prevHeight[0] = 0;
      prevTop[0] = 0;
      cont.find("#col-date .date").append(`<div class="vertical-date">
        <div data-toggle="tooltip" class="date-text" title="${mStart.format(conf.yTooltip).toUpperCase()}">
          <span class="date-text-c">${mStart.format(conf.label).split("-")[1].toUpperCase()}</span>
          <span class="date-text-c">${mStart.format(conf.label).split("-")[0]}</span>
          <!--<span class="label-line"></span>-->
        </div>
      </div>`);
      cont.find("#col-date .date .vertical-date").last().css({top: `0px`, height: `${height}px`});

      //other rules and hide or show rules
      if(opt == "Year")
      {
        //para la escala de año, deja visibles solo las reglas necesarias
        if(conf.zoom == "year" || conf.zoom == "month")
        {
          $ruleWeek.find(".week").empty();
          $ruleDay.find(".day").empty();
          $ruleWeek.hide();
          $ruleDay.hide();
        }
      }
      else if(opt == "Month" || opt == "Week")
      {
        //para la escala de semana, deja visibles solo las reglas necesarias
        $ruleWeek.find(".week").empty();
        $ruleDay.find(".day").empty();
        $ruleWeek.show();
        $ruleDay.hide();

        prevHeight[1] = 0;
        prevTop[1] = 0;
        cont.find("#col-week .week").append(`<div class="vertical-week">
          <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
            <span class="week-text-c">SEM.</span>
            <span class="week-text-c">${mStart.format(conf.wLabel)}</span>
            <!--<span class="label-line"></span>-->
          </div>
        </div>`);
        cont.find("#col-week .week .vertical-week").last().css({top: `0px`, height: `${height}px`});
      }
      else if(opt == "Day")
      {
        //para la escala de dias, mostrar las reglas necesarias
        $ruleWeek.find(".week").empty();
        $ruleDay.find(".day").empty();
        $ruleWeek.show();
        $ruleDay.show();
        //week-rule
        prevHeight[1] = 0;
        prevTop[1] = 0;
        cont.find("#col-week .week").append(`<div class="vertical-week">
          <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
            <span class="week-text-c">SEM.</span>
            <span class="week-text-c">${mStart.format(conf.wLabel)}</span>
            <!--<span class="label-line"></span>-->
          </div>
        </div>`);
        cont.find("#col-week .week .vertical-week").last().css({top: `0px`, height: `${height}px`});
        //day-rule
        prevHeight[2] = 0;
        prevTop[2] = 0;
        cont.find("#col-day .day").append(`<div class="vertical-day">
          <div data-toggle="tooltip" class="day-text" title="${mStart.format(conf.dTooltip).toUpperCase()}">
            <span class="day-text-c">${mStart.format(conf.dLabel.split(" ")[0]).toUpperCase()}</span>
            <span class="day-text-c">${mStart.format(conf.dLabel.split(" ")[1]).toUpperCase()}</span>
            <!--<span class="label-line"></span>-->
          </div>
        </div>`);
        cont.find("#col-day .day .vertical-day").last().css({top: `0px`, height: `${height}px`});
      }
    }
    else
    {
      //realiza la comparación dinámica de acuerdo al objeto de configuracion definido anteriormente en el switch
      if(eval(`lastDate.${conf.comp} == mStart.${conf.comp}`))
      {
        cont.find(".date .vertical-date").last().css({height: `${height*(i+1)-(prevHeight[0]+prevTop[0])}px`});
      }
      else
      {
        prevHeight[0] = cont.find(".date .vertical-date").last().outerHeight();
        prevTop[0] = cont.find(".date .vertical-date").last().position().top;
        lastDate = mStart.clone();
        cont.find(".date").append(`<div class="vertical-date">
          <div data-toggle="tooltip" class="date-text" title="${mStart.format(conf.yTooltip).toUpperCase()}">
            <span class="date-text-c">${mStart.format(conf.label).split("-")[1].toUpperCase()}</span>
            <span class="date-text-c">${mStart.format(conf.label).split("-")[0]}</span>
            <!--<span class="label-line"></span>-->
          </div>
        </div>`);
        cont.find(".date .vertical-date").last().css({top: `${prevHeight[0]+prevTop[0]}px`, height: `${height}px`});
      }

      //filtra de acuerdo a la opcion elegida en la escala
      if(opt != "Year")
      {
        if(opt == "Month" || opt == "Week")
        {
          if(eval(`lastWeek.${conf.wComp} == mStart.${conf.wComp}`))
          {
            cont.find(".week .vertical-week").last().css({height: `${height*(i+1)-(prevHeight[1]+prevTop[1])}px`});
          }
          else
          {
            prevHeight[1] = cont.find(".week .vertical-week").last().outerHeight();
            prevTop[1] = cont.find(".week .vertical-week").last().position().top;
            lastWeek = mStart.clone();
            cont.find(".week").append(`<div class="vertical-week">
              <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
                <span class="week-text-c">SEM.</span>
                <span class="week-text-c">${mStart.format(conf.wLabel)}</span>
                <!--<span class="label-line"></span>-->
              </div>
            </div>`);
            cont.find(".week .vertical-week").last().css({top: `${prevHeight[1]+prevTop[1]}px`, height: `${height}px`});
          }
        }
        else if(opt == "Day")
        {
          if(eval(`lastWeek.${conf.wComp} == mStart.${conf.wComp}`))
          {
            cont.find(".week .vertical-week").last().css({height: `${height*(i+1)-(prevHeight[1]+prevTop[1])}px`});
          }
          else
          {
            prevHeight[1] = cont.find(".week .vertical-week").last().outerHeight();
            prevTop[1] = cont.find(".week .vertical-week").last().position().top;
            lastWeek = mStart.clone();
            cont.find(".week").append(`<div class="vertical-week">
              <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
                <span class="week-text-c">SEM.</span>
                <span class="week-text-c">${mStart.format(conf.wLabel)}</span>
                <!--<span class="label-line"></span>-->
              </div>
            </div>`);
            cont.find(".week .vertical-week").last().css({top: `${prevHeight[1]+prevTop[1]}px`, height: `${height}px`});
          }

          if(eval(`lastDay.${conf.dComp} == mStart.${conf.dComp}`))
          {
            cont.find(".day .vertical-day").last().css({height: `${height*(i+1)-(prevHeight[2]+prevTop[2])}px`});
          }
          else
          {
            //day-rule
            prevHeight[2] = cont.find(".day .vertical-day").last().outerHeight();
            prevTop[2] = cont.find(".day .vertical-day").last().position().top;
            lastDay = mStart.clone();
            cont.find(".day").append(`<div class="vertical-day">
              <div data-toggle="tooltip" class="day-text" title="${mStart.format(conf.dTooltip).toUpperCase()}">
                <span class="day-text-c">${mStart.format(conf.dLabel.split(" ")[0]).toUpperCase()}</span>
                <span class="day-text-c">${mStart.format(conf.dLabel.split(" ")[1]).toUpperCase()}</span>
                <!--<span class="label-line"></span>-->
              </div>
            </div>`);
            cont.find(".day .vertical-day").last().css({top: `${prevHeight[2]+prevTop[2]}px`, height: `${height}px`});
          }
        }
      }
    }
    //Se le resta a la fecha inicial lo que se le habia sumado antes
    mStart.add(-i/escala, conf.type);
  }*/