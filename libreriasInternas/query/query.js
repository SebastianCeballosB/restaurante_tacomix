var query = new Object();

// Se agrega objDT en caso del llamado desde gestionDT
query.callAjax = function(urlConexion,metodo,data,successEvent, objDT, urlExterna){	
	 $.ajax({
		url: urlConexion+"/"+metodo, //variable definida en el index
		type: 'POST',
        dataType: 'json',
		async: true,
		data: data,
		error:function(objeto1,objeto2,objeto3){
			
			// console.log(objeto1);
			// console.log(objeto2);
			// console.log(objeto3);
			if(urlExterna == undefined){
				query.errorEvent(objeto1);
			}
			else{
				if(objeto1.status == 0){
					query.urlExterna();
				}
				else{
					query.errorEvent(objeto1);
				}
			}
		},
		timeout:600000,  // I chose 5 secs for kicks
		success: function (data){
			if(data.ESTADO == "TRUE" && objDT == undefined){
				successEvent(data.RESULTADO);
			}
			else if(data.ESTADO == "TRUE"){
				successEvent(data.RESULTADO, objDT);	
			}else{
				query.errorEvent();
			}
		}
	});
}

//AJAX hilos

query.callAjaxPHP = function(urlConexion,metodo,data,successEvent, objDT, urlExterna){	
	$.ajax({
	   url: urlConexion, //variable definida en el index
	   type: 'POST',
	   dataType: 'json',
	   async: true,
	   data: {action:metodo,data:data},
	   error:function(objeto1,objeto2,objeto3){
		   // console.log(objeto1);
		   // console.log(objeto2);
		   // console.log(objeto3);
		   if(urlExterna == undefined){
			   query.errorEvent(objeto1);
		   }
		   else{
			   if(objeto1.status == 0){
				   query.urlExterna();
			   }
			   else{
				   query.errorEvent(objeto1);
			   }
		   }
	   },
	   timeout:600000,  // I chose 5 secs for kicks
	   success: function (data){
		   if(data.ESTADO == "TRUE" && objDT == undefined){
			   successEvent(data.RESULTADO);
		   }
		   else if(data.ESTADO == "TRUE"){
			   successEvent(data.RESULTADO, objDT);	
		   }else{
			   query.errorEvent();
		   }
	   }
   });
}

query.callAjaxPost = function(urlConexion,metodo,data){	
	$.post( urlConexion+"/"+metodo,data, function( data ) {
	});
}

query.urlExterna = function(){
	//url inalcanzable
    gestionModal.alertaConfirmacion("P360",
    	"No se pudo conectar con la base de datos ya que no tiene acceso",
      "error", "OK", "#AB0000", function(){});
}

query.callAjaxSearchBar= function(urlConexion,metodo,data,successEvent){	
	$.ajax({
		url: urlConexion+"/"+metodo, //variable definida en el index
		type: 'POST',
        dataType: 'json',
		async: true,
		data: data,
		error:function(objeto1,objeto2,objeto3){
			/*
			console.log(objeto1);
			console.log(objeto2);
			console.log(objeto3);
			*/
			query.errorEvent(objeto1);
		},
		timeout:600000,  // I chose 5 secs for kicks
		success: function (data){
			if(data.ESTADO == "TRUE"){
				successEvent(data.RESULTADO_P,data.RESULTADO_S);
			}else{
				query.errorEvent();
			}
		}
	});
}

query.callAjaxUser = function(urlConexion,metodo,data,successEvent){
    $.ajax({
        url: urlConexion+"/"+metodo, //variable definida en el index
        type: 'POST',
        dataType: 'xml',
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        error:function(objeto1,objeto2,objeto3){
            /*
            console.log(objeto1);
            console.log(objeto2);
            console.log(objeto3);
            */
            query.errorEvent(objeto1);
        },
        timeout:600000,  // I chose 5 secs for kicks
        success: function (data_xml){
            var data = JSON.parse(data_xml.documentElement.innerHTML);
            if(data.ESTADO == "TRUE"){
                successEvent(data.RESULTADO);   
            }else{
                query.errorEvent();
            }  
        }
    });
}

query.callAjaxSidebar = function(urlConexion, metodo, data, successEvent){
	
	$.ajax({
		url: urlConexion+"/"+metodo, //variable definida en el index
		type: 'POST',
        dataType: 'json',
		async: true,
		data: data,
		error:function(objeto1,objeto2,objeto3){
			/*
			console.log(objeto1);
			console.log(objeto2);
			console.log(objeto3);
			*/
			query.errorEvent(objeto1);
		},
		timeout:600000,  // I chose 5 secs for kicks
		success: function (data){
			if(data.ESTADO == "TRUE"){
				successEvent(data.RESULTADO_SIDEBAR,data.RESULTADO_HEADER,data.RESULTADO_ONE_LEVEL,data.RESULTADO_TWO_LEVELS,data.RESULTADO_THREE_LEVELS);
			}else{
				query.errorEvent();
			}
		}
	});
}

query.callAjaxTP = function(urlConexion, metodo, data, successEvent){
	
	$.ajax({
		url: urlConexion+"/"+metodo, //variable definida en el index
		type: 'POST',
        dataType: 'json',
		async: true,
		data: data,
		error:function(objeto1,objeto2,objeto3){
			/*
			console.log(objeto1);
			console.log(objeto2);
			console.log(objeto3);
			*/
			query.errorEvent(objeto1);
		},
		timeout:600000,  // I chose 5 secs for kicks
		success: function (data){
			if(data.ESTADO == "TRUE"){
				successEvent(data.RESULTADO_1,data.RESULTADO_2,data.RESULTADO_3,data.RESULTADO_4,data.RESULTADO_5, data.draw);
			}else{
				query.errorEvent();
			}
		}
	});
}

// Se agrega objDT en caso del llamado desde gestionDT
query.callAjaxCards = function(urlConexion,metodo,data,successEvent, objDT){	
	 $.ajax({
		url: urlConexion+"/"+metodo, //variable definida en el index
		type: 'POST',
        dataType: 'json',
		async: true,
		data: data,
		error:function(objeto1,objeto2,objeto3){
			
			// console.log(objeto1);
			// console.log(objeto2);
			// console.log(objeto3);
			
			query.errorEvent(objeto1);
		},
		timeout:600000,  // I chose 5 secs for kicks
		success: function (data){
			if(data.ESTADO == "TRUE" && objDT == undefined){
				successEvent(data.RESULTADO, data.draw);
			}
			else if(data.ESTADO == "TRUE"){
				successEvent(data.RESULTADO, objDT, data.draw);	
			}else{
				query.errorEvent();
			}
		}
	});
}

query.callAjaxLogin= function(urlConexion,metodo,data,successEventFalse,successEventTrue){	
	$.ajax({
		url: urlConexion+"/"+metodo, //variable definida en el index
		type: 'POST',
        dataType: 'json',
		async: true,
		data: data,
		error:function(objeto1,objeto2,objeto3){
			query.errorEvent(objeto1);
		},
		timeout:600000,  // I chose 5 secs for kicks
		success: function (data){
			if(data.STATE == 'true'){
				successEventTrue(data.RESULT);
			}
			else if(data.STATE == 'false')
			{
				successEventFalse();
			}
		}
	});
}

query.errorEvent = function (objeto) {

	if (objeto == undefined)
	{
		gestionModal.alertaConfirmacion(
			"P360",
			"Algo ha fallado con esta función y el equipo de soporte ya ha sido notificado para solucionarlo lo más pronto posible. Disculpe las molestias ocasionadas.",
			"error",
			"Ok",
			"#f27474",
			function(){}
		);
	}
	else if(objeto.status == 601 || objeto.status == 503)
	{
		window.location.reload();
	}
	else
	{
		gestionModal.alertaConfirmacion(
			"P360",
			"Algo ha fallado con esta función y el equipo de soporte ya ha sido notificado para solucionarlo lo más pronto posible. Disculpe las molestias ocasionadas.",
			"error",
			"Ok",
			"#f27474",
			function(){}
		);
	}
}

query.callAjaxDataTablesExcel = function(urlConexion,metodo,data,successEvent){
    $.ajax({
        url: urlConexion+"/"+metodo, //variable definida en el index
        type: 'POST',
        dataType: 'json',
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        data: data,
        error:function(objeto1,objeto2,objeto3){
            /*
            console.log(objeto1);
            console.log(objeto2);
            console.log(objeto3);
            */
            query.errorEvent(objeto1);
        },
        timeout:600000,  // I chose 5 secs for kicks
        success: function (data){
            if(data.ESTADO == "TRUE"){
                successEvent(data.RESULTADO);   
            }else{
                query.errorEvent();
            }  
        }
    });
}

//Ajax para verificar si la url de base de datos retorna correctamente una imagen
query.callAjaxImg = function(urlImg, successEvent, errorEvent){
	urlImg = urlImg.substring(2);
	$.ajax({ 
		url: window.location.origin + urlImg,
		type:'HEAD', 
		error: function() 
		{ 
			errorEvent();
		},
		timeout:600000,  // I chose 5 secs for kicks
		success: function() 
		{ 
			successEvent();
		} 
	});
}