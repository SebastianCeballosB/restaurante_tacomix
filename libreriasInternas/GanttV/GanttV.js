//Añade funcionalidades de convertir em a pixeles o pixeles a em
$.fn.toEm = function(settings){
    settings = jQuery.extend({
        scope: 'body'
    }, settings);
    var that = parseInt(this[0],10),
        scopeTest = jQuery('<div style="display: none; font-size: 1em; margin: 0; padding:0; height: auto; line-height: 1; border:0;">&nbsp;</div>').appendTo(settings.scope),
        scopeVal = scopeTest.height();
    scopeTest.remove();
    return (that / scopeVal).toFixed(8) + 'em';
};

$.fn.toPx = function(settings){
    settings = jQuery.extend({
        scope: 'body'
    }, settings);
    var that = parseFloat(this[0]),
        scopeTest = jQuery('<div style="display: none; font-size: 1em; margin: 0; padding:0; height: auto; line-height: 1; border:0;">&nbsp;</div>').appendTo(settings.scope),
        scopeVal = scopeTest.height();
    scopeTest.remove();
    return Math.round(that * scopeVal) + 'px';
};

var GanttV = new Object();

moment.locale("es"); //Traducción de momentJS

GanttV.callback = function(){console.log("Listo")};
GanttV.resultado = "";//variable para guardar la consulta y las opciones
GanttV.lastScrollTop = 0;//variable para realizar el desplazamiento del texto con el scroll
GanttV.method = ""; //var para guardar el método del servicio
GanttV.urlconexion = "";  //var para la url del servicio
GanttV.id_cont = "";  //id del contenedor del gantt
GanttV.config = ""; //objeto css de configuración
GanttV.show = 0;  //elementos a mostrar (luego se convierte en un arreglo)
GanttV.hide = 0;  //elementos a esconder (luego se convierte en un arreglo)
GanttV.details = "";//guarda los objetos que hay en detalles antes de redibujar todo
GanttV.modalHeight = 0;//guarda la altura inicial del modal(en movil)

//parametros necesarios para utilizar convertT2Px, en la opcion de ir a una fecha y hora
GanttV.escala = 0; //guarda la escala actual
GanttV.height = 0; //guarda la altura actual
GanttV.mStart = 0; //guarda la fecha de inicio actual (global)
GanttV.opt = 0; //guarda la opcion actual

//Controles que se renderizan en la parte superior
GanttV.controls = [
  {
    "scale": "Year",
    "label": "Año",
    "zoom": [
      // {
      //   "val": "year",
      //   "label": "Anual",
      //   "length": [1,2]
      // },
      {
        "val": "month",
        "label": "Mensual",
        "length": [1,2,3,6,12]
      }
    ],
    "rules": [
      {
        "val": "col-date",
        "label": "Año-Mes"
      },
      {
        "val": "col-time",
        "label": "Mes"
      }
    ]
  },
  {
    "scale": "Month",
    "label": "Mes",
    "zoom": [
      // {
      //   "val": "month",
      //   "label": "Mensual",
      //   "length": [1,2,3,6,12]
      // },
      {
        "val": "week",
        "label": "Semanal",
        "length": [1,2,4,8]
      }
    ],
    "rules": [
      {
        "val": "col-date",
        "label": "Año-Mes"
      },
      {
        "val": "col-week",
        "label": "Semana"
      }//,
      // {
      //   "val": "col-time",
      //   "label": "Día"
      // }
    ]
  },
  {
    "scale": "Week",
    "label": "Semana",
    "zoom": [
      {
        "val": "day",
        "label": "Dias",
        "length": [1,2,4,5,7]
      }
    ],
    "rules": [
      {
        "val": "col-date",
        "label": "Año-Mes"
      },
      {
        "val": "col-week",
        "label": "Semana"
      },
      {
        "val": "col-time",
        "label": "Día"
      }
    ]
  },
  {
    "scale": "Day",
    "label": "Día",
    "zoom": [
      {
        "val": "hour",
        "label": "1 hora",
        "length": [1,2,4,8,16]
      },
      {
        "val": "hHour",
        "label": "30 min",
        "length": [1,2,4,8,16,32]
      },
      {
        "val": "qHour",
        "label": "15 min",
        "length": [1,2,4,8,16,32]
      },
      {
        "val": "5m",
        "label": "5 min",
        "length": [1,3,6,12,24]
      },
      {
        "val": "1m",
        "label": "1 min",
        "length": [1,2,5,15,30]
      }
    ],
    "rules": [
      {
        "val": "col-date",
        "label": "Año-Mes"
      },
      {
        "val": "col-week",
        "label": "Semana"
      },
      {
        "val": "col-day",
        "label": "Día"
      },
      {
        "val": "col-time",
        "label": "Hora"
      }
    ]
  }
];

/* initGantt */
/* Función para iniciar el ganttvertical, recibe el id del contenedor, url del servicio y nombre del webmethod
   además objeto @config con estructura CSS para aplicarle al contenedor del gantt $(".c1").css(config)*/
GanttV.initGantt = function(id_cont, urlconexion, data, method, config, callback) {

  //Se apagan los eventos del gantt horizontal para que no afecten los eventos del gantt vertical
  $("body").off("dblclick.GanttH");
  $("body").off("click.GanttH");
  $("body").off("mouseleave.GanttH");
  $("body").off("mouseenter.GanttH");

  /*La variable init se usa para identificar si es la petición inicial o es una petición realizada por la interacción del usuario
    con los controles que se han renderizado, init=true es la petición inicial, init=false interacción del usuario*/
  parameters = {
    id_cont: id_cont,
    init: true,
    config: config
  };
  //Variables globales para guardar datos necesarios en otras funciones
  GanttV.callback = callback;
  GanttV.urlconexion = urlconexion;
  GanttV.method = method;
  GanttV.id_cont = id_cont;
  GanttV.config = config;

  //AJAX para obtener los datos para dibujar el gantt, se modifico para 
  // que el ajax pueda mandar parametros
  GanttV.getData(parameters, data);
}

/* getData */
/* Hace llamado del AJAX para los datos */
GanttV.getData = function(parameters, data) {
  //Ajax de los datos

  query.callAjaxCards( GanttV.urlconexion, GanttV.method, data, GanttV.receiveInfo, parameters );
  $('[data-toggle="tooltip"]').tooltip("dispose");
}

/* receiveInfo */
/* Callback del ajax, recibe el objeto que contiene los eventos
   a dibujar y también recibe las opciones, si no están definidas
   se definen en esta función, además de sobreescribirlas si el 
   llamado es por interacción del cliente */
GanttV.receiveInfo = function(resultado, parameters, draw) {

  $("#Gantt").addClass("displayBlock");

  /*Si por algun motivo el resultado que recibe no tiene el objeto de opciones
    aqui se configura con valores por defecto*/

  // Se modifico la comparación para que se configure por defecto la hora de inicio y de fin
  // del turno cuando no existan registro
  if(draw == M_registro_linea_de_envase.loadCards.CardGantt)
  {
    if( resultado.options.start === undefined && resultado.options.end === undefined)
    {
      var fecha_aux = M_registro_linea_de_envase.fecha + ' ' + M_registro_linea_de_envase.horaIniString + ':00';
      var fecha_ini;
      var fecha_fin;

      if((M_registro_linea_de_envase.horaIni > M_registro_linea_de_envase.horaFin)
        &&(M_registro_linea_de_envase.id_turno_actual == 1)){

        fecha_ini = moment(fecha_aux,'YYYY-MM-DD HH:mm:ss').subtract(1, 'days').format('YYYY-MM-DD HH:mm:ss');
        fecha_fin = moment(fecha_ini,'YYYY-MM-DD HH:mm:ss').add(8, 'hours').format('YYYY-MM-DD HH:mm:ss');
        resultado.options = {
          "start": fecha_ini,
          "end": fecha_fin,
          "scale": "Day", //Year, Month, Week, Day
          "zoom": "hour",//hour //hHour(30mm) //qHour(15mm) // 5m(5mm) //1m(1mm)
          "length": 8,
          "showDate": 0
        };

      }
      else
      {
        fecha_ini = fecha_aux;
        fecha_fin = moment(fecha_ini,'YYYY-MM-DD HH:mm:ss').add(8, 'hours').format('YYYY-MM-DD HH:mm:ss');
        resultado.options = {
          "start": fecha_ini,
          "end": fecha_fin,
          "scale": "Day", //Year, Month, Week, Day
          "zoom": "hour",//hour //hHour(30mm) //qHour(15mm) // 5m(5mm) //1m(1mm)
          "length": 8,
          "showDate": 0
        };
      }
      //se define el objeto showdate para las reglas de las fechas
      GanttV.show = resultado.options.showDate;
      GanttV.hide = resultado.options.showDate;
    }
    else if(!parameters.init)
    {
      /*Si init es false se sobreescribe el objeto opciones que proviene de la consulta*/
      resultado.options = GanttV.resultado.options;
    }
    else
    {
      //en la petición inicial, define el objeto showdate que muestra u oculta las reglas
      GanttV.show = resultado.options.showDate;
      GanttV.hide = resultado.options.showDate;
    }
    /*se almacena el resultado de la petición en una variable global*/
    GanttV.resultado = resultado;
    //metodo para dibujar los contenedores del gantt
    GanttV.drawAll(resultado, parameters);
  }
}

/* drawAll */
/* dibuja los contenedores principales de los eventos y las reglas,
   dependiendo de la escala invoca a una función para configurar ciertos parametros */
GanttV.drawAll = function(resultado, parameters) {
  var cont = $(parameters.id_cont); //contenedor del gantt
  var height;//altura contenedor

  //altura de la cuadricula
  var box;

  cont.empty();//vaciar contenedor

  //dibuja los controles para cambiar las escalas del gantt, los botones para actualizar y ver detalles
  cont.append(`<div class="topnav" style="display: none;">
  <a href="javascript:void(0);" class="icon btn btn-outline-secondary btn-sm btn-block" id="iconNav">
    <i class="fa fa-bars"></i>
  </a>
  <div id="cont-controls">
    <div class="controls form-inline" id="controles">
      <div class="p-1" role="escala" data-toggle="tooltip" title="Escala Principal"><select class="custom-select custom-select-sm"></select></div>
      <div class="p-1" role="zoom" data-toggle="tooltip" title="División de la escala principal"><select class="custom-select custom-select-sm"></select></div>
      <div class="p-1"><label>x</label></div>
      <div class="p-1" role="length" data-toggle="tooltip" title="Nº de divisiones en pantalla"><select class="custom-select custom-select-sm"></select></div>
      <div class="p-1" role="shwDt" data-toggle="tooltip" title="Mostrar Reglas de Tiempo"><select class="selectpicker" multiple data-live-search="false" 
        data-actions-box="false" 
        data-selected-text-format="static"
        data-title="Ver reglas"
        data-width="100px"
        data-style="btn btn-outline-primary btn-sm"
        data-header="Ver reglas"></select></div>
      <div class="px-2 py-1" role="updateDraw" data-toggle="tooltip" title="Actualizar...">
        <button class="btn btn-primary btn-sm" id="updtBtn"><i class="fas fa-sync"></i></button>
      </div>
      <div class="p-1">
        <button class="btn btn-outline-primary btn-sm" id="searchDate"><i class="far fa-calendar-alt"></i></button>
      </div>
      <div class="p-1" role="shwDetails">
        <button class="btn btn-secondary btn-sm" id="shwDetBtn" data-target="#details" data-toggle="tooltip" title="Mostrar detalles de eventos">Detalles</button>
      </div>
    </div>
  </div>
  </div>`);

  //Eventos iniciados en los controles
  //Evento cambio de algun selector para cambiar las opciones de los otros que dependen de este
  $("#controles").find("select").on("change", GanttV.appendOptions);
  //Evento boton de actulizacion dependiendo de el cambio de los controles (escala)
  $("#updtBtn").on("click", GanttV.refreshGantt);
  //Evento boton mostrar el contenedor de los detalles
  $("#shwDetBtn").on("click", GanttV.shwDet);

  /*-------------Movil----------------*/
  //Evento boton para mostrar controles
  $("#iconNav").on("click", GanttV.showControls);
  /*----------------------------------*/

  //Añade las opciones a los selectores que existen en los controles
  GanttV.appendOptions(0, resultado.options.scale, resultado.options.zoom, resultado.options.length, resultado.options.showDate);

  //dibuja los elementos contenedores de los eventos, las etiquetas, las reglas y los detalles
  cont.append(`<div class="cont contA" id="cont-main" style="">
      <div class="inner-cont">
        <div aria-hidden="true" tabindex="-1" class="col-rule t-line" id="col-date">
          <div class="date"></div>
        </div>
        <div aria-hidden="true" tabindex="-1" class="col-rule" id="col-week">
          <div class="week"></div>
        </div>
        <div aria-hidden="true" tabindex="-1" class="col-rule" id="col-day">
          <div class="day"></div>
        </div>
        <div aria-hidden="true" tabindex="-1" class="col-rule" id="col-time">
          <div class="time"></div>
        </div>
        <div role="presentation" class="pres" id="scroll" tabindex="-1">
          <div class="gantt" role="row" id="col-ev">
            <div aria-hidden="true" class="rules"></div>
            <div class="vertical-line"></div>
            <div role="gridcell" tabindex="-1" class="cont-events cont-evs">
              <div class="no-v"></div>
              <div role="presentation" class="presentation"></div>
            </div>
          </div>
        </div>
        <div role="details" class="details" id="details" tabindex="-1">
          <div class="opt-cont">
            <div class="options-D" id="close-D" data-target="#details"><i class="fas fa-times"></i></div>
            <button class="btn btn-outline-secondary btn-sm btn-block" id="clearDetBtn" data-toggle="tooltip" title="Quitar todos los detalles">Limpiar</button>
          </div>
          <div class="cont-det" id="cont-dets">
          </div>
        </div>
      </div>

      <!-- Modal para el movil -->
      <div class="modalC" id="evsModalMobile">
        <div class="modalC-dialog">
          <div class="modalC-content">

            <div class="modalC-header">
              <h5 class="modalC-title"></h5>
              <div class="options" id="M-append-details" data-target="#details #cont-dets"><i class="fas fa-thumbtack"></i></div>
              <div class="options" id="M-edit-M"><i class="fas fa-pen"></i></div>
              <div class="options" id="M-delete-M"><i class="far fa-trash-alt"></i></div>
              <div class="options" id="M-close-M" data-dismiss="modal"><i class="fas fa-times"></i></div>
            </div>

            <div class="modalC-body">
            </div>

          </div>
        </div>
      </div>
    </div>`);

  //cierra contenedor de detalles
  $("#close-D").on("click", GanttV.shwDet);
  //Pone el evento que se dispara cuando todo esta listo, (funcion drawArrows)
  $("#cont-main").on("ready.GV", GanttV.readyDraw);
  //envuelve en c2 y c1 (<c1><c2></></>) el contenido dibujado
  cont.children().wrapAll("<div class='c2' />");
  cont.children().wrapAll("<div id='contGanttV' class='c1' />");

  //si el dibujado viene de la interacción del usuario se han guardado los eventos que hay en detalles y ahora se vuelven a poner
  cont.find("#details #cont-dets").append(GanttV.details);

  //aplica estilos del desarrollador que esta usando la libreria
  cont.find(".c1").css(parameters.config);

  //Si showDate es 1 muestra todas las reglas, si es 0 esconde todas las reglas
  if(resultado.options.showDate == 1)
  {
    $("#col-date").show();
    $("#col-week").show();
    $("#col-day").show();
    $("#col-time").show();
  }
  else
  {
    $("#col-date").hide();
    $("#col-week").hide();
    $("#col-day").hide();
    $("#col-time").hide();
  }

  //dibuja el modal para observar los datos de los eventos con click
  $(".gantt").append(`
    <div class="modalC" id="eventsModal">
      <div class="modalC-dialog">
        <div class="modalC-content">

          <div class="modalC-header">
            <h5 class="modalC-title"></h5>
            <div class="options" id="append-details" data-target="#details #cont-dets" data-toggle="tooltip" title="Añadir a detalles"><i class="fas fa-thumbtack"></i></div> 
            <div class="options" id="edit-M" data-toggle="tooltip" title="Editar"><i class="fas fa-pen"></i></div>
            <div class="options" id="delete-M" data-toggle="tooltip" title="Eliminar"><i class="far fa-trash-alt"></i></div>
            <div class="options" id="close-M" data-dismiss="modal"><i class="fas fa-times"></i></div>
          </div>

          <div class="modalC-body">
          </div>

        </div>
      </div>
    </div>`);

  //evento de scroll en el contenedor de los eventos para mover las reglas
  cont.find("#scroll").on("scroll", GanttV.moveTags);
  //Dibuja una sombra en el contenedor de los controles basandose en la posición del scroll
  $("#scroll").on("scroll", GanttV.drawHLine);
  //Events scroll (para mover las etiquetas con el scroll) and click in divs.event (para ver los datos en el modal)
  $("#scroll").on("scroll", GanttV.moveElem);
  //Scroll para mostrar las flechas de ir al final o al principio
  $("#scroll").on("scroll", GanttV.showScrollButtons);
  //evento para mostrar modal en las cajas de evento normal
  // $(".presentation").on("click", ".event:not(.group-events)", GanttV.showModal);
  //Click para los eventos en el popover
  $(".inner-cont").on("click", ".event:not(.group-events)", GanttV.showModal);

  //mostrar el popover de los grupos de eventos
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
    // Check if pointer events are supported.
    if (window.PointerEvent) {
      // Add Pointer Event Listener
      $(".presentation").on("pointerdown", ".group-events", GanttV.showPopOver);
      // swipeFrontElement.addEventListener('pointerdown', this.handleGestureStart, true);
      // swipeFrontElement.addEventListener('pointermove', this.handleGestureMove, true);
      // swipeFrontElement.addEventListener('pointerup', this.handleGestureEnd, true);
      // swipeFrontElement.addEventListener('pointercancel', this.handleGestureEnd, true);
    } else {
      //para mostar el pop-over como un backdrop en movil
      $(".presentation").on("touchstart", ".group-events", GanttV.showPopOver);
    }
    //abrir o cerrar detalles con desplazamientos horizontales sobre el contenedor .c1 (swipe rigth and left)
    $(".c1").on('touchstart', GanttV.gesture4Details);
  }
  else
  {
    $(".presentation").on("mouseenter", ".group-events", GanttV.showPopOver);
    //esconder popover cuando se posiciona otro evento
    $(".presentation").on("mouseenter", ".event:not(.group-events)", GanttV.hidePopOver);
    //esconder popover si se posiciona en el div de presentacion
    $(".presentation").on("mouseenter", GanttV.hidePopOver);
    //esconder popover si se deja el div de este
    $(".inner-cont").on("mouseleave", ".pop-over", GanttV.hidePopOver);
  }

  //boton añadir a lista de detalles, toma la información del modal y la copia en el contenedor de detalles
  $("#append-details, #M-append-details").on("click", GanttV.appendDetail);
  //borrar evento de lista de detalles
  $(".details").on("click", ".detail .delD", GanttV.deleteDet);
  //ir a un evento desde la lista de detalles
  $(".details").on("click", ".detail .gotoEvD", GanttV.goToEvent);
  //borrar todos los eventos de la lista de detalles
  $("#clearDetBtn").on("click", GanttV.deleteDetAll);
  //tooltip para el boton
  $("#searchDate").tooltip({title: "Ir a..."});
  //calendario para posicionarse en el gantt
  gestionDRP.rangeDRP($("#searchDate"), resultado.options.start, resultado.options.end);
  //evento cuando aplica la opcion del calendario
  $("#searchDate").on("apply.daterangepicker", GanttV.searchDate);

  //altura contenedor
  height = cont.find("#cont-main").height();
  // height = GanttV.config.height - 1;

  //altura cajas de cuadricula
  box = height/resultado.options.length;

  //concuerda con el css .time-item para que la altura minima sea 1em
  if( box < parseInt($(1).toPx()) )
  {
    box = parseInt($(1).toPx());
  }

  // Decisión para la primer escala en los controles, de acuerdo a esto llama a la función correspondiente
  switch(resultado.options.scale)
  {
    case "Year":
      GanttV.yearScale(resultado, box, cont);
      break;

    case "Month":
      GanttV.monthScale(resultado, box, cont);
      break;

    case "Week":
      GanttV.weekScale(resultado, box, cont);
      break;

    case "Day":
      GanttV.dayScale(resultado, box, cont);
      break;

    default:
      throw new Error('Unknown Scale Option');
  }
  // Se comenta la funcion que dibuja los botones 
  // para ir al inicio o al final del scroll, crea los eventos para desplar el scroll
  // GanttV.drawScrollBtns(cont, parameters.config);
}

/* Las funciones de cada escala toman como referencia las fechas de inicio y fin globales para crear el intervalo
  de dibujo de los eventos (int)*/

/* yearScale */
/* Escala de años con diferentes opciones de zoom, es decir se puede observar
  en años y meses, el intervalo se toma en Meses */
GanttV.yearScale = function(resultado, box, cont) {
  //start-end
  var mStart = moment(resultado.options.start);
  var mEnd = moment(resultado.options.end);
  var int = moment.duration(mEnd.diff(mStart));//cantidad de cajas a dibujar
  
  switch(resultado.options.zoom)
  {
    case "month":
      int = int.asMonths();
      GanttV.drawBoxes(mStart, box, int, cont, 1, resultado.data, resultado.options.scale, resultado.options.zoom);
      break;

    default:
      throw new Error('Unknown Zoom Option');
  }
}

/* monthScale */
/* Escala de meses con las opciones de mostrar por semana, el intervalo se toma en semanas */
GanttV.monthScale = function(resultado, box, cont) {
  //start-end
  var mStart = moment(resultado.options.start);
  var mEnd = moment(resultado.options.end);
  var int = moment.duration(mEnd.diff(mStart));//cantidad de cajas a dibujar

  switch(resultado.options.zoom)
  {
    case "week":
      int = int.asWeeks();
      GanttV.drawBoxes(mStart, box, int, cont, 1, resultado.data, resultado.options.scale, resultado.options.zoom);
      break;

    default:
      throw new Error('Unknown Zoom Option');
  }
}

/* weekScale */
/* Escala de semana con las opciones de mostrar por día, el intervalo se toma en dias */
GanttV.weekScale = function(resultado, box, cont) {
  //start-end
  var mStart = moment(resultado.options.start);
  var mEnd = moment(resultado.options.end);
  var int = moment.duration(mEnd.diff(mStart));//cantidad de cajas a dibujar

  switch(resultado.options.zoom)
  {
    case "day":
      int = int.asDays();
      GanttV.drawBoxes(mStart, box, int, cont, 1, resultado.data, resultado.options.scale, resultado.options.zoom);
      break;

    default:
      throw new Error('Unknown Zoom Option');
  }
}

/* dayScale */
/* Escala de días con las opciones de horas, 30min, 15min, 5min, 1min, el intervalo se toma en horas y minutos */
GanttV.dayScale = function(resultado, box, cont) {
  //start-end
  var mStart = moment(resultado.options.start);
  var mEnd = moment(resultado.options.end);
  var int = moment.duration(mEnd.diff(mStart));//cantidad de cajas a dibujar

  switch(resultado.options.zoom)
  {
    case "hour":
      int = int.asHours();
      GanttV.drawBoxes(mStart, box, int, cont, 1, resultado.data, resultado.options.scale, resultado.options.zoom);
      break;

    case "hHour":
      int = int.asHours()*2;
      GanttV.drawBoxes(mStart, box, int, cont, 2, resultado.data, resultado.options.scale, resultado.options.zoom);
      break;

    case "qHour":
      int = int.asHours()*4;
      GanttV.drawBoxes(mStart, box, int, cont, 4, resultado.data, resultado.options.scale, resultado.options.zoom);
      break;

    case "5m":
      int = int.asMinutes()/5;
      GanttV.drawBoxes(mStart, box, int, cont, 12, resultado.data, resultado.options.scale, resultado.options.zoom);
      break;

    case "1m":
      int = int.asMinutes();
      GanttV.drawBoxes(mStart, box, int, cont, 60, resultado.data, resultado.options.scale, resultado.options.zoom);
      break;

    default:
      throw new Error('Unknown Zoom Option');
  }
}

/* appendOptions */
/* crea las opciones a partir del objeto GanttV.controls que las contiene y las renderiza en selectores 
   también las renderiza cuando hay algún cambio en dichos selectores */
GanttV.appendOptions = function(e, escala, zoom, length, shw) {
  var $controls = $("#controles");
  var $select;
  var opt;
  var aZoom;
  var val;
  var obj;
  var rules;

  /* decision para hacer el dibujo inicial, o realizar un renderizado a petición del usuario por medio del evento de
  cambio de algun selector */
  if(e===0) //dibujo inicial
  {
    //encuentra el selector de la escala (Año, Mes, Semana, Dia)
    $select = $controls.find("div[role='escala'] select").empty();
    //dibuja las opciones
    for(var i=0; i<GanttV.controls.length; i++)
    {
      $select = $controls.find("div[role='escala'] select");
      opt=`<option value="${GanttV.controls[i].scale}">${GanttV.controls[i].label}</option>`;
      $select.append(opt);
      //selecciona la opcion del primer selector y busca los valores del segundo selector (zoom) y dibuja las opciones
      if(escala===GanttV.controls[i].scale)
      {
        $select.val(escala);
        aZoom = GanttV.controls[i].zoom;
        $select = $controls.find("div[role='zoom'] select");
        for(var j=0; j<aZoom.length; j++)
        {
          opt=`<option value="${aZoom[j].val}">${aZoom[j].label}</option>`;
          $select.append(opt);
          //selecciona la opcion del segundo selector y dibuja las opciones del tercer selector (length)
          if(zoom===aZoom[j].val)
          {
            $select.val(zoom);
            $select = $controls.find("div[role='length'] select");
            for(var k=0; k<aZoom[j].length.length; k++)
            {
              opt=`<option value="${aZoom[j].length[k]}">${aZoom[j].length[k]}</option>`;
              $select.append(opt);
              //selecciona la opcion del tercer selector
              if(length === aZoom[j].length[k])
              {
                $select.val(length);
              }
            }
            $select = $controls.find("div[role='zoom'] select");
          }
        }

        //dibujo opciones reglas
        rules = GanttV.controls[i].rules;
        $select = $controls.find("div[role='shwDt'] select");
        $select.empty();
        for(var j=0; j<rules.length; j++)
        {
          if(shw === 1)
          {
            $select.append(`<option title=' ' value='#${rules[j].val}' selected>${rules[j].label}</option>`);
          }
          else
          {
            $select.append(`<option title=' ' value='#${rules[j].val}'>${rules[j].label}</option>`);
          }
        }
        //todo esta configurado desde los 'data-*' presentes en el elemento
        $select.selectpicker();
      }
    }
  }
  else if($(e.currentTarget).parent().attr('role') === "escala") //cambio selector de escala
  {
    $select = $controls.find("div[role='zoom'] select");
    $select.empty();
    val = e.currentTarget.value
    obj = GanttV.controls.find( x => x.scale === val );
    aZoom = obj.zoom;
    for(var j=0; j<aZoom.length; j++)
    {
      opt=`<option value="${aZoom[j].val}">${aZoom[j].label}</option>`;
      $select.append(opt);
      if(j==0)
      {
        $select = $controls.find("div[role='length'] select");
        $select.empty();
        for(var k=0; k<aZoom[j].length.length; k++)
        {
          opt=`<option value="${aZoom[j].length[k]}">${aZoom[j].length[k]}</option>`;
          $select.append(opt);
        }
        $select = $controls.find("div[role='zoom'] select");
      }
    }
    //dibujo opciones reglas
    rules = obj.rules;
    $select = $controls.find("div[role='shwDt'] select");
    $select.empty();
    for(var j=0; j<rules.length; j++)
    {
      $select.append(`<option title=' ' value='#${rules[j].val}' selected>${rules[j].label}</option>`);
    }
    $select.selectpicker('refresh');
  }
  else if($(e.currentTarget).parent().attr('role') === "zoom") //cambio selector zoom
  {
    $select = $controls.find("div[role='escala'] select");
    val = $select.val();
    obj = GanttV.controls.find( x => x.scale === val );
    val = e.currentTarget.value
    aZoom = obj.zoom.find( x => x.val === val );
    $select = $controls.find("div[role='length'] select");
    $select.empty();
    for(var k=0; k<aZoom.length.length; k++)
    {
      opt=`<option value="${aZoom.length[k]}">${aZoom.length[k]}</option>`;
      $select.append(opt);
    }
  }
}

/* drawBoxes */
/* dibuja la cuadricula y las etiquetas de cada caja, además de dibujar las reglas */
GanttV.drawBoxes = function(mStart, height, int, cont, escala, data, opt, zoom) {
  var lastDate = mStart.clone();
  var lastWeek = mStart.clone();
  var lastDay = mStart.clone();
  var prevHeight = new Array();//elemento para almacenar la altura del objeto HTML anterior
  var prevTop = new Array();//elemento para almacenar la posicion del objeto HTML anterior
  var conf = new Object();//objeto de configuración para visualización de fechas con la libreria momentJS
  //reglas
  var $ruleDate = $("#col-date");
  var $ruleWeek = $("#col-week");
  var $ruleDay = $("#col-day");
  var $ruleTime = $("#col-time");

  // $ruleDate.show();
  // $ruleWeek.show();
  // $ruleDay.show();
  // $ruleTime.show();
  //Decisión para identificar la opción de la escala y configurar los parametos de la libreria momentJS
  switch(opt)
  {
    case "Year":
      conf.type = "months";
      conf.format = "MMM";
      conf.label = "YYYY-MMM";
      conf.comp = "year()";
      conf.zoom = zoom;
      conf.wLabel = "WW";
      conf.yTooltip = "MMMM YYYY";
      conf.tooltip = "MMMM";
      if(int < 12)
      {
        int = 12;
      }
      break;

    case "Month":
      conf.type = "weeks";
      conf.format = "ddd DD";
      conf.label = "YYYY-MMM";
      conf.comp = "format('YYYY-MM')";
      conf.wComp = "format('WW')";
      conf.zoom = zoom;
      conf.wLabel = "WW";
      conf.yTooltip = "MMMM YYYY";
      conf.tooltip = "dddd DD";
      if(int < 12)
      {
        int = 12;
      }
      break;

    case "Week":
      conf.type = "days";
      conf.format = "ddd DD";
      conf.label = "YYYY-MMM";
      conf.comp = "format('YYYY-MM')";
      conf.wComp = "format('WW')";
      conf.zoom = zoom;
      conf.wLabel = "WW";
      conf.yTooltip = "MMMM YYYY";
      conf.tooltip = "dddd DD";
      if(int < 7)
      {
        int = 7;
      }
      break;

    case "Day":
      conf.type = "hours";
      conf.format = "HH:mm";
      conf.label = "YYYY-MMM";
      conf.comp = "format('YYYY-MM')";
      conf.wComp = "format('WW')";
      conf.dComp = "format('DD')";
      conf.zoom = zoom;
      conf.wLabel = "WW";
      conf.dLabel = "ddd DD";
      conf.yTooltip = "MMMM YYYY";
      conf.dTooltip = "dddd DD";
      // if(int < 8)
      // {
      //   int = 8;
      // }
      break;

    default:
      throw new Error('Unknown Option Boxes');
  }

  //dibuja las lineas horizontales de la cuadricula dependiendo de la cantidad calculada en @int
  for(var i = 0; i<int; i++)
  {
    //line-h of the grid
    cont.find(".rules").append("<div class='rule'></div>");
    //time-rule
    cont.find(".time").append(`<div class="time-item" style="height: ${height}px"></div>`);
    //La suma funciona de acuerdo a @i divido entre @escala y el tipo de intervalo de tiempo @conf.type (año, mes, semana, hora)
    cont.find(".time").children().last().append(`
      <span class="item t-item">${mStart.add(i/escala, conf.type).format(conf.format.split(" ")[0]).toUpperCase()}</span>
      `);
    //decision para darle el formato a las reglas cuando tienen el nombre del dia y el numero o con la semana, (cuestion de visualizacion)
    if(conf.format.split(" ").length > 1)
    {
      cont.find(".time").children().last().append(`
      <span class="item t-item">${mStart.format(conf.format.split(" ")[1]).toUpperCase()}</span>
      `);
    }

    if(opt != "Day")
    {
      cont.find(".time").children().last().children().wrapAll(`<div data-toggle="tooltip" class="item" 
        title="${mStart.format(conf.tooltip).toUpperCase()}" />`);
      cont.find(`.time [data-toggle="tooltip"] .item`).removeClass("item");
    }
    //dibuja las etiquetas de la fecha en la parte izquierda
    if( i == 0 )
    {
      //(1) year-month rule
      prevHeight[0] = 0;
      prevTop[0] = 0;
      cont.find("#col-date .date").append(`<div class="vertical-date">
        <div data-toggle="tooltip" class="date-text" title="${mStart.format(conf.yTooltip).toUpperCase()}">
          <span class="date-text-c">${mStart.format(conf.label).split("-")[1].toUpperCase()}</span>
          <span class="date-text-c">${mStart.format(conf.label).split("-")[0]}</span>
          <!--<span class="label-line"></span>-->
        </div>
      </div>`);
      cont.find("#col-date .date .vertical-date").last().css({top: `0px`, height: `${height}px`});

      //other rules and hide or show rules
      if(opt == "Year")
      {
        //para la escala de año, deja visibles solo las reglas necesarias
        if(conf.zoom == "year" || conf.zoom == "month")
        {
          $ruleWeek.find(".week").empty();
          $ruleDay.find(".day").empty();
          $ruleWeek.hide();
          $ruleDay.hide();
        }
      }
      else if(opt == "Month" || opt == "Week")
      {
        //para la escala de semana, deja visibles solo las reglas necesarias
        $ruleWeek.find(".week").empty();
        $ruleDay.find(".day").empty();
        $ruleWeek.show();
        $ruleDay.hide();

        prevHeight[1] = 0;
        prevTop[1] = 0;
        cont.find("#col-week .week").append(`<div class="vertical-week">
          <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
            <span class="week-text-c">SEM.</span>
            <span class="week-text-c">${mStart.format(conf.wLabel)}</span>
            <!--<span class="label-line"></span>-->
          </div>
        </div>`);
        cont.find("#col-week .week .vertical-week").last().css({top: `0px`, height: `${height}px`});
      }
      else if(opt == "Day")
      {
        //para la escala de dias, mostrar las reglas necesarias
        $ruleWeek.find(".week").empty();
        $ruleDay.find(".day").empty();
        $ruleWeek.show();
        $ruleDay.show();
        //week-rule
        prevHeight[1] = 0;
        prevTop[1] = 0;
        cont.find("#col-week .week").append(`<div class="vertical-week">
          <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
            <span class="week-text-c">SEM.</span>
            <span class="week-text-c">${mStart.format(conf.wLabel)}</span>
            <!--<span class="label-line"></span>-->
          </div>
        </div>`);
        cont.find("#col-week .week .vertical-week").last().css({top: `0px`, height: `${height}px`});
        //day-rule
        prevHeight[2] = 0;
        prevTop[2] = 0;
        cont.find("#col-day .day").append(`<div class="vertical-day">
          <div data-toggle="tooltip" class="day-text" title="${mStart.format(conf.dTooltip).toUpperCase()}">
            <span class="day-text-c">${mStart.format(conf.dLabel.split(" ")[0]).toUpperCase()}</span>
            <span class="day-text-c">${mStart.format(conf.dLabel.split(" ")[1]).toUpperCase()}</span>
            <!--<span class="label-line"></span>-->
          </div>
        </div>`);
        cont.find("#col-day .day .vertical-day").last().css({top: `0px`, height: `${height}px`});
      }
    }
    else
    {
      //realiza la comparación dinámica de acuerdo al objeto de configuracion definido anteriormente en el switch
      if(eval(`lastDate.${conf.comp} == mStart.${conf.comp}`))
      {
        cont.find(".date .vertical-date").last().css({height: `${height*(i+1)-(prevHeight[0]+prevTop[0])}px`});
      }
      else
      {
        prevHeight[0] = cont.find(".date .vertical-date").last().outerHeight();
        prevTop[0] = cont.find(".date .vertical-date").last().position().top;
        lastDate = mStart.clone();
        cont.find(".date").append(`<div class="vertical-date">
          <div data-toggle="tooltip" class="date-text" title="${mStart.format(conf.yTooltip).toUpperCase()}">
            <span class="date-text-c">${mStart.format(conf.label).split("-")[1].toUpperCase()}</span>
            <span class="date-text-c">${mStart.format(conf.label).split("-")[0]}</span>
            <!--<span class="label-line"></span>-->
          </div>
        </div>`);
        cont.find(".date .vertical-date").last().css({top: `${prevHeight[0]+prevTop[0]}px`, height: `${height}px`});
      }

      //filtra de acuerdo a la opcion elegida en la escala
      if(opt != "Year")
      {
        if(opt == "Month" || opt == "Week")
        {
          if(eval(`lastWeek.${conf.wComp} == mStart.${conf.wComp}`))
          {
            cont.find(".week .vertical-week").last().css({height: `${height*(i+1)-(prevHeight[1]+prevTop[1])}px`});
          }
          else
          {
            prevHeight[1] = cont.find(".week .vertical-week").last().outerHeight();
            prevTop[1] = cont.find(".week .vertical-week").last().position().top;
            lastWeek = mStart.clone();
            cont.find(".week").append(`<div class="vertical-week">
              <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
                <span class="week-text-c">SEM.</span>
                <span class="week-text-c">${mStart.format(conf.wLabel)}</span>
                <!--<span class="label-line"></span>-->
              </div>
            </div>`);
            cont.find(".week .vertical-week").last().css({top: `${prevHeight[1]+prevTop[1]}px`, height: `${height}px`});
          }
        }
        else if(opt == "Day")
        {
          if(eval(`lastWeek.${conf.wComp} == mStart.${conf.wComp}`))
          {
            cont.find(".week .vertical-week").last().css({height: `${height*(i+1)-(prevHeight[1]+prevTop[1])}px`});
          }
          else
          {
            prevHeight[1] = cont.find(".week .vertical-week").last().outerHeight();
            prevTop[1] = cont.find(".week .vertical-week").last().position().top;
            lastWeek = mStart.clone();
            cont.find(".week").append(`<div class="vertical-week">
              <div data-toggle="tooltip" class="week-text" title="SEMANA ${mStart.format(conf.wLabel)}">
                <span class="week-text-c">SEM.</span>
                <span class="week-text-c">${mStart.format(conf.wLabel)}</span>
                <!--<span class="label-line"></span>-->
              </div>
            </div>`);
            cont.find(".week .vertical-week").last().css({top: `${prevHeight[1]+prevTop[1]}px`, height: `${height}px`});
          }

          if(eval(`lastDay.${conf.dComp} == mStart.${conf.dComp}`))
          {
            cont.find(".day .vertical-day").last().css({height: `${height*(i+1)-(prevHeight[2]+prevTop[2])}px`});
          }
          else
          {
            //day-rule
            prevHeight[2] = cont.find(".day .vertical-day").last().outerHeight();
            prevTop[2] = cont.find(".day .vertical-day").last().position().top;
            lastDay = mStart.clone();
            cont.find(".day").append(`<div class="vertical-day">
              <div data-toggle="tooltip" class="day-text" title="${mStart.format(conf.dTooltip).toUpperCase()}">
                <span class="day-text-c">${mStart.format(conf.dLabel.split(" ")[0]).toUpperCase()}</span>
                <span class="day-text-c">${mStart.format(conf.dLabel.split(" ")[1]).toUpperCase()}</span>
                <!--<span class="label-line"></span>-->
              </div>
            </div>`);
            cont.find(".day .vertical-day").last().css({top: `${prevHeight[2]+prevTop[2]}px`, height: `${height}px`});
          }
        }
      }
    }
    //Se le resta a la fecha inicial lo que se le habia sumado antes
    mStart.add(-i/escala, conf.type);
  }
  //configura altura de las reglas horizontales y verticales
  $(".rule").css({height: `${height}px`});
  $(".rule").last().css({height: `${height-1}px`});

  //verifica opciones de mostrar u ocultar reglas
  if(typeof(GanttV.show) !== "number" && typeof(GanttV.hide) !== "number")
  {
    for(var i=0; i<GanttV.hide.length; i++)
    {
      $(GanttV.hide[i]).hide();
    }
    for(var i=0; i<GanttV.show.length; i++)
    {
      $(GanttV.show[i]).show();
    }
    var $select = $("div[role='shwDt'] select");
    $select.selectpicker('val',GanttV.show);
  }
  else if(GanttV.show === 0 && GanttV.hide === 0)
  {
    $ruleDate.hide();
    $ruleWeek.hide();
    $ruleDay.hide();
    $ruleTime.show();
    var $select = $("div[role='shwDt'] select");
    $select.selectpicker('val',"#col-time");
  }
  //Invoca la función que dibuja las cajas de los eventos
  GanttV.drawEvents(mStart, height, int, cont, escala, data, opt);
}

/* drawEvents */
/* dibuja los eventos, calculando su tamaño en pixeles y su posicion, por medio de la función convertT2Px */
GanttV.drawEvents = function(mStart, height, int, cont, escala, data, opt) {
  GanttV.mStart = mStart;
  GanttV.height = height;
  GanttV.escala = escala;
  GanttV.opt = opt;

  var mdStart;
  var pInit;
  var pEnd;
  var eHeight;
  var pos;
  var field;
  var lastEH;
  var isCollide;
  var $lastEl;
  var $currEl;
  var idContPop;

  //Recorre el arreglo de los datos para identificar su intervalo de tiempo y convertirlo en pixeles de tamaño y posición
  for(var i=0; i<data.length; i++)
  {
    //Extrae su intervalo de tiempo
    mdStart = moment(data[i].start);
    mdEnd = moment(data[i].end);
    //Calcula la diferencia de estos intervalos con la fecha inicial global (options.start)
    pInit = moment.duration(mdStart.diff(mStart));
    pEnd = moment.duration(mdEnd.diff(mStart));

    //Crea el div que contiene cada evento, colocando algunas opciones en atributos data,
    // data-field se usa para reescribir la información cuando los eventos se agrupan, solo toma los dos primeros elementos
    // data-pop es la información que se muestra al hacer click en el evento
    // data-id identificación unica del evento
    $(".cont-events .presentation").append(`
      <div class="event" 
      data-id="${data[i].id}" 
      data-id_evt="${data[i].id_evt}"
      data-start="${data[i].start}" 
      data-end="${data[i].end}" 
      data-field='${JSON.stringify(data[i].data_field)}' 
      data-pop='${JSON.stringify(data[i].data_pop)}'>
      </div>`);
    //Obtiene posición y altura a partir del tiempo y la opción de visualización
    pos = GanttV.convertT2Px(escala, height, pInit, opt);
    eHeight = GanttV.convertT2Px(escala, height, pEnd, opt)-pos;
    //Configura posición y altura, los eventos tienen una regla de min-heigth: 3px;
    if(lastEH === undefined)
    {
      eHeight -= 2.5;
    }
    else if(eHeight >= 6 && lastEH >= 6)
    {
      eHeight -= 5;
      pos += 2.5;
      if(eHeight <= 3)
      {
        //put a POINT or ARROW
      }
    }
    else if(eHeight >= 6 && lastEH <= 6)
    {
      eHeight -= 5;
      pos += 2.5;
      if(eHeight <= 3)
      {
        //put a POINT or ARROW
      }
    }
    else if(eHeight <= 6 && lastEH >= 6)
    {
      //pos += 2.5;
      if(eHeight <= 3)
      {
        //put a POINT or ARROW
      }
    }

    // Se agrego este switch para identificar el tipo de parada y la producción para ponerle un determinado color 
    // switch (data[i].color) {
    // case '1':
    // $(`.cont-events .presentation .event`).last().css({top: `${pos}px`, height: `${eHeight}px`, "background-color": "#D50000"});
    // break;
    // case '2':
    // $(`.cont-events .presentation .event`).last().css({top: `${pos}px`, height: `${eHeight}px`, "background-color": "#F4511E"});
    // break;
    // case '3':
    // $(`.cont-events .presentation .event`).last().css({top: `${pos}px`, height: `${eHeight}px`, "background-color": "#7986CB"});
    // break;
    // case '4':
    // $(`.cont-events .presentation .event`).last().css({top: `${pos}px`, height: `${eHeight}px`, "background-color": "#039BE5"});
    // break;
    // case '5':
    // $(`.cont-events .presentation .event`).last().css({top: `${pos}px`, height: `${eHeight}px`, "background-color": "#0B8043"});
    // break;
    // default:
    // $(`.cont-events .presentation .event`).last().css({top: `${pos}px`, height: `${eHeight}px`, "background-color": "#E67C73"});
    // break;
    // }

    $(`.cont-events .presentation .event`).last().css({top: `${pos}px`, height: `${eHeight}px`, "background-color": `${data[i].color}`});
    
    lastEH = eHeight;//altura el elemento anterior
    /*dibuja los campos dentro de la caja del evento (data_field), verificando si el 
    nombre del campo será visible o solo el valor de este.*/
    field = data[i].data_field;
    //Función para añadir datos a un elemento de la forma Campo: Valor
    GanttV.appendData($(`.cont-events .presentation .event`).last(), field, field.length);
    //Verifica si la caja del evento presenta sobreflujo (overflow) y elimina los campos hasta que no exista sobreflujo (fn Recursiva)
    GanttV.isOverflow($(`.presentation>.event`).last());

    //elemento anterior y elemento actual
    $lastEl = $(`.presentation>.event`).last().prev();
    $currEl = $(`.presentation>.event`).last();

    //verificar si estan sobre puestos
    isCollide = GanttV.isCollide($currEl[0], $lastEl[0]);
    //si estan sobrepuestos agrupelos, de acuerdo a si se sobreponen dos eventos normales, o un evento tipo grupo con otro evento normal
    if(isCollide === true)
    {
      eHeight = pos - $lastEl.position().top + eHeight;
      pos = $lastEl.position().top;
      $(".cont-events .presentation").append(`<div class="event group-events" data-target="#group-events-${i}" data-group="true"></div>`);
      $(`.cont-events .presentation .event`).last().css({top: `${pos}px`, height: `${eHeight}px`, "background-color": "black"});

      //si se sobrepone con un grupo de eventos
      if($lastEl[0].hasAttribute('data-group'))
      {
        //quita el dibujo del evento tipo grupo anterior y agrega el ultimo evento dibujado al pop over
        $lastEl.remove();
        //configura el evento para ponerlo en el pop oveer
        $currEl = $currEl.detach();
        $currEl.addClass("pop-event").css({top: "", height: ""});
        field = $currEl.data("field");
        GanttV.appendData($currEl, field, 2);

        $(`#${idContPop}`).children().first().append($currEl);
        $(".event.group-events").last().attr("data-target", `#${idContPop}`);
      }
      else
      {
        //si son dos eventos normales, crear el pop-over y luego los cambia a este contendor
        $(".inner-cont").append(`<div class="pop-over" id="group-events-${i}"><div></div><div class="close-po"><i class="fas fa-times"></i></div>`);
        $currEl = $currEl.detach();
        $lastEl = $lastEl.detach();
        $currEl.addClass("pop-event").css({top: "", height: ""});
        $lastEl.addClass("pop-event").css({top: "", height: ""});

        field = $currEl.data("field");
        GanttV.appendData($currEl, field, 2);

        field = $lastEl.data("field");
        GanttV.appendData($lastEl, field, 2);

        idContPop = `group-events-${i}`;
        $(`#group-events-${i}`).children().first().append($lastEl);
        $(`#group-events-${i}`).children().first().append($currEl);
      }
    }

    //Dibujar las etiquetas verticales de cada grupo de eventos
    // GanttV.drawTagsGroup(i, cont, pos, eHeight, data);

  }

  if($(".pop-over").length > 0)
  {
    $(".close-po").on("touchstart", GanttV.hidePopOver);
  }
  //dibujar flechas para los eventos agrupados
  GanttV.drawArrows();
}

/* drawArrows */
/* después de dibujados los eventos verifica si hay evento agrupados y les pone una flecha en el lado izquierdo */
GanttV.drawArrows = function(){
  var $groupEv = $(".cont-events .presentation .group-events");
  var pos;
  var height;

  //de acuerdo a la cantidad de eventos tipo grupo se obtiene un posicion y altura para posicionar correctamente las flechas
  for(var i=0; i<$groupEv.length; i++)
  {
    pos = $($groupEv[i]).position().top;
    height = $($groupEv[i]).height();
    $(".cont-events .presentation").prepend(`<div class="arrow-group" style="top: ${pos+(height/2)-7}px"><i class="fas fa-chevron-right"></i></div>`);
  }
  // gestionModal.cerrar();
  $("#cont-main").trigger("ready.GV");
}

/* drawScrollBtns */
/* dibuja los botones para ir al inicio o al final del scroll, crea los eventos para desplar el scroll */
GanttV.drawScrollBtns = function($cont, config){
  var $target = $("#scroll");[0].offsetHeight
  //draw buttons to scroll start and end
  $target.append(`<div class="button button-up" id="btnUpScroll">
    <i class="fas fa-chevron-up"></i>
  </div>
  <div class="button button-down" id="btnDownScroll">
    <i class="fas fa-chevron-down"></i>
  </div>`);

  //Eventos para el scroll
  $("#btnUpScroll").on("click", {scroll: 0}, GanttV.scroll);
  $("#btnDownScroll").on("click", {scroll: 1}, GanttV.scroll);

  if($target[0].offsetHeight == $target[0].scrollHeight)
  {
    $("#btnUpScroll").hide();
    $("#btnDownScroll").hide();
  }
}

/* isOverflow */
/* quita las etiquetas de cada evento que no se pueden mostrar, 
   verificando si existe overflow de forma recursiva */
GanttV.isOverflow = function($obj)
{
  //calcula el scroll (Y) y la altura del elemento, es recursiva y borra elemento a elemento hasta que no exista mas overflow
  var scrollH = $obj[0].scrollHeight;
  var offsetH = $obj[0].offsetHeight;
  
  if(scrollH > offsetH)
  {
    $obj.children().last().remove();
    GanttV.isOverflow($obj);
  }
  else
  {
    return false;
  }
}

/* appendData */
/* agrega los datos al objeto enviado, se usa para poner datos en los modales y en los eventos agrupados.
   de acuerdo a la estructura Campo: Valor */
GanttV.appendData = function($El, data, length, init=0) {
  $El.empty();
  for(var i=init; i<length; i++)
  {
    line = "<div>";
    if(data[i].Mostrar_Campo === 1)
    {
      line = `${line}<span>${data[i].Campo}: </span>`;
    }
    line = `${line}${data[i].Valor}</div>`;
    $El.append(line);
  }
}

/* drawTagsGroup */
/* dibuja las etiquetas verticales del grupo de eventos, este llamado se ejecuta en un ciclo for */
/*GanttV.drawTagsGroup = function(i, cont, pos, eHeight, data){
  var lHeight;
  //Verificando el campo label de cada objeto se dibuja su etiqueta de grupo
  if(i==0)
  {
    cont.find(".gantt").append(`<div class="vertical-text">
       <span class="label-text" for="${data[i].label.replace(" ","_")}">${data[i].label}</span>
       <!--<span class="label-line"></span>-->
    </div>`);
    cont.find(".vertical-text").last().css({top: `${pos}px`, height: `${eHeight}px`});
  }
  else
  {
    if(data[i].label == data[i-1].label)
    {
       lHeight = pos+eHeight-cont.find(".vertical-text").last().position().top;
       cont.find(".vertical-text").last().css({height: `${lHeight}px`});
    }
    else
    {
       cont.find(".gantt").append(`<div class="vertical-text">
          <span class="label-text" for="${data[i].label.replace(" ","_")}">${data[i].label}</span>
          <!--<span class="label-line"></span>-->
       </div>`);
       cont.find(".vertical-text").last().css({top: `${pos}px`, height: `${eHeight}px`});
    }
  }
}*/

/* convertT2Px */
/* Convierte tiempo a pixeles, dependiendo de la opción seleccionada para la visualización */
GanttV.convertT2Px = function(escala, height, diff, opt) {
  //deacuerdo a la opcion hace la conversion en diferentes escalas de tiempo ( mes, semana, dia, hora )
  switch(opt)
  {
    case "Year":
      return (diff.asMonths()*escala)*height;
      break;

    case "Month":
      return (diff.asWeeks()*escala)*height;
      break;

    case "Week":
      return (diff.asDays()*escala)*height;
      break;

    case "Day":
      return (diff.asHours()*escala)*height;
      break;

    default:
      throw new Error('Unknown Option Boxes');
  }
}

/* drawHLine */
/* Dibuja una sombra en la parte superior del div que contiene los eventos de acuerdo a la posicion del scroll */
GanttV.drawHLine = function(e) {
  if($(this).scrollTop() == 0 )
  {
    $(".inner-cont").removeClass("h-line");
  }
  else
  {
    $(".inner-cont").addClass("h-line");
  }
}

/* moveTags */
/* scroll de los contenedores de las reglas */
GanttV.moveTags = function(e) {
  var scroll = $(`#${this.id}`).scrollTop();
  $("#col-date").scrollTop(scroll);
  $("#col-week").scrollTop(scroll);
  $("#col-day").scrollTop(scroll);
  $("#col-time").scrollTop(scroll);
}

/* moveElem */
/* callback de evento scroll, mueve las etiquetas de las reglas dentro de su contenedor */
GanttV.moveElem = function(e) {
  var y = $(`#${this.id}`).scrollTop();
  var height = $(`#${this.id}`).height()-$(".controls").height();
  var sHeight = y+height;//24-> margin top del div
  var $divsDate = $(".vertical-date");
  var $divsWeek = $(".vertical-week");
  var $divsDay = $(".vertical-day");
  var $divsTime = $(".time-item");
  // var $divsLabel = $(".vertical-text");
  var $spanDate;
  var $spanWeek;
  var $spanDay;
  // var $spanLabel;

  var $div;

  //año-mes
  for(var i=0; i<$divsDate.length; i++)
  {
    $div = $($divsDate[i]);
    if($divsDate[i].offsetTop < y)
    {
      $spanDate = $div.find(".date-text");
      if( y < GanttV.lastScrollTop && y < $div.height()-$spanDate.height()+$div.position().top)
      {
        //subir
        $spanDate.css({top: (y-$divsDate[i].offsetTop)});
      }
      else if( y > GanttV.lastScrollTop && ($spanDate.position().top-$div.position().top) < ($div.height()-$spanDate.height()))
      {
        //bajar
        $spanDate.css({top: (y-$divsDate[i].offsetTop)});
      }
    }
    else
    {
      $spanDate = $div.find(".date-text");
      if( y < GanttV.lastScrollTop )
      {
        $spanDate.css({top: 0});
      }
    }
  }

  //semana
  for(var i=0; i<$divsWeek.length; i++)
  {
    $div = $($divsWeek[i]);
    if($divsWeek[i].offsetTop < y)
    {
      $spanWeek = $div.find(".week-text");
      if( y < GanttV.lastScrollTop && y < $div.height()-$spanWeek.height()+$div.position().top)
      {
          //subir
        $spanWeek.css({top: (y-$divsWeek[i].offsetTop)});
      }
      else if( y > GanttV.lastScrollTop && ($spanWeek.position().top-$div.position().top) < $div.height()-$spanWeek.height())
      {
          //bajar
        $spanWeek.css({top: (y-$divsWeek[i].offsetTop)});
      }
    }
    else
    {
      $spanWeek = $div.find(".week-text");
      if( y < GanttV.lastScrollTop )
      {
        $spanWeek.css({top: 0});
      }
    }
  }

  //día
  for(var i=0; i<$divsDay.length; i++)
  {
    $div = $($divsDay[i]);
    if($divsDay[i].offsetTop < y)
    {
       $spanDay = $div.find(".day-text");
       if( y < GanttV.lastScrollTop && y < $div.height()-$spanDay.height()+$div.position().top)
       {
          //subir
          $spanDay.css({top: (y-$divsDay[i].offsetTop)});
       }
       else if( y > GanttV.lastScrollTop && ($spanDay.position().top-$div.position().top) < $div.height()-$spanDay.height())
       {
          //bajar
          $spanDay.css({top: (y-$divsDay[i].offsetTop)});
       }
    }
    else
    {
      $spanDay = $div.find(".day-text");
      if( y < GanttV.lastScrollTop )
      {
        $spanDay.css({top: 0});
      }
    }
  }

  //time
  for(var i=0; i<$divsTime.length; i++)
  {
    $div = $($divsTime[i]);
    if($divsTime[i].offsetTop < y && ($divsTime[i].offsetTop+$divsTime[i].offsetHeight-16) > y && $divsTime[i].offsetHeight > 35)
    {
       $spanTime = $div.find(".item");
       if( y < GanttV.lastScrollTop && y < $div.height()-$spanTime.height()+$div.position().top)
       {
          //subir
          $spanTime.css({top: (y-$divsTime[i].offsetTop)});
       }
       else if( y > GanttV.lastScrollTop && ($spanTime.position().top-$div.position().top) < $div.height()-$spanTime.height())
       {
          //bajar
          $spanTime.css({top: (y-$divsTime[i].offsetTop)});
       }
    } 
    else
    {
      $spanTime = $div.find(".item");
      if( y < GanttV.lastScrollTop )
      {
        $spanTime.css({top: 0});
      }
    }
  }
  // for(var i=0; i<$divsLabel.length; i++)
  // {
  //   $div = $($divsLabel[i]);
  //   if($divsLabel[i].offsetTop < y)
  //   {
  //      $spanLabel = $div.find(".label-text");
  //      if( y < GanttV.lastScrollTop  && y < $div.height()-$spanDate.height()+$div.position().top)
  //      {
  //         //subir
  //         $div.find(".label-text").css({top: (-sHeight+height+$divsLabel[i].offsetTop)});
  //      }
  //      else if( y > GanttV.lastScrollTop && $spanLabel.position().top < $div.height()-$spanLabel.height())
  //      {
  //         //bajar
  //         $($divsLabel[i]).find(".label-text").css({top: (-sHeight+height+$divsLabel[i].offsetTop)});
  //      }
  //   }
  // }
  GanttV.lastScrollTop = y;
}

/* isCollide */
/* función que verifica si los elementos estan sobre puestos o traslapados entre si 
   devuelve falso o verdadero */
GanttV.isCollide = function(el1, el2) {
  if(el1 !== undefined && el2 !== undefined)
  {
    el1.offsetBottom = el1.offsetTop + el1.offsetHeight;
    el1.offsetRight = el1.offsetLeft + el1.offsetWidth;
    el2.offsetBottom = el2.offsetTop + el2.offsetHeight;
    el2.offsetRight = el2.offsetLeft + el2.offsetWidth;
    
    return !((el1.offsetBottom < el2.offsetTop) ||
             (el1.offsetTop > el2.offsetBottom) ||
             (el1.offsetRight < el2.offsetLeft) ||
             (el1.offsetLeft > el2.offsetRight))
  }
  else
  {
    return "No Data";
  }
};

/* makeId */
/* crea un id aleatorio de tamaño @length */
GanttV.makeId = function(length) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < length; i++)
  {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }

  return text;
}

/* isEllipsis */
/* calcula overflow en ancho, se usa para los titulos de los modales */
GanttV.isEllipsis = function(elem) {
  return (elem.offsetWidth < elem.scrollWidth);
}

/* readyDraw */
/* callback evento personalizado, despues de cargados los dibujos, 
   verifica el ancho de la pantalla y quita el modal de carga,
   además activa los tooltips */
GanttV.readyDraw = function(e) {
  //verificacion para movil (ancho <= 768px)
  var mediaQuery = window.matchMedia("(max-width: 768px)");
  mediaQuery.addListener(GanttV.validateWindow);
  GanttV.validateWindow(mediaQuery);

  //cerrar spinner/backdrop
  $("#spinnerG").remove();
  $("#backd").remove();
  //activa los tooltop
  $('[data-toggle="tooltip"]').tooltip("dispose");
  $('[data-toggle="tooltip"]').tooltip({trigger: "hover"});

  GanttV.callback();
}

/*----------------------------------------------------------------------------------------*/
/*----------------------------User Interaction--------------------------------------------*/
/*----------------------------------------------------------------------------------------*/

/* showModal */
/* callback evento click en los diferentes eventos dibujados. Dibuja el modal con los datos del objeto data_pop,
   verifica además si es movil (width <= 768px) o desktop (width > 768px), si es movil invoca la funcion
   showModalMobile */
GanttV.showModal = function(e) {

  //verificacion para movil (ancho <= 768px)
  var mediaQuery = window.matchMedia("(max-width: 768px)");

  //si no es menor a 768px actua de manera normal en desktop
  if(!GanttV.validateWindow(mediaQuery)){
    //identifica el modal
    var $modal = $("#eventsModal");

    //ancho contenedor de detalles
    var widthD = $("#details").offset().left;

    if(widthD == 0)
    {
      // Se modifico la posicion donde saldra el modal ya que se modifico
      // el contenedor de detalles
      widthD = $(".c1").width() + $(".c1").offset().left - ($(".c1").width()*0.45);
    }
    else
    {
      widthD += 19;
    }

    //coloca atributo data-id para realizar cualquier accion en el modal (editar, eliminar)
    $modal.attr("data-id",$(this).attr("data-id"));
    $modal.attr("data-id_evt", $(this).attr("data-id_evt"));
    $modal.data("id",$(this).attr("data-id"));


    $(".modalC-open").removeClass("modalC-open");
    $(e.currentTarget).addClass("modalC-open");
    var modalHeight;
    var jPop = JSON.parse(e.currentTarget.dataset.pop);
    var $mBody = $modal.find(".modalC-body");
    var line;
    var parent = $(this).parents(".pop-over");
    //calcula posiciones del modal
    var top = $(this).offset().top;
    var left = $(this).offset().left;
    // var scroll = e.currentTarget.offsetTop;
    var contHeight = $(this).parents(".inner-cont").height();

    //Dibuja el titulo del modal
    $mBody.prev(".modalC-header").find("h5").empty().append(jPop[0].Valor);

    //dibuja el cuerpo del modal
    $mBody.empty();
    //drawData in the modal body
    GanttV.appendData($mBody, jPop, jPop.length, 1);

    //si el llamado es de eventos dentro del pop-over (eventos agrupados), calcula la posicion de acuerdo al evento tipo grupo
    // if(parent.length > 0)
    // {
    //   // $(".modalC-open").removeClass("modalC-open");
    //   top = $(`[data-target='#${parent.attr("id")}']`).offset().top;
    //   left = $(`[data-target='#${parent.attr("id")}']`).offset().left;
    //   scroll = $(`[data-target='#${parent.attr("id")}']`).position().top;
    //   // $(`[data-target='#${parent.attr("id")}']`).addClass("modalC-open");
    // }

    //calcular posicion el contenedor y ponerlo 100px abajo
    //$(id)[0].offsetTop;
    top = $("#scroll").offset().top + 100;

    //hace scroll al contendor
    // $("#scroll").scrollTop(scroll-100);

    //muestra el modal
    $modal.modal({backdrop: false, show: true});

    //posicion el modal
    $modal.css({top : `${top}px`, left: `calc(${widthD-$modal.width()}px - 23px)`}); //10px del ancho del scroll

    //calcula la altura del modal
    modalHeight = $modal.find(".modalC-dialog").height();

    //si la posicion y la altura del modal sobrepasan el tamaño de la ventana, lo cambia de posicion (superior)
    if((top+modalHeight) >= window.innerHeight)
    {
      top -= modalHeight;
    }

    //recalcula la posicion del modal
    $modal.css({top : `${top}px`});

    //si hay overflow en el titulo agrega un tooltip si no lo quita
    if( GanttV.isEllipsis($mBody.prev(".modalC-header").find("h5")[0]) )
    {
      $mBody.prev(".modalC-header").find("h5").tooltip("dispose");
      $mBody.prev(".modalC-header").find("h5").tooltip({"title": jPop[0].Valor});
    }
    else
    {
      $mBody.prev(".modalC-header").find("h5").tooltip("dispose");
      $mBody.prev(".modalC-header").find("h5").removeAttr("title data-toggle");
    }
  }
  else
  {
    /*Llama a la funcion de mostrar modal en movil*/
    GanttV.showModalMobile(e);
  }
}

/* showPopOver */
/* callback de evento mouseenter, muestra div con los eventos agrupados */
GanttV.showPopOver = function(e) {

  var $target = $($(e.currentTarget).data("target"));

  if(e.type === "touchstart" || e.type === "pointerdown")
  {
    e.preventDefault();
  }
  else
  {
    //calcula las posicion del evento y calcula las posicion que tendra inicialmente el pop-over
    var offset = $(this).parents(".inner-cont").offset();
    var contWidth = $(this).parents(".inner-cont").width();
    var contHeight = $(this).parents(".inner-cont").height();
    var popWidth = $target.width();
    var popHeight = $target.height();
    var relativeX = (e.clientX - offset.left)-50;
    var relativeY = (e.clientY - offset.top)-50;

    //calcula donde posicionar el pop-over de acuerdo a la posicion del evento mouseenter
    if((relativeX+popWidth) >= contWidth)
    {
      //put left
      relativeX -= popWidth;
    }
    if((relativeY+popHeight) >= contHeight)
    {
      //put up
      relativeY -= popHeight;
    }

    //posiciona el pop-voer
    $target.css({top: `${relativeY}px`, left: `${relativeX}px`});
  }
  //esconde los demas pop-over
  $(".pop-over").hide();
  //muestra el pop-over actual
  $target.show();
}

/* hidePopOver */
/* callback de evento mouseleave, esconde div con los eventos agrupados */
GanttV.hidePopOver = function(e) {
  $(".pop-over").hide();
}

/* refreshGantt */
/* callback de la interacción del usuario con el boton de refrescar,
   recoge los datos de los controles y realiza de nuevo la consulta,
   utilizando como función de callback a GanttV.receiveInfo.
   En el caso de mostrar fecha solo muestra o esconde el div que
   contiene las etiquetas */
GanttV.refreshGantt = function(e) {
  //extrae las opciones de los controles
  var opts = ["#col-date", "#col-week", "#col-day", "#col-time"];
  var escala = $(".controls div[role='escala'] select").val();
  var zoom = $(".controls div[role='zoom'] select").val();
  var length = +$(".controls div[role='length'] select").val();
  var $shwDt = $("div[role='shwDt'] select");
  var options = GanttV.resultado.options;
  var parameters;
  var values;
  var hide;
  var show;

  values = $shwDt.selectpicker('val');
  //extrae opciones del selector de reglas
  hide = opts.filter( x => !values.includes(x));
  show = opts.filter( x => values.includes(x));

  //esconde las opciones que se filtran en hide
  if(hide.length > 0)
  {
    for(var i=0; i<hide.length; i++)
    {
      $(hide[i]).hide();
    }
  }

  //muestra las opciones que estan en show
  if(show.length > 0)
  {
    for(var i=0; i<show.length; i++)
    {
      $(show[i]).show();
    }
  }

  //guarda en las variables goblales el valor de estos arreglos
  GanttV.hide = hide;
  GanttV.show = show;

  //Si las opciones han cambiado dibuje de nuevo todo, y actualice los datos de options
  if(escala !== options.scale || zoom !== options.zoom || length !== options.length)
  {
    $(id_cont).append(`<div id="spinnerG" class="spinner-border text-light"
    style="top: ${parseInt(GanttV.config.height)/2}px""></div>`);
    $(id_cont).append(`<div id="backd" style="width: ${$(GanttV.id_cont).width()}px; height: calc(${GanttV.config.height} + 20px)"></div>`);
    $("#details").hide();
    GanttV.details = $("#details #cont-dets .detail").detach();
    GanttV.resultado.options.scale = escala;
    GanttV.resultado.options.zoom = zoom;
    GanttV.resultado.options.length = length;
    GanttV.resultado.options.showDate = 1;
    parameters = {
      id_cont: GanttV.id_cont,
      init: false,
      config: GanttV.config,
    };
    //consulta
    GanttV.getData(parameters);
  }
}

/* shwDet */
/* muestra el contenedor con los modales que se han añadido
   y mueve los botones del scroll */
GanttV.shwDet = function(e) {
  //verificacion para movil (ancho <= 768px)
  var mediaQuery = window.matchMedia("(max-width: 768px)");

  //si no es menor a 768px actua de manera normal en desktop
  var id = $(this).data("target");

  if(!mediaQuery.matches)
  {
    if($(`${id}:hidden`).length === 1)
    {
      $("#btnUpScroll").animate({right: "32%"}, 300);
      $("#btnDownScroll").animate({right: "32%"}, 300);
    }
    else
    {
      $("#btnUpScroll").animate({right: "1rem"}, 300);
      $("#btnDownScroll").animate({right: "1rem"}, 300);
    }
  }

  $(id).toggle(300);
  // $("#cont-dets").css({height: `calc(100% - ${$("#cont-dets").prev().height()}px)`});
}

/* appendDetail */
/* añade la información del modal a el contenedor de detalles */
GanttV.appendDetail = function(e) {
  var mediaQuery = window.matchMedia("(max-width: 768px)");
  var id = $(".modalC-open").attr("data-id-detail");
  var scroll = 0;
  //verifica si existe ya el evento en detalles
  if($(id).length === 0)
  {
    //clona los datos del modal
    var $items = $(this).parent().next().clone();
    var title = $(this).prevAll(".modalC-title").text()
    var $target = $($(this).data("target"));
    var dataId = $(this).parents("#eventsModal").attr("data-id");
    var Id_evt = $(this).parents("#eventsModal").attr("data-id_evt");

    if(dataId === undefined)
    {
      dataId = $(this).parents("#evsModalMobile").attr("data-id");
      Id_evt = $(this).parents("#evsModalMobile").attr("data-id_evt");
    }
    //crea un id aleatorio
    id = GanttV.makeId(5);

    //verifica que no exista otro id igual
    while($(`#${id}`).length != 0)
    {
      id = GanttV.makeId(5);
    }

    $(".modalC-open").attr("data-id-detail", `#${id}`);
    //añade los datos del modal en un elemento del contenedor de detalles
    $target.append(`<div class="detail" id="${id}" data-id="${dataId}" data-id_evt="${Id_evt}">
        <div role="delete-d" class="text-right px-1" data-target="#${id}">
          <span class="gotoEvD"><i class="fas fa-calendar-day"></i></span>
          <span class="editEvD"><i class="fas fa-pen"></i></span>
          <span class="delEvD"><i class="far fa-trash-alt"></i></span>
          <span class="delD"><i class="fas fa-times-circle"></i></span>
          <div class="titleD">${title}</div>
        </div>
      </div>`);
    $(".gotoEvD").tooltip("dispose").tooltip({title: "Ir a evento"});
    $(".editEvD").tooltip("dispose").tooltip({title: "Editar evento"});
    $(".delEvD").tooltip("dispose").tooltip({title: "Eliminar evento"});
    $(".delD").tooltip("dispose").tooltip({title: "Quitar de detalles"});
    $target.find(`#${id}`).append($items);
    $(`#${id}`).addClass("selectEv");
    scroll = $(`#${id}`)[0].offsetTop;
  }
  else
  {
    //pone efecto de blink en detalles y desplaza el scroll
    $(id).addClass("selectEv");
    scroll = $(id)[0].offsetTop;
  }
  setTimeout(GanttV.deselectEv, 2000);

  //abre contenedor de detalles, y corre los botones de scroll
  if($("#details:hidden").length === 1 && !mediaQuery.matches)
  {
    $("#btnUpScroll").animate({right: "32%"}, 300);
    $("#btnDownScroll").animate({right: "32%"}, 300);
  }

  $("#details:hidden").show(300);
  $("#details").animate({scrollTop: scroll-10}, 500);

  // debido que cuando se termina de dibujar el gantt, si no hay notas en detalles
  // los eventos de editar y eliminar no funciona, por lo cual se debe asignar los
  // eventos cada vez que se agregue notas en detalles
  M_registro_linea_de_envase.funciones.ganttEvents();

}

/* deleteDet */
/* elimina un objeto de la lista de detalles y el atributo de su evento correspondiente */
GanttV.deleteDet = function(e) {
  // Se agrega este metodo para que cuando borre el modal se borre el tooltip también
  $(".tooltip").tooltip("dispose");
  var id = $(this).parent().data("target");
  $(`[data-id-detail="${id}"]`).removeAttr("data-id-detail");
  $(id).remove();
}

/* deleteDetAll */
/* elimina todos los objetos de la lista de detalles y todos los
   atributos en sus correspondientes eventos */
GanttV.deleteDetAll = function(e) {
  $("[data-id-detail]").removeAttr("data-id-detail")
  $(".detail").remove();
}

/* searchDate */
/* desplaza el scroll hasta una fecha y hora indicada desde el calendario del daterangepicker */
GanttV.searchDate = function(e, picker) {
  //extrae fecha seleccionada
  var date = picker.startDate;
  //diferencia de fechas
  var pInit = moment.duration(date.diff(GanttV.mStart));
  //conversion de tiempo a pixeles
  var scroll = GanttV.convertT2Px(GanttV.escala, GanttV.height, pInit, GanttV.opt);
  //scroll a esos pixeles
  $("#scroll").animate({scrollTop: scroll}, 500);
}

/* goToEvent */
/* desplaza el scroll hasta un evento especifico, y le agrega efecto de seleccion */
GanttV.goToEvent = function(e) {
  var mediaQuery = window.matchMedia("(max-width: 768px)");
  if(mediaQuery.matches)
  {
    $("#details").toggle(300);
    $(".gotoEvD").tooltip("hide");
  }
  //ir a evento
  var id = $(this).parents(".detail").data("id_evt");
  var $ev = $(`.event[data-id_evt="${id}"]`);
  var scroll = $ev.position().top;
  var left = 0;
  if($ev.parents(".pop-over").length != 0)
  {
    scroll = $(`[data-target="#${$ev.parents(".pop-over").attr("id")}"]`).addClass("selectEv").position().top;
    left = $(`[data-target="#${$ev.parents(".pop-over").attr("id")}"]`).position().left;
    $("#scroll").animate({scrollTop: scroll-100}, 300);

    if( !/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
      $ev.parents(".pop-over").css({left: `${left}px`})
    }
    $ev.parents(".pop-over").show();

    scroll = $ev[0].offsetTop;
    $ev.parent().animate({scrollTop: scroll-10}, 200);
  }
  else
  {
    $("#scroll").animate({scrollTop: scroll-100}, 500);
  }
  $ev.addClass("selectEv");
  setTimeout(GanttV.deselectEv, 2000);
}

/* deselectEv */
/* quita la clase que tiene la animación de seleccionar */
GanttV.deselectEv = function() {
  $(".selectEv").removeClass("selectEv");
  $(".pop-over").hide();
}

/* scrollButtons */
/* dibuja los botones y les pone los eventos para ir al principio o al final */
GanttV.showScrollButtons = function(e) {
  var $el = $(this);
  if($el.scrollTop() == 0 )
  {
    $("#btnUpScroll").hide();
  }
  else 
  {
    $("#btnUpScroll").show();
  }

  if( ($el.scrollTop() + $el.height()) >= $el[0].scrollHeight )
  {
    $("#btnDownScroll").hide();
  }
  else
  {
    $("#btnDownScroll").show();
  }
}

/* scroll */
/* scroll hacia el inicio o al final */
GanttV.scroll = function(e) {
  var opt = e.data.scroll;
  switch(opt)
  {
    case 0:
      $("#scroll").animate({scrollTop: opt}, 500);
      break;

    case 1:
      $("#scroll").animate({scrollTop: $("#col-ev").height()}, 500);
      break;

    default:
      throw new Error('Unknown Scale Option');
  }
}

/*----------------------------------------------------------------------------------*/
/*----------------------------------------Movil-------------------------------------*/
/*----------------------------------------------------------------------------------*/

/* validateWindow */
/* verifica el ancho de la pantalla, si es menor a 768px que es el llamado
  entonces esconde las reglas secundarias */
GanttV.validateWindow = function(e) {
  var opts = ["#col-date", "#col-week", "#col-day", "#col-time"];
  var values = new Array();

  if (e.matches) {
    /* The viewport width is currently <= 768px */
    values[0] = "#col-time";
    //extrae opciones del selector de reglas
    hide = opts.filter( x => !values.includes(x));

    //esconde las opciones que se filtran en hide
    if(hide.length > 0)
    {
      for(var i=0; i<hide.length; i++)
      {
        $(hide[i]).hide();
      }
    }
    //esconde el contenedor de detalles
    $("#details:visible").hide();
    $("#cont-controls:visible").hide();

    return true;
  }
  else
  {
    /* The viewport width is currently > 768px */
    $("#cont-controls:hidden").show();
    return false;
  }
}

/* showControls */
/* evento disparado por el boton del icono bars para mostrar los controles */
GanttV.showControls = function(e) {
  var $mBody = $("#evsModalMobile:visible").find(".modalC-body");
  var $nav = $("#cont-controls");

  /* cambiar tamaño de modal si esta abierto y los controles se esconden */
  if($("#cont-controls:visible").length === 1)
  {
    $mBody.css({height: GanttV.modalHeight});
  }
  $nav.slideToggle(300, GanttV.controlsToggle);
}

/* controlsToggle */
/* callback del metodo slideToggle de los controles, cambia el tamaño del modal si esta abierto */
GanttV.controlsToggle = function(e) {
  var $nav = $(this);
  var $mBody = $("#evsModalMobile:visible").find(".modalC-body");
  //revisar condicion para saber si esta abierto o cerrado
  if($("#cont-controls:visible").length === 1)
  {
    $mBody.css({height: `${$mBody.height()-$nav.height()}px`});
  }
  // else
  // {
  //   $mBody.css({height: GanttV.modalHeight});
  // }
}

/* showModalMobile */
/* muestra el modal como un backdrop que cubre todo el contenedor principal del gantt */
GanttV.showModalMobile = function(e) {
  $("#cont-controls:visible").slideToggle(300);
  //identifica el modal
  var $modal = $("#evsModalMobile");

  //coloca atributo data-id para realizar cualquier accion en el modal (editar, eliminar)
  $modal.attr("data-id", $(e.currentTarget).attr("data-id"));
  $(".modalC-open").removeClass("modalC-open");
  $(e.currentTarget).addClass("modalC-open");
  var modalHeight;
  var jPop = JSON.parse(e.currentTarget.dataset.pop);
  var $mBody = $modal.find(".modalC-body");
  var line;

  //Dibuja el titulo del modal
  $mBody.prev(".modalC-header").find("h5").empty().append(jPop[0].Valor);

  //dibuja el cuerpo del modal
  $mBody.empty();
  //drawData in the modal body
  GanttV.appendData($mBody, jPop, jPop.length, 1);

  //si hay overflow en el titulo agrega un tooltip si no lo quita
  if( GanttV.isEllipsis($mBody.prev(".modalC-header").find("h5")[0]) )
  {
    $mBody.prev(".modalC-header").find("h5").tooltip("dispose");
    $mBody.prev(".modalC-header").find("h5").tooltip({"title": jPop[0].Valor});
  }
  else
  {
    $mBody.prev(".modalC-header").find("h5").tooltip("dispose");
    $mBody.prev(".modalC-header").find("h5").removeAttr("title data-toggle");
  }

  $modal.modal({backdrop: false, show: true});
  GanttV.modalHeight = `calc(${$("#cont-main").height()-$mBody.prev().height()}px - 0.5rem)`;
  $mBody.css({height: GanttV.modalHeight});
}

/* gesture4Details */
/* Captura el evento de touchstart y touchmove sobre el contenedor de .c1, el desplazamiento 
   que se captura es horizontal, para abrir o cerrar el contenedor de los detalles */
GanttV.gesture4Details = function(e){

  var swipe = e.originalEvent.touches,
  //captura la posicion inicial del movimiento
  start = swipe[0].pageX;

  //captura el evento de touchmove
  $(`${GanttV.id_cont} .c1`).on('touchmove', function(e) {
    var contact = e.originalEvent.touches,
    //posicion final del movimiento
    end = contact[0].pageX,
    //calcula la distancia desplazada
    distance = end-start;

    //abre
    if (distance < -100 && $("#details:visible").length === 0){
      $("#details").show(300);
    }
    else if (distance > 100 && $("#details:visible").length === 1){
      //cierra
      $("#details").hide(300);
    }
  }).one('touchend', function() {
    //pone 1 vez el evento touchend y quita los eventos de touchmove y touchend
    $(this).off('touchmove touchend');
  });
}
