var gestionDT = new Object();
gestionDT.sIdx=0; //iterar dentro de evento search, ya que al llamar al método search se dispara un evento, pero al llamar el metodo draw tamién se dispara este evento, solo se necesista que corra 1 vez
gestionDT.$initModalBody=""; //Guarda el estado actual del modal al abrirlo
gestionDT.Fchange; //Flag change, para saber si se filtro o no la tabla anteriormente
gestionDT.urlConexion;
gestionDT.replace;

/* método initTable */
/* Hace el llamado para obtener las columnas a consultar y dibujar 											*/
/* @param col_extra, es el titulo de la columna extra que se puede adicionar 								*/
/* @param col_extra_html, es el texto a html que lleva esta columna en cada una de sus celdas, se repite 	*/
/* @param callbackSelect, funcion de callback para el evento de seleccionar una fila */
/* @var parameters, prepara los parametros necesarios para realizar las configuraciones después de llamar 	*/
/* 				   al webservice																			*/
gestionDT.initTable = function (urlConexion, nombre, id_table, callback,replace,col_extra="", col_extra_html="") {
	gestionDT.urlConexion = urlConexion;
	var metodo = "data";
	var col_name = metodo+'_column_names';
	gestionDT.deleteDT( id_table );

	if(replace == undefined || replace == null){
		gestionDT.replace = " ";
	}
	else{
		gestionDT.replace = JSON.stringify(replace);
	}

	var parameters = {
		metodo,
		id_table,
		col_extra,
		col_extra_html,
		cols: "",
		callback,
		urlConexion,
		nombre
	};

	query.callAjax( urlConexion, col_name, {nombre: nombre}, gestionDT.receiveInfo, parameters );
	//new $.fn.dataTable.Api( "#tabla" ); //Obtener Api datable object
}

/* método receiveInfo */
/* Recibe la información de las columnas y se la envia a otras funciones para dibujar la tabla y obtener los filtros */
/* @param columns, respuesta del AJAX 									   */
/* @param parameters, var pasada a través de la función que invocó el AJAX */
/* @var $table, objeto JQuery de la tabla que se va a dibujar en el html   */
gestionDT.receiveInfo = function (columns, parameters) {
	var $table = $(parameters.id_table); // tabla objetivo
	var n_col = columns.length;
	// Se construyen los headers de la tabla
	// thead-> Principal					
	// filter-> Head donde estan los filtros

	$table.empty();
	$table.append(`<thead><tr class="thead"></tr></thead>
	<thead><tr class="filter"></tr></thead>`);

	//Si contiene DT_RowId quitelo para no tomar en cuenta esa columna en la visualizacion y en las consultas de los filtros
	columns = columns.filter( c => c.data != "DT_RowId" );

	//Si se define una columna extra
	if( parameters.col_extra != "" ){
		n_col++;
	}

	gestionDT.Fchange = new Array(n_col).fill(1);

	gestionDT.drawTable(columns, parameters);
	gestionDT.drawFilters(columns, parameters);
}

/* método drawModal */
/* La primera ejecución recibe los datos de las columnas, nombre y tipo, dibujando N modales para cada filtro avanzado, */
/* habilitando los diferentes eventos para cada modal */
gestionDT.drawModal = function (parameters) {
	var auxCol = 0;

	var opt = {
		number: [ { label: "Es igual a", value: "=" },
				  { label: "No es igual a", value: "!=" },
				  { label: "Mayor que", value: ">" },
				  { label: "Mayor o igual que", value: ">=" },
				  { label: "Menor que", value: "<"},
				  { label: "Menor o igual que", value: "<=" } ],

		text: [ { label: "Es igual a", value: "=" },
				{ label: "No es igual a", value: "!=" },
				{ label: "Comienza por", value: "LIKE**/'_%'"},
				{ label: "Termina con", value: "LIKE**/'%_'"},
				{ label: "Contiene", value: "LIKE**/'%_%'"},
				{ label: "No contiene", value: "NOT**/LIKE**/'%_%'"} ]
	}

	$("#divtabla").remove("[data-id='modals']").append("<div data-id='modals'></div>");
	$("#divtabla [data-id='modals']").empty();

	if( parameters.col_extra != "" ){
		auxCol = 1;
	}

	for (var i = auxCol; i < parameters.columns.length; i++)
	{
		var hModal = `
		  <div class="modal" id="customFilter${i}">
		    <div class="modal-dialog">
		      <div class="modal-content">
		      
		        
		        <div class="modal-header">
		          <h5 class="modal-title" data-id="modal-title">Mostrar las filas en las cuales: <span class="form-check">${parameters.columns[i].data}</span></h5>
		          <button type="button" class="close" data-dismiss="modal" data-id="close_${i}">&times;</button>
		        </div>
		        
		        <div class="modal-body" data-id="modal-body${i}">
		    		<div class="row">
		    			<div class="col-4 px-2">
			        		<select class="form-control" id="opt_${i}"></select>
			        	</div>
			        	<div class="col-6 input-group">
			        			<input class="form-control" id="data_${i}" />
		        		</div>
						<div class="col-2 text-center">
							<a class="cursor_sel" id="add_${i}"><i class="fas fa-plus-circle text-success"></i></a>
						</div>
		        	</div>
		        </div>
		        
		        <div class="modal-footer" data-id="modal-footer">
		          <div class="col-6">
		          	<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" id="cleanF_${i}">Limpiar Filtro</button>
		          </div>
		          <div class="col-6 text-right">
		          	<button type="button" class="btn btn-primary btn-sm" data-dismiss="modal" id="accept_${i}">Aplicar</button>
		          </div>
		        </div>
		        
		      </div>
		    </div>
		  </div>`;
		
		$("#divtabla [data-id='modals']").append(hModal);

		//Instrucciones dependiendo el tipo de dato
		if(parameters.columns[i].type == "int" || parameters.columns[i].type == "float")
		{
			for( var j in opt.number )
			{
				$(`#customFilter${i} #opt_${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
			}
		}
		else if(parameters.columns[i].type == "varchar" || parameters.columns[i].type == "nvarchar")
		{
			for( var j in opt.text )
			{
				$(`#customFilter${i} #opt_${i}`).append(`<option value="${opt.text[j].value}">${opt.text[j].label}</option>`);
			}
		}
		else if(parameters.columns[i].type == "date")
		{
			//DateRangePicker en el input
			$(`#divtabla .daterangepicker`).remove();
			gestionDRP.simpleDRP($(`[data-id="modals"] #data_${i}`), "YYYY-MM-DD");
			$(`[data-id="modals"] #data_${i}`).val("");
			for( var j in opt.number )
			{
				$(`#customFilter${i} #opt_${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
			}
		}
		else if(parameters.columns[i].type.includes("datetime"))
		{
			//DateRangePicker en el input
			$(`#divtabla .daterangepicker`).remove();
			gestionDRP.simpleDRP($(`[data-id="modals"] #data_${i}`), "YYYY-MM-DD HH:mm:ss");
			$(`[data-id="modals"] #data_${i}`).val("");
			for( var j in opt.number )
			{
				$(`#customFilter${i} #opt_${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
			}
		}
		else if(parameters.columns[i].type.includes("time"))
		{
			//DateRangePicker en el input
			$(`#divtabla .daterangepicker`).remove();
			gestionDRP.simpleDRP($(`[data-id="modals"] #data_${i}`), "HH:mm:ss");
			$(`[data-id="modals"] #data_${i}`).val("");
			for( var j in opt.number )
			{
				$(`#customFilter${i} #opt_${i}`).append(`<option value="${opt.number[j].value}">${opt.number[j].label}</option>`);
			}
		}

		//Eventos para botones y teclas
		$(`#customFilter${i} #add_${i}`).on('click', {columns: parameters.columns}, gestionDT.addCustomFilter);

		$(`#customFilter${i} #accept_${i}`).on('click', {parameters: parameters}, gestionDT.searchCustom);

		$(`#customFilter${i} #cleanF_${i}`).on('click', {parameters: parameters}, gestionDT.cleanFilters);

		$(`#customFilter${i}`).on('keyup', gestionDT.keyupEv);

		$(`#customFilter${i} [data-id="close_${i}"]`).on('click', gestionDT.closeModal);


	}
}

/* método openModal */
/* Callback cuando se presiona el botón que muestra el modal */
/* redibuja selectores y guarda la vista inicial */
gestionDT.openModal = function(parameters) {
	//Siguientes llamados después de inicialización
	//var idCol = parseInt(e.currentTarget.getAttribute("data-col"));

	var idCol = parameters.idCol;
	var $modalBody = $(`body [data-id="modal-body${idCol}"]`);
	var $select = $(`#divtabla #select_${idCol}`);
	var $selectClone = $select.clone();
	var $selectM = `<select data-id="selectM_${idCol}" class="selectpicker"></select>`;
	//var columns = e.data.parameters.columns;
	var columns = parameters.columns;
	var $selects;

	//Borra el BS Selector y pone un selector normal, además de borra las opcs seleccionadas al momento de clonar
	$selectClone.children().prop("selected",false);
	$modalBody.children().children(".col-6").find(".dropdown.bootstrap-select").remove();
	$modalBody.children().children(".col-6").remove('select').append($selectM);
	$modalBody.children().children(".col-6").find('select').append( $selectClone.children() );

	$selectM = $modalBody.children().children(".col-6").find("select");

	//Aplica estio con BS Select y evento
	gestionBSelect.wrapSelectDTM($selectM);
	$($selectM).off('changed.bs.select').on('changed.bs.select', gestionDT.changeSelM);
	$(`#customFilter${idCol} #add_${idCol}`).off('click').on('click', {columns: parameters.columns}, gestionDT.addCustomFilter);
	$(`#customFilter${idCol} .fas.fa-minus-circle.text-danger`).parent().off("click").on("click", gestionDT.deleteCustomFilter);

	if(parameters.op == "DENIED"){
		$($selectM).next().prop("disabled", true);
		gestionDT.Fchange[idCol] = 1;
	}
	//Para fechas
	if(columns[idCol].type == "date")
	{
		$(`#divtabla .daterangepicker`).remove();
		gestionDRP.simpleDRP($modalBody.find(`input#data_${idCol}`), "YYYY-MM-DD");
	} 
	else if(columns[idCol].type.includes("datetime")) 
	{
		$(`#divtabla .daterangepicker`).remove();
		gestionDRP.simpleDRP($modalBody.find(`input#data_${idCol}`), "YYYY-MM-DD HH:mm:ss");
	}
	else if(columns[idCol].type == "time")
	{
		$(`#divtabla .daterangepicker`).remove();
		gestionDRP.simpleDRP($modalBody.find(`input#data_${idCol}`), "HH:mm:ss");
	}

	//Estado inicial
	gestionDT.$initModalBody = $modalBody.clone();

	//Poner los selectores de la copia en las mismas opciones
	$selects = gestionDT.$initModalBody.find(`select#opt_${idCol}`);
	
	for (var i=0; i<$selects.length; i++)
	{
		$selects[i].value = $modalBody.find(`select#opt_${idCol}`)[i].value;
	}

	gestionModal.cerrar();
}

/* método closeModal */
/* Callback cuando se cierra el modal con tecla esc o con click en X, para regresarlo a su estado anterior */
gestionDT.closeModal = function(e) {

	var $closeModalBody;
	if( e.type == 'click')
	{
		$closeModalBody = $(e.currentTarget).parent().next();
		$closeModalBody.click();
	}
	else if( e.type = 'keyup')
	{
		$closeModalBody = $(e.currentTarget).find(".modal-body");
	}

	//Devolver al estado inicial, revisar eventos de los botones agregar y eliminar, y revisar selector
	$closeModalBody.empty().append(gestionDT.$initModalBody.children());
}

/* método keyupEv */
/* Callback evento keyup en el modal, key: "esc"(27) cierra el modal, key: "enter"(13) aplica el filtro */
gestionDT.keyupEv = function(e) {
	var keyP = e.which;
	switch(keyP) {
		case 13:
			$(e.currentTarget).find("button.btn-primary").last().click();
		break;

		case 27:
			$(e.currentTarget).modal("hide");
			gestionDT.closeModal(e);
		break;

		default:
		//Nothing to do
	}
}

/* método changeSelM */
/* Evento que dispara el cambio de un selector en el modal, el cual escribe dentro del input de esa fila */
gestionDT.changeSelM = function(e) {
	var $select = $(e.target);
	var selectValue = $select.val();
	$select.parent().prev().val(selectValue); //esto cambia con imask
}

/* método addCustomFilter */
/* Crea una fila en el modal del filtro avanzado para agregar otra opción de filtrado */
gestionDT.addCustomFilter = function(e) {
	
	var columns = e.data.columns;
	var idCol = parseInt(e.currentTarget.id.split("_")[1]);
	var $modalBody = $(e.currentTarget).parent().parent().parent();
	var $row = $(e.currentTarget).parent().parent();
	var $btnAdd = $(e.currentTarget).detach();
	var $rowClone = $rowClone = $row.clone();
	var $select = $rowClone.find('select.selectpicker').detach();

	$modalBody.append(`<div class="row">
								<div class="col-12 form-inline py-2">
						        	<div class="form-check px-1">
									  <label class="form-check-label">
									    <input type="radio" class="form-check-input" name="optfilter${parseInt(e.timeStamp)}" value="AND" checked>Y
									  </label>
									</div>
									<div class="form-check px-1">
									  <label class="form-check-label">
									    <input type="radio" class="form-check-input" name="optfilter${parseInt(e.timeStamp)}" value="OR">O
									  </label>
									</div>
								</div>
						</div>`);

	$rowClone.children(".col-2").empty()
								.append(`<a class="cursor_sel" id="deleteBtnModal"><i class="fas fa-minus-circle text-danger"></i></a>`)
								.append($btnAdd)
								.children("#deleteBtnModal").on("click", gestionDT.deleteCustomFilter);

	$rowClone.find("div.dropdown").remove();
	$select[0][0].remove();
	$rowClone.children(".col-6").append($select);
	$rowClone.find('input').val("");

	if(columns[idCol].type == "date")
	{
		gestionDRP.simpleDRP($rowClone.find('input'), "YYYY-MM-DD");
		$rowClone.find('input').val("");
	} 
	else if(columns[idCol].type.includes("datetime")) 
	{
		gestionDRP.simpleDRP($rowClone.find('input'), "YYYY-MM-DD HH:mm:ss");
		$rowClone.find('input').val("");
	}
	else if(columns[idCol].type.includes("time")) 
	{
		gestionDRP.simpleDRP($rowClone.find('input'), "HH:mm:ss");
		$rowClone.find('input').val("");
	}

	$modalBody.append($rowClone);

	gestionBSelect.wrapSelectDTM($rowClone.find('select.selectpicker'));
	$rowClone.find('select.selectpicker').on('changed.bs.select', gestionDT.changeSelM);

	if( $row.prev().length == 0 && $row.children(".col-2").children().length == 0 )
	{
		$row.children(".col-2").append(`<a class="cursor_sel" id="deleteBtnModal"><i class="fas fa-minus-circle text-danger"></i></a>`)
							   .children("#deleteBtnModal").on("click", gestionDT.deleteCustomFilter);
	}
}

/* método deleteCustomFilter */
/* Elimina una fila en el modal del filtro avanzado */
gestionDT.deleteCustomFilter = function(e) {
	
	var $row = $(e.currentTarget).parent().parent();
	var $btnAdd = $(e.currentTarget).next().detach();

	if( $row.prev().length == 0 )
	{
		$row.next().remove();
		if( $row.next().next().length == 0 && $row.prev().length == 0 ){
			$btnAdd = $row.next().children(".col-2").children().last().detach()
			$row.next().children(".col-2").empty();
			$row.next().children(".col-2").append($btnAdd);
		}
		$row.remove();
	} else {
		$row.prev().remove();
		if( $row.prev().prev().length == 0 && $row.next().length == 0 ){
			$row.prev().children(".col-2").empty();
		}
		$row.prev().children(".col-2").append($btnAdd);
		$row.remove();
	}
}

/* método searchCustom */
/* Esta función es el llamado de un evento click sobre el boton aceptar del modal de filtro avanzado, 		*/
/* recoge los datos del formulario y luego llama al método de buscar de datatable para que haga la consulta */
gestionDT.searchCustom = function(e) {

	var idCol = parseInt(e.currentTarget.id.split("_")[1]);
	var $rows = $(`[data-id="modals"] [data-id="modal-body${idCol}"] .row`);
	var searchVals = new Array();
	gestionDT.$initModalBody = "";
	var $table = $.fn.dataTable.Api(e.data.parameters.id_table);

	if( $rows.length == 1)
	{
		//Una sola fila
		var op;
		var val;
		op = $($rows[0].children[0]).find("select").val();
		val = $($rows[0].children[1]).find("input").val();
		if( op.includes("LIKE") )
		{
			op = op.replace("_", val);
			searchVals.push(`**/data**/${op}**/`);
		} else {
			searchVals.push(`**/data**/${op}**/'${val}'**/`);
		}

	} else {
		var op;
		var val;
		var opl;
		var searchVal;
		//Varias filas
		//Primer elemento
		op = $($rows[0].children[0]).find("select").val();
		val = $($rows[0].children[1]).find("input").val();
		if( op.includes("LIKE") )
		{
			op = op.replace("_", val);
			searchVal = `**/data**/${op}**/`;
		} else {
			searchVal = `**/data**/${op}**/'${val}'**/`;
		}
		searchVals.push(searchVal);

		//Resto de elementos
		for(var i=2; i<$rows.length; i+=2)
		{
			op = $($rows[i].children[0]).find("select").val();
			val = $($rows[i].children[1]).find("input").val();
			opl = $($rows[i-1].children).find("input:checked").val()
			if( op.includes("LIKE") )
			{
				op = op.replace("_", val);
				searchVal = `**/${opl}**/data**/${op}**/`;
			} else {
				searchVal = `**/${opl}**/data**/${op}**/'${val}'**/`;
			}
			searchVals.push(searchVal);
		}
	}

	var lastSearch = $table.column( idCol ).search();

	//Concatenar busqueda
	if(lastSearch != "")
	{
		lastSearch = lastSearch.split("#,");
		lastSearch = lastSearch.filter( s => !s.includes("**/"));
		searchVals = searchVals.concat(lastSearch);
	}
	//Se concatena cada valor con caracteres de separacion
	for(var i = 0; i < searchVals.length-1; i++){
		searchVals[i] = `${searchVals[i]}#`;
	}
	$table.column( idCol ).search( searchVals ).draw();

	$("#clearBtnFilter").prop("disabled", false);
	$(`.opciones [data-for='${idCol}']`).show().next().removeClass("btn-light").addClass("btn-primary");
	// $.fn.dataTable.Api("#tabla").column( 2 )
									   // .search( ["**/data**/>**/12**/", "**/AND**/data**/<**/14**/"] ).draw();
}

/* método drawTable */
/* Crea los headers de la tabla y configura el objeto para llamar a DataTables (DT), DT se encarga de hacer la petición para traer los datos  */
/* @param columns, respuesta del AJAX pasada desde otra función 		   */
/* @param parameters, var pasada a través de la función que invocó el AJAX */
/* */
/* -------------- Hace la peticion y Dibuja la tabla ------------------ */
gestionDT.drawTable = function (columns, parameters) {

	var n_col = columns.length; //numero de columnas desde el servidor
	var $table = $(parameters.id_table); // tabla objetivo

	var auxCol = 0;

	//Si se define una columna extra
	if( parameters.col_extra != "" ){
		//Para colocar contenido en una columna
		columns.unshift( {"targets": 0, //Ultima columna
			             "data": "",
			             //Se agrego el tipo ya que en los forEach de verificación sin el tipo producía un error
			             "type": "html", 
			             "defaultContent": parameters.col_extra_html,
			             "orderable": false,
			             "searchable": false } );
		//Se especifica que agregue en los titulos la nueva columna y no en los filtros
		$table.find("thead tr.thead").append(`<th>${parameters.col_extra}</th>`);

		n_col++;

		auxCol = 1;
	}

	//Nombres de las columnas
	for(var i=auxCol; i<n_col; i++)
	{
		$table.find("thead tr.thead").append(`<th>${columns[i].data}</th>`);
	}

	/*----------------------------------------------------*/

	var dom = `<"top px-2"p<"dropdown"i<"dropdown-menu">>><"bg-processing"r><"m-2 px-2"t>`;
	//Agregar función de callback para renderizar datos
	columns.forEach( c => c.type == "date" ? c.render=gestionDT.renderDate : "" );
	columns.forEach( c => c.type.includes("datetime") ? c.render=gestionDT.renderDateTime : "" );
	columns.forEach( c => c.type == "int" || c.type == "float" ? c.render=gestionDT.renderNumber : "" );

	//Objeto configuración dataTables
	var config = {
		scrollResize: true,
	    scrollY: '100vh',
        scrollX: true,
		scrollCollapse: true,

    	serverSide: true,

	    ajax: {
	    		url: parameters.urlConexion+'/'+parameters.metodo,
	    		dataSrc: 'RESULTADO',
	    		type: 'POST',
	    		data: {"n_col": n_col, "nombre": parameters.nombre, "replace": gestionDT.replace} 
	    		//info adicional para el total de columnas
    	},
	    columns: columns, //nombres columnas
	    initComplete: gestionDT.tableReady, //cuando inicie ponga el evento al div de los selectores
	    processing: true,	//info processin ante cualquier cambio en la tabla
	    sDom: dom,//'<"top"pil><"bg-processing"r>t',	//mostrar todas las opciones menos f (find input)
	    sServerMethod: 'POST',
	    pagingType: "simple",
	    // scrollY: parameters.t_height,
	    language: {
            url: "/libreriasExternas/DataTables/i18n/Spanish.json"
        },
        select: {
        	info: false,
        	blurable: true
        },
        keys: true
     //    fixedColumns: true, //investigar mas
	};

	//Iniciar datatable en la tabla
	$($table).DataTable(config);
	$table.on('init.dt', {cllbck: parameters.callback}, gestionDT.readyTable);
}

/* método readyTable */
/* callback del evento init.dt, recibe la función de callback definida por el usuario */
gestionDT.readyTable = function(e, settings, json){

	setTimeout(e.data.cllbck,2,e, settings, json);
}

/* método getFilters */
/* Ejecuta la consulta para traer los campos de los selectores de filtros 		 */
/* @param parameters, recibe los parametros necesarios para ejecutar la consulta */
/*  				  en la inicialización desde receiveInfo y cuando se realiza */
/*					  una busqueda desde el método filterSel 					 */
gestionDT.getFilters = function(e) {
	/* Ejecuta la consulta para cargar los selectores, de acuerdo a los nombre recibidos */
	var idCol;
	if(e.type=="click")
	{
		idCol = $(e.currentTarget).data("col");
	}
	else
	{
		idCol = $(this)[0].id.split("_").slice(1);
	}

	var parameters = e.data.parameters;
	parameters.idCol = idCol;
	parameters.eType = e.type;
	if( gestionDT.Fchange[idCol] == 1)
	{
		var $table = $.fn.dataTable.Api( parameters.id_table );
		var cols = $table.context[0].oAjaxData.columns;

		var col_filter = parameters.metodo+'_column_filters';
		var data = { col_name: JSON.stringify(parameters.columns.map( c => c.data )),
					 cols: JSON.stringify(cols),
					 col: parameters.columns[idCol].data,
					 nombre: parameters.nombre,
					 replace: gestionDT.replace
			};
		gestionModal.alertaBloqueante("Procesando");
		query.callAjax( parameters.urlConexion, col_filter, data, gestionDT.loadFilters, parameters );
		gestionDT.Fchange[idCol] = 0;
	} else {
		if(e.type=="click")
		{
			gestionDT.openModal(parameters);
		}
	}
}

/* método drawFilters */
/* Dibuja los selectores y botones en los headers por primera vez, luego carga de forma dínamica las opciones de los selectores */
/* @param respuesta, recibe los datos que devuelve el AJAX que invoca getFilters 					   */
/* @param parameters, recibe los parametros necesarios para realizar las diferentes acciones		   */
/* -------------- Dibujar y cargar selectores en thead>tr.filter ------------------ */
gestionDT. drawFilters = function(columns, parameters) {

	var $arrTh = $("tr.filter th>div.opciones"); //Para verificar si los selectores existen o no
	
	parameters.columns = columns;
	n_col = columns.length;
	/* Dibujar y cargar selectores, verifica que el Nº de columnas obtenidas son iguales a las columnas que se pidieron */
	/*	Dibuje los selectores una vez, no se redibujan	*/
	if($arrTh.length == 0)
	{
		var auxCol = 0;

		// Se agrega esta comparación por si existe una columna extra, en la parte de filtros se
		// agrega una etiqueta "th" vacía para crear un campo vacío con los estilos de la tabla

		if( parameters.col_extra != "" ){
			$("table").find("thead tr.filter").append(`<th></th>`);
			auxCol = 1;
		}

		for(var i = auxCol; i < n_col; i++)
		{
			$("table").find("thead tr.filter").append(`<th><div class="opciones" data-col='${i}'>
				<button class="btn btn-light btn-sm" data-for="${i}"><small><i class="fas fa-times text-danger"></i><i class="fas fa-filter"></i></small></button>
				<button class="btn btn-light btn-sm" data-toggle="modal" data-target="#customFilter${i}" data-col='${i}' data-backdrop="static" data-keyboard="false">
					<i class="fas fa-ellipsis-v"></i>
				</button>
				<select multiple id='select_${i}' class='form-control selectpicker' data-col='${i}' data-style='btn-sm'><option value="gestionDT.Init"></option></select>
			</div></th>`);

			/* Estilo para selectores */
			gestionBSelect.wrapSelectDT(`#select_${i}`, columns[i].data);
			$(`#select_${i}`).on("show.bs.select", {parameters: parameters}, gestionDT.getFilters);
			$(`button[data-target="#customFilter${i}"]`).on("click", {parameters: parameters}, gestionDT.getFilters);
		}



		gestionDT.filtersReady(parameters); //Pone los eventos a los selectores y botones la primera vez que se dibujan
		gestionDT.drawModal(parameters);
	}

	$("thead tr.filter").trigger("filtersReady.gDT");
}

/* método loadFilters */
/* carga los datos de los filtros que se obtienen de la consulta */
gestionDT.loadFilters = function(opts, parameters){
	/* Cargue los selectores con los datos, la carga se hace en cada busqueda */
	var values = new Array();//Extrae las opciones seleccionadas para redibujarlas de este modo
	var idCol = parameters.idCol;
	var col_name = parameters.columns[idCol].data;
	var type = parameters.columns[idCol].type;
	var $table = $.fn.dataTable.Api( parameters.id_table );
	// Primera carga
	if(parameters.cols == "" && opts.length != 0 && opts[0][col_name] != "DENIED")
	{

		//Carga cuando se empieza a buscar, se verifica si esta o no seleccionada la opción para mostrarla asi de nuevo
		values[idCol] = $table.column(idCol).search().split("#,");
		$(`#select_${idCol}`).empty();
		
		if( opts.length > 0)
		{
			for(var j = 0; j < opts.length; j++)
			{
				if(opts[j][col_name] != null && opts[j][col_name] != ""){
					
					var opt;
				
					//Renderizado de date y datetime
					if(type == "date"){
						opts[j][col_name] = gestionDT.renderDate(opts[j][col_name]);
					} 
					else if (type.includes("datetime")){
						opts[j][col_name] = gestionDT.renderDateTime(opts[j][col_name]);
					}

					//Para volver a poner como seleccionado
					if( $.inArray(`${opts[j][col_name]}`, values[idCol]) > -1)
					{
						opt = `<option value='${opts[j][col_name]}' title=' ' selected>${opts[j][col_name]}</option>`;
					} 
					else {
						opt = `<option value='${opts[j][col_name]}' title=' '>${opts[j][col_name]}</option>`;
					}

					$(`#select_${idCol}`).append(opt);
				}
			}
		} else {
			var opt = "<option value='0' disabled></option>"
			$(`#select_${idCol}`).append(opt);
		}
		$(`#select_${idCol}`).selectpicker('refresh');//Refresca el selector por medio de la API de selectpicker

		if(parameters.eType == "click")
		{
			parameters.op = "";
			gestionDT.openModal(parameters);
		}
		else
		{
			gestionModal.cerrar(); //Cierra modal
		}
	} 
	else
	{
		gestionModal.cerrar(); //Cierra modal
		$(`#select_${idCol}`).empty().append("<option></option>");
		$(`#select_${idCol}`).selectpicker('refresh');
		if(parameters.eType == "click")
		{
			parameters.op = "DENIED";
			gestionDT.openModal(parameters);
		}
		else
		{
			var label;
			if(opts.length == 0)
			{
				label = "Este filtro no tiene opciones disponibles";
			} else {
				label = "Este filtro no se encuentra disponible por el número de opciones, sólo se puede usar el filtro avanzado"
			}
			setTimeout(gestionDT.delayConfirm, 2, label);
			$(`#select_${idCol}`).selectpicker('close');
			gestionDT.Fchange[idCol] = 1;
		}
	}
	//Se pone el icono o el 1
	var cantidad = $(`#select_${idCol}`).val().length;
	$(`button[data-id='select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).empty();
	if(cantidad > 0){
		$(`button[data-id='select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).append(`${cantidad}`);
	}
	else if($(`#select_${idCol}`).val().length == 0){
		$(`button[data-id='select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).append("<i class='fas fa-filter'></i>");
	}
}

/* método delayConfirm */
/* delay para la alerta de confirmación, por algunos problemas en los tiempos con sweetalert se usa un delay */
gestionDT.delayConfirm = function(label) {
	gestionModal.alertaConfirmacion("",label,"info","OK","#007bff",function(Continuar){});
}

/* método filtersReady */
/* Después de dibujados los elementos de los filtros, les asigna los eventos */
gestionDT.filtersReady = function (parameters) {
	//Evento para cuando se selecciona una opción
	$("tr.filter th>div.opciones").on('changed.bs.select', {id_table: parameters.id_table}, gestionDT.searchSel);

	//Evento para la carga del modal
	//$("tr.filter th>div.opciones>button[data-target]").on('click', {parameters: parameters}, gestionDT.openModal);

	//Evento limpiar filtro por columna
	$("tr.filter th>div.opciones>button[data-for]").on("click", {parameters: parameters}, gestionDT.cleanFilters);
}

/* método tableReady */
/* Callback del llamado de DataTables, solo se ejecuta después de que la tabla esta lista (una vez) */
/* Adicionar eventos de busqueda y dibujo a la tabla, dibuja botón limpiar filtros */
/* @param s, settings de DataTable */
gestionDT.tableReady = function(s) {
	$tabla = new $.fn.dataTable.Api( `#${s.nTable.id}` ); //Instancia de DataTable para utilizar el Api para los filtros

	$tabla.on( 'search.dt', gestionDT.filterSel );
	// $tabla.on( 'draw.dt', gestionDT.drawReady);

	// $("#divtabla table").first().parent().addClass("mx-auto");
	// Se pone esta función al comienzo para que genere el dropdown de la paginación, las paginas
	// y los registros que se quieren ver al tiempo
	gestionDT.drawDropdown();

	// Se crean los botones para limpiar los filtros y generar un excel de la tabla actual
	// con los filtros aplicados, si se da el caso, dependiendo si se esta en escritorio o en
	// movil, se carga un dropdown o se pone directamente los botones
	var txt;
	// Versión movil
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
		txt = ` 
			<div class="dropdown btn-table-movil">
				<div data-toggle="dropdown">
					<i class="material-icons" id="btn-toggle-table">menu</i>
				</div>
				<div class="dropdown-menu" id="btn-toggle-option">
					
					<button id='clearBtnFilter' type='button' class='btn btn-primary btn-sm' disabled>
						<i class="material-icons" style="vertical-align: middle; font-size: 21px;">
							clear
						</i>
						<p style="vertical-align: middle; margin: 0; cursor: pointer; display: inherit;">
							Limpiar Filtros
						</p>
					</button>

					<button id='downloadBtnExcel' type='button' class='btn btn-info btn-sm'>
						<i class="material-icons" style="vertical-align: middle; font-size: 21px;">
							cloud_download
						</i>
						<p style="vertical-align: middle; margin: 0; cursor: pointer; display: inherit;">
							Enviar a excel
						</p>
					</button>

				</div>
			</div>`;
		$("div.top").append(txt);
	}
	else
	{

		txt = `	<span class='px-2'>
					<button id='clearBtnFilter' type='button' class='btn btn-primary btn-sm' disabled>
						<i class="material-icons" style="vertical-align: middle; font-size: 21px;">
							clear
						</i>
						<p style="vertical-align: middle; margin: 0; cursor: pointer; display: inherit;">
							Limpiar Filtros
						</p>
					</button>
				</span>
				<span class='px-2'>
					<button id='downloadBtnExcel' type='button' class='btn btn-info btn-sm'>
						<i class="material-icons" style="vertical-align: middle; font-size: 21px;">
							cloud_download
						</i>
						<p style="vertical-align: middle; margin: 0; cursor: pointer; display: inherit;">
							Enviar a excel
						</p>
					</button>
				</span>`;
		$("div.top").append(txt);
	}

	$("#clearBtnFilter").on("click", {id_table: `#${s.nTable.id}` }, gestionDT.cleanFilters);

	//Evento que genera el excel
	$("#downloadBtnExcel").on("click", {id_table: `#${s.nTable.id}` }, gestionDT.downloadExcel);

	
	if(!( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))){
		// se quita la clase para habilitar los eventos en el sidebar nuevamente
		$("#accordion").removeClass("disableEvents");
	}


}

/* método drawDropdown */
/* Dibuja un dropdown en la información de la tabla */
gestionDT.drawDropdown = function () {
	var $dropDown = $("#divtabla .top .dropdown");

	$dropDown.addClass('btn-light cursor_sel rounded');

	//table_info
	$dropDown.children().first().attr('data-toggle','dropdown')
								.attr('aria-haspopup','true')
								.attr('aria-expanded','false');
	//dropdown-menu
	var links = `<a class="dropdown-item cursor_sel" data-id='first_page'>Primera Página</a>
    <a class="dropdown-item cursor_sel" data-id='last_page'>Última Página</a>
    <div class="dropdown dropleft float-right dropdown-item">
	    <a class="dropdown-toggle cursor_sel" data-toggle="dropdown">
	      Mostrar
	    </a>
	    <div class="dropdown-menu block-menu">
	      <a class="dropdown-item" data-id='10'>10</a>
	      <a class="dropdown-item" data-id='25'>25</a>
	      <a class="dropdown-item" data-id='50'>50</a>
	      <a class="dropdown-item" data-id='100'>100</a>
	    </div>
  	</div>`

    $dropDown.children().last().on('click', gestionDT.dropDownEvents);

	$dropDown.children().last().attr('aria-labelledby','tabla_info')
							   .addClass("block-menu")
							   .append(links);
}

/* método dropDownEvents */
/* Se activa con un evento click en el dropdown de la información de la tabla, */
/* reconoce el elemento que disparo el evento y realiza una acción:			   */
/* Ir a Primera página-Última página, Mostrar N registros					   */
gestionDT.dropDownEvents = function(e) {
	var stTarget = e.target.getAttribute("data-id");
	var tableId = `#${$("#divtabla table tbody").parent().attr('id')}`;
	var $table = $.fn.dataTable.Api(tableId);
	
	switch(stTarget)
	{
		case 'first_page':
			$table.page('first').draw('page');
			break;

		case 'last_page':
			$table.page('last').draw('page')
			break;

		case '10':
			$table.page.len(stTarget).draw();
			break;

		case '25':
			$table.page.len(stTarget).draw();
			break;

		case '50':
			$table.page.len(stTarget).draw();
			break;

		case '100':
			$table.page.len(stTarget).draw();
			break;

		default:
			//Nothing
	}
}

/*******************************************************************************************/
//Metodo para obtener la información de la tabla y hacer la petición para hacer
// el archivo de excel
gestionDT.downloadExcel = function(){

	gestionModal.alertaBloqueante("P360", "Procesando...");
	var tableId = `#${$("#divtabla table tbody").parent().attr('id')}`;
	var $table = $.fn.dataTable.Api(tableId);

	var data = new FormData();

	var info = $table.ajax.params();
	
	data.append("draw", info.draw);
	data.append("length", info.length);
	data.append("n_col", info.n_col);
	data.append("nombre", info.nombre);	
	data.append("replace", gestionDT.replace);	
	data.append("start", info.start);
	data.append("name_pag", Sidebar.menuNombreActual);

	for(x in info.columns)
	{
		data.append(`columns[${x}][data]`,info.columns[x].data);
		data.append(`columns[${x}][name]`,info.columns[x].name);
		data.append(`columns[${x}][orderable]`,info.columns[x].orderable);
		data.append(`columns[${x}][search][regex]`,info.columns[x].search.regex);
		data.append(`columns[${x}][search][value]`,info.columns[x].search.value);
		data.append(`columns[${x}][searchable]`,info.columns[x].searchable);
	}
	data.append("order[0][column]", info.order[0].column);
	data.append("order[0][dir]", info.order[0].dir);
	data.append("search[value]", info.search.value);
	data.append("search[regex]", info.search.regex);

	query.callAjaxDataTablesExcel(gestionDT.urlConexion, "excelDataTables", data, gestionDT.queryStateExcel);
	// query.callAjaxDataTablesExcel("http://localhost:10228/WS.asmx", "excelDataTables", data, gestionDT.queryStateExcel);
}

// se manda la consulta para verificar el estado del excel de la tabla D_Registro_Excel
gestionDT.queryStateExcel = function(resultado){

	var data = {
		id: 	resultado[0].id
	}
	query.callAjax(gestionDT.urlConexion,'excelState',data,gestionDT.excelState);
}

// Cuando se consulta la tabla de registro, dependiendo si falla para, detiene el metodo que manda la consulta
// a la tabla de registro y le muestra el mensaje al usuario
// Fallo 
// true --> Fallo la generación del excel
// false --> No fallo o no ha fallado la generación del excel
// Estado 
// true --> Se esta generando el excel
// false --> Se termino de generar el excel
gestionDT.excelState = function(resultado){
	if(resultado[0].Fallo == true){
		gestionDT.ErrorConsulta();
	}
	else if(resultado[0].Estado == false)
	{
		gestionDT.generarExcel(resultado[0].Nombre);
	}
	// Se retorna el id del registro que se acabo de generar para consulta en un 
	// intervalo de 250 ms si el archivo ya se genero
	else
	{
		setTimeout(gestionDT.queryStateExcel, 250, resultado);
	}
}

// cuando se crea el archivo en el servidor, esta función se crea para poder descargarlo
// en el cliente
gestionDT.generarExcel = function(nombre){
	//se crea una etiqueta a
	var downloadLink = document.createElement("a");
	//en el atributo href se le asigna la url de descarga
	downloadLink.href = "/apiServices/RegistroTablas/" + nombre;
	//se le da el link de descarga //en esta linea se cambian los datos de descarga
	downloadLink.download = nombre;
	//se añade al documento html la etiqueta a
	document.body.appendChild(downloadLink);
	//timeToLdr(false, 'bgInitLdr');     
	// se clickea esa etiqueta
	downloadLink.click();
	//se remueve el link de la etiqueta
	document.body.removeChild(downloadLink);
	gestionModal.cerrar();
}

gestionDT.ErrorConsulta = function(){
	gestionModal.cerrar();	
	gestionModal.alertaConfirmacion(
		"P360",
		"Ha ocurrido un error al generar el archivo, Vuelva a intentar más tarde",
		"error",
		"Ok",
		"#f27474",
		function(){}
	);
}

/*******************************************************************************************/

/* método cleanFilters */
/* Limpia los filtros, todas las opciones seleccionadas en los filtros-selectores */
/* y filtros avanzados o el mismo comportamiento por columna */
gestionDT.cleanFilters = function (e) {

	var $table;
	var searchs;
	var $rows;
	var idCol;
	
	if( e.currentTarget.id == "clearBtnFilter" ){
		/*Mejorar, que no haga una consulta por cada columna que tiene filtro, hacer una consulta general 
		que vacie todo*/
		$table = $.fn.dataTable.Api(e.data.id_table);
		$('#divtabla .selectpicker').selectpicker('deselectAll');
		e.currentTarget.disabled = true;
		$('#divtabla div.opciones [data-for]').hide();
		$('#divtabla div.opciones .btn-primary[data-target]').removeClass('btn-primary').addClass('btn-light');
		$('#divtabla div.opciones .bootstrap-select .btn-primary').removeClass('btn-primary');
		//$table.columns().search("").draw();
		searchs = $table.context[0].aoPreSearchCols;
		
		for( var i=0; i<searchs.length; i++ )
		{
			if(searchs[i].sSearch != "")
			{	
				gestionDT.cleanModal($table, i);
				$table.column(i).search("").draw();
			}
			gestionDT.Fchange[i] = 1;
		}
	}
	else if( $(e.currentTarget).data("for") != undefined ) 
	{
		$table = $.fn.dataTable.Api(e.data.parameters.id_table);
		var $rows;
		idCol = $(e.currentTarget).data("for");

		// Deseleccionar lo que haya en el selector de filtro
		$(`#divtabla #select_${idCol}`).selectpicker('deselectAll');
		//El estilo se quita en el método searchSel
		
		/* Disparar evento de busqueda en DataTable para limpiar modal */
		if($table.column( idCol ).search() != "")
		{
			gestionDT.cleanModal($table, idCol);
		} 
		
		// Logica estilo de los botones
		$(e.currentTarget).hide();
		e.currentTarget.nextElementSibling.classList.remove("btn-primary");
		e.currentTarget.nextElementSibling.classList.add("btn-light");
		$(e.currentTarget).next().next().children("button").removeClass("btn-primary");

		searchs = $table.context[0].aoPreSearchCols;

		if (searchs.filter( s => s.sSearch != "" ).length == 0 ){
			$("#divtabla #clearBtnFilter").prop('disabled', true);
		}

		$table.column(idCol).search("").draw();
		gestionDT.Fchange[idCol] = 1;
		//Se pone el icono o el 1
		var cantidad = $(`#select_${idCol}`).val().length;
		$(`button[data-id='select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).empty();
		if(cantidad > 0){
			$(`button[data-id='select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).append(`${cantidad}`);
		}
		else if($(`#select_${idCol}`).val().length == 0){
			$(`button[data-id='select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).append("<i class='fas fa-filter'></i>");
		}
	}
	else 
	{
		//limpiar el modal
		$table = $.fn.dataTable.Api(e.data.parameters.id_table);
		idCol = e.currentTarget.id.split("_").slice(1);
		gestionDT.cleanModal($table, idCol);
		$(`#divtabla button[data-target="#customFilter${idCol}"]`).removeClass("btn-primary").addClass("btn-light");
	}
}

/* método cleanModal */
/* Limpia un modal, volviendo a su estado inicial y la busqueda de la columna, con cada llamado */
/* @param $table, objeto DataTable con referencia a la tabla */
/* @param idCol, número de columna a borrar */
gestionDT.cleanModal = function($table, idCol) {
	var $rows = $(`[data-id="modal-body${idCol}"] .row`);
	var lastSearch;
	//Borrar una sola fila
	if( $rows.length == 1 )
	{
		$rows.find(`select#opt_${idCol}`).prop("selectedIndex", 0);
		$(`#divtabla [data-id='modals'] [data-id='selectM_${idCol}']`).selectpicker('val', "")
		$rows.find("input").val("");
	}
	else //Borrar N filas
	{	
		var btnAdd = $rows.find(`#add_${idCol}`).detach();
		$rows.find(`select#opt_${idCol}`).first().prop("selectedIndex", 0);
		$(`#divtabla [data-id='modals'] [data-id='selectM_${idCol}']`).first().selectpicker('val', "");
		$rows.find("input").first().val("");
		$rows.find(`.col-2.text-center`).first().empty().append(btnAdd);

		$rows.slice(1).remove();
	}

	lastSearch = $table.column(idCol).search();

	if(lastSearch != "")
	{
		lastSearch = lastSearch.split("#,");
		lastSearch = lastSearch.filter( s => !s.includes("**/"));
		//Se concatena cada valor con caracteres de separacion
		for(var i = 0; i < lastSearch.length-1; i++){
			lastSearch[i] = `${lastSearch[i]}#`;
		}

		$table.column(idCol).search(lastSearch).draw();
		//Estilos botones
		if(lastSearch.length == 0)
		{
			$(`div.opciones button[data-for="${idCol}"`).click();
		}
	} 
	else 
	{
		$table.column(idCol).search("").draw();
	}
	//Organizar cuando el llamado es de cleanFilter btn limpiar modal
}

/* método drawReady */
/* Se invoca después de cada evento de dibujo (draw) */
// gestionDT.drawReady = function() {
// 	gestionModal.cerrar(); //cierra modal del método filterSel
// }

/* método searchSel */
/* Dispara el evento de acuerdo a la opción seleccionada, usando value 			 */
/* @param e, objeto del evento, para extraer la opción seleccionada y la columna */
/* Logica de filtrado */
/* Filtro Columna */
gestionDT.searchSel = function(e, clickedIndex, isSelected, previousValue) {
	
    //gestionModal.alertaBloqueante("","Procesando...");

	var idCol = parseInt(e.target.getAttribute("data-col"));
	var searchValue = $(e.target).val();
	var $table = $.fn.dataTable.Api(e.data.id_table);
	var searchs;
	
	//Cambia clase de acuerdo a si hay uno una opción seleccionada
	if(searchValue != ""){
		e.target.nextElementSibling.classList.add("btn-primary");
		$(e.currentTarget.firstElementChild).show();
		$("#clearBtnFilter").prop("disabled", false);
	} else {
		e.target.nextElementSibling.classList.remove("btn-primary");
	}
	var lastSearch = $table.column(idCol).search();

	if( lastSearch != "" )
	{
		lastSearch = lastSearch.split("#,");
		lastSearch = lastSearch.filter( s => s.includes("**/"));
		searchValue = searchValue.concat(lastSearch);
		//Estilo botones
		if(searchValue.length == 0)
		{
			$(e.currentTarget.firstElementChild).hide();
		}
	}
	else
	{
		// $(e.currentTarget.firstElementChild).hide();
	}
	//Se concatena cada valor con caracteres de separacion
	for(var i = 0; i < searchValue.length-1; i++){
		searchValue[i] = `${searchValue[i]}#`;
	}
	/* Disparar evento de busqueda en DataTable */
	$table.column( idCol ).search( searchValue ).draw();

	//Estilo boton limpiar todo
	searchs = $table.columns().search();
	searchs = searchs.filter( s => s!="");
	if(searchs.length == 0)
	{
		$("#clearBtnFilter").prop("disabled", true);
	}
	//Se pone el icono o el 1
	var cantidad = $(`#select_${idCol}`).val().length;
	$(`button[data-id='select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).empty();
	if(cantidad > 0){
		$(`button[data-id='select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).append(`${cantidad}`);
	}
	else if($(`#select_${idCol}`).val().length == 0){
		$(`button[data-id='select_${idCol}'] div.filter-option div.filter-option-inner div.filter-option-inner-inner`).append("<i class='fas fa-filter'></i>");
	}
}

/* método filterSel */
/* Callback del evento de busqueda, llama al método que redibuja las opciones de los selectores filtros*/
/* este evento se dispara dos veces por el funcionamiento de la libreria */
/* @param e, objeto evento JQuery										 */
/* @param s, settings DataTables 										 */
gestionDT.filterSel = function(e, s){
	gestionDT.sIdx++;
	//Solo funciona con el segundo llamado

	if(gestionDT.sIdx>1)
	{
		var columns = s.oAjaxData.columns;
		var search = s.aoPreSearchCols;

		for (var i=0; i<search.length; i++)
		{
			if (search[i].sSearch != columns[i].search.value)
			{
				gestionDT.Fchange[i] = 0;
			} else {
				gestionDT.Fchange[i] = 1;
			}
		}
		gestionDT.sIdx=0;
	}
}

/* método renderDate */
/* Renderiza el tipo de dato date, para que solo se muestre la fecha */
gestionDT.renderDate = function(data, t, r) {
	data = moment(data).format("YYYY-MM-DD");
	return data;
}

/* método renderDateTime */
/* Renderiza el tipo de dato datetime*, para se muestre la fecha y hora */
gestionDT.renderDateTime = function(data, t, r) {
	data = moment(data).format("YYYY-MM-DD HH:mm:ss");
	return data;
}

/* método renderNumber */
/* Renderiza el tipo de dato int o float, con separadores de miles */
gestionDT.renderNumber = function(data, t, r) {
	data = Intl.NumberFormat("es-CO").format(data);
	return data;
}

/* método deleteDT */
/* eliminar la instancia de DataTable */
gestionDT.deleteDT = function(id_table) {
	var $table = $.fn.dataTable.Api(id_table);
	$table.destroy();
}