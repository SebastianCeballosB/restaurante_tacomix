<?php
 	if($_POST){
		switch ($_POST['action']) {
		    case 'obtenerStarter':
		        // Read JSON file
				//https://jsonformatter.org/e67259
		    	$contents = file_get_contents('./json/starter.json');
				$json=json_decode($contents, true);
				echo json_encode($json);
		        break;
		    case 'obtenerCakes':
		        // Read JSON file
				//https://gist.github.com/prayagKhanal/8cdd00d762c48b84a911eca2e2eb3449
		    	$contents = file_get_contents('./json/cakes.json');			
				$json=json_decode($contents, true);
				echo json_encode($json);
		        break;
		    case 'agregarAcceso':
				//Load the file
				$contents = file_get_contents('./json/logins.json');
				 
				//Decode the JSON data into a PHP array.
				$contentsDecoded = json_decode($contents, true);
				 
				//Modify the counter variable.
				$contentsDecoded['RESULTADO'];
				 
				//Encode the array back into a JSON string.
				$json = json_encode($contentsDecoded);
				 
				//Save the file.
				file_put_contents('./json/logins.json', $json);

				echo $json;
				break;
		    case 'agregarUser':
				//Load the file
				 $contents = file_get_contents('./json/users.json');
				 
				 
				//Decode the JSON data into a PHP array.
				 $contentsDecoded = json_decode($contents, true);
				 
				//Push new data inside users file
				 array_push($contentsDecoded['RESULTADO'],  array("nombres" => $_POST['data']['nombres'],"apellidos" => $_POST['data']['apellidos'],"usuario" => $_POST['data']['usuario'],"password" => $_POST['data']['password'], "email" => $_POST['data']['email'],"rol" => $_POST['data']['rol'] ));
				 
				//Encode the array back into a JSON string.
				$json = json_encode($contentsDecoded);
				 
				// //Save the file.
				file_put_contents('./json/users.json', $json);

				$usuarioRegistrado = array("usuario" => $_POST['data']['usuario'], "rol" => $_POST['data']['rol']);
				$respuesta = array(
					"ESTADO" => "TRUE",
					"MENSAJE" => "Usuario agregado exitosamente",
					"RESULTADO" => $usuarioRegistrado
				);
				$pruebaJSON = json_encode($respuesta);
				
				echo $pruebaJSON;
				/* echo $json; */
				break;

				case 'loginUser':
					//Load the file
					$contents = file_get_contents('json/users.json');
					 
					//Decode the JSON data into a PHP array.
					$contentsDecoded = json_decode($contents, true);
					
					/* $usuario = "";
					$password = ""; */
					foreach($contentsDecoded['RESULTADO'] as $valor){
						/* $usuario = $_POST['data']['usuario'];
						$password = $_POST['data']['password']; */
						if($valor['usuario'] == $_POST['data']['usuario']){
							/* 	 */
							if($valor['password'] == $_POST['data']['password']){
								

								/* $usuarioExistente = array("usuario" => $valor['usuario'], "rol" => $valor['rol']); */

								$respuesta = array(
									"ESTADO" => "TRUE",
									"MENSAJE" => "Usuario Logueado exitosamente",
									"RESULTADO" => $valor
								);

				
								$pruebaJSON = json_encode($respuesta);
							
								echo $pruebaJSON;
							
							} else {
								$usuarioExistente = "Los datos son erroneos";
	
								$respuesta = array(
									"ESTADO" => "TRUE",
									"MENSAJE" => "Los datos de usuario son erroneos",
									"RESULTADO" => $usuarioExistente
								);
				
								$pruebaJSON = json_encode($respuesta);
							
								echo $pruebaJSON;
							break;
							
							}

						break;

						} 
					}

					
					/* echo json_encode($contentsDecoded); */
					break;	

		    default:
		    	/* $contents = file_get_contents('./json/default.json');
		        $json=json_decode($contents, true);
				echo json_encode($json); */
		}
	 }
?>