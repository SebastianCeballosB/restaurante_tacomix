$(document).ready(function() {

	/* Se crea los eventos necesarios en las vistas, se recomienda siempre usar
	la función off() antes asiganar el evento al contenedor debido a que si se llama
	la vista multiples veces ejecutara el evento la cantidad de veces que se asigno 
	y la funcion off() los apaga, puede ser mas especifico en que evento quiere 
	apagar por ejemplo .off("click") y solo apagara el evento click del contenedor
	dejando los otros encendidos.
	se usa el evento .on("click",function(){}) en vez de por ejemplo .click(function(){})
	ya que este evento es mas completo y se pueden enviar parametros además que es mas 
	eficiente en terminos de computación 
	Para mayor información consultar la documentación de Jquery */
	$("#Vista_1").off().on("click",Pag_construccion.funciones.vista1);
	$("#Vista_2").off().on("click",Pag_construccion.funciones.vista2);
	$("#Vista_3").off().on("click",Pag_construccion.funciones.vista3);

	gestionModal.cerrar();
	
});