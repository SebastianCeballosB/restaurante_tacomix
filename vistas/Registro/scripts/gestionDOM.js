
/* Se crea un nuevo objeto llamado funciones a partir del objeto principal de
la vista, este sera el encargado de contener las funciones donde se modificara el DOM, 
si se modifica la interfaz de alguna forma se tiene que hacer aqui, como por ejemplo
las alertas bloqueantes */

Registro.gestionDOM = new Object();




// modal de carga
Registro.gestionDOM.CargaPag = function(){
	gestionModal.alertaBloqueante(primario.aplicacion,"Procesando...");
}

Registro.gestionDOM.errorConexion = function(){
    gestionModal.alertaConfirmacion(
        primario.aplicacion,
        "Error en la consulta, posiblemente es una falla en la conexión a Internet, intentelo de nuevo más tarde",
        "error",
        "Ok",
        "#f27474",
        function(){}
    );
}