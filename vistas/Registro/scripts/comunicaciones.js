
/* Se crea un nuevo objeto llamado comunicaciones a partir del objeto principal de
la vista, este sera el encargado de contener las funciones de callback cuando se
manden peticiones al servidor y este las contestara aquí con la información solicitada */

Registro.comunicaciones = new Object();


Registro.comunicaciones.Registrarse = function(resultado){
	if (resultado != undefined) {
		gestionModal.alertaConfirmacion(
		    primario.aplicacion,
		    `El registro del usuario ${resultado.usuario} como ${resultado.rol} ha sido realizado con éxito`,
		     "success",
		     "OK",
		     "#007bff",
		);
		Registro.funciones.Login(resultado);
	}
}