
/* Se crea un nuevo objeto llamado funciones a partir del objeto principal de
la vista, este sera el encargado de contener las funciones donde se podra hacer
peticiones al servidor, operaciones logico-matematicas, actualización de
variables entre otras que se consideren pertinentes */

Registro.funciones = new Object();

Registro.funciones.Comprobacion = function(){

	Registro.nombres = $("#nombres").val();
	Registro.apellidos = $("#apellidos").val();
	Registro.usuario = $("#usuario").val();
	Registro.password = $("#password").val();
	Registro.password_confirmation = $("#password_confirmation").val();
	Registro.email = $("#email").val();
	Registro.rol = $(".user:checked").val();

	// Para comprobar que todos los campos tengan algo diferente a vacio
	if(Registro.nombres && Registro.apellidos && Registro.usuario && Registro.password && Registro.password_confirmation && Registro.email && Registro.rol != ""){ 
		
		$("#boton_Registro").addClass("Registro");
		$("#boton_Registro").removeClass("deshabilitado");

	}

}

Registro.funciones.Registrarse = function(){

	/* Se recibe los valores de contraseña */
	Registro.password = $("#password").val(); 
	Registro.password_confirmation = $("#password_confirmation").val();

	Registro.data = {

		"nombres": $("#nombres").val(),
		"apellidos": $("#apellidos").val(),
		"usuario": $("#usuario").val(),
		"password": $("#password").val(),
		"email": $("#email").val(),
		"rol": $(".user:checked").val()

	};

	if(Registro.password != Registro.password_confirmation){ /* Para hacer la comparacion de que la contraseña sea igual */
		alert("Las constraseñas no coinciden");
	} else {
	query.callAjaxPHP(Registro.urlConexion,
		"agregarUser",
		Registro.data, 
		Registro.comunicaciones.Registrarse);
	}

	console.log(Registro.data);

}

Registro.funciones.IrLogin = function(){
	gestionModal.alertaBloqueante(primario.aplicacion, "Procesando...");
	$("#contenedorPrincipal").empty();
	$("#contenedorPrincipal").load("vistas/Login/Login.html");
}

Registro.funciones.Login = function(resultado) {
	if(resultado.rol == "Cliente"){
		$("#contenedorPrincipal").empty();
		$("#contenedorPrincipal").load("vistas/Cliente/Cliente.html");
	} else if (resultado.rol == "Administrador"){
		$("#contenedorPrincipal").empty();
		$("#contenedorPrincipal").load("vistas/Administrador/Administrador.html");
	}
}
