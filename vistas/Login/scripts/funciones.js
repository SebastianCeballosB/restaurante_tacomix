
/* Se crea un nuevo objeto llamado funciones a partir del objeto principal de
la vista, este sera el encargado de contener las funciones donde se podra hacer
peticiones al servidor, operaciones logico-matematicas, actualización de
variables entre otras que se consideren pertinentes */

Login.funciones = new Object();

Login.funciones.Comprobacion = function(){

	Login.usuario = $("#usuario").val();
	Login.password = $("#password").val();

	// Para comprobar que todos los campos tengan algo diferente a vacio
	if(Login.usuario && Login.password != ""){ 
		
		$("#boton_Login").addClass("Login");
		$("#boton_Login").removeClass("deshabilitado");

	}

}

Login.funciones.InicioSesion = function(){

	/* gestionModal.alertaBloqueante(primario.aplicacion, "Procesando..."); */

	/* empty es la funcion encargada de limpiar el contenido del contenedor y la funcion load
	carga el html en el contenedor */


	/* $("#contenedorPrincipal").empty();
	$("#contenedorPrincipal").load("vistas/Pag_construccion/Pag_construccion.html"); */

	

	Login.data = {
		"usuario": $("#usuario").val(),
		"password": $("#password").val()
	}

	query.callAjaxPHP(Login.urlConexion,
		"loginUser",
		Login.data, 
		Login.comunicaciones.Login);


	console.log(Login.data);


	
	
}

Login.funciones.IrRegistro = function(){
	gestionModal.alertaBloqueante(primario.aplicacion, "Procesando...");
	$("#contenedorPrincipal").empty();
	$("#contenedorPrincipal").load("vistas/Registro/Registro.html");
}

Login.funciones.Login = function(resultado) {
	if(resultado.rol == "Cliente"){
		$("#contenedorPrincipal").empty();
		$("#contenedorPrincipal").load("vistas/Cliente/Cliente.html");
	} else if (resultado.rol == "Administrador"){
		$("#contenedorPrincipal").empty();
		$("#contenedorPrincipal").load("vistas/Administrador/Administrador.html");
	}
}



