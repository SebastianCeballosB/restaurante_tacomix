
/* Se crea un nuevo objeto llamado comunicaciones a partir del objeto principal de
la vista, este sera el encargado de contener las funciones de callback cuando se
manden peticiones al servidor y este las contestara aquí con la información solicitada */

Login.comunicaciones = new Object();

Login.comunicaciones.Login = function(resultado){
	if (resultado != undefined) {
		gestionModal.alertaConfirmacion(
		    primario.aplicacion,
		    `El usuario: ${resultado.usuario} ingreso como ${resultado.rol} con éxito`,
		     "success",
		     "OK",
			 "#007bff",
			 function(Continuar){}
		);
		Login.funciones.Login(resultado);
	}
}